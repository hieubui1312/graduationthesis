const Utility = require("../../app_modules/utility");
const ErrorCode = require("../../app_modules/error_code");
const {HospitalRepository} = require("../../repositories");

class HospitalController {
    async getList(req, res){
        try{
            let records = await HospitalRepository.getAll();
            return res.status(200).json(records);
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    }
}

module.exports = new HospitalController();
