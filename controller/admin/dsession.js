var Utility = require("../../app_modules/utility");
const ErrorCode = require("../../app_modules/error_code");
var {DsessionRepository} = require("../../repositories");
const Config = require("../../app_modules/config");

let DSessionController = function () {
    this.getList = async (req, res) => {
        try{
            let {page} = req.params;
            if (!page) page = 1;

            let totalRecord = await DsessionRepository.count();
            let pages = (totalRecord % Config.ITEM_PER_PAGE) ?
                Math.floor(totalRecord / Config.ITEM_PER_PAGE) + 1
                : totalRecord / Config.ITEM_PER_PAGE;
            this.validatePage(page, pages);

            let dsessions = await DsessionRepository.getList(page);
            return res.status(200).json({dsessions, pages});

        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    };

    this.validatePage = (page, pages) => {
        if (page < 1 || page > pages) {
            throw new Error("Page is not correct");
        }
    }
};

module.exports = new DSessionController();
