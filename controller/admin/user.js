var {UserRepository} = require("../../repositories");
const UserType = require("../../config/user_type");
var Utility = require("../../app_modules/utility");
const ErrorCode = require("../../app_modules/error_code");
const Config = require("../../app_modules/config");

let UserController = function () {
    this.getPatients = async(req, res) => {
        try{
            let {page} = req.params;
            let {phone} = req.query;

            if (page) page = 1;
            let totalRecord = await UserRepository.count(UserType.PATIENT);
            let pages = totalRecord / Config.ITEM_PER_PAGE ? Math.floor(totalRecord / Config.ITEM_PER_PAGE) + 1
                : totalRecord / Config.ITEM_PER_PAGE;

            this.validatePage(page, pages);

            let patients = await UserRepository.getList(page, UserType.PATIENT, phone);

            return res.status(200).json({data: patients, pages});
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    };

    this.togglePatient = async (req, res) => {
        try{
            let {id} = req.params;

            let patient = await UserRepository.findByID(id, UserType.PATIENT);
            this.validateExist(patient);
            patient.activated = !patient.activated;
            await patient.save();
            return res.status(200).json({success: 1})

        } catch (e) {
            return  Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    };

    this.validatePage = (page, pages) => {
        if (page < 1 || page > pages) throw new Error("Page is not correct");
    };

    this.validateExist = (object) => {
        if (!object) throw new Error("Not found object");
    }
};

module.exports = new UserController();
