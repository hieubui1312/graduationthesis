const Utility = require("../../app_modules/utility");
const ErrorCode = require("../../app_modules/error_code");
const {WithdrawRepository} = require("../../repositories");
const Config = require("../../app_modules/config");

class WithDrawController {
    async getList(req, res){
        try{
            let count = await WithdrawRepository.count();
            let pages = count % Config.ITEM_PER_PAGE ? Math.floor(count / Config.ITEM_PER_PAGE) + 1 :
                count / Config.ITEM_PER_PAGE;
            let {page} = req.params;
            let records = await WithdrawRepository.getList(page);
            return res.status(200).json({data: records, pages})
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    }

    async decline(req, res){
        try{
            let {id} = req.params;
            let record = await WithdrawRepository.decline(id);
            return res.status(200).json({success: 1})
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    }

    async accept(req, res){
        try{
            let {id} = req.params;
            await WithdrawRepository.accept(id);
            return res.status(200).json({success: 1})
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    }
}

module.exports = new WithDrawController();
