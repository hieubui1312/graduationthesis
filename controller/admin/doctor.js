var {DoctorRepository, UserRepository, HospitalRepository} = require("../../repositories");
let Config = require("../../app_modules/config");
var Utility = require("../../app_modules/utility");
const ErrorCode = require("../../app_modules/error_code");
const UserType = require("../../config/user_type");
const DoctorDegree = require("../../config/degee_doctor");

let DoctorController = function () {
    this.getList = async (req, res) => {
        try{
            let page;
            if (req.params.page) {
                page = parseInt(req.params.page);
            } else page = 1;

            let countRecord = await DoctorRepository.count();
            let pages = this.totalPage(countRecord);
            this.validatePageCorrect(page, pages);
            let doctors = await DoctorRepository.getList(page);

            return res.status(200).json({doctors, pages});
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    };

    this.getDetail = async (req, res) => {
        try{
            let {id} = req.params;
            let doctor = await DoctorRepository.getInfo(id);
            console.log("Doctor", doctor.dataValues);
            this.validateExist(doctor);

            return res.status(200).json(doctor)
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    };

    this.delete = async (req, res) => {
        try{
            let id = req.params("id");

            await DoctorRepository.delete(id);

            return res.status(200).json({success: 1});
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR)
        }
    };

    this.toggleActive = async(req, res) => {
        try{
            let {id} = req.params;
            await UserRepository.toggleActive(id);
            return res.status(200).json({success: 1})
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    };

    this.create = async (req, res) => {
        try {
            let {user, doctor} = req.body;

            let userObj = await UserRepository.findByPhone(user.phone);
            if(userObj) throw new Error(`User have been exist. You can't create doctor with doctor ${user.phone}`);

            userObj = await UserRepository.createUser(user);
            await DoctorRepository.createDoctor({
                ...doctor,
                doctor_id: userObj.id,
                average_rating: 5
            });

            return res.status(200).json({success: 1});
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    };

    this.update = async (req, res) => {
        try{
            let {id} = req.params;
            let {user, doctor} = req.body;
            console.log("User", user);
            console.log("Doctor", doctor);
            await UserRepository.update(user, user.id);
            await DoctorRepository.updateInfo(user.id, doctor);
            return res.status(200).json({success: 1});

        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    };

    this.analytic = async (req, res) => {
        try{
            let result = await DoctorRepository.analytic();
            return res.status(200).json(result)
        } catch (e) {
          return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    };

    this.validatePageCorrect = (page, pages) => {
        if (page < 1 || page > pages) throw new Error("Page not correct");
    };

    this.validateExist = (object) => {
        if (!object)
            throw new Error("Not found object");
    };

    this.validateDegreeDoctor = (id) => {
        for (let degree of Object.keys(DoctorDegree)) {
            if (DoctorDegree[degree] === id) return true;
        }
        throw new Error("Not found doctor degree");
    };

    this.validateDoctor = async (id) => {
        let doctor = await UserRepository.findById(id, UserType.DOCTOR);
        if (!doctor) throw new Error(`Not found doctor with id ${doctor}`);
    };

    this.validateHospitalExist = async(id) => {
        let hospital = await HospitalRepository.findByID(id);
        if (!hospital) throw new Error(`Not found hospital id ${id}`);
    };

    this.totalPage = (totalRecord) => {
        let pages = totalRecord % Config.ITEM_PER_PAGE ? Math.floor(totalRecord / Config.ITEM_PER_PAGE) + 1 : (totalRecord / Config.ITEM_PER_PAGE);
        return pages;
    }
};

module.exports = new DoctorController();
