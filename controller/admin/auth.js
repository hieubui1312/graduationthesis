var {UserRepository} = require("../../repositories");
var Utility = require("../../app_modules/utility");
var NotFoundError = require("../../errors/not_found");
const ErrorCode = require("../../app_modules/error_code");
const UserType = require("../../config/user_type");
var md5 = require("md5");

let AuthController = function () {
    this.login = async (req, res) => {
        try{
            let {email, password} = req.body;
            let user = await UserRepository.findByEmailAndPassword(email, md5(password));
            console.log("User", user);
            this.validateUserExist(user);
            this.validateAdmin(user);

            user.dataValues.access_token =  Utility.encodeJWT(user);
            return res.status(200).json({user})
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    };

    this.validateUserExist = (object) => {
        if (!object) throw new NotFoundError("Not found user");
    };

    this.validateAdmin = (user) => {
        if (user.type !== UserType.ADMIN) throw new Error("User must be admin");
    }
};

module.exports = new AuthController();
