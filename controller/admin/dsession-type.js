const Utility = require("../../app_modules/utility");
const ErrorCode = require("../../app_modules/error_code");
const {DsessionTypeRepository} = require("../../repositories");

class DSessionTypeController {
    async getList(req, res){
        try{
            let data = await DsessionTypeRepository.getList();
            return res.status(200).json(data);
        } catch (e) {
          return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    }

    async create(req, res) {
        try{
            let data = req.body;
            let result = await DsessionTypeRepository.create(data);
            return res.status(200).json(result);
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    }

    async update(req, res){
        try{
            let {id} = req.params;
            let data = req.body;
            await DsessionTypeRepository.update(id, data);
            return res.status(200).json({success: 1});
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    }

    async delete(req, res){
        try{
            let {id} = req.params;
            await DsessionTypeRepository.delete(id);
            return res.status(200).json({success: 1});
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    }

    async detail(req, res){
        try{
            let {id} = req.params;
            let data = await DsessionTypeRepository.findById(id);
            return res.status(200).json(data);
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    }

    async getFees(req, res){
        try{
            let result = await DsessionTypeRepository.getFees();
            return res.status(200).json(result);
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    }

    async createFee(req, res){
        try{
            let {
                dsession_type,
                doctor_level,
                fee
            } = req.body;
            await DsessionTypeRepository.createFee(dsession_type, doctor_level, fee);
            return res.status(200).json({success: 1})
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    }
}

module.exports = new DSessionTypeController();
