const Utility = require("../../app_modules/utility");
const ErrorCode = require("../../app_modules/error_code");
const {PaymentRepository} = require("../../repositories");

class PaymentController {
    async getList(req, res){
        try{
            let {page} = req.params;
            let {phone} = req.query;
            let data = await PaymentRepository.getList(page, phone);
            return res.status(200).json(data);
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    }
}

module.exports = new PaymentController();
