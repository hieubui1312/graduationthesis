var Utility = require("../../app_modules/utility");
const ErrorCode = require("../../app_modules/error_code");
var {CreditRepository} = require("../../repositories");
const Config = require("../../app_modules/config");

let CreditLogController = function () {
    this.getList = async (req, res) => {
        try{
            let {page} = req.params;
            if (!page) page = 1;
            let {phone} = req.query;

            let data = await CreditRepository.getList(page, phone);
            return  res.status(200).json(data);
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    };

    this.validatePage = (page, pages) => {
        if (page < 1 || page > pages) {
            throw new Error("Page is not correct");
        }
    }
};

module.exports = new CreditLogController();
