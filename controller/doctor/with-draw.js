var Utility = require("../../app_modules/utility");
const ErrorCode = require("../../app_modules/error_code");
var {WithdrawRepository} = require("../../repositories");

module.exports = class Withdraw {
    async getList(req, res){
        try{
            let {currentUser} = req;
            let withdraws = await WithdrawRepository.find(currentUser.id);
            return res.status(200).json(withdraws);
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    }

    async request(req, res){
        try{
            let {currentUser} = req;
            let {amount} = req.body;
            if (parseInt(amount) > currentUser.credit) {
                return Utility.sendErrorResponse(res, "Tài khoản của bạn không đủ để rút tiền", 500, ErrorCode.INTERNAL_SERVER_ERROR);
            }

            let withdraw = await WithdrawRepository.create(req.body, currentUser);
            return res.status(200).json(withdraw);
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    }
};
