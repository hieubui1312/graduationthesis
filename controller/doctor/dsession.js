var {DsessionRepository, UserRepository, NotificationRepository, CreditRepository, DsessionMessage, DsessionPatient} = require("../../repositories");
const DSessionStatus = require("../../config/dsession");
const ErrorCode = require("../../app_modules/error_code");
const UserType = require("../../config/user_type");
const CREDIT_LOG_TYPE = require("../../config/credit_log_type");
var Utility = require("../../app_modules/utility");
const publisher = require("../../app_modules/redis/client");
const ChannelRedis = require("../../config/channelRedis");
const NotificationType = require("../../config/notifycation_type");

let DSessionController = function () {
    this.acceptDSession = async (req, res) => {
        try{
            let {id} = req.body;
            let dSession = await DsessionRepository.findById(id);

            this.validateExist(dSession);
            this.validateDSessionStatus(dSession, DSessionStatus.STATUS_WAITING);

            let currentUser = req.currentUser;
            this.validateDSessionBelongDoctor(dSession, currentUser);

            let patient = await UserRepository.findByID(dSession.patient_id, UserType.PATIENT, true);
            this.validateExist(patient);

            publisher.publish(ChannelRedis.DSESSION_ACTION, JSON.stringify({
                dsession_id: id,
                status: "accept"
            }));


            const data = {
                title: "Khám bệnh",
                message: `Bác sĩ đã đồng ý phiên khám ${id}`,
                link: `/dsession-chat/${id}`,
                reference_id: id,
                user_id: dSession.patient_id,
                type: NotificationType.DSESSION_ACCEPT,
                is_seen: false
            };
            publisher.publish(ChannelRedis.NOTIFY_CHANNEL, JSON.stringify(data));
            await NotificationRepository.create(data);

            // // Chuyen trang thai phien kham
            await DsessionRepository.acceptDSession(dSession);
            // // Cap nhat credit - credit hold
            let feeDSession = dSession.price;
            //
            await CreditRepository.create(currentUser.id, currentUser.credit,
                currentUser.credit + feeDSession, CREDIT_LOG_TYPE.ACCEPT_DSESSION,
                `Khám bệnh phiên khám ${id}`);
            await UserRepository.updateCredit(currentUser.id, currentUser.credit + feeDSession, currentUser.credit_hold);

            await CreditRepository.create(dSession.patient_id, patient.credit + patient.credit_hold,
                patient.credit + patient.credit_hold - feeDSession, CREDIT_LOG_TYPE.CREATE_DSESSION,
                `Tạo phiên khám ${id}`);
            await UserRepository.updateCredit(dSession.patient_id, patient.credit, patient.credit_hold - feeDSession);

            return res.status(200).json({success: 1})

        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR)
        }
    };

    this.declineDSession = async (req, res) => {
        try{
            let dSessionId = req.body.id;
            let currentUser = req.currentUser;
            let dSession = await DsessionRepository.findById(dSessionId);

            this.validateExist(dSession);
            this.validateDSessionStatus(dSession, DSessionStatus.STATUS_WAITING);
            this.validateDSessionBelongDoctor(dSession, currentUser);


            let patient = await UserRepository.findByID(dSession.patient_id, UserType.PATIENT, true);
            this.validateExist(patient);

            const data = {
                title: "Khám bệnh",
                message: `Bác sĩ đã từ chối phiên khám ${dSessionId}`,
                link: `/dsession-chat/${dSessionId}`,
                reference_id: dSessionId,
                user_id: dSession.patient_id,
                type: NotificationType.DSESSION_ACCEPT,
                is_seen: false
            };
            publisher.publish(ChannelRedis.NOTIFY_CHANNEL, JSON.stringify(data));
            await NotificationRepository.create(data);

            await DsessionRepository.declineDSession(dSession);
            let feeDSession = dSession.price;
            await UserRepository.updateCredit(patient.id, patient.credit + feeDSession, patient.credit - feeDSession);

            publisher.publish(ChannelRedis.DSESSION_ACTION, JSON.stringify({
                dsession_id: dSessionId,
                status: "decline"
            }));

            return res.status(200).json({success: 1})
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    };

    this.getMessages = async (req, res) => {
        try{
            let dSessionId = req.params.id;
            let dSession =await DsessionRepository.findById(dSessionId);
            this.validateExist(dSession);

            let currentUser = req.currentUser;
            this.validateDSessionBelongDoctor(dSession, currentUser);

            let messages = await DsessionMessage.getMessageOfDSession(dSessionId);
            return res.status(200).json({messages});
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    };

    this.getList = async (req, res) => {
        try{
            let currentUser = req.currentUser;
            let dSession = await DsessionRepository.findByDoctorID(currentUser.id);
            return res.status(200).json({dSession})
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    };

    this.getPatientInfoOfDSession = async (req, res) => {
        try{
            let dSessionId = req.params.id;
            let dSession = await DsessionRepository.findById(dSessionId);

            this.validateExist(dSession);

            let currentUser = req.currentUser;
            this.validateDSessionBelongDoctor(dSession, currentUser);

            let patient = await DsessionPatient.findByDSessionId(dSessionId);
            return res.status(200).json({patient})
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    };

    this.getDSessionOldWithPatient = async (req, res) => {
        try{
            let patientId = req.params.id;
            let currentUser = req.currentUser;

            let dSessions = await DsessionRepository.findByDoctorIdAndPatientId(currentUser.id, patientId);
            return res.status(200).json({dSessions})
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    };



    this.validateDSessionStatus = (dSession, status) => {
        if (dSession.status !== status) throw new Error("DSession status not correct");
    };

    this.validateDSessionBelongDoctor = (dSession, currentUser) => {
        if (dSession.doctor_id !== currentUser.id) {
            throw new Error("Doctor haven't authenticated to accept dSession");
        }
    };

    this.validateExist = (object) => {
        if (!object) throw new Error("Not found object");
    }
};

module.exports = new DSessionController();
