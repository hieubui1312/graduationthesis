var Utility = require("../../app_modules/utility");
const ErrorCode = require("../../app_modules/error_code");
const UserType = require("../../app_modules/usertype");
var {UserRepository, DoctorRepository, DsessionRepository} = require("../../repositories");
var md5 = require("md5");

let UserController = function () {

    this.createPassword = async (req, res) => {
        try{
            let {password} = req.body;
            let access_token = req.get('Access-Token');
            let {data, expiresIn} = Utility.verifyToken(access_token);

            data = JSON.parse(data);
            if (!data.phone) {
                return Utility.sendErrorResponse(res, "Token fail", 500, ErrorCode.AUTHENTICATION_FAIL);
            }

            let user = await UserRepository.findByPhone(data.phone, UserType.DOCTOR, true);
            if (!user) {
                return Utility.sendErrorResponse(res, "Not found doctor", 404, ErrorCode.NOT_FOUND, true);
            }

            if (user.password) {
                return Utility.sendErrorResponse(res, "User have been password", 500, ErrorCode.INTERNAL_SERVER_ERROR, true);
            }

            user = await UserRepository.createPassword(user, password);

            return res.status(200).json({user})
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    };

    this.info = async (req, res) => {
        try{
            let currentUser = req.currentUser;
            let {id} = currentUser;
            let doctor = await DoctorRepository.getInfo(id);
            return res.status(200).json({doctor});
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    };

    this.getMe = async (req, res) => {
        try{
            let currentUser = req.currentUser;
            console.log("Current user tim dk", currentUser);
            return res.status(200).json({user: currentUser});
        }catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    };

    this.updateInfo = async (req, res) => {
        try{
            let currentUser = req.currentUser;
            let user = await UserRepository.updateInfo(currentUser.id, req.body);
            return res.status(200).json(user);
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    };

    this.getSpecialize = async (req, res) => {
        try{
            let currentUser = req.currentUser;
            let userInfo = await UserRepository.getInfoSpecial(currentUser.id);
            return res.status(200).json(userInfo);
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    };

    this.updateSpecial = async (req, res) => {
        try{
            let doctorInfo = await DoctorRepository.updateInfoSpecial(req.body, req.currentUser.id);
            return res.status(200).json(doctorInfo);
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    };

    this.changePassword = async (req, res) => {
        try{
            let {newPassword, oldPassword} = req.body;
            let {currentUser} = req;

            if (md5(oldPassword) !== currentUser.password) {
                console.log("New pass", md5(oldPassword));
                console.log("Old pass", currentUser.password);
                return Utility.sendErrorResponse(res, "Password cũ của bạn không đúng. Vui lòng nhập lại!", 500, ErrorCode.INTERNAL_SERVER_ERROR, true);
            }

            if (newPassword.length !== 6)
                return Utility.sendErrorResponse(res, "Password của bạn chỉ có thể gồm 6 kí tự", 500, ErrorCode.INTERNAL_SERVER_ERROR, true);

            let user = await UserRepository.updateInfo(currentUser.id, {
                password: md5(newPassword)
            });
            return res.status(200).json(user);
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    };

    this.getAccountInfo = async (req, res) => {
        try{
            let {currentUser} = req;
            let analytic = await DsessionRepository.countSpecific(currentUser.id);
            return res.status(200).json(analytic);
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    }
};

module.exports = new UserController();
