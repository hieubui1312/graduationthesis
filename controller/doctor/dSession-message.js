var Utility = require("../../app_modules/utility");
const ErrorCode = require("../../app_modules/error_code");
var {DsessionRepository, DsessionMessage, MedicineRepository,
    DsessionResultRepository, DiseaseTestRepository, DsessionDiseaseTestRepository} = require("../../repositories");
const MessageType = require("../../config/message_type");
const DSessionStatus = require("../../config/dsession");
const publisher = require("../../app_modules/redis/client");
const ChannelRedis = require("../../config/channelRedis");
const {SERVER_PATIENT} = require("../../config/sever");

let DSessionMessageController= function () {
    this.chat = async (req, res) => {
        try{
            let {dsession_id, message} = req.body;

            let dSession = await DsessionRepository.findById(dsession_id);

            this.validateExist(dSession);
            this.validateDSessionBelongDoctor(dSession, req.currentUser);
            this.validateDSessionCancelOrComplete(dSession);

            await DsessionMessage.create(dsession_id, req.currentUser.id, message, MessageType.MESSAGE_TEXT);
            await dSession.update({
                last_message: message,
                last_message_user: req.currentUser.id,
                last_message_time: new Date()
            });

            publisher.publish(ChannelRedis.CHAT_CHANNEL, JSON.stringify({
                dsession_id, message,
                type: MessageType.MESSAGE_TEXT,
                user_id: req.currentUser.id
            }));
            return res.status(200).json({success: 1})
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    };

    this.videoCallLog = async (req, res) => {
        try{
            let {dsession_id, message} = req.body;
            let dSession = await DsessionRepository.findById(dsession_id);
            await DsessionMessage.create(dsession_id, -1, message, MessageType.MESSAGE_VIDEO_CALL_LOG);
            await dSession.update({
                last_message: message,
                last_message_user: -1,
                last_message_time: new Date()
            });

            publisher.publish(ChannelRedis.CHAT_CHANNEL, JSON.stringify({
                dsession_id,
                message,
                type: MessageType.MESSAGE_VIDEO_CALL_LOG,
                user_id: -1
            }));
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    };

    this.sendPrescription = async (req, res) => {
        try{
            let {id, diagnose, medicines, diet, body_clean, other_note, date_test_again} = req.body;

            let dSession = await DsessionRepository.findById(id);
            this.validateExist(dSession);

            this.validateDSessionBelongDoctor(dSession, req.currentUser);

            this.validateDSessionCancelOrComplete(dSession);

            // this.validateMedicineExist(JSON.parse(medicines));
            await dSession.update({
               last_message: "Gửi phiếu khám bệnh",
               last_message_user: req.currentUser.id,
               last_message_time: new Date()
            });

            let prescription = await DsessionResultRepository.create(id,
                diagnose,
                JSON.stringify(medicines),
                diet,
                body_clean,
                other_note,
                new Date(date_test_again));

            await DsessionMessage.create(id,
                req.currentUser.id,
                prescription.id,
                MessageType.MESSAGE_PRESCRIPTION,
                `${SERVER_PATIENT}/prescription-sent/${prescription.id}`
            );

            publisher.publish(ChannelRedis.CHAT_CHANNEL,
                JSON.stringify({dsession_id: id,
                    message: prescription.id,
                    type: MessageType.MESSAGE_PRESCRIPTION,
                    user_id: req.currentUser.id,
                    obj_url: `${SERVER_PATIENT}/prescription-sent/${prescription.id}`
                })
            );

            return res.status(200).json({success: 1})
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    };

    this.sendDiagnoseTest = async (req, res) => {
        try{
            let {dsession_id, disease_test} = req.body;

            let dSession = await DsessionRepository.findById(dsession_id);
            this.validateExist(dSession);

            this.validateDSessionBelongDoctor(dSession, req.currentUser);
            this.validateDSessionCancelOrComplete(dSession);
            this.validateDiseaseTest(JSON.parse(disease_test));

            await dSession.update({
                last_message: "Gửi phiếu xét nghiệm",
                last_message_user: req.currentUser.id,
                last_message_time: new Date()
            });

            let diseaseTest = await DsessionDiseaseTestRepository.create(dsession_id, disease_test);
            await DsessionMessage.create(dsession_id,
                req.currentUser.id,
                diseaseTest.id,
                MessageType.MESSAGE_DISEASE_TEST,
                `${SERVER_PATIENT}/disease-test/${diseaseTest.id}`
            );

            publisher.publish(ChannelRedis.CHAT_CHANNEL, JSON.stringify({dsession_id,
                message: diseaseTest.id,
                type: MessageType.MESSAGE_DISEASE_TEST,
                user_id: req.currentUser.id,
                obj_url: `${SERVER_PATIENT}/disease-test/${diseaseTest.id}`
            }));

            return res.status(200).json({success: 1})

        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    };

    this.sendImage = async (req, res) => {
        try {
            let {id, url} = req.body;

            let dSession = await DsessionRepository.findById(id);
            this.validateExist(dSession);

            this.validateDSessionBelongDoctor(dSession, req.currentUser);

            this.validateDSessionCancelOrComplete(dSession);

            await dSession.update({
                last_message: "Gửi hình ảnh",
                last_message_user: req.currentUser.id,
                last_message_time: new Date()
            });

            await DsessionMessage.create(id, req.currentUser.id, url, MessageType.MESSAGE_IMAGE, url);

            publisher.publish(ChannelRedis.CHAT_CHANNEL, JSON.stringify({
                dsession_id: id,
                message: url,
                type: MessageType.MESSAGE_IMAGE,
                obj_url: url,
                user_id: req.currentUser.id
            }));

        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    };

    this.setAppointment = async (req, res) => {
        try{
            let {time, id} = req.body;

            let dSession = await DsessionRepository.findById(id);
            this.validateExist(dSession);
            this.validateDSessionBelongDoctor(dSession, req.currentUser);
            this.validateDSessionCancelOrComplete(dSession);

            await DsessionMessage.create(dSession.id, req.currentUser.id, time, MessageType.MESSAGE_APPOINTMENT);
            await dSession.update({
                last_message_user: req.currentUser.id,
                last_message_time: new Date(),
                last_message: "Hẹn lịch khám"
            });

            publisher.publish(ChannelRedis.CHAT_CHANNEL, JSON.stringify({
                dsession_id: id,
                message: time,
                type: MessageType.MESSAGE_APPOINTMENT,
                user_id: req.currentUser.id
            }));

        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    };

    this.validateDiseaseTest = async (disease_tests) => {
        for (let disease_test of disease_tests) {
            let test = await DiseaseTestRepository.findById(disease_test);
            if (!test)
                throw new Error("Not found disease test");
        }
    };

    this.validateMedicineExist = async (medicines) => {
        for (let medicine of medicines) {
            let med = await MedicineRepository.findByID(medicine);
            if (!med)
                throw new Error("Not found medicine");
        }
    };

    this.validateExist = (object) => {
        if (!object) throw new Error("Not found object");
    };

    this.validateDSessionBelongDoctor = (dSession, doctor) => {
        if (dSession.doctor_id !== doctor.id)
            throw new Error("DSession not belong to doctor")
    };

    this.validateDSessionCancelOrComplete = (dSession) => {
        if (dSession.status !== DSessionStatus.STATUS_WAITING && dSession.status !== DSessionStatus.STATUS_ACCEPT) {
            throw new Error("DSession must waiting or accept");
        }
    };

};

module.exports = new DSessionMessageController();
