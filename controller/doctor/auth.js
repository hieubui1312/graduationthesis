var Utility = require("../../app_modules/utility");
var ErrorCode = require("../../app_modules/error_code");
// var firebaseAdmin = require("../../app_modules/firebase_auth/doctor");
var {UserRepository} = require("../../repositories");
const UserType = require("../../config/user_type");
var loginFactory = require("../../app_modules/login/login_factory");
var md5 = require("md5");
var env = require("dotenv").config();

let AuthController = function () {
    this.login = async (req, res) => {
        try{
            let {token} = req.body;
            let user = await loginFactory(UserType.DOCTOR).login(token);
            console.log("User lay dk", user);
            user.dataValues.access_token = Utility.encodeJWT(user);
            return res.status(200).json({user});
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    };

    this.specialLogin = async (req, res) => {
        try{
            let {code, phone} = req.body;
            if (code !== process.env.SECRET_LOGIN_CODE)
                return Utility.sendErrorResponse(res, "Secret code not correct", 401, ErrorCode.INTERNAL_SERVER_ERROR, true);

            const ACTIVATED = true;
            let user = await UserRepository.findByPhone(phone, UserType.DOCTOR, ACTIVATED);
            if (!user) return Utility.sendErrorResponse(res, "Not found user", 404, ErrorCode.NOT_FOUND, true);

            let token = Utility.encodeJWT(user);

            return res.status(200).json({
                success: 1,
                token
            });

        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    };

    this.verifyPassword = async (req, res) => {
        try{
            let access_token = req.get("Access-Token");
            let {password} = req.body;

            let data = Utility.verifyToken(access_token);
            data = JSON.parse(data.data);

            if (!data.phone)
                return Utility.sendErrorResponse(res, "Token fail", 500, ErrorCode.INTERNAL_SERVER_ERROR, true);

            let user = await UserRepository.findByPhone(data.phone, UserType.DOCTOR, true);
            if (md5(password) !== user.password)
                return  Utility.sendErrorResponse(res, "Password fail", 500, ErrorCode.PASSWORD_FAIL);

            user.access_token = Utility.encodeJWT(user);
            return res.status(200).json({user});
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    }



};

module.exports = new AuthController();
