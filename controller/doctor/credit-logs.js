var {CreditRepository} = require("../../repositories");
var Utility = require("../../app_modules/utility");
const ErrorCode = require("../../app_modules/error_code");

let CreditLogController = function () {
    this.getLogs = async (req, res) => {
        try{
            let currentUser = req.currentUser;
            let logs = await CreditRepository.findCreditLogOfUser(currentUser.id);
            return res.status(200).json({logs});
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR)
        }
    }
};

module.exports = new CreditLogController();
