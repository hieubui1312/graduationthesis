var {DiseaseTestRepository} = require("../../repositories");
var Utility = require("../../app_modules/utility");
const ErrorCode = require("../../app_modules/error_code");

let DiseaseTestController = function () {
  this.getList = async (req, res) => {
      try{
          let diseaseTests =  await DiseaseTestRepository.findAll(null);

          return res.status(200).json(diseaseTests);
      } catch (e) {
          Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
      }
  };

  this.getSub = async (req, res) => {
      try{
          let {id} = req.params;

          let diseaseTest = await DiseaseTestRepository.findAll(id);

          return res.status(200).json(diseaseTest);
      } catch (e) {
          return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
      }
  }
};


module.exports = new DiseaseTestController();
