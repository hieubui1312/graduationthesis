var Utility = require("../../app_modules/utility");
var {CreditRepository} = require("../../repositories");
const ErrorCode = require("../../app_modules/error_code");
const Config = require("../../app_modules/config");

module.exports = class CreditChangeLog  {
    async getList(req, res){
        let {currentUser} = req;
        let page = req.params.page ?? 1;

        let totalRecord = await CreditRepository.count(currentUser.id);
        let totalPages = totalRecord % Config.ITEM_PER_PAGE ? Math.floor(totalRecord / Config.ITEM_PER_PAGE) : totalRecord / Config.ITEM_PER_PAGE;

        let credits = await CreditRepository.getList(page, currentUser.id);
        return res.status(200).json({credits, pages: totalPages});
    }
};
