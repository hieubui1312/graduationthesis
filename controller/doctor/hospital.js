let {HospitalRepository} = require("../../repositories");
let Utility = require("../../app_modules/utility");
const ErrorCode = require("../../app_modules/error_code");

module.exports = class HospitalController {
    async getList(req, res){
        try{
            let hospitals = await HospitalRepository.getAll();
            return res.status(200).json(hospitals);
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    }
};
