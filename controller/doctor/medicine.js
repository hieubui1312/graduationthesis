var {MedicineRepository} = require("../../repositories");
var Utility = require("../../app_modules/utility");
const ErrorCode = require('../../app_modules/error_code');

let MedicineController = function () {
    this.getAll = async (req, res) => {
        try{
            let medicines = await MedicineRepository.getAll();
            return res.status(200).json(medicines);
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    }
};

module.exports = new MedicineController();
