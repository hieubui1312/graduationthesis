const {PaymentRepository, UserRepository, CreditRepository} = require("../../repositories");
const Utility = require("../../app_modules/utility");
const ErrorCode = require("../../app_modules/error_code");
const CreditLogType = require("../../config/credit_log_type");

class PaymentController {
    async create(req, res){
        try{
            let {amount} = req.body;
            let {currentUser} = req;

            let record = await PaymentRepository.create({user_id: currentUser.id,
                amount
            });
            let user = await UserRepository.findByID(currentUser.id);
            await CreditRepository.create(currentUser.id,
                user.credit,
                user.credit + amount,
                CreditLogType.PAYMENT,
                "Nạp tiền trong tài khoản qua internet banking"
                );
            await user.update({
                credit: user.credit + amount
            });
            return res.status(200).json({success: 1})
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    }
}

module.exports = new PaymentController();
