var Utility = require("../../app_modules/utility");
var ErrorCode = require("../../app_modules/error_code");
var {UserRepository} = require("../../repositories");
const UserType = require("../../config/user_type");

var UserController = function () {
    this.getPersonalInformation = (req, res) => {
        try{
            let currentUser = req.currentUser;
            console.log("Current user", currentUser);
            currentUser.dataValues.access_token = Utility.encodeJWT(currentUser);
            return res.status(200).json({user: currentUser, success: 1});
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    };

    this.register = async (req, res) => {
        try{
            let {name, email, address, sex, age} = req.body;
            let user = await UserRepository.findUserByEmail(email);
            if (user) return  Utility.sendErrorResponse(res, "Email have been exist", 500, ErrorCode.EMAIL_EXIST);

            let currentUser = req.currentUser;
            await UserRepository.updateInfo(currentUser.id, {name, email, address, sex, age});
            user = await UserRepository.findByID(currentUser.id, UserType.PATIENT, true);
            user.dataValues.access_token = Utility.encodeJWT(user);
            return res.status(200).json({user});
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    };

    this.updateInfo = async (req, res) => {
        try{
            let body = {};
            let currentUser = req.currentUser;

            for(let attr in req.body) {
                if ((req.body)[attr]) body[attr] = (req.body)[attr];
            }

            let user = await UserRepository.updateInfo(currentUser.id, body);

            return res.status(200).json({user});
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    };

};

module.exports = new UserController();
