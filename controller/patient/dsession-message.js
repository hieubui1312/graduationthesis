const Utility = require("../../app_modules/utility");
const ErrorCode = require("../../app_modules/error_code");
var {DsessionMessage, DsessionRepository} = require("../../repositories");
var dateformat = require("dateformat");
const MessageType = require("../../config/message_type");

const publisher = require("../../app_modules/redis/client");
const channelRedis = require("../../config/channelRedis");
const pushNotify = require("../../app_modules/push-notification/push-notification");


const getLastMessage = ({type, message}) => {
    switch (type) {
        case MessageType.MESSAGE_IMAGE:
            return "Gửi hình ảnh";
        case MessageType.MESSAGE_TEXT:
            return message;
        case MessageType.MESSAGE_DISEASE_TEST:
            return "Bác sĩ đã gửi phiếu xét nghiệm";
        case MessageType.MESSAGE_APPOINTMENT:
            return "Bác sĩ đã đặt lịch khám";
        case MessageType.MESSAGE_PRESCRIPTION:
            return "Bác sĩ đã gửi đơn thuốc";
    }
};

const DSessionMessageController = function () {
    this.chat = async (req, res) => {
        try{
            let {dsession_id, message, type, obj_id, obj_url} = req.body;
            let {currentUser} = req;

            if (!type) type = 1;
            let dsession = await DsessionRepository.findById(dsession_id);
            if (dsession.patient_id !== req.currentUser.id)
                return Utility.sendErrorResponse(res,
                    `Current user don't have permission to chat in dsession ${dsession_id}`,
                    500,
                    ErrorCode.INTERNAL_SERVER_ERROR,
                    true
                );

            await dsession.update({
                last_message: getLastMessage({type, message}),
                last_message_time: new Date(),
                last_message_user: currentUser.id,
                last_message_is_seen: false
            });

            await DsessionMessage.create(dsession_id,
                currentUser.id,
                message,
                type,
                obj_url,
                obj_id
            );

            publisher.publish(channelRedis.CHAT_CHANNEL, JSON.stringify({
                dsession_id,
                message,
                type,
                user_id: currentUser.id,
                obj_id,
                obj_url
            }));

            pushNotify.pushToUser(dsession.doctor_id, {
                title: `Tin nhắn mới`,
                body: `${currentUser.name} đã gửi tin nhắn cho bạn`,
                click_action: "http://localhost:8081"
            });

            return res.status(200).json({success: 1});
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    };

    this.seenMessage = async (req, res) => {
        let {id} = req.params;
        try{
            const {currentUser} = req;
            let dsession = await DsessionRepository.findById(id);
            if (!dsession) {
                return Utility.sendErrorResponse(res, "Not found dsession", 500, ErrorCode.INTERNAL_SERVER_ERROR, true);
            }
            if(currentUser.id !== dsession.last_message_user && !dsession.last_message_is_seen){
                await dsession.update({
                    last_message_is_seen: true,
                });
                publisher.publish(channelRedis.SEEN_CHANNEL, {
                    dsession_id: dsession.id,
                    last_message_user: dsession.last_message_user
                });
                return res.status(200).json({success: 1})
            } else {
                return res.status(200).json();
            }
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    };

    this.videoCallLog = async (req, res) => {
        try{
            let {dsession_id, message} = req.body;
            let dSession = await DsessionRepository.findById(dsession_id);
            await DsessionMessage.create(dsession_id, -1, message, MessageType.MESSAGE_VIDEO_CALL_LOG);
            await dSession.update({
                last_message: message,
                last_message_user: -1,
                last_message_time: new Date()
            });

            publisher.publish(channelRedis.CHAT_CHANNEL, JSON.stringify({
                dsession_id,
                message,
                type: MessageType.MESSAGE_VIDEO_CALL_LOG,
                user_id: -1
            }));
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    };
};

module.exports = new DSessionMessageController();
