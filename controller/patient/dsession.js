var {DsessionTypeRepository, UserRepository,
    DsessionRepository, DsessionPatient,
    DsessionFeeRepository, DoctorRepository, DsessionMessage} = require("../../repositories");
var UserType = require("../../app_modules/usertype");
var Utility = require("../../app_modules/utility");
var DsessionStatus = require("../../config/dsession");
var ErrorCode = require("../../app_modules/error_code");
var DsessionConstant = require("../../config/dsession");
var DoctorDegree = require("../../config/degee_doctor");
const MessageType = require('../../config/message_type');
var dateFormat = require("dateformat");
const publisher = require("../../app_modules/redis/client");
const ChannelRedis = require("../../config/channelRedis");

let DsessionController = function () {

    this.cancelDSession = async (req, res) => {
        try{
            let dSessionId = req.params.id;
            let currentUser = req.currentUser;

            let dSession = await DsessionRepository.findByDSessionIdAndPatientID(dSessionId, currentUser.id);

            if (!dSession)
                return Utility.sendErrorResponse(res, "Not found dsession of user", 404, ErrorCode.NOT_FOUND, true);

            if (dSession.status !== DsessionStatus.STATUS_WAITING)
                return Utility.sendErrorResponse(res, "Can't convert dSession status", 500, ErrorCode.CANCEL_DSESSION_FAIL);

            let creditNew = currentUser.credit + dSession.price;
            let creditHoldNew = currentUser.credit_hold - dSession.price;
            await UserRepository.updateCredit(currentUser.id, creditNew, creditHoldNew);

            return  res.status(200).json({success: 1})
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    },

    this.getMessages = async (req, res) => {
        try{
            let dSessionId = req.params.id;
            let currentUser = req.currentUser;

            let dSession = await DsessionRepository.findByDSessionIdAndPatientID(dSessionId, currentUser.id);
            if (!dSession) return Utility.sendErrorResponse(res, `Not found dSession ${dSession}`, 400, ErrorCode.NOT_FOUND);

            let messages = await DsessionMessage.getMessageOfDSession(dSessionId);
            return res.status(200).json({messages});
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    },

    this.getList = async (req, res) => {
      try{
          let {status} = req.query;
          let currentUser = req.currentUser;
          let dsessions = await DsessionRepository.findByPatientID(currentUser.id, status);
          return res.status(200).json({dsessions});
      }catch (e) {
          return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
      }
    },

    this.getListSimple = async (req, res) => {
        try{
            let currentUser = req.currentUser;
            let dsessions = await DsessionRepository.getListDSessions(currentUser.id);
            return res.status(200).json({dsessions});
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    },

    this.createDsession = async (req, res) => {
        try{
            let currentUser = req.currentUser;
            let patient_id = currentUser.id;
            let {doctor_id, type_id} = req.body;

            let doctor = await DoctorRepository.findById(doctor_id, UserType.DOCTOR, true);
            let patient = await UserRepository.findByID(patient_id, UserType.PATIENT, true);

            let fee = await this.calculateFeeDsession(type_id, doctor.degree);

            if (patient.credit < fee)
                return Utility.sendErrorResponse(res, "Patient not enough money", 400, ErrorCode.NOT_ENOUGH_MONEY, true);

            let credit = patient.credit - fee;
            let credit_hold = patient.credit_hold + fee;

            await patient.update({
                credit, credit_hold
            });

            let dsession = await DsessionRepository.create({
                doctor_id,
                patient_id,
                type_id,
                fee,
                currentUser
            });

            // const data = {
            //     doctor_id,
            //     patient_id: patient_id,
            //     User: currentUser,
            //     id: dsession.id,
            //     last_message: "Phiên khám đã được thiết lập. Vui lòng chờ bác sĩ trong giây lát"
            // };
            // publisher.publish(ChannelRedis.CREATE_DSESSION, JSON.stringify(data));

            await DsessionMessage.create(dsession.id, -1, "Phiên khám đã được thiết lập. Vui lòng chờ bác sĩ trong giây lát", MessageType.MESSAGE_TEXT);

            return res.status(200).json({data: dsession});
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    },

    this.updateInfoPatientInDsession = async (req, res) => {
        try{
            let {dsession_id, name, age, address, sex, description, time_begin} = req.body;

            let dsession = await DsessionRepository.findById(dsession_id);
            if (!dsession) return Utility.sendErrorResponse(res, "Not found dsession", 404, ErrorCode.NOT_FOUND, true);
            if (dsession.status === DsessionStatus.STATUS_CANCEL || dsession.status === DsessionStatus.STATUS_COMPLETE){
                return Utility.sendErrorResponse(res, "Dsession have been cancel or complete. Can't update info patient",
                    500, ErrorCode.INTERNAL_SERVER_ERROR, true);
            }
            if (dsession.patient_id !== req.currentUser.id)
                return Utility.sendErrorResponse(res, "User haven't permission to update info", 500, ErrorCode.INTERNAL_SERVER_ERROR, true);

            await DsessionPatient.updateInfo({dsession_id, name, age, sex, address, description, time_begin});
            return res.status(200).json({data: "success"});
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR, true);
        }
    },

    this.patientInfo = async (req, res) => {
        try{
            let {id} = req.params;
            let info = await DsessionPatient.findByDSessionId(id);
            console.log("Info dsession patient", info);
            return res.status(200).json(info ? info : {});
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    },

    this.calculateFeeDsession = async (dsessionType, degreeDoctor) => {
        let doctorLevel = this.getLevelDoctor(degreeDoctor);
        let dsessionFee = await DsessionFeeRepository.getFee({
            dsession_type: parseInt(dsessionType),
            doctor_level: doctorLevel
        });
        return dsessionFee;
    };

    this.getDSessionType = async (req, res) => {
        try{
            let {degree} = req.query;

            let doctorLevel = this.getLevelDoctor(degree);

            let dSessionTypes = await DsessionFeeRepository.getDSessionTypeDoctorLevel(doctorLevel);

            return res.status(200).json({data: dSessionTypes});
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    };

    this.getLevelDoctor = (degreeDoctor) => {
        let doctorLevel;

        switch (parseInt(degreeDoctor)) {
            case DoctorDegree.BS:
            case DoctorDegree.BS_CK1:
            case DoctorDegree.ThS_BS:
                doctorLevel = 1;
                break;
            case DoctorDegree.BS_CK2:
            case DoctorDegree.TS_BS:
                doctorLevel = 2;
                break;
            case DoctorDegree.PGS_TS_BS:
            case DoctorDegree.GS_TS_BS:
                doctorLevel = 3;
                break;
            default:
                throw new Error("Not found degree doctor");
        }

        return doctorLevel;
    };

    this.getDetail = async (req, res) => {
        try{
            let {id} = req.params;
            let currentUser = req.currentUser;
            let dSession = await DsessionRepository.findById(id);
            this.validateDSessionBelongPatient(dSession, currentUser);
            return res.status(200).json({dSession})
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    }

    this.validateDSessionBelongPatient = (dSession, patient) => {
        if (dSession.patient_id !== patient.id)
            throw new Error(`DSession ${dSession.id} not belong to patient ${patient.id}`);
    }

};

module.exports = new DsessionController();
