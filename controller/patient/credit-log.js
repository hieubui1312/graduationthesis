var Utility = require("../../app_modules/utility");
var ErrorCode = require("../../app_modules/error_code");
var {CreditRepository} = require("../../repositories");

let CreditLogController = function () {
    this.getList = async (req, res) => {
        try{
            let currentUser = req.currentUser;
            let creditLogs = await CreditRepository.findCreditLogOfUser(currentUser.id);
            return res.status(200).json({creditLogs});
        } catch (e) {
            Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    }
};

module.exports = new CreditLogController();
