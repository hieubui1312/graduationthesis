var Utility = require("../../app_modules/utility");
var ErrorCode = require("../../app_modules/error_code");
var {NotificationRepository} = require("../../repositories");

let NotificationController = function () {
    this.getList = async (req, res) => {
        try{
            let currentUser = req.currentUser;
            let notifications = await NotificationRepository.findNotificationsByUserId(currentUser.id);
            return res.status(200).json({notifications});
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    };

    this.seen = async (req, res) => {
        try{
            let {id} = req.params;
            await NotificationRepository.seen(id);
            return res.status(200).json({success: 1});
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    }
};

module.exports = new NotificationController();
