var firebaseAdmin = require("../../app_modules/firebase_auth");
var Utility = require("../../app_modules/utility");
var ErrorCode = require("../../app_modules/error_code");
var UserRepository = require("../../repositories/user_repository");
var UserType = require("../../app_modules/usertype");
var env = require("dotenv").config();

var AuthController = function () {

  this.login = async (req, res) => {
    try{
      let firebaseToken = req.body.token;
      let userRecord = await firebaseAdmin.auth().verifyIdToken(firebaseToken);

      let phoneNumber = userRecord.phone_number;
      phoneNumber = phoneNumber.replace("+84", "0");
      if (!phoneNumber) return Utility.sendErrorResponse(res, "Error auth firebase", 401, ErrorCode.NOT_AUTH_FIREBASE, true);

      let user = await UserRepository.findByPhone(phoneNumber);

      if (!user) {
        user = await UserRepository.create(phoneNumber, UserType.PATIENT, true);
      } else {
        if (user.type === UserType.DOCTOR)
          return Utility.sendErrorResponse(res, "Error: Login by doctor account", 401, ErrorCode.LOGIN_BY_DOCTOR_ACCOUNT_ERROR);
        if (!user.activated)
          return Utility.sendErrorResponse(res, "Account patient not active", 401, ErrorCode.ACCOUNT_NOT_ACTIVE);
      }

      user.dataValues.access_token = Utility.encodeJWT(user);
      return res.status(200).json({user});
    } catch (e) {
      return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
    }
  };

  this.specialLogin = async (req, res) => {
    try{
      let {code, phone} = req.body;
      if (code !== process.env.SECRET_LOGIN_CODE)
        return Utility.sendErrorResponse(res, "Secret code not correct", 401, ErrorCode.INTERNAL_SERVER_ERROR, true);

      const ACTIVATED = true;
      let user = await UserRepository.findByPhone(phone, UserType.PATIENT, ACTIVATED);
      if (!user) return Utility.sendErrorResponse(res, "Not found user", 404, ErrorCode.NOT_FOUND, true);

      let token = Utility.encodeJWT(user);

      return res.status(200).json({
        success: 1,
        token
      });

    } catch (e) {
      return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
    }
  };

};

module.exports = new AuthController();
