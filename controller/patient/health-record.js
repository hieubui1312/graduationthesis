var Utility = require("../../app_modules/utility");
const ErrorCode = require("../../app_modules/error_code");
var {HealthRecordRepository, UserRepository} = require("../../repositories");

module.exports = class HealthRecordController {
    async getList(req, res){
        try{
            let {currentUser} = req;
            let user = await UserRepository.findByID(currentUser.id);
            let records = await user.getHealthRecords({order: [["id", "DESC"]]});
            return res.status(200).json(records);
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    }

    async getDetail(req, res){
        try{
            let {currentUser} = req;
            let {id} = req.params;
            let user = await UserRepository.findByID(currentUser.id);
            let record = await user.getHealthRecords({
                where: {
                    id
                }
            });
            return res.status(200).json(record);
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    }

    async create(req, res){
        try{
            let data = req.body;
            let record = await HealthRecordRepository.create(data);
            return res.status(200).json(record);
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    }

    async delete(req, res) {
        let {id} = req.params;
        try{
            await HealthRecordRepository.destroy(id);
            return res.status(200).json({success: 1});
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    }

    async update(req, res){
        try{
            let {id} = req.params;
            let data = req.body;
            console.log("Data", data);
            let result = await HealthRecordRepository.update(data, id);
            return res.status(200).json(result);
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    }
};
