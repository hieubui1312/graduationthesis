var Utility = require("../../app_modules/utility");
const ErrorCode = require("../../app_modules/error_code");
var {DsessionDiseaseTestRepository, DiseaseTestRepository, DsessionRepository} = require("../../repositories");

let DiseaseTestController = function () {
    this.getDetail = async (req, res) => {
        try{
            let {id} = req.params;

            let dsessionDiseaseTest = await DsessionDiseaseTestRepository.findByID(id);

            let diseaseTestArr = JSON.parse(dsessionDiseaseTest.disease_test);

            let dsession = await DsessionRepository.findById(dsessionDiseaseTest.dsession_id);

            let diseaseTests = await DiseaseTestRepository.findByArr(diseaseTestArr);

            return res.status(200).json({diseaseTests, diagnostic:dsessionDiseaseTest , dsession});
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    }
};

module.exports = new DiseaseTestController();
