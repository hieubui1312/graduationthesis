const Utility = require("../../app_modules/utility");
const ErrorCode = require("../../app_modules/error_code");
const {DsessionResultRepository, DsessionRepository} = require("../../repositories");

const PrescriptionSentController = function () {
    this.getDetail = async (req, res) => {
        try{
            let {id} = req.params;
            let record = await DsessionResultRepository.findById(id);
            let dsession = await DsessionRepository.findById(record.dsession_id);
            return res.status(200).json({record, dsession});
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    }
};

module.exports = new PrescriptionSentController();
