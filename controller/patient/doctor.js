let {Doctor, User, DsessionType} = require("../../models");
let {UserRepository, DoctorRepository} = require("../../repositories");
let {ITEM_PER_PAGE} = require("../../app_modules/config");
let USER_TYPE = require("../../app_modules/usertype");
let Utility = require("../../app_modules/utility");
let ErrorCode = require("../../app_modules/error_code");
let {Op} = require("sequelize");

let DoctorController = function () {

    this.getList = async (req, res) => {
        let { degree, major} = req.query;
        let _where = {};
        if (degree) {
            _where.degree = degree;
        }
        if (major) {
            _where = {
                ..._where,
                majors: {
                    [Op.like]: `%${major}%`
                }
            }
        }

        let doctors = await Doctor.findAll({
            attributes: ['doctor_id', 'average_rating', 'degree', 'majors', "experience_year"],
            where: _where,
            include: [{
                model: User,
                attributes: ['name', 'phone', "avatar"],
                where: {
                    activated: true
                }
            }]
        });

        return res.status(200).json({data: doctors});
    };

    this.getMajorOfDoctor = async (doctorId) => {
        let majors = await DoctorRepository.getMajorOfDoctor(doctorId);
        return majors;
    };

    this.getDetail = async (req, res) => {
        try{
            let {id} = req.params;
            let record = await DoctorRepository.getDetail(id);
            return res.status(200).json(record);
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    }

};

module.exports = new DoctorController();
