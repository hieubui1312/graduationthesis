var {MajorRepository} = require( "../../repositories");
var Utility = require("../../app_modules/utility");
const ErrorCode = require("../../app_modules/error_code");

let MajorController = function () {
    this.getList = async(req, res) => {
        try{
            let majors = await MajorRepository.getList();
            return res.status(200).json({majors});
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR)
        }
    }
};

module.exports = new MajorController();
