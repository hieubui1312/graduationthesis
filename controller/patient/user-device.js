const Utility = require("../../app_modules/utility");
const ErrorCode = require("../../app_modules/error_code");
const {UserDeviceRepository} = require("../../repositories");

const UserDeviceController = function() {
    this.registerToken = async (req, res) => {
        try{
            const {currentUser} = req;
            const {token} = req.body;

            let userDevice = await UserDeviceRepository.findByUserId(currentUser.id);
            if (userDevice) {
                await userDevice.update({
                    token
                });
            } else {
                await UserDeviceRepository.create({
                    user_id: currentUser.id,
                    token,
                    is_failed: false
                })
            }

            return res.status(200).json({success: 1})
        } catch (e) {
            return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
        }
    }
};

module.exports = new UserDeviceController();
