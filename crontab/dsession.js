const schedule = require("node-schedule");
const {DsessionRepository, NotificationRepository} = require("../repositories");
const {Op} = require("sequelize");
const DSessionStatus = require("../config/dsession");
const PushNotify = require("../app_modules/push-notification/push-notification");
const publisher = require("../app_modules/redis/client");
const NotificationType = require("../config/notifycation_type");
const ChannelRedis = require("../config/channelRedis");
const SocketEvent = require("../config/socket_event");
const Server = require("../config/sever");

class DSessionCrontab{
    complete(){
        schedule.scheduleJob("* * * * *", async function () {
            let records = await DsessionRepository.getListDSessionExpire();
            for (let item of records) {
                PushNotify.pushToUser(item.patient_id, {
                    title: "Thông báo",
                    body: `Phiên khám ${item.id} của bạn đã hoàn thành!`,
                }, {
                    fcm_options: {
                        link: `${Server.SERVER_PATIENT}/dsession-chat/${item.id}`
                    }
                });

                const data = {
                    user_id: item.patient_id,
                    message: `Phiên khám ID ${item.id} của bạn đã hoàn thành`,
                    type: NotificationType.DSESSION_COMPLETE,
                    title: `Phiên khám ${item.id}`,
                    reference_id: item.id,
                    link: `/dsession-chat/${item.id}`
                };
                await NotificationRepository.create(data);
                publisher.publish(ChannelRedis.NOTIFY_CHANNEL, JSON.stringify(
                    data
                ));

                await item.update({
                   status:  DSessionStatus.STATUS_COMPLETE
                });
            }
        });
    }
}

module.exports = new DSessionCrontab();
