const {HealthRecordRepository, NotificationRepository} = require("../repositories");
const schedule = require("node-schedule");
const PushService = require("../app_modules/push-notification/push-notification");
const publisher = require("../app_modules/redis/client");
const RedisChannel = require("../config/channelRedis");
const SocketEvent = require("../config/socket_event");
const NotificationType = require("../config/notifycation_type");

class HealthRecordCrontab{

    remindExamineAgain(){
        schedule.scheduleJob("* * * * *", async function () {
            let records = await HealthRecordRepository.remindExamination();
            for (let item of records) {
                PushService.pushToUser(item.user_id, {
                    title: "Thông báo mới",
                    body: "Bạn có lịch khám lại vào ngày mai"
                });

                const data = {
                    title: "Tái khám",
                    message: "Bạn có lịch khám lại vào ngày mai",
                    link: `/health-record-info/${item.id}`,
                    reference_id: item.id,
                    user_id: item.user_id,
                    type: NotificationType.RE_EXAMINATION_TEST
                };

                await NotificationRepository.create(data);

                item.update({
                   is_remind: true
                });
            }
        });
    }
}

module.exports = new HealthRecordCrontab();
