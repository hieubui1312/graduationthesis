const dSessionCrontab = require("./dsession");
const healthRecordCrontab = require("./health-record");

dSessionCrontab.complete();

healthRecordCrontab.remindExamineAgain();

