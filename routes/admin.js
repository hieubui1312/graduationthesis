var app = require("express");
var router = app.Router();
var AuthController = require("../controller/admin/auth");
var DoctorController = require("../controller/admin/doctor");
var validateAdmin = require("../middleware/validateAdmin");
var UserController = require("../controller/admin/user");
var CreditLogController = require("../controller/admin/credit-log");
var DSessionController = require("../controller/admin/dsession");
var MajorController = require("../controller/admin/major");
var PaymentController = require("../controller/admin/payment");
var HospitalController = require("../controller/admin/hospital");
var WithdrawController = require("../controller/admin/withdraw");
var DSessionTypeController = require("../controller/admin/dsession-type");

router.post('/login', AuthController.login);
router.get('/doctors/:page', [validateAdmin], DoctorController.getList);
router.get('/doctor/:id', [validateAdmin], DoctorController.getDetail);
router.delete("/doctor/:id", [validateAdmin], DoctorController.delete);
router.get("/doctor/toggle-active/:id", [validateAdmin], DoctorController.toggleActive);
router.get("/doctor-analytic", [validateAdmin], DoctorController.analytic);
router.post("/doctor/create", [validateAdmin], DoctorController.create);
router.post("/doctor/update/:id", [validateAdmin], DoctorController.update);

router.get("/majors", [validateAdmin], MajorController.getList);
router.get("/hospitals", [validateAdmin], HospitalController.getList);

router.get('/patient/:page?', [validateAdmin], UserController.getPatients);
router.get('/patient/toggle-active/:id', [validateAdmin], UserController.togglePatient);

router.get('/credit-log/:page', [validateAdmin], CreditLogController.getList);
router.get('/payment/:page', [validateAdmin], PaymentController.getList);

router.get('/dsession/:page', [validateAdmin], DSessionController.getList);
router.get("/withdraw/:page", [validateAdmin], WithdrawController.getList);
router.get("/withdraw/decline/:id", [validateAdmin], WithdrawController.decline);
router.get("/withdraw/accept/:id", [validateAdmin], WithdrawController.accept);

router.get("/dsession-type/list", [validateAdmin], DSessionTypeController.getList);
router.post("/dsession-type/create", [validateAdmin], DSessionTypeController.create);
router.post("/dsession-type/update/:id", [validateAdmin], DSessionTypeController.update);
router.get("/dsession-type/delete/:id", [validateAdmin], DSessionTypeController.delete);
router.get("/dsession-type/:id", [validateAdmin], DSessionTypeController.detail);
router.get("/dsession-fee", [validateAdmin], DSessionTypeController.getFees);
router.post("/dsession-fee/create", [validateAdmin], DSessionTypeController.createFee);

module.exports = router;
