let router = require("express").Router();
let upload = require("../controller/common/upload");
let {SERVER_API} = require("../config/sever");

router.post("/upload", upload.single("file"), function(req, res, next) {
    if(!req.file) {
        res.status(500);
        return next(err);
    }
    res.json({ fileUrl: `${SERVER_API}/images/${req.file.filename}`});
});

module.exports = router;
