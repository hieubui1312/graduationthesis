var express = require("express");
var router = express.Router();
var DoctorController = require("../controller/patient/doctor");
var AuthController = require("../controller/patient/auth");
var UserController = require("../controller/patient/user");
var DsessionController = require("../controller/patient/dsession");
var DSessionMessageController = require("../controller/patient/dsession-message");
var CreditLogController = require("../controller/patient/credit-log");
var MajorController = require("../controller/patient/major");
var NotificationController = require("../controller/patient/notification");
var DiseaseTestController = require("../controller/patient/disease-test");
var HealthRecordController = new (require("../controller/patient/health-record"))();
var UserDeviceController = require("../controller/patient/user-device");
var PaymentController = require("../controller/patient/payment");
var PrescriptionSentController = require("../controller/patient/prescription-sent");
var validatePatient = require("../middleware/validatePatient");
var validateDsessionCreate = require("../validate_request/dsession-create");
var validateUpdateInfoPatientInDsession = require("../validate_request/update-info-patient-dsession");
var validateRegisterRequest = require("../validate_request/register-patient");


router.post('/login', AuthController.login);
router.post('/special-login', AuthController.specialLogin);
router.get('/me/info', [validatePatient], UserController.getPersonalInformation);
router.post('/register', [validatePatient, ...validateRegisterRequest], UserController.register);
router.post("/update-info", [validatePatient], UserController.updateInfo);
router.post("/register-token", [validatePatient], UserDeviceController.registerToken);

router.get('/majors', MajorController.getList);
router.get('/doctors', DoctorController.getList);
router.get("/doctor-detail/:id", DoctorController.getDetail);

router.post('/dsession/create', [validatePatient, ...validateDsessionCreate], DsessionController.createDsession);
router.post('/dsession/update/patient-info', [validatePatient, ...validateUpdateInfoPatientInDsession], DsessionController.updateInfoPatientInDsession);
router.get("/dsession/patient-info/:id", [validatePatient], DsessionController.patientInfo);
router.get('/dsession/type', DsessionController.getDSessionType);
router.post('/dsession/chat', [validatePatient], DSessionMessageController.chat);
router.post("/dsession/video-call-log", [validatePatient], DSessionMessageController.videoCallLog);
router.post("/dsession/is_seen/:id", [validatePatient], DSessionMessageController.seenMessage);
router.get("/dsessions", [validatePatient], DsessionController.getList);
router.get("/dsessions/simple", [validatePatient], DsessionController.getListSimple);
router.get("/dsession/specific/:id", [validatePatient], DsessionController.getDetail);
router.get("/dsession/:id", [validatePatient], DsessionController.getMessages);
router.get("/dsession/cancel/:id", [validatePatient], DsessionController.cancelDSession);

router.get("/credit-logs", [validatePatient], CreditLogController.getList);
router.get("/notifications", [validatePatient], NotificationController.getList);
router.get("/notification_seen/:id", [validatePatient], NotificationController.seen);
router.get("/disease-test/:id", DiseaseTestController.getDetail);
router.get("/prescription-sent/:id", PrescriptionSentController.getDetail);
router.post("/payment", [validatePatient], PaymentController.create);

router.get("/health-record", [validatePatient], HealthRecordController.getList);
router.get("/health-info/:id", [validatePatient], HealthRecordController.getDetail);
router.post("/health-record/create", [validatePatient], HealthRecordController.create);
router.get("/health-record/delete/:id", [validatePatient], HealthRecordController.delete);
router.post("/health-record/update/:id", [validatePatient], HealthRecordController.update);


module.exports = router;
