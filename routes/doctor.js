var app = require("express");
var router = app.Router();
var AuthController = require("../controller/doctor/auth");
var UserController = require("../controller/doctor/user");
var DSessionController = require("../controller/doctor/dsession");
var DSessionMessageController = require("../controller/doctor/dSession-message");
var CreditLogController = require("../controller/doctor/credit-logs");
var DiseaseTestController = require("../controller/doctor/disease-test");
var MedicineController = require("../controller/doctor/medicine");
var validateCreatePasswordRequest = require("../validate_request/create-password-request");
var HospitalController = new (require("../controller/doctor/hospital"))();
var MajorController = new (require("../controller/doctor/major"))();
var CreditController = require("../controller/doctor/credit-logs");
var WithdrawController = new (require("../controller/doctor/with-draw"))();
var validateDoctor = require("../middleware/validateDoctor");
var validateDSessionId = require("../validate_request/dsession-accept-decline");
var validateDSessionParam = require("../validate_request/dsession-id-param");
var validateDiseaseTestInput = require("../validate_request/disease-test-req");
var validateUpdateInfoReq = require("../validate_request/validate-update-info");

router.post('/login', AuthController.login);
router.post('/special-login', AuthController.specialLogin);
router.post("/check-password", AuthController.verifyPassword);
router.get("/me/info", validateDoctor, UserController.info);
router.get("/me", validateDoctor, UserController.getMe);
router.post("/create-password", [...validateCreatePasswordRequest], UserController.createPassword);
router.post("/update-info", [validateDoctor, ...validateUpdateInfoReq], UserController.updateInfo);
router.get("/specialize", [validateDoctor], UserController.getSpecialize);
router.post("/update-info-special", [validateDoctor], UserController.updateSpecial);
router.post("/change-password", [validateDoctor], UserController.changePassword);
router.get("/account-info", [validateDoctor], UserController.getAccountInfo);

router.get("/hospital", [validateDoctor], HospitalController.getList);
router.get("/major", [validateDoctor], MajorController.getList);
router.get("/credit-change-log", [validateDoctor], CreditController.getLogs);
router.get("/withdraw", [validateDoctor], WithdrawController.getList);
router.post("/withdraw-request", [validateDoctor], WithdrawController.request);

router.post("/dsession/accept", [validateDoctor, ...validateDSessionId], DSessionController.acceptDSession);
router.post('/dsession/decline', [validateDoctor, ...validateDSessionId], DSessionController.declineDSession); // can co validate request
router.get('/dsession/message/:id', [validateDoctor, ...validateDSessionParam], DSessionController.getMessages); // co validate request
router.get('/dsessions', [validateDoctor], DSessionController.getList);
router.get('/dsession/patient-info/:id', [validateDoctor, ...validateDSessionParam], DSessionController.getPatientInfoOfDSession);
router.get('/dsession/patient/:id', [validateDoctor, ...validateDSessionParam], DSessionController.getDSessionOldWithPatient);

router.get("/credit-logs", [validateDoctor], CreditLogController.getLogs);

router.get("/disease-test/group", [validateDoctor], DiseaseTestController.getList);
router.get("/disease-test/:id", [validateDoctor], DiseaseTestController.getSub);

router.get("/medicine", [validateDoctor], MedicineController.getAll);

router.post('/dsession/chat', [validateDoctor], DSessionMessageController.chat);
router.post('/dsession/video-call-log', [validateDoctor], DSessionMessageController.videoCallLog);

router.post('/dsession/disease-test', [validateDoctor, ...validateDiseaseTestInput], DSessionMessageController.sendDiagnoseTest);
router.post('/dsession/prescription', [validateDoctor, ...validateDSessionId], DSessionMessageController.sendPrescription);
router.post("/dsession/image", [validateDoctor, ...validateDSessionId], DSessionMessageController.sendImage);
router.post("/dsession/appointment-schedule", [validateDoctor, ...validateDSessionId], DSessionMessageController.setAppointment);

module.exports = router;


