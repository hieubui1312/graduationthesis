const firebaseAdmin = require("../firebase_auth/index");
const {UserDeviceRepository} = require("../../repositories");

class PushNotification {
    async pushToUser(user_id, data, webPush){
        let record = await UserDeviceRepository.findByUserId(user_id);
        if (record) {
            const message = {
                notification: data,
                token: record.token
            };
            if (webPush) {
                message.webpush = webPush;
            }
            firebaseAdmin.messaging().send(message);
        }
    }
}

module.exports = new PushNotification();
