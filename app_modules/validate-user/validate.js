var NotFoundError = require("../../errors/not_found");
var Utility = require("../../app_modules/utility");
var {UserRepository} = require("../../repositories");
var UserType = require("../../app_modules/usertype");

module.exports = function (token, userType) {
    function validateTokenExist(){
        if (!token) throw new NotFoundError("Not found Token");
    }
    async function getUser(callback) {
        let {data} = Utility.verifyToken(token);
        data = JSON.parse(data);
        let user = await UserRepository.findByID(data.id, userType);

        if (!user) throw new NotFoundError("Not found user");

        if (!user.activated) throw new Error("User must be activated");

        callback(data, user);
        return user;
    }

    async function getDoctor(){
        let checkPassword = (data, user) => {
            // if ((user.password !== data.password) || user.password == null) throw new Error("Password fail");
            return true;
        };
        return await getUser(checkPassword);
    }

    async function  getPatient(){
        return await getUser();
    }

    async function handle() {
        validateTokenExist();
        switch (userType) {
            case UserType.PATIENT:
                return await getPatient();
            case UserType.DOCTOR:
                return await getDoctor();
            case UserType.ADMIN:
                return await getDoctor();
            default:
                throw new Error("User type not correct");
        }
    }

    return handle;
};
