const redis = require("redis");
var client = redis.createClient();
var logger = require("../logger");


client.on("error", function(error) {
    logger.error("Client redis error: " + error.message);
});

module.exports = client;
