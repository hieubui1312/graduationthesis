module.exports = {
  NOT_FOUND: {
      code: "1",
      message: "Not found resource "
  },
  INTERNAL_SERVER_ERROR: {
      code: "2",
      message: "INTERNAL_SERVER_ERROR"
  },
  NOT_AUTH_FIREBASE: {
      code: "3",
      message: "Not authentication with firebase"
  },
  AUTHENTICATION_FAIL: {
      code: "4",
      message: "Authentication fail"
  },
  ACCESS_TOKEN_OUT_OF_DATE: {
      code: "5",
      message: "Access token out of date"
  },
  PARAM_NOT_CORRECT: {
      code: "6",
      message: "Param is not correct"
  },

  // Error of patient: Error code of patient begin with "1"
  NOT_ENOUGH_MONEY: {
      code: '101',
      message: "Patient not enough money"
  },

    LOGIN_BY_DOCTOR_ACCOUNT_ERROR: {
        code: '102',
        message: "Patient can't login with account doctor"
    },
  ACCOUNT_NOT_ACTIVE: {
      code: '103',
      message: "Account patient not active"
  },

  CANCEL_DSESSION_FAIL: {
      code: "104",
      message: "Can't cancel dsession"
  },

  EMAIL_EXIST: {
      code: "105",
      message: "Email exist"
  },

  // Error for doctor: Error for doctor begin with "2"
  PASSWORD_FAIL: {
      code: "201",
      message: "Password fail"
  }

};
