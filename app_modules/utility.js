var jwt = require('jsonwebtoken');
var env = require('dotenv').config();
var winston = require('winston');
var logger = require('../app_modules/logger');

module.exports = {
    sendErrorResponse(res, err, httpCode, vietskinError, customCode){
        let logMessage, errorCode;

        if (customCode) {
            errorCode = customCode === true ? vietskinError.code : customCode;
            if (!err) logMessage = vietskinError.message;
            else if (typeof err === 'string') logMessage = err;
            else logMessage = err.message;
        } else {
            errorCode = vietskinError.code;
            logMessage = vietskinError.message;
        }

        if (typeof  err == 'object') logger.error(err.message);
        else logger.error(err);
        return res.status(httpCode).json({
            errorCode, message: logMessage
        })
    },

    encodeJWT(data){
        data = JSON.stringify(data);
        let secretCode = process.env.PRIVATE_KEY_JWT ? process.env.PRIVATE_KEY_JWT : "HIEUBUIMINH";

        let timeCurrent = Math.floor(Date.now() / 1000);
        let expiresIn = timeCurrent + 7 * 86400;
        let token = jwt.sign({data, expiresIn}, secretCode );
        return token;
    },

    verifyToken(token){
        let secretCode = process.env.PRIVATE_KEY_JWT ? process.env.PRIVATE_KEY_JWT : "HIEUBUIMINH";
        let result = jwt.verify(token, secretCode);

        let {expiresIn, data} = result;
        if (!data) throw new Error("Authentication fail");

        let currentTime = Math.floor(Date.now() / 1000);
        if (currentTime > expiresIn) throw new Error("Access token out of date");
        return result;
    }
};
