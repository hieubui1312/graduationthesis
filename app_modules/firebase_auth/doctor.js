var admin = require("firebase-admin");

var serviceAccount = require("../../keys/firbase-auth-service");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://patientaccess-bfeab.firebaseio.com"
}, "doctor");

module.exports = admin;
