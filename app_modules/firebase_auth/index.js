var admin = require('firebase-admin');

var serviceAccount = require("./../../keys/admin-firebase-key");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://fir-notify-v2.firebaseio.com"
});

module.exports = admin;
