var UserType = require("../usertype");
var LoginPatient = require("./login_patient");
var LoginDoctor = require("./login_doctor");

module.exports = (userType) => {
  switch (userType) {
      case UserType.DOCTOR:
          return new LoginDoctor();
      case UserType.PATIENT:
          return new LoginPatient();
      default:
          throw new Error("Not found user type correct to login");
  }
};
