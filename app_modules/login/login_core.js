var firebaseAdmin = require("../../app_modules/firebase_auth/doctor");
var {UserRepository} = require("../../repositories");
var UserType = require("../../config/user_type");

class Login {
    constructor(){
        this.user = null;
        this.user_type = null;
    }

    async getPhoneNumberByFirebaseToken(firebaseToken){
        let userRecord = await firebaseAdmin.auth().verifyIdToken(firebaseToken);
        let phoneNumber = userRecord.phone_number;

        if (!phoneNumber)
            throw new Error("Auth firebase fail");

        phoneNumber = phoneNumber.replace("+84", "0");

        return phoneNumber;
    };


    validateUserCorrectType () {
        if (this.user.type !== this.user_type)
            throw new Error("User must be patient");
        return true;
    };

    validateUserActivated () {
        const USER_ACTIVATED = true;
        if (this.user.activated !== USER_ACTIVATED)
            throw new Error("User must be activated");
        return true;
    };
}

module.exports = Login;

// module.exports = function () {
//     this.loginByPatient =  (firebaseToken) => {
//         let phoneNumber = this.getPhoneNumberByFirebaseToken(firebaseToken);
//         this.user = this.getUser(phoneNumber);
//         this.validateUserCorrectType();
//         this.validateUserActivated();
//         return this.user;
//     };
//
//     this.getPhoneNumberByFirebaseToken = (firebaseToken) => {
//         let userRecord = firebaseAdmin.auth().verifyIdToken(firebaseToken);
//         let phoneNumber = userRecord.phone_number;
//         if (phoneNumber)
//             throw new Error("Auth firebase fail");
//         return phoneNumber;
//     };
//
//     this.getUser = (phoneNumber) => {
//         let user = UserRepository.findByPhone(phoneNumber);
//         if (this.user) user = UserRepository.create(phoneNumber, UserType.PATIENT);
//         return user;
//     };
//
//     this.validateUserCorrectType = () => {
//         if (this.user.type !== UserType.PATIENT)
//             throw new Error("User must be patient");
//         return true;
//     };
//
//     this.validateUserActivated = () => {
//         const USER_ACTIVATED = 1;
//          if (user.activated !== USER_ACTIVATED)
//             throw new Error("User must be activated");
//          return true;
//     };
// };
