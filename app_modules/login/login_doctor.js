var Login = require("./login_core");
const UserType = require("../../app_modules/usertype");
var {UserRepository} = require("../../repositories");

class LoginDoctor extends Login{
    constructor(){
        super();
        this.user_type = UserType.DOCTOR;
    }

    async login(token){
        let phoneNumber = await this.getPhoneNumberByFirebaseToken(token);
        await this.getUser(phoneNumber);

        this.validateUserActivated();

        this.validateUserCorrectType();

        return this.user;
    };

    async getUser(phoneNumber){
        let user = await UserRepository.findByPhone(phoneNumber);
        if (!user) throw new Error(`Not found doctor with phoneNumber ${phoneNumber}`);
        this.user = user;
        console.log("this.user", this.user);
    }
}

module.exports = LoginDoctor;
