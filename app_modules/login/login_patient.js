var Login = require("./login_core");
const UserType = require("../../config/user_type");
var {UserRepository} = require("../../repositories");

class LoginPatient extends Login{
    constructor(){
        super();
        this.user_type = 1;
    }
    // user_type = UserType.PATIENT;

    login(token){
        let phoneNumber = this.getPhoneNumberByFirebaseToken(token);
        this.user = this.getUser(phoneNumber);
        this.validateUserActivated();
        this.validateUserCorrectType();
        return this.user_type;
    };

    getUser(phoneNumber){
        this.user = UserRepository.findByPhone(phoneNumber);
        if (this.user) {
            this.validateUserCorrectType();
            this.validateUserActivated();
        } else {
            this.user = UserRepository.create(phoneNumber, UserType.PATIENT);
        }
        return this.user;
    }
}

module.exports = LoginPatient;
