var app = require("express")();
var http = require("http").createServer(app);
var io = require("socket.io")(http);
var {DsessionRepository} = require("./repositories");
var subscriber = require("./app_modules/redis/client");
const channelRedis = require("./config/channelRedis");
const SOCKET_EVENT = require("./config/socket_event");
var Utility = require("./app_modules/utility");
const DSessionStatus = require("./config/dsession");
require('dotenv').config();

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, Access-Token"
    );
    res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
    next()
});

subscriber.on("message", function (channel, message) {
    if (channel === channelRedis.CHAT_CHANNEL || channel === channelRedis.SEEN_CHANNEL) {
        console.log("Co nguoi gui chat message", message);
        message = JSON.parse(message);
        let roomName = `Room-${message.dsession_id}`;
        io.to(roomName).emit("chat-message", message);
    }

    if (channel === channelRedis.NOTIFY_CHANNEL) {
        message = JSON.parse(message);
        let roomName = `Room-${message.reference_id}`;
        console.log("Room name", roomName);
        io.emit("notify", message);
    }

    if (channel === channelRedis.DSESSION_ACTION) {
        message = JSON.parse(message);
        let roomName = `Room-${message.dsession_id}`;
        io.to(roomName).emit("dsession-action", message);
    }

    if (channel === channelRedis.CREATE_DSESSION) {
        io.emit("dsession_create", message);
    }
});

subscriber.subscribe(channelRedis.CHAT_CHANNEL);
subscriber.subscribe(channelRedis.SEEN_CHANNEL);
subscriber.subscribe(channelRedis.DSESSION_ACTION);
subscriber.subscribe(channelRedis.NOTIFY_CHANNEL);
subscriber.subscribe(channelRedis.CREATE_DSESSION);

function getUserIds() {
    let sockets = io.sockets.sockets;
    let userIds = [];
    for (let key of Object.keys(sockets)){
        let socket = sockets[key];
        let user = socket.user;
        if (user) userIds.push(user.id);
    }
    return userIds;
}

function verifyUserIsLogin(token){
    let user = Utility.verifyToken(token);
    user = JSON.parse(user.data);
    let userIds = getUserIds();
    if (userIds.indexOf(user.id) !== -1) {
        return user;
    } else {
        throw new Error("User is not login");
    }
}

async function verifyUserExistInRoomChat(user_id, rooms){
    let dsessions = await DsessionRepository.findByPatientID(user_id,
        [DSessionStatus.STATUS_WAITING, DSessionStatus.STATUS_ACCEPT]);
    if (!dsessions) {
        dsessions = await DsessionRepository.findByDoctorID(user_id,
            [DSessionStatus.STATUS_WAITING, DSessionStatus.STATUS_ACCEPT]
        );
    }
    if (!dsessions) throw new Error("Not found room to join");

    let dsessionIds = [];
    for (let key of Object.keys(dsessions)) {
        dsessionIds = dsessionIds.concat(dsessions[key]);
    }
    dsessionIds = dsessionIds.map(dsession => dsession.id);

    for (let room of rooms) {
        if (dsessionIds.indexOf(room) === -1) throw new Error(`Can't join room ${room}`);
    }
    return true;
}

io.on("connection", function (socket) {
    console.log("User connected");
    socket.on(SOCKET_EVENT.LOGIN, function (data) {
        try{
            let user = Utility.verifyToken(data);
            socket.user = JSON.parse(user.data);
            socket.emit(SOCKET_EVENT.LOGIN_SUCCESS, {user})
        } catch (e) {
            socket.emit(SOCKET_EVENT.LOGIN_FAIL, e.message);
        }
    });

    socket.on(SOCKET_EVENT.JOIN_ROOM, async function ({rooms, token}) {
        try{
            let user = verifyUserIsLogin(token);
            await verifyUserExistInRoomChat(user.id, rooms);
            for (let room of rooms) {
                socket.join(`Room-${room}`)
            }
        } catch (e) {
            socket.emit(SOCKET_EVENT.INTERNAL_ERROR, e.message);
        }
    });

    socket.on(SOCKET_EVENT.START_TYPING, async({room, token}) => {
        try{
            let user = verifyUserIsLogin(token);
            io.to(room).emit(SOCKET_EVENT.START_TYPING, {
               id: user.id
            });
        } catch (e) {
            socket.emit(SOCKET_EVENT.INTERNAL_ERROR, e.message);
        }
    });

    socket.on(SOCKET_EVENT.STOP_TYPING, async ({room, token}) => {
        try{
            let user = verifyUserIsLogin(token);
            io.to(room).emit(SOCKET_EVENT.STOP_TYPING, {
                id: user.id
            });
        } catch (e) {
            socket.emit(SOCKET_EVENT.INTERNAL_ERROR, e.message);
        }
    })
});

let port = process.env.PORT_SOCKET ? process.env.PORT_SOCKET:3000;
http.listen(port, function () {
   console.log("Socket server listening in port " + port);
});
