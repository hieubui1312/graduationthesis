'use strict';
var DoctorFactory = require("../factory/doctor");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    let doctors = await DoctorFactory.generates();
    return queryInterface.bulkInsert("tbl_doctor", doctors);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("tbl_doctor", {}, null);
  }
};
