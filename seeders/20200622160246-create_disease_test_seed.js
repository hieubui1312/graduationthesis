'use strict';

const DiseaseTestFactory = require("../factory/disease_test");

module.exports = {
  up: (queryInterface, Sequelize) => {
    const data = DiseaseTestFactory.generate();
    return queryInterface.bulkInsert("tbl_disease_test", data);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("tbl_disease_test", null, {});
  }
};
