'use strict';
var HospitalFactory = require("../factory/hospital");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    let hospitals = HospitalFactory.generateHospital();
    return queryInterface.bulkInsert("tbl_hospital", hospitals);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("tbl_hospital", null, {});
  }
};
