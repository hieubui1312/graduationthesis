'use strict';
var UserFactory = require("../factory/user");
module.exports = {
  up: async (queryInterface, Sequelize) => {
    const countUser = 10;
    let doctors =  UserFactory.generateDoctor();
    let users =  UserFactory.generateUser();
    return queryInterface.bulkInsert("tbl_user", [...doctors, ...users]);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("tbl_user", null, {});
  }
};
