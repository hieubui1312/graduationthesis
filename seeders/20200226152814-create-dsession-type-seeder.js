'use strict';

let dsessionTypes = [{
  name: "Gói khám 7 ngày",
  time: 7 * 86400,
  description: "Gói khám đặc trị kéo dài 7 ngày",
}, {
  name: "Gói khám 14 ngày",
  time: 14 * 86400,
  description: "Gói khám đặc trị kéo dài 14 ngày",
}];
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("tbl_dsession_type", dsessionTypes);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('tbl_dsession_type', null, {});
  }
};
