'use strict';

const MedicineFactory = require("../factory/medicine");

module.exports = {
  up: (queryInterface, Sequelize) => {
    let data = MedicineFactory.generate();
    return queryInterface.bulkInsert("tbl_medicine", data);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("tbl_medicine", null, {});
  }
};
