'use strict';
var MajorFactory = require("../factory/major");

module.exports = {
  up: (queryInterface, Sequelize) => {
    let majors = MajorFactory.generateMajors();
    return queryInterface.bulkInsert("tbl_major", majors);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("tbl_major", {}, null);
  }
};
