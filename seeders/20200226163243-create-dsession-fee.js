'use strict';

let dsessionFees = [];
for (let i = 1; i<=2; i++) {
  for (let j = 1; j <= 3; j++) {
    dsessionFees.push({
      dsession_type: i,
      doctor_level: j,
      fee:  Math.random() >= 0.5 ? 100000:200000
    });
  }
}

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("tbl_dsession_fee", dsessionFees);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('tbl_dsession_fee', null, {});
  }
};
