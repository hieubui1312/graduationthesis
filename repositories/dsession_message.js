const {DsessionMessage} = require("../models");

module.exports = {
    async create(dsession_id, user_id, message, type, obj_url){
        return await DsessionMessage.create({
            dsession_id, user_id, message, type, obj_url
        });
    },

    async getMessageOfDSession(dsession_id) {
      return DsessionMessage.findAll({
          where: {
              dsession_id
          },
          order: [["created_at", "asc"]]
      })
    }
};
