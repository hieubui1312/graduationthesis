var {Doctor, User, Hospital, Major, DoctorMajorMap, Dsession} = require("../models");
var UserType = require("../config/user_type");
var DSessionType = require("../config/dsession");
const Config = require("../app_modules/config");
var {sequelize} = require("../models");

module.exports = {
    async findById(id){
        const ACTIVATED = true;
        let doctor = await Doctor.findOne({
            attributes: ["doctor_id", "degree"],
            include: {
                model: User,
                where: {
                    type: UserType.DOCTOR,
                    activated: ACTIVATED,
                    id
                }
            }
        });
        if (!doctor) throw new Error("Not found doctor");
        return doctor;
    },

    async getList(page){
        let doctors = await User.findAll({
            where: {
                type: UserType.DOCTOR
            },
             include: {
                model: Doctor
             },
            limit: Config.ITEM_PER_PAGE,
            order: [["id", "DESC"]],
            offset: (page - 1) * Config.ITEM_PER_PAGE
        });
        return doctors;
    },

    async count(){
        let count = await Doctor.count();
        return count;
    },

    async getDetail(id){
        let doctor = await User.findOne({
            where: {
                id,
                type: UserType.DOCTOR
            },
            include:[{
                model: Doctor,
                include:[{
                    model: Hospital
                }]
            }]
        });
        return doctor;
    },

    async delete(id){
        let user = await User.findOne({
            where: {id}
        });
        let doctor = await Doctor.findOne({
            where: {id}
        });
        if (!user || !doctor) throw new Error(`Not found User Or Doctor id ${id}`);

        await doctor.destroy();
        await user.destroy();
    },

    async update(id, info){
        let doctor = await Doctor.findOne({
            where: {doctor_id: id}
        });
        doctor.hospital_id = info.hospital_id;
        doctor.degree = info.degree;
        doctor.cert_date = info.cert_date;
        doctor.atm_number = info.atm_number;
        doctor.atm_info = info.atm_info;
        doctor.experience = info.experience;
        doctor.certificate = info.certificate;
        await doctor.save();
        return  doctor;
    },

    async updateInfo(id, info){
        return Doctor.update(info, {
            where: {doctor_id: id}
        })
    },

    async create(doctor_id){
        let doctor = await Doctor.create({
            doctor_id
        });
        return doctor;
    },

    async createDoctor(data){
        let doctor = await Doctor.create(data);
        return doctor;
    },

    async getInfo(id){
        let doctor = await User.findOne({
            where: {
                id
            },
            include: [
                {
                    model: Doctor,
                    include: [{
                        model: Hospital
                    }]
                }
            ]
        });
        return doctor;
    },

    async updateInfoSpecial(info, id){
        let doctorInfo = await Doctor.update({
            ...info
        }, {where: {doctor_id: id}});
        return doctorInfo;
    },

    async getMajorOfDoctor(id) {
        let majors = await DoctorMajorMap.findAll({
            where: {
                doctor_id: id
            },
            include: [{
                model: Major,
                attributes: ["name"]
            }]
        });
        let majorArr = [];

        if (majors.length) {
            majorArr = majors.map((major) => major.Major.name)
        }

        return majorArr;
    },

    async analytic(){
        let doctors = await User.findAll({
           where: {
               type: UserType.DOCTOR
           }
        });
        let resultAll = await Dsession.findAll({
            attributes: ["doctor_id", [sequelize.fn("COUNT", sequelize.col("Dsession.id")), "count"]],
            group: ["Dsession.doctor_id"]
        });
        let all = {};
        resultAll.forEach(item => {
            all[item.doctor_id] = item.dataValues.count;
        });

        let resultAccept =  await Dsession.findAll({
            attributes: ["doctor_id", [sequelize.fn("COUNT", sequelize.col("Dsession.id")), "count"]],
            group: ["Dsession.doctor_id"],
            where: {
                status : DSessionType.STATUS_ACCEPT
            }
        });
        let accept = {};
        resultAccept.forEach(item => {
           accept[item.doctor_id] = item.dataValues.count;
        });

        let resultDecline =  await Dsession.findAll({
            attributes: ["doctor_id", [sequelize.fn("COUNT", sequelize.col("Dsession.id")), "count"]],
            group: ["Dsession.doctor_id"],
            where: {
                status : DSessionType.STATUS_DECLINE
            }
        });
        let decline = {};
        resultDecline.forEach(item => {
            decline[item.doctor_id] = item.dataValues.count;
        });

        let resultComplete =  await Dsession.findAll({
            attributes: ["doctor_id", [sequelize.fn("COUNT", sequelize.col("Dsession.id")), "count"]],
            group: ["Dsession.doctor_id"],
            where: {
                status : DSessionType.STATUS_COMPLETE
            }
        });
        let complete = {};
        resultComplete.forEach(item => {
            complete[item.doctor_id] = item.dataValues.count;
        });

        console.log("All", all);

        doctors.forEach(doctor => {
            doctor.dataValues.all = all[doctor.id] ? all[doctor.id] : 0;
            doctor.dataValues.accept = accept[doctor.id] ? accept[doctor.id] : 0;
            doctor.dataValues.decline = decline[doctor.id] ? decline[doctor.id] : 0;
            doctor.dataValues.complete = complete[doctor.id] ? complete[doctor.id] : 0;
        });

        return doctors;
    }
};
