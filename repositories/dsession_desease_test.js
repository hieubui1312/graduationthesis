var {DSessionDiseaseTest} = require("../models");

module.exports = {
    async create(dsession_id, disease_test){
        return await DSessionDiseaseTest.create({
            dsession_id, disease_test
        });
    },

    async findByID(id) {
        let diseaseTest = await DSessionDiseaseTest.findOne({
            where: {
                id
            }
        });

        return diseaseTest;
    }
};
