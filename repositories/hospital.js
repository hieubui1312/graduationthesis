var {Hospital} = require("../models");

module.exports = {
    async findByID(id){
        let hospital = await Hospital.findOne({
            where: {id}
        });
        return hospital;
    },

    async getAll(){
        let hospitals = await Hospital.findAll();
        return hospitals;
    }
};
