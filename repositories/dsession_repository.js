var {DsessionType, Dsession, User, Doctor} = require("../models");
var DsessionStatus = require("../config/dsession");
const Config = require("../app_modules/config");
var db = require("../models");
var Op = db.Sequelize.Op;
var sequelize = db.sequelize;
const publisher = require('../app_modules/redis/client');
const ChannelRedis = require("../config/channelRedis");

module.exports = {
    async create({doctor_id, patient_id, type_id, fee, currentUser}){
        let dsessionType = await DsessionType.findOne({
            where: {id: type_id}
        });

        let timeDuration = dsessionType.time;
        let endTime = Math.floor(Date.now() / 1000) + timeDuration;

        let dsession = await Dsession.create({
            doctor_id,
            patient_id,
            type_id,
            price: fee,
            status: DsessionStatus.STATUS_WAITING,
            end_time: endTime,
            last_message: "Phiên khám đã được thiết lập. Vui lòng chờ bác sĩ trong giây lát",
            last_message_time: new Date(),
            last_message_is_seen: false,
            last_message_user: -1
        });
        const data = {
            doctor_id,
            patient_id: patient_id,
            User: currentUser,
            id: dsession.id,
            end_time: endTime,
            last_message: "Phiên khám đã được thiết lập. Vui lòng chờ bác sĩ trong giây lát"
        };
        publisher.publish(ChannelRedis.CREATE_DSESSION, JSON.stringify(data));
        return dsession;
    },

    async findById(id){
        let dsession = await Dsession.findOne({
            where: {id},
            include: [{
                model: Doctor,
                include: [{
                    model: User
                }]
            }, {
                model: User
            }]
        });
        return dsession;
    },

    async findByUserID(_where, dsession_status){
        if (dsession_status) {
            _where = {
                ..._where,
                status: {
                    [Op.in]: dsession_status
                }
            }
        }
        let {rows, count} = await Dsession.findAndCountAll({
            where: _where,
            order: [["last_message_time", "DESC"]],
            include: [{
                model: Doctor,
                include: [{model: User}]
            }, {
                model: User
            }]
        });

        if (count <= 0 ) return null;
        let dsessions = {
          complete: [],
          waiting: [],
          cancel: [],
          accept: [],
          decline: []
        };
        for (let row of rows) {
            switch (row.status) {
                case DsessionStatus.STATUS_WAITING:
                    (dsessions.waiting).push(row);
                    break;
                case DsessionStatus.STATUS_ACCEPT:
                    (dsessions.accept).push(row);
                    break;
                case DsessionStatus.STATUS_COMPLETE:
                    (dsessions.complete).push(row);
                    break;
                case DsessionStatus.STATUS_CANCEL:
                    (dsessions.cancel).push(row);
                    break;
                case DsessionStatus.STATUS_DECLINE:
                    (dsessions.decline).push(row);
                    break;
            }
        }
        return  dsessions;

    },

    async getListDSessions(patient_id){
        let dsessions = await Dsession.findAll({
            where: {
                patient_id
            },
            order: [["last_message_time", "DESC"]],
            include: [{
                model: Doctor,
                include: [{model: User}]
            }, {
                model: User
            }]
        });
        return dsessions;
    },

    async findByPatientID(id, status){
        return await module.exports.findByUserID({patient_id: id}, status);
    },

    async findByDoctorID(id, status){
        return await module.exports.findByUserID({doctor_id: id}, status);
    },

    async findByDSessionIdAndPatientID(dsession_id, patient_id){
        let dsessions = await Dsession.findOne({
            where: {
                id: dsession_id,
                patient_id
            }
        });
        return dsessions;
    },

    async getListDSessionExpire(){
        let currentTime = Math.floor(Date.now() / 1000);
        let records = await Dsession.findAll({
           where: {
               end_time: {
                   [Op.lt] : currentTime
               },
               status: DsessionStatus.STATUS_ACCEPT
           }
        });
        return records;
    },



    async findByDoctorIdAndPatientId(doctor_id, patient_id){
        let dSessions = await Dsession.findAll({
            where: {
                doctor_id, patient_id
            }
        });
        return dSessions;
    },

    async cancelDsession(dsession){
        dsession.status = DsessionStatus.STATUS_CANCEL;
        await dsession.save();
        return dsession;
    },

    async acceptDSession(dSession){
        dSession.status = DsessionStatus.STATUS_ACCEPT;
        dSession.updated_at = new Date();
        await dSession.save();
        return dSession;
    },

    async declineDSession(dSession){
        dSession.status = DsessionStatus.STATUS_DECLINE;
        await dSession.save();
        return dSession;
    },

    async count(){
        let count = await Dsession.count();
        return count;
    },

    async getList(page){
        let dSessions = await Dsession.findAll({
            order: [["created_at", "DESC"]],
            limit: Config.ITEM_PER_PAGE,
            offset: (page -1) * Config.ITEM_PER_PAGE,
            include: [{
                model: User,
                foreignKey: "patient_id"
            }]
        });
        for (let dSession of dSessions) {
            let doctor = await User.findOne({
                where: {
                    id: dSession.doctor_id
                }
            });
            dSession.dataValues.Doctor = doctor;
        }
        return dSessions;
    },

    async countSpecific(doctor_id){
        let dsession = await Dsession.findAll({
            attributes: ["status", [sequelize.fn("count", sequelize.col("id")), "count"]],
            group: ["status"],
            where: {doctor_id}
        })
        console.log("Dsession", dsession);
        return dsession;
    }
};
