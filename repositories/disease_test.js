var {DiseaseTest} = require("../models");
var db = require("../models");
var Op = db.Sequelize.Op;

module.exports = {
    async findById(id){
        let diseaseTest = await DiseaseTest.findOne({
           where: {
               id
           }
        });
        return diseaseTest;
    },

    async findAll(parent_id){
        let diseaseTests = await DiseaseTest.findAll({
            where: {
                parent_id
            }
        });
        return diseaseTests;
    },

    async findByArr(idArr){
        let diseaseTest = await DiseaseTest.findAll({
            where: {
                id: {
                    [Op.in] : idArr
                }
            }
        });
        return diseaseTest;
    }
};
