var {Withdraw, User, CreditLog} = require("../models");
const WithdrawStatus = require("../config/withdraw-status");
const Config = require("../app_modules/config");

module.exports = class WithdrawRepository {
    async find(user_id){
        return Withdraw.findAll({
           where: {
               user_id
           }
        });
    }

    async decline(id){
        let withdraw = await Withdraw.findOne({
            where: {id}
        });
        await withdraw.update({
            status: 3
        });
        let user = await User.findOne({
            where: {
                id: withdraw.user_id
            }
        });
        await user.update({
            credit_hold: user.credit_hold - withdraw.amount,
            credit: user.credit + withdraw.amount
        });
    }

    async accept(id){
        let withdraw = await Withdraw.findOne({
            where: {id}
        });
        await withdraw.update({
            status: 2
        });
        let user = await User.findOne({
            where: {
                id: withdraw.user_id
            }
        });
        await CreditLog.create({
            user_id: withdraw.user_id,
            credit_from: user.credit + withdraw.amount,
            credit_to: user.credit ,
            type: 4,
            reason: "Chuyển tiền"
        });
        await user.update({
            credit_hold: user.credit_hold - withdraw.amount,
        });
    }

    async getList(page){
        let results = await Withdraw.findAll({
            include: {
                model: User
            },
            offset: (page - 1),
            limit: Config.ITEM_PER_PAGE
        });
        return results;
    }

    async count(){
        let count = await Withdraw.count();
        return count;
    }

    async findById(id){
        let record = await Withdraw.findOne({
            where: {
                id
            }
        });
        return record;
    }

    async create(data, user){
        let {credit, credit_hold} = user;
        let {amount} = data;
        amount = parseInt(amount);

        let withdraw = await Withdraw.create({
            user_id: user.id,
            amount,
            status: WithdrawStatus.STATUS_WAITING
        });
        await User.update({
            credit: credit - amount,
            credit_hold: credit_hold + amount
        }, {where: {id: user.id}});

        return withdraw;
    }
};
