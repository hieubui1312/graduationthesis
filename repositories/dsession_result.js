var {DsessionResult} = require("../models");

module.exports = {
  async create(dsession_id, diagnose, medicine, diet, body_clean, other_note, date_test_again){
      return await DsessionResult.create({
          dsession_id,
          diagnose,
          medicine,
          diet,
          body_clear: body_clean,
          other_note,
          date_test_again
      });
  },

  async findById(id){
      let record = await DsessionResult.findOne({
          where: {id}
      });
      return record;
  }
};
