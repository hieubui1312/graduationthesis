let {Medicine} = require("../models");

module.exports =  {
    async findByID(id) {
        let medicine = await Medicine.findOne({
            where: {
                id
            }
        });
        return medicine;
    },

    async getAll(){
        let medicines = await Medicine.findAll();
        return medicines;
    }
};
