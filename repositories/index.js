var UserRepository = require("./user_repository");
var DsessionTypeRepository = require("./dsession_type_repository");
var DsessionRepository = require("./dsession_repository");
var DsessionPatient = require("./dsession_patient");
var DsessionFeeRepository = require("./dsession_fee");
var DsessionMessage = require("./dsession_message");
var DoctorRepository = require("./doctor");
var CreditRepository = require("./credit_log");
var NotificationRepository = require("./notification");
var MedicineRepository = require("./medicine");
var DsessionResultRepository = require("./dsession_result");
var DiseaseTestRepository = require("./disease_test");
var DsessionDiseaseTestRepository = require("./dsession_desease_test");
var HospitalRepository = require("./hospital");
var MajorRepository = require("./major");
var WithdrawRepository = new (require("./withdraw"))();
var HealthRecordRepository = require("./health_record");
var UserDeviceRepository = require("./user_device");
var PaymentRepository = require("./payment");

module.exports = {
  UserRepository,
  DsessionTypeRepository,
  DsessionRepository,
  DsessionPatient,
  DsessionFeeRepository,
  DoctorRepository,
  DsessionMessage,
  CreditRepository,
  NotificationRepository,
  MedicineRepository,
  DsessionResultRepository,
  DiseaseTestRepository,
  DsessionDiseaseTestRepository,
  HospitalRepository,
  MajorRepository,
  WithdrawRepository,
  HealthRecordRepository,
  UserDeviceRepository,
  PaymentRepository
};
