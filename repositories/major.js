var {Major} = require("../models");

module.exports = {
    async getList(){
        let majors = await Major.findAll();
        return majors;
    }
};
