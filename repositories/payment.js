const {Payment, User} = require("../models");
const Config = require("../app_modules/config");

module.exports = {
    create: async(data) => {
        console.log("Data", data);
        let record = await Payment.create(data);
        return record;
    },

    getList: async (page, phone) => {
        console.log("Page", page);
        let _where = {};
        if (phone) _where.phone = phone;
        const count = await Payment.count({
            include: [{
                model: User,
                where: _where
            }]
        });
        const pages = count % Config.ITEM_PER_PAGE ? Math.floor(count / Config.ITEM_PER_PAGE) + 1 :
            count / Config.ITEM_PER_PAGE;
        const records = await Payment.findAll({
            include: [{
                model: User,
                where: _where
            }],
            limit: Config.ITEM_PER_PAGE,
            offset: (page - 1) * Config.ITEM_PER_PAGE
        });
        return {data: records, pages};
    }
};
