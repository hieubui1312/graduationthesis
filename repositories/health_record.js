var {HealthRecord} = require("../models");
const {Op} = require("sequelize");

module.exports = {
    async findByUserID(id){
        let records = await HealthRecord.findAll({
            where: {user_id: id},
            order: [["id", "DESC"]]
        });
        return records;
    },

    async create(data){
        let record = await HealthRecord.create(data);
        return record;
    },

    async destroy(id){
        return await HealthRecord.destroy({
            where: {id}
        })
    },

    async update(data, id) {
        return await HealthRecord.update(data, {
            where: {id}
        })
    },

    async remindExamination(){
        const records = await HealthRecord.findAll({
           where: {
               is_remind: false,
               re_examination_date: {
                   [Op.lt] : new Date(new Date().getTime() + 86400000)
               }
           }
        });
        return records;
    }
};
