var {DsessionPatient} = require("../models");

module.exports = {
    async updateInfo({dsession_id, name, age, sex, address, description, time_begin}){
        let dsessionPatient = await DsessionPatient.findOne({
            where: {
                dsession_id
            }
        });

        if (dsessionPatient) {
            dsessionPatient.name = name;
            dsessionPatient.age = age;
            dsessionPatient.sex = sex;
            dsessionPatient.address = address;
            dsessionPatient.description = description;
            dsessionPatient.time_begin = time_begin;
            await dsessionPatient.save();
        } else {
            dsessionPatient = await DsessionPatient.create({
                dsession_id, name, age, sex, address, description, time_begin
            });
        }

        return dsessionPatient;
    },

    async findByDSessionId(id){
        let patient = await DsessionPatient.findOne({
            where: {
                dsession_id: id
            }
        });

        return patient;
    }
};
