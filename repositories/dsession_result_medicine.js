var {DsessionResultMedicine} = require("../models");

module.exports = {
  async create({medicine_id, count, usage, note}){
        let resultMedicine = await DsessionResultMedicine.create({
            medicine_id, count, usage, note
        });
        return resultMedicine;
  },

  async creates(medicines){
      let meds = await DsessionResultMedicine.bulkCreate(medicines, {returning: true});
      return  meds;
  }

};
