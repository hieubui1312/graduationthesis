var {CreditLog, User} = require("../models");
const Config = require("../app_modules/config");

module.exports = {
    async findCreditLogOfUser(user_id) {
        let creditLogs = await CreditLog.findAll({
            where: {
                user_id
            }
        });
        return creditLogs;
    },

    async count(user_id){
        let _where = {};
        if (user_id) _where.user_id = user_id;
        let count = await CreditLog.count({
            where: _where
        });
        return count;
    },

    async getList(page, phone){
        let _whereUser = {};
        if (phone) _whereUser = {phone};

        let count = await CreditLog.count({
            include: [{
                model: User,
                where: _whereUser
            }]
        });
        let pages = count % Config.ITEM_PER_PAGE ?
            Math.floor(count / Config.ITEM_PER_PAGE) + 1 : count / Config.ITEM_PER_PAGE;

        let creditLogs = await CreditLog.findAll({
            include: [{
                model: User,
                where: _whereUser
            }],
            order: [["created_at", "ASC"]],
            limit: Config.ITEM_PER_PAGE,
            offset: page
        });
        return {credit_logs: creditLogs, pages};
    },

    async create(user_id, credit_from, credit_to, type, reason){
        return  await CreditLog.create({
           user_id, credit_from, credit_to, type, reason
        });
    }
};
