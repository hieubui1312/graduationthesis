var {DsessionType, DsessionFee} = require("../models");

module.exports = {
    async findById(id){
        let dsessionType = await DsessionType.findOne({
            where: {id}
        });

        if (!dsessionType) throw new Error(`Not found dsession type with id ${id}`);
        return dsessionType;
    },

    async create(data){
        let type = await DsessionType.create(data);
        return type;
    },

    async delete(id){
        await DsessionType.destroy({
            where: {id}
        });
    },

    async update(id, data){
        let dsession = await DsessionType.findOne({
            where: {id}
        });
        await dsession.update(data);
    },

    async getList(){
        let dsessions = await DsessionType.findAll();
        return dsessions;
    },

    async getFees(){
        let types = await DsessionType.findAll();
        let result = [];
        for (let type of types) {
            let fees = await DsessionFee.findAll({
                where: {
                    dsession_type: type.id
                }
            });
            result.push({type, fees});
        }
        return result;
    },

    async createFee(dsession_type, doctor_level, fee){
        let dsessionFee = await DsessionFee.findOne({
            where: {
                dsession_type,
                doctor_level
            }
        });
        if (dsessionFee){
            await dsessionFee.update({
                fee
            })
        } else {
            await DsessionFee.create({
                dsession_type,
                doctor_level,
                fee
            })
        }
    }
};
