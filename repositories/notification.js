var {Notification} = require("../models");

module.exports = {
    async findNotificationsByUserId(user_id) {
        let notifications =  await Notification.findAll({
            where: {
                user_id
            },
            order: [["id", "DESC"]]
        });
        return notifications;
    },

    async create(data) {
        let record = await Notification.create(data);
        return record;
    },

    async seen(id){
        let notify = await Notification.findOne({
            where: {
                id
            }
        });
        if (!notify) throw new Error("Not found notification");
        await notify.update({
            is_seen: true
        });
        return true;
    }
};
