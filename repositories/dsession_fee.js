var {DsessionFee, DsessionType} = require("../models");

module.exports = {
    async getFee(_where){
        let dsessionFee = await DsessionFee.findOne({
            attributes: ["fee"],
            where: _where
        });
        if (!dsessionFee) throw new Error("Not found dsession fee");
        return dsessionFee.fee;
    },

    async getDSessionTypeDoctorLevel(doctorLevel){
        let dSesstionTypes = await DsessionFee.findAll({
            where: {
                doctor_level: doctorLevel
            },
            include: [{
                model: DsessionType
            }]
        });
        return dSesstionTypes;
    }

};
