var {User, Doctor, Hospital} = require("../models");
var UserType = require("../app_modules/usertype");
var md5 = require("md5");
var Config = require("../app_modules/config");
var {Op} = require("sequelize");

module.exports = {
    async findByID(id, type , activated) {
        let _where = {
            id
        };
        if (type) _where.type = type;
        if (activated !== undefined) _where.activated = activated;

        let user = await User.findOne({where: _where});
        if (!user) throw new Error(`Not found user with id ${id}`);

        return user;
    },
    async findByPhone(phone, type , activated) {
        let _where = {
            phone
        };
        if (type) _where.type = type;
        if (activated !== undefined) _where.activated = activated;
        let user = await User.findOne({where: _where});
        return  user;
    },

    async updateCredit(id, credit, credit_hold) {
        let user = await User.findOne({
            where: {id}
        });
        user.credit = credit;
        user.credit_hold = credit_hold;
        await user.save();
        return user;
    },

    async findUserByEmail(email){
        let user = await User.findOne({
            where: {
                email
            }
        });
        return user;
    },

    async create(phone, type, activated = true) {
        let user = await User.create({
            phone,
            activated,
            type
        });
        return user;
    },

    async createUser(data){
        let user = await User.create({...data, type: UserType.DOCTOR});
        return user;
    },

    async updateInfo(id, personalInfo){
        // let {name, email, address, sex, age} = personalInfo;
        let user = await User.update(personalInfo, {
            where: {id}
        });
        return user;
    },

    async createPassword(user, password){
        password = md5(password);
        user.password = password;
        await user.save();
        return user;
    },

    async findByEmailAndPassword(email, password){
        let user = await User.findOne({
            where: {
                email, password
            }
        });
        return user;
    },

    async toggleActive(id){
        let user = await User.findOne({
            where: {
                id
            }
        });
        user.activated = !user.activated;
        await user.save();
        return user;
    },

    async getList(page, userType, phone){
        let _where = {
            type: userType
        };
        if (phone){
            _where = {..._where,
                phone
            }
        }
        let users = await User.findAll({
            where: _where,
            order: [["created_at", "DESC"]],
            limit: Config.ITEM_PER_PAGE,
            offset: (page - 1) * Config.ITEM_PER_PAGE
        });
        return users;
    },

    async count(userType){
        let count = await User.count({
            where: {
                type: userType
            }
        });
        return count;
    },

    async getInfoSpecial(id){
        let userInfo = await Doctor.findOne({
            include: [{
                model: Hospital
            }],
           where: {doctor_id: id}
        });
        return userInfo;
    },

    async update(data, id) {
        await User.update(data, {
            where: {id}
        })
    }
};
