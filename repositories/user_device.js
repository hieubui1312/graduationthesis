const {UserDevice} = require("../models");

module.exports  = {
    async create(data){
        let userDevice = await UserDevice.create(data);
        return userDevice;
    },

    async findByUserId(user_id){
        let record = await UserDevice.findOne({
            where: {
                user_id
            }
        });
        return record;
    },

};
