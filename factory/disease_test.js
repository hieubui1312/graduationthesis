module.exports = {
    generate: () => {
        return [
            {
                name: "Vi khuẩn - kí sinh trùng",
                parent_id: null,
                description: "Vi khuẩn - kí sinh trùng"
            },
            {
                name: "Huyết thanh",
                parent_id: null,
                description: "Vi khuẩn - kí sinh trùng"
            },
            {
                name: "Giải phẫu bệnh",
                parent_id: null,
                description: "Vi khuẩn - kí sinh trùng"
            },
            {
                name: "Nước tiểu",
                parent_id: null,
                description: "Vi khuẩn - kí sinh trùng"
            },
            {
                name: "Sinh hoá máu",
                parent_id: null,
                description: "Vi khuẩn - kí sinh trùng"
            },{
                name: "Siêu âm",
                parent_id: null,
                description: "Vi khuẩn - kí sinh trùng"
            },
            {
                name: "Nấm - kí sinh trùng",
                parent_id: null,
                description: "Vi khuẩn - kí sinh trùng"
            },
            {
                name: "Chụp và phân tích da",
                parent_id: null,
                description: "Vi khuẩn - kí sinh trùng"
            },
            {
                name: "Toxocara (giũn đũa, chó, mèo) Ab miễn dịch bán tự động",
                parent_id: 1,
                description: "Vi khuẩn - kí sinh trùng"
            },{
                name: "Cysticercus cellulosae (sán lợn) Ab miễn dịch bán tự động",
                parent_id: 1,
                description: "Vi khuẩn - kí sinh trùng"
            },{
                name: "Strongyloides stercoralis (Giun lươn) miễn dịch bán tự động",
                parent_id: 1,
                description: "Vi khuẩn - kí sinh trùng"
            },{
                name: "Entamoeba histolytica (Amip) Ab miễn dịch bán tự động",
                parent_id: 1,
                description: "Vi khuẩn - kí sinh trùng"
            },{
                name: "Vi nấm soi tươi (dịch âm đạo/niệu đạo)",
                parent_id: 1,
                description: "Vi khuẩn - kí sinh trùng"
            },{
                name: "HBsAg test nhanh",
                parent_id: 2,
                description: "Vi khuẩn - kí sinh trùng"
            },{
                name: "HIV khẳng định",
                parent_id: 2,
                description: "Vi khuẩn - kí sinh trùng"
            },{
                name: "HIV Ag/Ab test nhanh",
                parent_id: 2,
                description: "Vi khuẩn - kí sinh trùng"
            },{
                name: "Treponema pallidum RPR định tính và định lượng (định lượng)",
                parent_id: 2,
                description: "Vi khuẩn - kí sinh trùng"
            },{
                name: "Treponema pallidum RPR định tính và định lượng (định tính)",
                parent_id: 2,
                description: "Vi khuẩn - kí sinh trùng"
            },{
                name: "Xét nghiệm mô bệnh học thường quy cố định, chuyển, đúc, cắt, nhuộm… các bệnh phẩm sinh",
                parent_id: 3,
                description: "Vi khuẩn - kí sinh trùng"
            },{
                name: "Chọc hút kim nhỏ các khối sưng, khối u dưới da",
                parent_id: 3,
                description: "Vi khuẩn - kí sinh trùng"
            },
            {
                name: "Xét nghiệm sinh thiết tức thì bằng khăn lạnh",
                parent_id: 3,
                description: "Vi khuẩn - kí sinh trùng"
            },
            {
                name: "Tế bào học dịch các tổn thương dạng nang",
                parent_id: 3,
                description: "Vi khuẩn - kí sinh trùng"
            },
            {
                name: "Nhuộm miễn dịch huỳnh quang trực tiếp phát hiện kháng nguyên",
                parent_id: 3,
                description: "Vi khuẩn - kí sinh trùng"
            },
            {
                name: "Xét nghiệm tế bào cặn nước tiểu (bằng phương pháp thủ công)",
                parent_id: 4,
                description: "Vi khuẩn - kí sinh trùng"
            },
            {
                name: "Tổng phân tích nước tiểu (bằng máy tự động)",
                parent_id: 4,
                description: "Vi khuẩn - kí sinh trùng"
            },
            {
                name: "Định tính Porphyrin (niệu)",
                parent_id: 4,
                description: "Vi khuẩn - kí sinh trùng"
            },
            {
                name: "Xét nghiệm đường máu mao mạch tại giường",
                parent_id: 5,
                description: "Vi khuẩn - kí sinh trùng"
            },
            {
                name: "Đo hoạt độ CK (Creatine kinase) (máu)",
                parent_id: 5,
                description: "Vi khuẩn - kí sinh trùng"
            },
            {
                name: "Định lượng Acid Uric (máu)",
                parent_id: 5,
                description: "Vi khuẩn - kí sinh trùng"
            },
            {
                name: "Siêu âm Doppler các khối u trong ổ bụng",
                parent_id: 6,
                description: "Vi khuẩn - kí sinh trùng"
            },
            {
                name: "Siêu âm 3D/4D tim",
                parent_id: 6,
                description: "Vi khuẩn - kí sinh trùng"
            },
            {
                name: "Siêu âm phần mềm (hạch, nách, bẹn, khoeo, u phần mềm dưới da)\n" +
                    "Siêu âm khớp (một vị trí)",
                parent_id: 6,
                description: "Vi khuẩn - kí sinh trùng"
            },
            {
                name: "Kháng sinh đồ (Nấm)",
                parent_id: 7,
                description: "Vi khuẩn - kí sinh trùng"
            },
            {
                name: "Vi nấm soi tươi (vị trí thứ nhất)",
                parent_id: 7,
                description: "Vi khuẩn - kí sinh trùng"
            },
            {
                name: "Vi nấm soi tươi (vị trí thứ hai)",
                parent_id: 7,
                description: "Vi khuẩn - kí sinh trùng"
            },
            {
                name: "Vi nấm soi tươi (vị trí thứ ba)",
                parent_id: 8,
                description: "Vi khuẩn - kí sinh trùng"
            },
            {
                name: "Chụp và phân tích da bằng máy Dermoscopy (chụp và phân tích da cho một tổn thương)",
                parent_id: 8,
                description: "Vi khuẩn - kí sinh trùng"
            },
            {
                name: "Chụp và phân tích da bằng máy Dermoscopy (chụp và phân tích từ 2 đến 3 tổn thương)",
                parent_id: 8,
                description: "Vi khuẩn - kí sinh trùng"
            },
            {
                name: "Chụp và phân tích da bằng máy Dermoscopy (chụp toàn bộ cơ thể và phân tích tổn thương)",
                parent_id: 8,
                description: "Vi khuẩn - kí sinh trùng"
            }
        ]
    }
};
