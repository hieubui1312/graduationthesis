var faker = require("faker");
faker.locale = 'vi';

module.exports = {
    generate(count){
        let hospitals = [];
        for (let i = 0; i < count; i++) {
            let hospital = module.exports.generateSingle();
            hospitals.push(hospital);
        }
        return hospitals;
    },

    generateSingle(){
        let hospital = faker.company.companyName();
        let province = faker.address.city();
        let detail = "Bệnh viện tuyến trung ương";
        return {
            name: hospital, province, detail
        }
    },

    generateHospital(){
        return [
            {
                name: "Bệnh Viện Tai Mũi Họng Trung Ương",
                province: "Hà Nội",
                detail: "78 Giải Phóng"
            },
            {
                name: "Bệnh viện Lão khoa Trung Ương",
                province: "Hà Nội",
                detail: "1A Phương Mai"
            },
            {
                name: "Bệnh Viện Da Liễu Trung Ương",
                province: "Hà Nội",
                detail: "15A Phương Mai"
            }
            ,
            {
                name: "Bệnh Viện Bạch Mai",
                province: "Hà Nội",
                detail: "78 Đường Giải Phóng"
            }
            ,
            {
                name: "Bệnh viện Mắt Trung Ương",
                province: "Hà Nội",
                detail: "85 Bà Triệu"
            }
            ,
            {
                name: "Bệnh viện Trung ương Quân đội 108",
                province: "Hà Nội",
                detail: "Số 1 Trần Hưng Đạo"
            }
            ,
            {
                name: "Bệnh viện Y Học Cổ Truyền Trung Ương",
                province: "Hà Nội",
                detail: "29 Nguyễn Bỉnh Khiêm"
            }
            ,
            {
                name: "Bệnh Viện Bệnh Nhiệt Đới Trung Ương",
                province: "Hà Nội",
                detail: "78 Giải Phóng"
            }
            ,
            {
                name: "Bệnh Viện Răng Hàm Mặt Trung Ương Hà Nội",
                province: "Hà Nội",
                detail: "40 Tràng Thi"
            }
            ,
            {
                name: "Bệnh Viện Nội Tiết Trung Ương (Thái Thịnh)",
                province: "Hà Nội",
                detail: "80 Ngõ 82 Yên Lãng"
            }
            ,
            {
                name: "Bệnh Viện Đa Khoa Hà Nội",
                province: "Hà Nội",
                detail: "29 P.Hàn Thuyên"
            }
            ,
            {
                name: "Bệnh viện Phổi Trung ương",
                province: "Hà Nội",
                detail: "463 Hoàng Hoa Thám"
            }
        ]
    }
};
