var faker = require("faker");
faker.locale = 'vi';

module.exports = {

    generates(count){
        let majors = [];
        for (let i = 0; i < count; i++) {
            let major = module.exports.generate();
            majors.push(major);
        }
        return majors;
    },

    generate(){
        let name = faker.random.word();
        let description = faker.random.words();
        return {
            name,
            description,
            depth: 0
        }
    },

    generateMajors(){
        return [
            {
                name: "Da liễu",
                description: "Da liễu",
                depth: 0
            },
            {
                name: "Răng hàm mặt",
                description: "Răng hàm mặt",
                depth: 0
            },
            {
                name: "Nha khoa tổng quát",
                description: "Nha khoa tổng quát",
                depth: 0
            },
            {
                name: "Y học cổ truyền",
                description: "Y học cổ truyền",
                depth: 0
            },
            {
                name: "Phục hồi chức năng",
                description: "Phục hồi chức năng",
                depth: 0
            },
            {
                name: "Hoạt động trị liệu",
                description: "Hoạt động trị liệu",
                depth: 0
            },
            {
                name: "Thẩm mỹ",
                description: "Thẩm mỹ",
                depth: 0
            },
            {
                name: "Phẫu thuật thẩm mỹ",
                description: "Phẫu thuật thẩm mỹ",
                depth: 0
            },
            {
                name: "Ngoại Tiêu hoá",
                description: "Ngoại Tiêu hoá",
                depth: 0
            },
            {
                name: "Ngoại Tim mạch",
                description: "Ngoại Tim mạch",
                depth: 0
            },
            {
                name: "Nội Hô hấp",
                description: "Nội Hô hấp",
                depth: 0
            },
            {
                name: "Truyền nhiễm",
                description: "Truyền nhiễm",
                depth: 0
            },
            {
                name: "Phẫu Thuật chỉnh hình",
                description: "Phẫu Thuật chỉnh hình",
                depth: 0
            },
            {
                name: "Tai - Mũi - Họng",
                description: "Tai - Mũi - Họng",
                depth: 0
            }
        ]
    }
};
