var faker = require("faker");
faker.locale = 'vi';

module.exports = {
    async generate(count){
        let users = [];
        for (let i = 0 ; i < count; i++) {
            let user = module.exports.generateSingle();
            users.push(user);
        }
        return users;
    },

    generateSingle(){
        let name = faker.name.findName();
        let email = faker.internet.email();
        let address = faker.address.streetAddress();
        let phone = faker.phone.phoneNumber();
        let sex = Math.floor(Math.random() * 2) === 0 ? "male" : "female";
        let age = 20;
        let credit = 500000;
        let activated = true;
        let type = Math.floor(Math.random() * 2) + 1;
        return {
            name, email, address, phone, sex, age, credit, activated, type
        }
    },

    generateDoctor(){
        return [
            {
                name: "Nguyễn Trí Đoàn",
                avatar: "https://d9iixa2xxa0x2.cloudfront.net/file/i/e0b8dfd9-a409-434a-beef-edaa7accb800.jpg?urc=c5ef4481-c3ff-49a6-84b5-f52630fc8fa3",
                email: "tridoan@gmail.com",
                phone: "0913456789",
                address: "Quận 1, Thành phố Hồ Chí Minh",
                sex: "male",
                type: 2
            },
            {
                name: "Nguyễn Công Viên",
                avatar: "https://mhealth-beta.s3.amazonaws.com/media/images/5b48842e7d5325b35ca72166/emr/885a82a7-8c4b-52f8-b8bb-8928906fd83e.png",
                email: "congvien@gmail.com",
                phone: "0914456789",
                address: "Quận 2, Thành phố Hồ Chí Minh",
                sex: "male",
                type: 2
            },
            {
                name: "Trần Thị Mỹ Duyên",
                avatar: "https://d9iixa2xxa0x2.cloudfront.net/file/i/dcca8ec2-606e-4a2d-b843-0fcc23d8282e.jpg?urc=9201289d-7f6a-40d1-9669-535d2e77afb0",
                email: "myduyen@gmail.com",
                phone: "0945678999",
                address: "Quận 1, Thành phố Hồ Chí Minh",
                sex: "female",
                type: 2,
            },
            {
                name: "Phạm Thị Xuân Linh",
                avatar: "https://d9iixa2xxa0x2.cloudfront.net/file/i/325464ec-0c40-41eb-973f-4ab6b98e210b.jpg?urc=1ac94a24-5195-4600-b26a-f41bf55bf4cd",
                email: "xuanlinh@gmail.com",
                phone: "0945678945",
                address: "Quận 1, Thành phố Hồ Chí Minh",
                sex: "male",
                type: 2,
            },
            {
                name: "Trần Thị Mai Linh",
                avatar: "https://d9iixa2xxa0x2.cloudfront.net/file/i/5285bad4-3c6c-468d-9664-1f343ebfe2a9.jpg?urc=deda8b59-39fe-4cbc-b82e-43d00adf4ca1",
                email: "mailinh@gmail.com",
                phone: "0945678546",
                address: "Quận 1, Thành phố Hồ Chí Minh",
                sex: "female",
                type: 2,
            },
            {
                name: "Trương Hữu Khanh",
                avatar: "https://mhealth-beta.s3.amazonaws.com/cms/4o57dnX9Szgc5AfO.jpg",
                email: "huukhanh@gmail.com",
                phone: "0944568999",
                address: "Quận 1, Thành phố Hồ Chí Minh",
                sex: "male",
                type: 2,
            },
            {
                name: "Phan Quốc Bảo",
                avatar: "https://mhealth-beta.s3.amazonaws.com/media/images/59faca82bf7de45ea0d743d5/temp/68d26a97-b09e-514c-a9c0-5370dd5adbee.jpeg",
                email: "quocbao@gmail.com",
                phone: "0945633399",
                address: "Quận 1, Thành phố Hồ Chí Minh",
                sex: "male",
                type: 2,
            },
            {
                name: "Trần Quang Trung",
                avatar: "https://d9iixa2xxa0x2.cloudfront.net/images/100x100/51ce0d07-5bb5-5de2-9220-db3139bcb164.png",
                email: "quangtrung@gmail.com",
                phone: "0945536999",
                address: "Quận 1, Thành phố Hồ Chí Minh",
                sex: "male",
                type: 2,
            },
            {
                name: "Trình Ngô Bỉnh",
                avatar: "https://mhealth-beta.s3.amazonaws.com/cms/f1n4A2GjKHN_6MJB.png",
                email: "ngobinh@gmail.com",
                phone: "0945678111",
                address: "Quận 1, Thành phố Hồ Chí Minh",
                sex: "male",
                type: 2,
            },
            {
                name: "Nguyễn Vĩnh Tường",
                avatar: "https://d9iixa2xxa0x2.cloudfront.net/file/i/158f6ea3-181a-47a8-a3e3-95f3a11a0e46.jpg?urc=9cb5e777-5642-49b0-beff-c1c14e82ef0a",
                email: "vinhtuong@gmail.com",
                phone: "0945327999",
                address: "Quận 1, Thành phố Hồ Chí Minh",
                sex: "male",
                type: 2,
            },
            {
                name: "Huỳnh Thanh Tân",
                avatar: "https://d9iixa2xxa0x2.cloudfront.net/file/i/57e849eb-83de-48c6-b8cf-f63ac2bc967a.jpg?urc=560b9e79-a8c3-41d1-a439-c5e089517427",
                email: "thanhtam@gmail.com",
                phone: "0111678999",
                address: "Quận 1, Thành phố Hồ Chí Minh",
                sex: "female",
                type: 2,
            },
            {
                name: "Nguyễn Cảnh Nam",
                avatar: "https://mhealth-beta.s3.amazonaws.com/media/images/5bd95e015ee1b9725fe5e761/temp/bf700429-8c8d-5ebe-9e91-3110e4cf2863.jpeg",
                email: "canhnam@gmail.com",
                phone: "0945673257",
                address: "Quận 1, Thành phố Hồ Chí Minh",
                sex: "male",
                type: 2,
            },
            {
                name: "Nguyễn Lê Trà Mi",
                avatar: "https://wellcare-wordpress.s3-ap-southeast-1.amazonaws.com/uploads/2016/05/NguyenCanhNam2.jpg",
                email: "trami@gmail.com",
                phone: "0945678214",
                address: "Quận 1, Thành phố Hồ Chí Minh",
                sex: "male",
                type: 2,
            },
            {
                name: "Trần Thị Hồng An",
                avatar: "https://d9iixa2xxa0x2.cloudfront.net/file/i/7b1fc683-f01f-48b5-9ab5-a4d64db4a5e9.jpg?urc=2feab5e2-7d92-4340-82fa-9304ebde0cfc",
                email: "hongan@gmail.com",
                phone: "0945678059",
                address: "Quận 1, Thành phố Hồ Chí Minh",
                sex: "female",
                type: 2,
            },
            {
                name: "Phan Nhật Kha",
                avatar: "https://d9iixa2xxa0x2.cloudfront.net/file/i/453b9a52-94d6-46d9-86ca-9fd2777f6ba9.png?urc=348ac9e8-df07-47d2-8b18-b68291d13a85",
                email: "nhankha@gmail.com",
                phone: "0945673168",
                address: "Quận 1, Thành phố Hồ Chí Minh",
                sex: "male",
                type: 2,
            },
            {
                name: "Trần Như Minh Hằng",
                avatar: "https://d9iixa2xxa0x2.cloudfront.net/file/i/fbb415d6-ec36-42e4-8172-540eae2d69b5.jpg?urc=89cb0bb8-57e7-470f-9c14-98767064dda8",
                email: "minhhang@gmail.com",
                phone: "09456793206",
                address: "Quận 1, Thành phố Hồ Chí Minh",
                sex: "female",
                type: 2,
            },{
                name: "Nguyễn Thụy Đoan Trang",
                avatar: "https://d9iixa2xxa0x2.cloudfront.net/file/i/453b9a52-94d6-46d9-86ca-9fd2777f6ba9.png?urc=348ac9e8-df07-47d2-8b18-b68291d13a85",
                email: "doangtrang@gmail.com",
                phone: "0945674568",
                address: "Quận 1, Thành phố Hồ Chí Minh",
                sex: "female",
                type: 2,
            },
            {
                name: "Đặng Khánh An",
                avatar: "https://d9iixa2xxa0x2.cloudfront.net/file/i/2014a4a0-5186-478a-bd87-4572b6ff4239.jpg?urc=3b23f254-7764-4fe5-9671-486f2cfa494c",
                email: "khanhan@gmail.com",
                phone: "0945672114",
                address: "Quận 1, Thành phố Hồ Chí Minh",
                sex: "male",
                type: 2,
            },
            {
                name: "Mai Phan Tường Anh",
                avatar: "https://d9iixa2xxa0x2.cloudfront.net/file/i/c679d020-c6ca-4587-9612-7cb3237c7c08.png?urc=f3b9bf57-cf30-48e2-816b-a50bd6d089bf",
                email: "tuonganh1@gmail.com",
                phone: "0945673412",
                address: "Quận 1, Thành phố Hồ Chí Minh",
                sex: "female",
                type: 2,
            },
        ];
    },

    generateUser(){
        return [
            {
                name: "HieuBui",
                avatar: "https://d9iixa2xxa0x2.cloudfront.net/file/i/c679d020-c6ca-4587-9612-7cb3237c7c08.png?urc=f3b9bf57-cf30-48e2-816b-a50bd6d089bf",
                email: "hieubui@gmail.com",
                phone: "0111111111",
                address: "Gia Lộc, Hải Dương",
                sex: "male",
                type: 1,
            },
            {
                name: "Admin",
                email: "admin@gmail.com",
                phone: "admin",
                address: "Gia Loc, Hai Duong",
                sex: "male",
                type: 3
            }
        ]
    }
};
