var faker = require("faker");
var db = require("./../models");
var {User, Hospital} = require('./../models');
var UserType = require("../config/user_type");
var DegreeDoctor = require("../config/degee_doctor");

module.exports = {
    async generates(){
        let users = await User.findAll({
            where: {type: UserType.DOCTOR}
        });

        let doctors = [];
        for (let user of users) {
            let doctor = await module.exports.generateSingle(user);
            doctors.push(doctor);
        }
        return doctors;
    },
    async generateSingle(user){
        if (user.type !== UserType.DOCTOR) throw new Error("User isn't doctor");
        let doctorId = user.id;
        let hospital = await Hospital.findOne({ order: db.Sequelize.fn( 'RANDOM' ) });
        let hospitalId = await hospital.id;
        let degrees = Object.keys(DegreeDoctor);
        let degree = DegreeDoctor[degrees[Math.floor(Math.random() * degrees.length)]];
        let certDate = faker.date.past();
        let averageRating = 5;
        let experience_year = Math.floor(Math.random() * 30);
        return {
            doctor_id: doctorId,
            hospital_id: hospitalId,
            degree,
            cert_date: certDate,
            average_rating: averageRating,
            experience_year,
            degree_text: "Phó giáo sư, Tiến sĩ, Bác sĩ cao cấp chuyên khoa Da liễu\n" +
                "\n" +
                "Trưởng bộ môn Da liễu trường học viện quân y 103\n" +
                "\n" +
                "Giảng viên bộ môn Da liễu trường học viện quân y 103\n" +
                "\n" +
                "Thường xuyên tham gia các hội thảo Da liễu, Hội nghị Quốc tế Da liễu.",
            intro_text: "Là chuyên gia đầu ngành về da liễu tại Việt Nam với 39 năm kinh nghiệm.",
            experience: "Quá trình công tác:\n" +
                "\n" +
                "Là chuyên gia đầu ngành về Da liễu tại Việt Nam, có hơn 39 năm kinh nghiệm trong việc nghiên cứu và điều trị các bệnh da và hoa liễu khác\n" +
                "\n" +
                "1. Trưởng Bộ môn Da liễu trường Học viện quân y 103\n" +
                "\n" +
                "2. Giảng viên Bộ môn Da liễu trường Học viện Quân y 103\n" +
                "\n" +
                "3. Thường xuyên tham gia các Hội thảo Da liễu, Hội nghị Quốc tế",
            certificate: "1. Tốt nghiệp Đại học Y Hà Nội (1977)\n" +
                "\n" +
                "2. Đạt chứng chỉ Diploma về Da liễu tại Viện da liễu Băng Cốc - Thái Lan",
        }
    }
};
