'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn("tbl_dsession_disease_test", "diagnostic", {
      type: Sequelize.STRING,
      allowNull: true
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn("tbl_dsession_disease_test" , "diagnostic");
  }
};
