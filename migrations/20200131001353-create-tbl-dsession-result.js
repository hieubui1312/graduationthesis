'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("tbl_dsession_result", {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      dsession_id: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      diagnose: {
        type: Sequelize.STRING,
        allowNull: false
      },
      medicine: {
        type: Sequelize.STRING,
        allowNull: false
      },
      diet: {
        type: Sequelize.STRING,
        allowNull: true
      },
      body_clear: {
        type: Sequelize.STRING,
        allowNull: true
      },
      other_note: {
        type: Sequelize.STRING,
        allowNull: true
      },
      buy_medicine: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false
      },
      status: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0
      },
      created_at: {
        type: 'TIMESTAMP',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
      },
      updated_at: {
        type: 'TIMESTAMP',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
      }
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("tbl_dsession_result");
  }
};
