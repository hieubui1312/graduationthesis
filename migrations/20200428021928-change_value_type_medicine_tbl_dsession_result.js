'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn("tbl_dsession_result", "medicine", {
      type: Sequelize.TEXT,
      allowNull: false
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn("tbl_dsession_result", "medicine")
  }
};
