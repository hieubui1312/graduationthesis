'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return Promise.all([
          queryInterface.addColumn("tbl_dsession", "last_message", {
            type: Sequelize.STRING,
            allowNull: true
          } ),
          queryInterface.addColumn("tbl_dsession", "last_message_time", {
            type: "TIMESTAMP",
            allowNull: true
          }, ),
          queryInterface.addColumn("tbl_dsession", "last_message_is_seen", {
            type: Sequelize.BOOLEAN,
            allowNull: true
          })
      ])
  },

  down: (queryInterface, Sequelize) => {
          return Promise.all([
              queryInterface.removeColumn("tbl_dsession", "last_message_time"),
              queryInterface.removeColumn("tbl_dsession", "last_message_is_seen"),
              queryInterface.removeColumn("tbl_dsession", "last_message")
          ])
  }
};
