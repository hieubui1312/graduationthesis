'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn("tbl_dsession_result", "date_test_again", {
      type: Sequelize.DATE,
      allowNull: true
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn("tbl_dsession_result", "date_test_again");
  }
};
