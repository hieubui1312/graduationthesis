'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
        queryInterface.addColumn("tbl_health_record", "name", {
          type: Sequelize.STRING,
          allowNull: true
        }),
        queryInterface.addColumn("tbl_health_record", "age", {
          type: Sequelize.INTEGER,
          allowNull: true
        }),
        queryInterface.addColumn("tbl_health_record", "sex", {
          type: Sequelize.STRING,
          allowNull: true
        }),
        queryInterface.addColumn("tbl_health_record", "address", {
          type: Sequelize.STRING,
          allowNull: true
        }),
        queryInterface.addColumn("tbl_health_record", "examination_date", {
          type: "TIMESTAMP",
          allowNull: true
        }),
        queryInterface.addColumn("tbl_health_record", "note", {
            type: Sequelize.STRING,
            allowNull: true
        })
        ],
    )
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([
        queryInterface.removeColumn("tbl_health_record", "name"),
        queryInterface.removeColumn("tbl_health_record", "age"),
        queryInterface.removeColumn("tbl_health_record", "sex"),
        queryInterface.removeColumn("tbl_health_record", "address"),
        queryInterface.removeColumn("tbl_health_record", "examination_date")
    ]);
  }
};
