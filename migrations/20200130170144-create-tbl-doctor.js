'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("tbl_doctor", {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      doctor_id: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      hospital_id: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      degree: {
        type: Sequelize.STRING,
        allowNull: false
      },
      cert_date: {
        type: 'TIMESTAMP',
        allowNull: true,
        defaultValue: null
      },
      atm_number: {
        type: Sequelize.STRING,
        allowNull: true
      },
      atm_info: {
        type: Sequelize.STRING,
        allowNull: true
      },
      average_rating: {
        type: Sequelize.FLOAT,
        allowNull: true
      },
      created_at: {
        type: 'TIMESTAMP',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
      },
      updated_at: {
        type: 'TIMESTAMP',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
      }
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("tbl_doctor");
  }
};
