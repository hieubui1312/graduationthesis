'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("tbl_health_record", {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      user_id: {
        type: Sequelize.UUID,
        allowNull: false
      },
      diagnose: {
        type: Sequelize.STRING,
        allowNull: false
      },
      medicine: {
        type: Sequelize.STRING,
        allowNull: true
      },
      paper_test: {
        type: Sequelize.STRING,
        allowNull: true
      },
      re_examination_date: {
        type: "TIMESTAMP",
        allowNull: true
      },
      created_at: {
        type: 'TIMESTAMP',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
      },
      updated_at: {
        type: 'TIMESTAMP',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
      }
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("tbl_health_record");
  }
};
