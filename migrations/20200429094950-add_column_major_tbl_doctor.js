'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn("tbl_doctor", "majors", {
      type: Sequelize.TEXT,
      allowNull: true
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn("tbl_doctor", "majors")
  }
};
