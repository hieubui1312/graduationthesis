'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
         queryInterface.changeColumn("tbl_doctor", "experience", {
           type: Sequelize.TEXT,
           allowNull: true
         }),
        queryInterface.changeColumn("tbl_doctor", "intro_text", {
          type: Sequelize.TEXT,
          allowNull: true
        }),
        queryInterface.changeColumn("tbl_doctor", "degree_text", {
          type: Sequelize.TEXT,
          allowNull: true
        }),
        queryInterface.changeColumn("tbl_doctor", "certificate", {
          type: Sequelize.TEXT,
          allowNull: true
        }),
        queryInterface.addColumn("tbl_doctor", "experience_year", {
          type: Sequelize.INTEGER,
          allowNull: true
        })
    ]);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn("tbl_doctor", "experience_year");
  }
};
