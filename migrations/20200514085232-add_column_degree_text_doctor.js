'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
       queryInterface.addColumn("tbl_doctor", "degree_text", {
         type: Sequelize.TEXT,
         allowNull: true
       }),
       queryInterface.addColumn("tbl_doctor", "intro_text", {
         type: Sequelize.TEXT,
         allowNull: true
       })
    ]);
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([
        queryInterface.removeColumn("tbl_doctor", "degree_text"),
      queryInterface.removeColumn("tbl_doctor", "intro_text")
    ])
  }
};
