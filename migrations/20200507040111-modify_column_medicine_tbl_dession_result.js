'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn("tbl_health_record", "medicine", {
      type: Sequelize.TEXT
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn("tbl_health_record", "medicine");
  }
};
