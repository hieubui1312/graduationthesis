'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
        queryInterface.removeColumn("tbl_health_record", "user_id"),
        queryInterface.addColumn("tbl_health_record", "user_id", {
          type: Sequelize.INTEGER
        })
    ])
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.removeColumn("tbl_health_record", "user_id");
  }
};
