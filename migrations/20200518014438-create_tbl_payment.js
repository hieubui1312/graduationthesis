'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("tbl_payment", {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      user_id: {
        type: Sequelize.INTEGER,
      },
      amount: {
        type: Sequelize.INTEGER
      },
      created_at: {
        type: "TIMESTAMP"
      },
      updated_at: {
        type: "TIMESTAMP"
      }
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("tbl_payment");
  }
};
