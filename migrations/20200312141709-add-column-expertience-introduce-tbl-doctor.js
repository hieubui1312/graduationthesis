'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
        queryInterface.addColumn("tbl_doctor", "experience", {
          type: Sequelize.STRING,
          allowNull: true
        }),
        queryInterface.addColumn("tbl_doctor", "certificate", {
          type: Sequelize.STRING,
          allowNull: true
        })
    ]);
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([
        queryInterface.removeColumn("tbl_doctor", "experience"),
        queryInterface.removeColumn("tbl_doctor", "certificate")
    ]);
  }
};
