const {body} = require("express-validator");
var validateInput = require("../middleware/validateInput");

module.exports = [
    body("id").exists().isInt().withMessage("DSession Id must be integer and not null"),
    validateInput
];
