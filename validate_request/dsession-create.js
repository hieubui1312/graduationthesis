const { body} = require('express-validator');
var validateInput = require("../middleware/validateInput");

module.exports = [
    body('doctor_id').exists().isInt().withMessage("Doctor id must be integer"),
    body('type_id').exists().isInt().withMessage("Type id is must be integer"),
    validateInput
];
