const {body} = require("express-validator");
var validateInput = require("../middleware/validateInput");

module.exports = [
    body("name").exists().withMessage("Name not empty"),
    body("email").exists().isEmail().withMessage("Email must be correct format"),
    body("address").exists().withMessage("Address not empty"),
    body("sex").exists().isIn(["male", "female"]).withMessage("Sex must be male or female"),
    body("age").exists().isInt().withMessage("Age must be integer"),
    validateInput
];
