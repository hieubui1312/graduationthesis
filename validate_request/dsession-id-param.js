const {param, body} = require("express-validator");
var validateInput = require("../middleware/validateInput");

module.exports = [
    param("id").exists().isInt().withMessage("DSession id must be not null and integer"),
    validateInput
];
