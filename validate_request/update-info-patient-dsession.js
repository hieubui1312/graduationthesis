const {body} = require("express-validator");
var validateInput = require("../middleware/validateInput");
module.exports = [
  body('dsession_id').exists().isInt().withMessage("Dsession id must be integer"),
  body("name").exists().withMessage("Name must be string not null"),
  body("age").exists().isInt().withMessage("Age must be integer"),
  body("address").exists().withMessage("Address must be not null"),
  body("sex").exists().isIn(["male", "female"]).withMessage("Sex must be male or female"),
  body("description").exists().withMessage("Description must be not null"),
  body("time_begin").exists().withMessage("Time begin must be not null"),
  validateInput
];
