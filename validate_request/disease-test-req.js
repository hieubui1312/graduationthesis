const {body} = require("express-validator");
var validateInput = require("../middleware/validateInput");

module.exports = [
    body("dsession_id").exists().isInt().withMessage("DSession id must be not empty and integer"),
    body("disease_test").exists().withMessage("Disease test must be not empty"),
    validateInput
];
