const {body} = require("express-validator");
var validateInput = require("../middleware/validateInput");

module.exports = [
    body('password').exists().custom((value, {req}) => {
        if (value.length !== 6) throw new Error("Length password is six");
        return true;
    }).withMessage("Password not null and length is six"),
    validateInput
];
