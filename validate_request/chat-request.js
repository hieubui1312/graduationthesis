const {body} = require("express-validator");
var validateInput = require("../middleware/validateInput");

module.exports = [
    body("dsession_id").exists().isInt().withMessage("DSession id must be not null and integer"),
    body("message").exists().withMessage("Message must be not null"),
    validateInput
];
