const {body} = require("express-validator");
let validateInput = require("../middleware/validateInput");

module.exports = [
    body("avatar").exists(),
    body("name").exists(),
    body("phone").exists(),
    body("sex").exists(),
    body("email").exists().isEmail(),
    validateInput
];
