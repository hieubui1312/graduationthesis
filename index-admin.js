const express = require('express');
const path = require('path');
const app = express();
console.log("Dir name", path.join(__dirname, 'react-build/admin'));
app.use(express.static(path.join(__dirname, 'react-build/admin')));
app.get('*', function(req, res) {
    res.sendFile(path.join(__dirname, 'react-build/admin', 'index.html'));
});
app.listen(9002);
