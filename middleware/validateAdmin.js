var Utility = require("../app_modules/utility");
const ErrorCode = require("../app_modules/error_code");
var ValidateUserByToken = require("../app_modules/validate-user/validate");
const UserType = require("../config/user_type");

module.exports = function (req, res, next) {
  try{
      let access_token = req.get('Access-Token');
      let user = ValidateUserByToken(access_token, UserType.ADMIN);
      req.currentUser = user;

      let accessToken = Utility.encodeJWT(user);
      res.setHeader("Access-Token", accessToken);

      next();
  } catch (e) {
      Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
  }
};
