var Utility = require("../app_modules/utility");
var ErrorCode = require("../app_modules/error_code");
const {validationResult} = require('express-validator');

module.exports = function (req, res, next) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return Utility.sendErrorResponse(
            res,
            JSON.stringify(errors.array()),
            500,
            ErrorCode.PARAM_NOT_CORRECT,
            true
        );
    }
    next();
};
