var Utility = require("../app_modules/utility");
var ErrorCode = require("../app_modules/error_code");
var UserType = require("../app_modules/usertype");
var UserRepository = require("../repositories/user_repository");

module.exports = async function (req, res, next) {
  try {
    let token = req.get('Access-Token');
    if (!token)
      return Utility.sendErrorResponse(res, "You can't not permission to access system", 401, ErrorCode.INTERNAL_SERVER_ERROR);
    let {data} = Utility.verifyToken(token);

    data = JSON.parse(data);
    let user = await UserRepository.findByID(data.id, UserType.PATIENT);

    if (!user.activated)
      return Utility.sendErrorResponse(res, "User is not activated", 401, ErrorCode.ACCOUNT_NOT_ACTIVE);

    req.currentUser = user;
    let resetToken = Utility.encodeJWT(user);
    res.setHeader("Access-Token", resetToken);

    next();

  } catch (e) {
    return Utility.sendErrorResponse(res, e, 401, ErrorCode.INTERNAL_SERVER_ERROR);
  }
};
