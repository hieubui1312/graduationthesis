var Utility = require("../app_modules/utility");
const ErrorCode = require("../app_modules/error_code");
var ValidateUserByToken = require("../app_modules/validate-user/validate");
const UserType = require("../app_modules/usertype");

module.exports = async function (req, res, next) {
    try{
        let token = req.get("Access-Token");
        let user = await ValidateUserByToken(token, UserType.DOCTOR)();

        let accessToken = Utility.encodeJWT(user);
        user.dataValues.access_token = accessToken;

        req.currentUser = user;

        res.setHeader("Access-Token", accessToken);
        next();
    } catch (e) {
        return Utility.sendErrorResponse(res, e, 500, ErrorCode.INTERNAL_SERVER_ERROR);
    }
};
