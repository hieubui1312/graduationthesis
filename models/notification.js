const {Model} = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  class Notification extends Model {}

  Notification.init({
      message: {
          type: DataTypes.STRING
      },
      user_id: {
          type: DataTypes.UUID
      },
      title: {
          type: DataTypes.STRING
      },
      link: {
          type: DataTypes.STRING
      },
      type: {
          type: DataTypes.INTEGER
      },
      status: {
          type: DataTypes.BOOLEAN
      },
      reference_id: {
          type: DataTypes.INTEGER,
          allowNull: true
      },
      is_seen: {
          type: DataTypes.BOOLEAN,
          defaultValue: false
      },
      created_at: {
          type: "TIMESTAMP"
      },
      updated_at: {
          type: "TIMESTAMP"
      }
  }, {
      sequelize,
      timestamps: true,
      createdAt: "created_at",
      updatedAt: "updated_at",
      tableName: "tbl_notification",
      modelName: "Notification"
  });

  Notification.associate = (models) => {
      Notification.belongsTo(models.User, {foreignKey: "user_id"})
  };
  return Notification;
};
