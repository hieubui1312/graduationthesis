const {Model} = require("sequelize");

module.exports = (sequelize, DataTypes) => {
    class Doctor extends Model {

    }

    Doctor.init({
        doctor_id: {
            type: DataTypes.UUID,
            allowNull: false
        },
        hospital_id: {
            type: DataTypes.UUID,
            allowNull: false
        },
        degree: {
            type: DataTypes.STRING,
        },
        cert_date: {
            type: "TIMESTAMP"
        },
        atm_number: {
            type: DataTypes.STRING
        },
        atm_info: {
            type: DataTypes.STRING
        },
        created_at: {
            type: "TIMESTAMP"
        },
        average_rating: {
            type: DataTypes.FLOAT
        },
        updated_at: {
            type: "TIMESTAMP"
        },
        experience: {
            type: DataTypes.TEXT,
            allowNull: true
        },
        certificate: {
            type: DataTypes.TEXT,
            allowNull: true
        },
        majors: {
            type: DataTypes.TEXT,
            allowNull: true
        },
        atm_own_name: {
            type: DataTypes.STRING,
            allowNull: true
        },
        degree_text: {
            type: DataTypes.TEXT,
            allowNull: true
        },
        intro_text: {
            type: DataTypes.TEXT,
            allowNull: true
        },
        experience_year: {
            type: DataTypes.INTEGER,
            allowNull: true
        }
    }, {
        sequelize,
        modelName: "Doctor",
        tableName: "tbl_doctor",
        createdAt: "created_at",
        updatedAt: "updated_at",
        timestamps: true
    });
    Doctor.associate = (models) => {
      Doctor.belongsTo(models.User, {foreignKey: "doctor_id"});
      Doctor.hasMany(models.DoctorMajorMap, {foreignKey: "doctor_id"});
      Doctor.belongsTo(models.Hospital, {foreignKey: "hospital_id"});
      Doctor.hasMany(models.Dsession, {foreignKey: "doctor_id"})
    };
    return Doctor;
};
