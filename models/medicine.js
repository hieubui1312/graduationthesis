const {Model} = require("sequelize");

module.exports = (sequelize, DataTypes) => {
    class Medicine extends  Model {}
    Medicine.init({
        name: {
            type: DataTypes.STRING
        },
        description: {
            type: DataTypes.STRING
        },
        guide: {
            type: DataTypes.STRING
        },
        ingredient: {
            type: DataTypes.STRING
        },
        people_unallowed: {
            type: DataTypes.STRING
        },
        created_at: {
            type: "TIMESTAMP"
        },
        updated_at: {
            type: "TIMESTAMP"
        }
    }, {
        sequelize,
        timestamps: true,
        createdAt: "created_at",
        updatedAt: "updated_at",
        tableName: "tbl_medicine",
        modelName: "Medicine"
    });

    return Medicine;
};
