const {Model} = require("sequelize");
module.exports = (sequelize, DataTypes) => {
    class User extends Model {}
    User.init({
        name: {
            type: DataTypes.STRING
        },
        password: {
            type: DataTypes.STRING
        },
        email: {
            type: DataTypes.STRING
        },
        address: {
            type: DataTypes.STRING
        },
        phone: {
            type: DataTypes.STRING
        },
        sex: {
            type: DataTypes.STRING
        },
        age: {
            type: DataTypes.INTEGER
        },
        avatar: {
            type: DataTypes.STRING
        },
        credit: {
            type: DataTypes.INTEGER
        },
        credit_hold: {
            type: DataTypes.INTEGER
        },
        activated: {
            type: DataTypes.BOOLEAN
        },
        type: {
            type: DataTypes.INTEGER
        },
        created_at: {
            type: "TIMESTAMP"
        },
        updated_at: {
            type: "TIMESTAMP"
        }
    }, {
        sequelize,
        modelName: "User",
        tableName: "tbl_user",
        createdAt: "created_at",
        updatedAt: "updated_at",
        timestamps: true,
    });

    User.associate = function (models) {
        User.hasMany(models.UserDevice, {foreignKey: "user_id"});
        User.hasOne(models.Doctor, {foreignKey: "doctor_id"});
        User.hasMany(models.Withdraw, {foreignKey: "user_id"});
        User.hasMany(models.HealthRecord, {foreignKey: "user_id"});
        User.hasMany(models.CreditLog, {foreignKey: "user_id"});
        User.hasMany(models.Payment, {foreignKey: "user_id"});
    };
    return User;
};
