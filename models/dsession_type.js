const {Model} = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  class DsessionType extends Model{}

  DsessionType.init({
      name: {
          type: DataTypes.STRING
      },
      time: {
          type: DataTypes.INTEGER
      },
      description: {
          type: DataTypes.STRING
      },
      created_at: {
          type: "TIMESTAMP"
      },
      updated_at: {
          type: "TIMESTAMP"
      }
  }, {
      sequelize,
      tableName: "tbl_dsession_type",
      createdAt: "created_at",
      updatedAt: "updated_at",
      modelName: "DsessionType"
  });
  DsessionType.associate = (models) => {
    DsessionType.hasMany(models.Dsession, {foreignKey: "type_id"});
    DsessionType.hasMany(models.DsessionFee, {foreignKey: "dsession_type"});
  };
  return DsessionType;
};
