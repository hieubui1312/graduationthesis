const {Model} = require("sequelize");

module.exports = (sequelize, DataTypes) => {
    class DsessionMessage extends Model {}
    DsessionMessage.init({
        user_id: {
            type: DataTypes.UUID
        },
        dsession_id: {
            type: DataTypes.UUID
        },
        message: {
            type: DataTypes.STRING
        },
        type: {
            type: DataTypes.INTEGER
        },
        obj_url: {
            type: DataTypes.STRING,
            allowNull: true
        },
        created_at: {
            type: "TIMESTAMP"
        },
        updated_at: {
            type: "TIMESTAMP"
        },
        is_notify: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        }
    }, {
        sequelize,
        timestamps: true,
        createdAt: "created_at",
        updatedAt: "updated_at",
        tableName: "tbl_dsession_message",
        modelName: "DsessionMessage"
    });
    DsessionMessage.associate = (models) => {
      DsessionMessage.belongsTo(models.Dsession, {foreignKey: "dsession_id"});
      DsessionMessage.belongsTo(models.User, {foreignKey: "user_id"});
    };
    return DsessionMessage;
};
