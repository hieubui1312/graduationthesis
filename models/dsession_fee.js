const {Model} = require("sequelize");

module.exports = (sequelize, DataTypes) => {
    class DsessionFee extends Model{}

    DsessionFee.init({
        dsession_type: {
            type: DataTypes.UUID
        },
        doctor_level: {
            type: DataTypes.INTEGER
        },
        fee: {
            type: DataTypes.INTEGER
        },
        created_at: {
            type: "TIMESTAMP"
        },
        updated_at: {
            type: "TIMESTAMP"
        }
    }, {
        sequelize,
        tableName: "tbl_dsession_fee",
        createdAt: "created_at",
        updatedAt: "updated_at",
        modelName: "DsessionFee"
    });
    DsessionFee.associate = function (models) {
      DsessionFee.belongsTo(models.DsessionType, {foreignKey: "dsession_type"})
    };
    return DsessionFee;
};
