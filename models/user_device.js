const {Model} = require("sequelize");

module.exports = (sequelize, DataTypes) => {
    class UserDevice extends Model{
    }

    UserDevice.init({
        user_id: {
            type: DataTypes.UUID,
            allowNull: false,
            foreignKey: true,
            references: {
                model: "User",
                key: "id"
            }
        },
        token: {
            type: DataTypes.STRING
        },
        is_failed: {
            type: DataTypes.BOOLEAN
        },
        created_at: {
            type: "TIMESTAMP"
        },
        updated_at: {
            type: "TIMESTAMP"
        }
    }, {
        sequelize,
        modelName: "UserDevice",
        tableName: "tbl_user_device",
        createdAt: "created_at",
        updatedAt: "updated_at",
        timestamps: true
    });

    UserDevice.associate = (models) => {
        UserDevice.belongsTo(models.User, {foreignKey: "user_id", targetKey: "id"})
    };
    return UserDevice
};
