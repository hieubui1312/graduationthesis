const {Model} = require("sequelize");

module.exports = (sequelize, DataTypes) => {
    class DSessionDiseaseTest extends Model{}

    DSessionDiseaseTest.init({
        dsession_id: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        disease_test: {
            type: DataTypes.STRING,
            allowNull: false
        },
        diagnostic: {
            type: DataTypes.STRING,
            allowNull: true
        },
        created_at: {
            type: "TIMESTAMP"
        },
        updated_at: {
            type: "TIMESTAMP"
        }
    }, {
        sequelize,
        timestamps: true,
        tableName: "tbl_dsession_disease_test",
        modelName: "DSessionDiseaseTest",
        createdAt: "created_at",
        updatedAt: "updated_at"
    });
    return DSessionDiseaseTest;
};
