const {Model} = require("sequelize");

module.exports = (sequelize, DataTypes) => {
    class DoctorMajorMap extends Model{}
    DoctorMajorMap.init({
        doctor_id: {
            type: DataTypes.UUID,
            allowNull: false
        },
        major_id: {
            type: DataTypes.UUID,
            allowNull: false
        },
        created_at: {
            type: "TIMESTAMP"
        },
        updated_at: {
            type: "TIMESTAMP"
        }
    }, {
        sequelize,
        timestamps: true,
        tableName: "tbl_doctor_major_map",
        modelName: "DoctorMajorMap",
        createdAt: "created_at",
        updatedAt: "updated_at"
    });
    DoctorMajorMap.associate = (models) => {
      DoctorMajorMap.belongsTo(models.Doctor, {foreignKey: "doctor_id"});
      DoctorMajorMap.belongsTo(models.Major, {foreignKey: "major_id"});
    };
    return DoctorMajorMap;
};
