const {Model} = require("sequelize");

module.exports = (sequelize, DataTypes) => {
    class DiseaseTest extends Model{}
    DiseaseTest.init({
        name: {
            type: DataTypes.STRING
        },
        parent_id: {
            type: DataTypes.INTEGER
        },
        description: {
            type: DataTypes.STRING
        },
        created_at: {
            type: "TIMESTAMP"
        },
        updated_at: {
            type: "TIMESTAMP"
        }
    }, {
        sequelize,
        timestamps: true,
        createdAt: "created_at",
        updatedAt: "updated_at",
        tableName: "tbl_disease_test",
        modelName: "DiseaseTest"
    });

    return DiseaseTest;
};
