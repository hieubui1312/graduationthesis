const {Model} = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  class HealthRecord extends Model{}

  HealthRecord.init({
      diagnose: {
          type: DataTypes.STRING
      },
      user_id: {
          type: DataTypes.INTEGER
      },
      medicine: {
          type: DataTypes.TEXT
      },
      paper_test: {
          type: DataTypes.STRING
      },
      re_examination_date: {
          type: "TIMESTAMP"
      },
      name: {
          type: DataTypes.STRING,
          allowNull: true
      },
      age: {
          type: DataTypes.STRING,
          allowNull: true
      },
      address: {
          type: DataTypes.STRING,
          allowNull: true
      },
      sex: {
          type: DataTypes.STRING,
          allowNull: true
      },
      examination_date: {
          type: "TIMESTAMP",
          allowNull: true
      },
      note: {
          type: DataTypes.STRING,
          allowNull: true
      },
      is_remind: {
          type: DataTypes.BOOLEAN,
          defaultValue: false
      },
      created_at: {
          type: "TIMESTAMP"
      },
      updated_at: {
          type: "TIMESTAMP"
      }
  }, {
      sequelize,
      timestamps: true,
      createdAt: "created_at",
      updatedAt: "updated_at",
      tableName: "tbl_health_record",
      modelName: "HealthRecord"
  });
  HealthRecord.associate = (models) => {
      HealthRecord.belongsTo(models.User, {foreignKey: "user_id"})
  };
  return HealthRecord;
};
