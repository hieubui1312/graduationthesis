const {Model} = require("sequelize");

module.exports = (sequelize, DataTypes) => {
    class DsessionResultMedicine extends Model{}

    DsessionResultMedicine.init({
        medicine_id: {
            type: DataTypes.INTEGER
        },
        count: {
            type: DataTypes.INTEGER
        },
        unit: {
            type: DataTypes.STRING
        },
        usage: {
            type: DataTypes.STRING
        },
        note: {
            type: DataTypes.STRING,
            allowNull: false
        },
        created_at:{
            type: "TIMESTAMP"
        },
        updated_at: {
            type: "TIMESTAMP"
        }
    }, {
        sequelize,
        modelName: "DsessionResultMedicine",
        tableName: "tbl_dsession_result_medicine",
        timestamps: true,
        createdAt: "created_at",
        updatedAt: "updated_at"
    });

    return DsessionResultMedicine;
};
