const {Model} = require("sequelize");

module.exports = (sequelize, DataTypes) => {
    class DsessionResult extends Model {}

    DsessionResult.init({
        dsession_id: {
            type: DataTypes.UUID
        },
        diagnose: {
            type: DataTypes.STRING
        },
        medicine: {
            type: DataTypes.STRING
        },
        diet: {
            type: DataTypes.STRING
        },
        body_clear: {
            type: DataTypes.STRING
        },
        other_note: {
            type: DataTypes.STRING
        },
        buy_medicine: {
            type: DataTypes.BOOLEAN
        },
        status: {
            type: DataTypes.INTEGER
        },
        date_test_again: {
            type: "TIMESTAMP",
            allowNull: true
        },
        created_at:{
            type: "TIMESTAMP"
        },
        updated_at: {
            type: "TIMESTAMP"
        }
    }, {
        sequelize,
        modelName: "DsessionResult",
        tableName: "tbl_dsession_result",
        timestamps: true,
        createdAt: "created_at",
        updatedAt: "updated_at"
    });
    DsessionResult.associate = (models) => {
        DsessionResult.belongsTo(models.Dsession, {foreignKey: "dsession_id"});
    };
    return DsessionResult;
};
