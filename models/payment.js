const {Model} = require("sequelize");

module.exports = (sequelize, DataTypes) => {
    class Payment extends Model{

    }

    Payment.init({
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        user_id: {
            type: DataTypes.INTEGER
        },
        amount: {
            type: DataTypes.INTEGER
        },
        created_at: {
            type: "TIMESTAMP"
        },
        updated_at: {
            type: "TIMESTAMP"
        }
    }, {
        sequelize,
        tableName: "tbl_payment",
        createdAt: "created_at",
        updatedAt: "updated_at",
        modelName: "Payment",
        timestamps: true
    });

    Payment.associate = (model) => {
        Payment.belongsTo(model.User, {foreignKey: "user_id"})
    };

    return Payment;
};
