const {Model} = require("sequelize");

module.exports = (sequelize, DataTypes) => {
    class Major extends Model {}
    Major.init({
        name: {
            type: DataTypes.STRING
        },
        description: {
            type: DataTypes.STRING
        },
        parent_id: {
            type: DataTypes.INTEGER,
            allowNull: true,
            defaultValue: null
        },
        depth: {
            type: DataTypes.INTEGER
        },
        created_at: {
            type: "TIMESTAMP"
        },
        updated_at: {
            type: "TIMESTAMP"
        }
    }, {
       sequelize,
       timestamps: true,
       tableName: "tbl_major",
       modelName: "Major",
       createdAt: "created_at",
       updatedAt: "updated_at"
    });
    Major.associate = (models) => {
        Major.hasMany(models.Major, {foreignKey: "parent_id"});
        Major.belongsTo(models.Major, {foreignKey: "parent_id"});
        Major.hasMany(models.DoctorMajorMap, {foreignKey: "major_id"});
    };
    return Major;
};
