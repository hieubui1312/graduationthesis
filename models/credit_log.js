const {Model} = require("sequelize");

module.exports = (sequelize, DataTypes) => {
    class CreditLog extends Model{}
    CreditLog.init({
        user_id: {
            type: DataTypes.UUID
        },
        credit_from: {
            type: DataTypes.INTEGER
        },
        credit_to: {
            type: DataTypes.INTEGER
        },
        type: {
            type: DataTypes.INTEGER
        },
        reason: {
            type: DataTypes.STRING
        },
        created_at: {
            type: "TIMESTAMP"
        },
        updated_at: {
            type: "TIMESTAMP"
        }
    }, {
        sequelize,
        timestamps: true,
        createdAt: "created_at",
        updatedAt: "updated_at",
        tableName: "tbl_credit_log",
        modelName: "CreditLog"
    });
    CreditLog.associate = (models) => {
      CreditLog.belongsTo(models.User, {foreignKey: "user_id"});
    };
    return CreditLog;
};
