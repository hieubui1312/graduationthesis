const {Model} = require("sequelize");

module.exports = (sequelize, DataTypes) => {
    class Withdraw extends Model{}

    Withdraw.init({
        user_id: {
            type: DataTypes.UUID,
            allowNull: false,
            foreignKey: true,
            references: {
                model: "User",
                key: "id"
            }
        },
        amount: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        status: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        created_at: {
            type: "TIMESTAMP"
        },
        updated_at: {
            type: "TIMESTAMP"
        }
    }, {
        sequelize,
        modelName: "Withdraw",
        tableName: "tbl_withdraw",
        createdAt: "created_at",
        updatedAt: "updated_at",
        timestamps: true
    });

    Withdraw.associate = (model) => {
        Withdraw.belongsTo(model.User, {foreignKey: "user_id", targetKey: "id"})
    };

    return Withdraw;
};
