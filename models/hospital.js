const {Model} = require("sequelize");

module.exports = (sequelize, DataTypes) => {
    class Hospital extends Model {}
    Hospital.init({
        name: {
            type: DataTypes.STRING
        },
        province: {
            type: DataTypes.STRING
        },
        detail: {
            type: DataTypes.STRING
        },
        created_at: {
            type: "TIMESTAMP"
        },
        updated_at: {
            type: "TIMESTAMP"
        }
    }, {
        sequelize,
        timestamps: true,
        createdAt: "created_at",
        updatedAt: "updated_at",
        tableName: "tbl_hospital",
        modelName: "Hospital"
    });

    Hospital.associate = (models) => {
        Hospital.hasMany(models.Doctor, {foreignKey: "hospital_id"})
    };

    return Hospital;
};
