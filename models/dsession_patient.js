const {Model} = require("sequelize");

module.exports = (sequelize, DataTypes) => {
    class DsessionPatient extends Model{}
    DsessionPatient.init({
        dsession_id: {
            type: DataTypes.UUID
        },
        name: {
            type: DataTypes.STRING
        },
        age: {
            type: DataTypes.INTEGER
        },
        sex: {
            type: DataTypes.STRING
        },
        address: {
            type: DataTypes.STRING
        },
        description: {
            type: DataTypes.STRING
        },
        time_begin: {
            type: DataTypes.STRING
        },
        created_at: {
            type: "TIMESTAMP"
        },
        updated_at: {
            type: "TIMESTAMP"
        }
    }, {
        sequelize,
        timestamps: true,
        createdAt: "created_at",
        updatedAt: "updated_at",
        tableName: "tbl_dsession_patient",
        modelName: "DsessionPatient"
    });
    DsessionPatient.associate = (models) => {
      DsessionPatient.belongsTo(models.Dsession, {foreignKey: "dsession_id"});
    };
    return DsessionPatient;
};
