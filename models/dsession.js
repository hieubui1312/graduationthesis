const {Model} = require("sequelize");

module.exports = (sequelize, DataTypes) => {
    class Dsession extends Model {}

    Dsession.init({
        doctor_id: {
            type: DataTypes.UUID
        },
        patient_id: {
            type: DataTypes.UUID
        },
        paper_test: {
            type: DataTypes.STRING,
            allowNull: true
        },
        rating: {
            type: DataTypes.FLOAT
        },
        status: {
            type: DataTypes.INTEGER
        },
        type_id: {
            type: DataTypes.UUID
        },
        end_time: {
            type: DataTypes.INTEGER
        },
        price: {
            type: DataTypes.INTEGER
        },
        created_at: {
            type: "TIMESTAMP"
        },
        updated_at: {
            type: "TIMESTAMP"
        },
        last_message: {
            type: DataTypes.STRING,
            allowNull: true
        },
        last_message_time: {
            type: "TIMESTAMP",
            allowNull: true
        },
        last_message_is_seen: {
            type: DataTypes.BOOLEAN,
            allowNull: true
        },
        last_message_user: {
            type: DataTypes.INTEGER,
            allowNull: false
        }
    }, {
        sequelize,
        timestamps: true,
        tableName: "tbl_dsession",
        modelName: "Dsession",
        createdAt: "created_at",
        updatedAt: "updated_at"
    });
    Dsession.associate = (models) => {
      Dsession.belongsTo(models.Doctor, {foreignKey: "doctor_id", targetKey: "doctor_id"});
      Dsession.belongsTo(models.User, {foreignKey: "patient_id"});
      Dsession.belongsTo(models.DsessionType, {foreignKey: "type_id"});
    };
    return Dsession;
};
