importScripts("https://www.gstatic.com/firebasejs/5.9.4/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/5.9.4/firebase-messaging.js");
firebase.initializeApp({
    apiKey: "AIzaSyC8pn-AqLaE2TJQYd1o335qgU7-Bj-a3zs",
    authDomain: "patientaccess-bfeab.firebaseapp.com",
    databaseURL: "https://patientaccess-bfeab.firebaseio.com",
    projectId: "patientaccess-bfeab",
    storageBucket: "patientaccess-bfeab.appspot.com",
    messagingSenderId: "369644303597",
    appId: "1:369644303597:web:07bf31491a1439568e48e4",
    measurementId: "G-2XRBFS9QRN"
});
const messaging = firebase.messaging();
messaging.setBackgroundMessageHandler(function(payload) {
    const promiseChain = clients
        .matchAll({
            type: "window",
            includeUncontrolled: true
        })
        .then(windowClients => {
            for (let i = 0; i < windowClients.length; i++) {
                const windowClient = windowClients[i];
                windowClient.postMessage(payload);
            }
        })
        .then(() => {
            return registration.showNotification("my notification title");
        });
    return promiseChain;
});
self.addEventListener('notificationclick', function(event) {
    // do what you want
    // ...
});
