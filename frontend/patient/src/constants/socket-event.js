export const JOIN_ROOM = "join_room";
export const CHAT_MESSAGE = "chat-message";
export const START_TYPING = "start_typing";
export const STOP_TYPING = "stop_typing";
export const DSESSION_ACTION = "dsession-action";
