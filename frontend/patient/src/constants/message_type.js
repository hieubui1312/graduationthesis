export const MESSAGE_TEXT = 1;
export const MESSAGE_IMAGE = 2;
export const MESSAGE_PRESCRIPTION_SENT = 3;
export const MESSAGE_DISEASE_TEST = 4;
export const MESSAGE_APPOINTMENT_SCHEDULE = 5;
