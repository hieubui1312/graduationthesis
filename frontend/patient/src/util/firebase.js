import * as firebase from 'firebase/app';
import "firebase/auth";

var app = firebase.initializeApp({
    apiKey: "AIzaSyC8pn-AqLaE2TJQYd1o335qgU7-Bj-a3zs",
    authDomain: "patientaccess-bfeab.firebaseapp.com",
    databaseURL: "https://patientaccess-bfeab.firebaseio.com",
    projectId: "patientaccess-bfeab",
    storageBucket: "patientaccess-bfeab.appspot.com",
    messagingSenderId: "369644303597",
    appId: "1:369644303597:web:07bf31491a1439568e48e4",
    measurementId: "G-2XRBFS9QRN"
});

export default app;
