import React, {useEffect} from 'react';
import logo from './logo.svg';
import './App.css';
import Login from "./components/login";
import {NotificationContainer} from 'react-notifications';
import Register from "./components/register";
import Home from "./components/home";
import {BrowserRouter as Router, Switch, Route,} from "react-router-dom";
import Header from "./components/header";
import {connect, useDispatch, useSelector} from "react-redux";
import openSocket from "socket.io-client";
import DiseaseTest from "./components/disease-test";
import { messaging } from "./init-fcm";
import {callAPI} from "./actions";
import {NotificationManager} from "react-notifications";
import PrescriptionSent from "./components/prescription-sent";
import Recharge from "./components/reacharge";
import DsessionDetail from "./components/dsession-detail";
import {Redirect} from "react-router-dom";
import DoctorList from "./components/doctor-list";
import DsessionList from "./components/dsession-list";
import PersonalInformation from "./components/personal-information";
import HealthRecordList from "./components/health-record-list";
import HealthRecordInfo from "./components/health-record-info";
import DoctorDetail from "./components/doctor-detail";
import {SERVER_SOCKET} from "./constants/server_base";
import Socket from "./components/socket";

const PrivateRoute = ({path, comp}) => {
    const currentUser = useSelector(state => state.currentUser);

    function getBody() {
        if (currentUser.name) {
            return <Route path={path} render={() => comp}/>
        } else return <Redirect to={"/login"}/>
    }
    return <div>
        {getBody()}
    </div>
};

class App extends React.Component{

    componentDidMount() {
        let {currentUser} = this.props;
        if (!window.socket && currentUser.access_token) {
            window.socket = openSocket(SERVER_SOCKET);
            window.socket.emit("login", currentUser.access_token);

            window.socket.on("login_success", function (data) {
                console.log("Data login", data)
            });
            window.socket.on("login_fail", function (data) {
                console.log("Login fail", data)
            });
            window.socket.on("internal_error", function (data) {
                console.log("Error", data);
            });
        }

        // setTimeout(() => {
        //     if (window.socket){
        //         console.log("Socket cuar to");
        //         window.socket.on("notify", (message) => {
        //             NotificationManager.error("Vao roi", "Loi", 5000);
        //             alert("dgdfgdfgdfgdf");
        //             console.log("Co notify lucky");
        //             console.log("Co notify", message);
        //         })
        //     }
        // }, 2000);

        messaging.requestPermission()
            .then(async () => {
                const token = await messaging.getToken();
                let {currentUser} = this.props;
                console.log("Token", token);
                if (Object.keys(currentUser).length > 0) {
                    this.props.callAPI("patient/register-token", currentUser.access_token, data => {
                        console.log("Data update token", data);
                    }, error => {
                        // NotificationManager.error("Update token thất bại", "Thông báo");
                    }, {
                        token
                    })
                }
            })
            .catch(function(err) {
                console.log("Unable to get permission to notify.", err);
            });
        navigator.serviceWorker.addEventListener("message", (message) => console.log(message));
    }

    // componentDidUpdate(prevProps, prevState, snapshot) {
    //     if (window.socket && this.props.currentUser){
    //         window.socket.on("notify", (message) => {
    //             NotificationManager.error("Vao roi", "Loi", 5000);
    //             alert("dgdfgdfgdfgdf");
    //             console.log("Co notify lucky");
    //             console.log("Co notify", message);
    //         })
    //     }
    // }

    render() {
        return (
            <div>
                <Router>
                <Header/>
                <Socket/>
                    <Switch>
                        <PrivateRoute path={"/recharge"} comp={<Recharge/>}/>
                        <Route path={"/prescription-sent/:id"} render={() => <PrescriptionSent/>} />
                        <PrivateRoute path={"/dsession-chat/:id"} comp={<DsessionDetail />} />
                        <Route path={"/login"} render={() => <Login/>} />
                        <Route path={"/register"} render={() => <Register/>} />
                        <Route path={"/disease-test/:id"} render={() => <DiseaseTest/>}/>
                        <PrivateRoute path={"/doctor-list"} comp={<DoctorList/>} />
                        <PrivateRoute path={"/dsession-list"} comp={<DsessionList/>} />
                        <PrivateRoute path={"/personal-information"} comp={<PersonalInformation/>} />
                        <PrivateRoute path={"/health-record"} comp={<HealthRecordList/>} />
                        <PrivateRoute path={"/health-record-info/:id"} comp={<HealthRecordInfo />}/>
                        <PrivateRoute path={"/doctor-detail/:id"} comp={<DoctorDetail/>}/>
                        <PrivateRoute path={"/"} comp={<Home/>} />
                    </Switch>
                <NotificationContainer/>
                </Router>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        isLoading: state.loading,
        currentUser: state.currentUser
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        callAPI: (path, accessToken, successFunc, errorFunc, postBody, method) =>
            dispatch(callAPI(path, accessToken, successFunc, errorFunc, postBody, method))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
