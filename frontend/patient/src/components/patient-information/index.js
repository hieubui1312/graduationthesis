import React, {useState} from "react";
import "./index.scss";
import UpdatePersonalInformation from "../update-personal-information";
import PasswordChange from "../password-change";
import {useSelector} from "react-redux";
function PatientInformation() {
    let [is_update_info, setUpdateInfo] = useState(false);

    const closeUpdateInfo = () => setUpdateInfo(false);
    const openUpdateInfo = () => setUpdateInfo(true);

    let [is_change_password, setIsChangePassword] = useState(false);
    const closeChangePassword = () => setIsChangePassword(false);
    const openChangePassword = () => setIsChangePassword(true);

    const currentUser = useSelector(state => state.currentUser);


    return <div className="patient-information">
        <div className="account p-md-4 mb-md-3 d-flex align-items-center justify-content-between">
            <p className={"mb-md-0 fs-20 text-violet"}><small>Tài khoản:</small> <span className={"text-danger"}>{currentUser.credit} (VNĐ)</span></p>
            <a href="#" className={"btn btn-primary"}>
                Nạp điểm
            </a>
        </div>
        <div className="info">
            <h5>Thông tin cá nhân</h5>
            <div>
                <p><b>Email:</b> {currentUser.email}</p>
                <p><b>Số điện thoại:</b> {currentUser.phone}</p>
                <p><b>Địa chỉ:</b> {currentUser.address}</p>
                <p>
                    <a href={"#"} onClick={openUpdateInfo} className={"text-pink"}>
                        <b>Cập nhật thông tin cá nhân</b>
                    </a>
                </p>
            </div>
        </div>
        <div className="info">
            <h5>Password</h5>
            <div>
                <p>Một số quy tắc giữ password của bạn đươc an toàn</p>
                <ul>
                    <li>Không bao giờ chia sẻ mật khẩu cho người khác</li>
                    <li>Không bao giờ viết mật khẩu</li>
                </ul>
                <p>
                    <a href="#" onClick={openChangePassword} className={"text-pink"}>
                        {/*<b>Thay đổi mật khẩu</b>*/}
                    </a>
                </p>
            </div>
        </div>
        <UpdatePersonalInformation show={is_update_info} handleClose={closeUpdateInfo}/>
        <PasswordChange is_change_password={is_change_password} closeChangePassword={closeChangePassword} />
    </div>
}

export default PatientInformation;
