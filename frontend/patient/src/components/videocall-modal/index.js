/* global JitsiMeetExternalAPI */
import React, {useEffect, useState} from "react";
import {Modal, Button} from "react-bootstrap";
import firebasePatient from "../../util/firebase";
import {useDispatch, useSelector} from "react-redux";
import "./index.scss";
import {sendVideoCallLog} from "../../actions/messageDSession";

const VideoCallModal = () => {
    const currentUser = useSelector(state => state.currentUser);
    const currentDSession = useSelector(state => state.currentDSession);
    const [status, setStatus] = useState(-1);
    const [doctor, setDoctor] = useState("");
    const [room, setRoom] = useState(null);

    function handleClose(){

    }

    const dispatch = useDispatch();

    useEffect(() => {
        if (currentUser.id) {
            let dataRef = firebasePatient.database().ref(`patient-access-${currentUser.id}`);
            dataRef.on("value", (snapshot) => {
                let value = snapshot.val();
                if (!value) return;
                if (value && value.sender && value.sender === "doctor") {
                    setStatus(value.status);
                    setDoctor(value.fromUserName);
                    setRoom(value.room);
                }
                if (value.sender === "doctor" && value.status === 4) {
                    alert("Bác sĩ đã kết thúc cuộc gọi");
                }
            })
        }
    }, [currentUser]);

    useEffect(() => {
        if (status === 1) {
            const domain = 'jitsi.vietskin.vn';
            const options = {
                roomName: room,
                width: 700,
                height: 700,
                parentNode: document.querySelector('#meet')
            };
            const api = new JitsiMeetExternalAPI(domain, options);
            api.addEventListener("videoConferenceLeft", () => {
                let dataRef = firebasePatient.database().ref(`patient-access-${currentUser.id}`);
                dataRef.set({
                    status: 4,
                    sender: "patient",
                    fromUserName: currentUser.name,
                    room: `patient4455646464646-${currentDSession.patient_id}`
                });
                setTimeout(() => {
                    firebasePatient.database().ref(`patient-access-${currentUser.id}`).remove();
                }, 1000);
                setStatus(4);
                dispatch(sendVideoCallLog(currentUser.access_token, {
                    dsession_id: currentDSession.id,
                    message: "Cuộc gọi Video thành công"
                }));
            })
        }
    }, [status]);

    function decline() {
        firebasePatient.database().ref(`patient-access-${currentUser.id}`).set({
            status: 2,
            sender: "patient",
            fromUserName: currentUser.name,
            room: `patient4455646464646-${currentDSession.patient_id}`
        });
        setStatus(2);
        dispatch(sendVideoCallLog(currentUser.access_token, {
            dsession_id: currentDSession.id,
            message: "Bênh nhân đã từ chối cuộc gọi"
        }));
        setTimeout(() => {
            firebasePatient.database().ref(`patient-access-${currentUser.id}`).remove();
        }, 1000);
    }

    function getBody(){
        switch (status) {
            case 0:
                return <div>
                    <div>Bác sĩ <b>{doctor}</b> đang gọi điện cho bạn</div>
                    <div className={"mt-md-3 text-right"}>
                        <Button variant="secondary" className={"mr-md-2"} onClick={decline}>
                            Từ chối
                        </Button>
                        <Button variant="primary" onClick={accept}>
                            Nghe điện
                        </Button>
                    </div>
                </div>;
            case 1:
                return <div id="meet" />;
            default:
                return <div />
        }
    }

    function accept(){
        firebasePatient.database().ref(`patient-access-${currentUser.id}`).set({
            status: 1,
            sender: "patient",
            fromUserName: currentUser.name,
            room: `patient4455646464646-${currentDSession.patient_id}`
        });
        setStatus(1);
    }

    return <div className="video-call-modal">
        <Modal show={status === 0 || status === 1 } dialogClassName={"custom-video-modal"}>
            <Modal.Body>
                {getBody()}
            </Modal.Body>
        </Modal>
    </div>
};

export default VideoCallModal;
