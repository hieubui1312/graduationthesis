import React from "react";
import {Col, Container, Row} from "react-bootstrap";
import avatar from "../../asset/images/avatar.jpg";
import "./index.scss";
import * as MessageType from "../../constants/message_type";
import {useDispatch, useSelector} from "react-redux";
import {setCurrentMessageImage} from "../../actions/currentMessageImage";

function MessageItem({isMessageOwner = false, message}) {
    const dispatch = useDispatch();
    const currentDSession = useSelector(state => state.currentDSession);
    const avatarDoctor = currentDSession.Doctor.User.avatar;

    function getContentMessage(){
        switch (message.type) {
            case MessageType.MESSAGE_TEXT:
                return message.message;
            case MessageType.MESSAGE_IMAGE:
                return <img style={{cursor: "pointer"}}
                            src={message.message}
                            alt={"Image message"}
                            onClick={() => dispatch(setCurrentMessageImage(message))}
                />;
            case MessageType.MESSAGE_APPOINTMENT_SCHEDULE:
                return <div>
                    {getMessageAppointment()}
                </div>;
            case MessageType.MESSAGE_PRESCRIPTION_SENT:
                return <a href={message.obj_url}>
                    Bác sĩ đã kê đơn thuốc {message.message}
                </a>;
            case MessageType.MESSAGE_DISEASE_TEST:
                return <a href={message.obj_url}>
                    Bác sĩ đã kê phiếu xét nghiệm {message.message}
                </a>
            default:
                return message.message;
        }
    }

    function getMessageAppointment(){
        let date = new Date(message.message);
        return `Bác sĩ đã đặt lịch hẹn vào lúc ${date.getHours()}:${date.getMinutes()} ngày ${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`;
    }

    return <div className="message-item mb-md-3">
        <Container>
            <Row className={isMessageOwner ? "d-flex flex-row-reverse" : ""}>
                {isMessageOwner ? "": <Col md={1} className={isMessageOwner ? "position-relative" : "ml--15 position-relative"}>
                    <img className={"avatar-friend"} src={avatarDoctor ?? avatar} alt={"Avatar"} />
                </Col>}
                <Col md={8} className={isMessageOwner ? "text-right " : "ml--15"}>
                    <div className="message-content p-md-3 mb-md-1">
                        {getContentMessage()}
                    </div>
                    <br/>
                </Col>
            </Row>
        </Container>
    </div>
}

export default MessageItem;
