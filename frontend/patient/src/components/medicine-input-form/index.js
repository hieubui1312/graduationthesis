import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTimes} from "@fortawesome/free-solid-svg-icons";

const MedicineInputForm = ({medicine, index, remove, setMedicine}) => {

    function onChange(e) {
        let {name, value} = e.target;
        setMedicine(index, name, value);
    }

    return <tr>
        <td>
            <input type={"text"}
                   name={"name-med"}
                   className={"form-control"}
                   onChange={onChange}
                   required
                   value={medicine["name-med"] ?? ""}
            />
        </td>
        <td>
            <input type={"number"}
                   name={"count"}
                   className={"form-control"}
                   onChange={onChange}
                   required
                   value={medicine.count ?? ""}
            />
        </td>
        <td>
            <select className={"form-control"}
                    name={"unit"}
                    value={medicine.unit ?? "Chai"}
                    onChange={onChange}
            >
                <option value={"Chai"}>Chai</option>
                <option value={"Lọ"}>Lọ</option>
                <option value={"Vỉ"}>Vỉ</option>
                <option value={"Viên"}>Viên</option>
            </select>
        </td>
        <td>
            <input type={"text"}
                   name={"usage"}
                   className={"form-control"}
                   onChange={onChange}
                   value={medicine.usage ?? ""}
            />
        </td>
        <td>
            <input type={"text"}
                   name={"note"}
                   className={"form-control"}
                   onChange={onChange}
                   value={medicine.note ?? ""}
            />
        </td>
        <td>
            <FontAwesomeIcon icon={faTimes}
                             onClick={() => remove(index)}
                             style={{cursor: "pointer"}}
            />
        </td>
    </tr>
};

export default MedicineInputForm;
