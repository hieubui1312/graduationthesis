import React, {useEffect} from "react";
import avatar from "../../asset/images/avatar.png";
import "./index.scss";
import {Link} from "react-router-dom";
import DSessionStatus from "../../constants/dsession_status";
import {useSelector} from "react-redux";
import * as SocketEvent from "../../constants/socket-event";
import {addMessage} from "../../actions/messageDSession";

const DSessionChatItem = ({dsession}) => {
    const doctor = dsession.Doctor;
    const currentDSession = useSelector(state => state.currentDSession);
    const currentUser = useSelector(state => state.currentUser);

    function getStatus(status) {
        switch (status) {
            case DSessionStatus.STATUS_WAITING:
                return <span className={"badge badge-warning"} >
                    Phiên khám chờ
                </span>;
            case DSessionStatus.STATUS_COMPLETE:
                return <span className={"badge badge-success"}>
                    Phiên khám hoàn thành
                </span>;
            case DSessionStatus.STATUS_ACCEPT:
                return <span className={"badge badge-primary"}>
                    Phiên đang khám
                </span>;
            case DSessionStatus.STATUS_DECLINE:
                return <span className={"badge badge-warning"}>
                    Phiên khám từ chối
                </span>;
            default:
                return <span className={"badge badge-danger"}>
                    Phiên khám huỷ
                </span>
        }
    }

    function getUser() {
        if (dsession.last_message_user !== currentUser.id){
            return "Bs"
        } else return "Bạn"
    }


    return <div className={currentDSession.id === dsession.id ? "dsession-chat-item p-md-3 active-item" : "dsession-chat-item p-md-3"}>
        <a href={`/dsession-chat/${dsession.id}`}>
            <div className="row">
                <div className="col-md-3">
                    <img src={doctor.User.avatar ?? avatar} alt={"avatar"}/>
                </div>
                <div className="col-md-9">
                    <div className="doctor-name">
                        BS.{doctor.User.name}
                    </div>
                    <div className="last-message">
                        Mã phiên {dsession.id}
                    </div>
                    <div className="last-message">
                        {getUser()}: {dsession.last_message}
                    </div>
                </div>
            </div>
        </a>
        <div>
            {getStatus(dsession.status)}
        </div>
    </div>
};

export default DSessionChatItem;
