import React, {useState} from "react";
import {Modal, Button} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faAngleLeft, faAngleRight} from "@fortawesome/free-solid-svg-icons";
import "./index.scss";
import {useDispatch, useSelector} from "react-redux";
import {removeMessageImage, setCurrentMessageImage} from "../../actions/currentMessageImage";
import * as MessageType from "../../constants/message_type";

const ImageModal = () => {
    const [messageImage, setMessageImage] = useState({});

    const dispatch = useDispatch();
    const currentMessageImage = useSelector(state => state.currentMessageImage);
    const messageDSessions = useSelector(state => state.messageDSessions);
    const messageImages = messageDSessions.filter((item) => {
        return item.type === MessageType.MESSAGE_IMAGE
    });

    function handleClose() {
        dispatch(removeMessageImage())
    }

    function prevImage(){
        let index = getIndexCurrentImage();
        let length = messageImages.length;
        if (index === 0) {
            dispatch(setCurrentMessageImage(messageImages[length - 1]))
        } else {
            dispatch(setCurrentMessageImage(messageImages[index - 1]))
        }
    }

    function nextImage(){
        let index = getIndexCurrentImage();
        let length = messageImages.length;
        if (index === (length - 1)) {
            dispatch(setCurrentMessageImage(messageImages[0]))
        } else {
            dispatch(setCurrentMessageImage(messageImages[index + 1]))
        }
    }

    function getIndexCurrentImage(){
        let idx = null;
        messageImages.forEach((item, index) => {
           if (item.id === currentMessageImage.id) {
               idx = index;
           }
        });
        return idx;
    }

    return <div className="image-modal">
        <Modal show={Object.keys(currentMessageImage).length > 0} dialogClassName={"custom-image"} onHide={handleClose}>
            <Modal.Body>
                <div className={"image-modal-content d-flex align-items-center justify-content-between"}>
                    <FontAwesomeIcon icon={faAngleLeft} onClick={prevImage} />
                    <img className={"image"} src={currentMessageImage.message} alt={"Image url"}/>
                    <FontAwesomeIcon icon={faAngleRight} onClick={nextImage}/>
                </div>
            </Modal.Body>
        </Modal>
    </div>
};

export default ImageModal;
