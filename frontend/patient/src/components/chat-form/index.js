import React, {useEffect, useState} from "react";
import "./index.scss";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faImage} from "@fortawesome/free-solid-svg-icons";
import {useDispatch, useSelector} from "react-redux";
import {sendMessage} from "../../actions/messageDSession";
import {SERVER_API} from "../../constants/server_base";
import * as MessageType from "../../constants/message_type";
import * as SOCKET_EVENT from "../../constants/socket-event";
import {seenLastMessage} from "../../actions/currentDSession";
import DSessionStatus from "../../constants/dsession_status";


function ChatForm() {
    const dispatch = useDispatch();
    const currentDSession = useSelector(state => state.currentDSession);
    const currentUser = useSelector(state => state.currentUser);

    const [message, setMessage] = useState("");
    const [stopTyping, setStopTyping] = useState("");

    function onChange(e){
        let {value} = e.target;
        setMessage(value);
        if (window.socket) {
            window.socket.emit(SOCKET_EVENT.START_TYPING, {
                room: `Room-${currentDSession.id}`,
                token: currentUser.access_token
            });
            if (stopTyping) {
                clearTimeout(stopTyping);
            }
            setStopTyping(setTimeout(() => {
                window.socket.emit(SOCKET_EVENT.STOP_TYPING, {
                    room: `Room-${currentDSession.id}`,
                    token: currentUser.access_token
                })
            }, 1000));

        }
    }

    function onSubmit(e){
        e.preventDefault();
        if (message.length > 0) {
            dispatch(sendMessage(currentUser.access_token, {
                dsession_id: currentDSession.id, message
            }));
            setMessage("");
        } else {
            alert("Bạn cần nhập tin nhắn trước khi submit");
        }
    }

    function uploadImage(e) {
        let file = e.target.files[0];
        let form = new FormData();
        form.append("file", file);
        fetch(`${SERVER_API}/common/upload`, {
            method: "POST",
            body: form
        }).then(result => {
            return result.json();
        }).then(res => {
           dispatch(sendMessage(currentUser.access_token, {
               dsession_id: currentDSession.id,
               message: res.fileUrl,
               obj_url: res.fileUrl,
               type: MessageType.MESSAGE_IMAGE
           }))
        });
    }

    function onClick(){
        if (currentDSession.last_message_user !== currentUser.id && currentDSession.last_message_is_seen) {
            dispatch(seenLastMessage(currentDSession.id, currentUser.access_token));
        }
    }

    function getPlaceholder(){
        switch (currentDSession.status) {
            case DSessionStatus.STATUS_WAITING:
            case DSessionStatus.STATUS_ACCEPT:
                return "Nhập tin nhắn của bạn ";
            default:
                return "Phiên khám của bạn đã kết thúc. Vui lòng tạo phiên khám mới!";
        }
    }

    return <div className="chat-form bg-violet ml--15 p-md-3">
        <form onSubmit={onSubmit}>
            <div className="row">
                <div className="col-md-10 d-flex align-items-center">
                    <input type={"text"}
                           onChange={onChange}
                           value={message}
                           placeholder={getPlaceholder()}
                           disabled={currentDSession.status !== 1 && currentDSession.status !== 2}
                           style={{background: "white"}}
                    />
                </div>
                <div className="col-md-2 text-white fs-24 position-relative">
                    <FontAwesomeIcon icon={faImage} className={"mt-md-2"} />
                    <input type={"file"}
                           name={"file"}
                           accept="image/*"
                           className={"position-absolute"}
                           onChange={uploadImage}
                           disabled={currentDSession.status !== 1 && currentDSession.status !== 2}
                    />
                </div>
            </div>
        </form>
    </div>
}

export default ChatForm;
