import React, {useEffect, useState} from "react";
import {useDispatch} from "react-redux";
import {useRouteMatch} from "react-router-dom";
import {callAPI} from "../../actions";
import {NotificationManager} from "react-notifications";



function DiseaseTest(){
    const dispatch = useDispatch();

    const match = useRouteMatch("/disease-test/:id");
    const dSessionId = match.params.id;

    const [object, setObject] = useState({});

    useEffect(() => {
        dispatch(callAPI(`patient/disease-test/${dSessionId}`, "null", data => {
            console.log("Data disease test", data);
            setObject(data);
        }, error => {
            NotificationManager.error("Lỗi không tìm thấy phiếu xét nghiệm", "Thông báo");
        }))
    }, []);

    function getDiseaseTest() {
        if (object.diseaseTests) {
            return <tbody>
            {
                object.diseaseTests.map((test, index) => <tr key={`test-${index}`}>
                    <th scope="row">{index + 1}</th>
                    <td>{test.name}</td>
                </tr>)
            }
            </tbody>
        }
    }

    function getDate() {
        if (object.diagnostic) {
            let date = new Date(object.diagnostic.created_at);
            return `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`
        }
    }

    function getSex(){
        if (object.dsession){
            return object.dsession.User.sex === "male" ? "Nam" : "Nữ"
        }
    }

    return <div className="disease-test">
        <div className="disease-test-title text-center mt-md-4">
            <h3>Phiếu xét nghiệm</h3>
        </div>
        <div className="container">
            <div className="disease-test-body">
                <div className="patient-info">
                    <div className={"mb-md-2"}>
                        <b>Họ tên người bệnh: </b>{object.dsession ? object.dsession.User.name : ""}
                    </div>
                    <div className={"mb-md-2"}>
                        <b>Tuổi: </b>{object.dsession ? object.dsession.User.age : ""}
                    </div>
                    <div className={"mb-md-2"}>
                        <b>Giới tính: </b> {getSex()}
                    </div>
                </div>
                <table className="table table-bordered mt-md-5">
                    <thead>
                    <tr>
                        <th scope="col">STT</th>
                        <th scope="col">Tên xét nghiệm</th>
                    </tr>
                    </thead>
                    {getDiseaseTest()}
                </table>
                <div className={"d-flex justify-content-end"}>
                    <div className={"disease-test-end text-center"} style={{width: "300px"}}>
                        <p>
                            <b>Chỉ định ngày {getDate()}</b>
                        </p>
                        <p>
                            <b>Bác sĩ</b>
                        </p>
                        <p><b>
                            {object.dsession ? object.dsession.Doctor.User.name : ""}
                        </b></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
}

export default DiseaseTest;
