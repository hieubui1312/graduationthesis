import React, {useEffect, useState} from "react";
import "./index.scss";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEllipsisV, faPlus} from "@fortawesome/free-solid-svg-icons";
import HealthRecordFormCreate from "../health-record-form-create";
import {useDispatch, useSelector} from "react-redux";
import {fetchHealthRecord} from "../../actions/healthRecord";
import dateformat from "dateformat";
import HealthRecordInfo from "../health-record-info";
import {postDeleteHealthRecord} from "../../actions/healthRecord";
import {Modal, Button, Container, Row, Col} from "react-bootstrap";
import HealthRecordFormUpdate from "../health-record-form-update";
import {Link} from "react-router-dom";
import Menu from "../menu";
import PersonalSelector from "../personal-selector";

const HealthRecordItem = (props) => {
    let {diagnose,
        re_examination_date,
        medicine,
        examination_date,
        name,
        age,
        sex,
        address,
        note,
        id, created_at} = props;

    const [toggleAction, setToggleAction] = useState(false);
    const [toggleModalDelete, setToggleModalDelete] = useState(false);

    const dispatch = useDispatch();
    const currentUser = useSelector(state => state.currentUser);

    function getMedicine(){
        let meds = JSON.parse(medicine);
        let med = meds[0];
        return `${med["name-med"]} ${med["count"]} ${med["unit"]}`
    }

    function onClick(){
        props.click({
            diagnose, re_examination_date, medicine, examination_date, name, age, sex, address, note, id
        });
        setToggleAction(false);
    }

    function update(){
        props.update({
            diagnose, re_examination_date, medicine, examination_date, name, age, sex, address, id, note, created_at
        });
        setToggleAction(false);
    }

    function showHideAction(){
        if (toggleAction) {
            return <div className={"text-right position-absolute record-action bg-white"}>
                <span className={"badge badge-primary mr-1"}
                      onClick={update}
                >
                    Sửa
                </span>
                <span className={"badge badge-warning"}
                      onClick={() => setToggleModalDelete(true)}
                >Xoá</span>
            </div>;
        } else return "";
    }

    function deleteRecord(){
        dispatch(postDeleteHealthRecord(id,
            currentUser.access_token))
    }

    function closeModalDelete(){
        setToggleModalDelete(false)
    }

    return <div className="health-record-item p-md-3 mr-md-3 position-relative" style={{cursor: "pointer"}}>
        <div className="action text-right position-absolute ellipsis" onClick={() => setToggleAction(!toggleAction)}>
            <FontAwesomeIcon icon={faEllipsisV} />
        </div>
        {showHideAction()}
        <Link to={`/health-record-info/${id}`} style={{textDecoration: "none"}}>
            <b>Chuẩn đoán: </b>
            {diagnose}
        </Link>
        <div>
            <b>Thuốc: </b>
            {getMedicine()}
        </div>
        <div>
            <b>Ngày khám lại:</b>
            {dateformat(new Date(re_examination_date), "dd/mm/yyyy")}
        </div>
        <Modal show={toggleModalDelete} onHide={closeModalDelete}>
            <Modal.Header closeButton>
                <Modal.Title>Modal heading</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                Bạn có chắc chắn muốn xoá ?
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={closeModalDelete}>
                    Thoát
                </Button>
                <Button variant="primary" onClick={deleteRecord}>
                    Xoá
                </Button>
            </Modal.Footer>
        </Modal>
    </div>
};

const  HealthRecordList = () => {
    const [toggleCreateForm, setToggleCreateForm] = useState(false);
    const [records, setRecords] = useState([]);
    const [currentRecord, setCurrentRecord] = useState({});
    const [recordUpdate, setRecordUpdate] = useState({});

    const dispatch = useDispatch();
    const currentUser = useSelector(state => state.currentUser);
    const healthRecords = useSelector(state => state.healthRecords);

    useEffect(() => {
        dispatch(fetchHealthRecord(currentUser.access_token));
    }, []);

    useEffect(() => {
        getRecords();
    }, [healthRecords]);

    function getRecords(){
        let arr = [],
            date,
            index = -1;

        healthRecords.forEach(item => {
            let dateCreated = dateformat(item.created_at, "yyyy-mm-dd");
            if (date !== dateCreated){
                index++;
                date = dateCreated;
                arr[index] = {date, data: [item]}
            } else {
                arr[index].data.push(item);
            }
        });
        setRecords(arr);
    }

    function getRecordElements(){
        return records.map((record, index) => {
            return <div className="health-record-section mt-md-3" key={`record-${index}`}>
                <div className="health-record-title mb-md-3">
                    <b>Ngày {record.date}</b>
                </div>
                <div className="health-record-body d-flex flex-wrap">
                    {
                        record.data.map(item => <HealthRecordItem key={`health-record-${item.id}`}
                                                                  {...item }
                                                                  click = {(result) => setCurrentRecord(result)}
                                                                  update = {(result) => setRecordUpdate(result)}
                        />)
                    }
                </div>
            </div>
        })
    }

    return <div className="home">
            <Container fluid={true} className={"pl-md-4 pr-md-4"}>
                <Row>
                    <Col md={2} >
                        <Menu/>
                    </Col>
                    <Col md={10} className={"bg-gray"} style={{borderRadius: "8px"}}>
                        <div className="health-records p-md-4">
                            {getRecordElements()}
                            <div className="btn-create-health-record"
                                 onClick={() => setToggleCreateForm(true)}
                            >
                                <FontAwesomeIcon icon={faPlus} />
                            </div>
                            <HealthRecordFormCreate
                                toggle={toggleCreateForm}
                                close={() => setToggleCreateForm(false)}
                            />
                            <HealthRecordFormUpdate record={recordUpdate}
                                                    close={() => setRecordUpdate({})}
                            />
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
}

export default HealthRecordList;
