import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {fetchData} from "../../actions/prescriptionSent";
import {useRouteMatch} from "react-router-dom";
import "./index.scss";
import dateFormat from "dateformat";

const PrescriptionSent = () => {
    const dispatch = useDispatch();
    const currentUser = useSelector(state => state.currentUser);
    const [prescription, setPrescription] = useState({});

    const match = useRouteMatch("/prescription-sent/:id");
    const dSessionId = match.params.id;

    useEffect(() => {
        dispatch(fetchData(dSessionId, (data) => {
            setPrescription(data);
        }))
    }, []);

    function getMedicine(){
        let record = prescription.record;
        if (!record) return "";
        let {medicine} = record;
        medicine = JSON.parse(medicine);
        console.log("Medicines", medicine);
        return medicine.map((item, key) => <tr key={`medicine-${key}`}>
            <td>{key + 1}</td>
            <td>{item.medicine_id}</td>
            <td>{item.count}</td>
            <td>{item.usage}</td>
            <td>{item.note}</td>
        </tr>)
    }

    return <div className="prescription-sent">
        <div className="disease-test-title text-center mt-md-4">
            <h3>Đơn thuốc</h3>
        </div>
        <div className="container">
            <div className="disease-test-body">
                <div className="patient-info">
                    <div className={"mb-md-2"}>
                        <b>Họ tên người bệnh: </b>{currentUser.name}
                    </div>
                    <div className={"mb-md-2"}>
                        <b>Tuổi: </b>{currentUser.age}
                    </div>
                    <div className={"mb-md-2"}>
                        <b>Giới tính: </b>{currentUser.sex === "male" ? "Nam" : "Nữ"}
                    </div>
                    <div className={"mb-md-2"}>
                        <b>Chuẩn đoán: </b>{prescription.record ? prescription.record.diagnose : ""}
                    </div>
                </div>
                <table className="table table-bordered mt-md-5">
                    <thead>
                        <tr>
                            <th scope="col">STT</th>
                            <th scope="col">Tên thuốc</th>
                            <th scope="col">Số lượng</th>
                            <th scope="col">Cách dùng</th>
                            <th scope="col">Lưu ý khác</th>
                        </tr>
                    </thead>
                    <tbody>
                        {getMedicine()}
                    </tbody>
                    {/*{getDiseaseTest()}*/}
                </table>
                <div className="form-group">
                    <b>Lời dặn bác sĩ</b>
                    <ul>
                        <li>Chế độ ăn uống: {prescription.record ? prescription.record.diet : ""}</li>
                        <li>Vệ sinh thân thể: {prescription.record ? prescription.record.body_clear : ""}</li>
                        <li>Khám lại: {prescription.record ? dateFormat(new Date(prescription.record.date_test_again), "dd-mm-yyyy") : ""}</li>
                        <li>Lưu ý đặc biệt khác: {prescription.record ? prescription.record.other_note : ""}</li>
                    </ul>
                </div>
                <div className={"d-flex justify-content-end"}>
                    <div className={"disease-test-end text-center"} style={{width: "300px"}}>
                        <p>
                            <b>Chỉ định ngày </b>
                        </p>
                        <p>
                            <b>Bác sĩ</b>
                        </p>
                        <p><b>
                            Bác sĩ {prescription.dsession ? prescription.dsession.Doctor.User.name : ""}
                        </b></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
};

export default PrescriptionSent;
