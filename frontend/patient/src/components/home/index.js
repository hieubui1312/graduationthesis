import React, {useEffect} from "react";
import Menu from "../menu";
import {Col, Container, Row} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faBook, faCalendarAlt, faCoins, faEdit, faPlus, faSignInAlt, faUserMd, faWallet} from "@fortawesome/free-solid-svg-icons";
import Header from "../header";
import "./index.scss";
import DoctorList from "../doctor-list";
import DsessionList from "../dsession-list";
import DsessionDetail from "../dsession-detail";
import PersonalInformation from "../personal-information";
import {BrowserRouter as Router, Switch, Route, Link,} from "react-router-dom";
import HealthRecordList from "../health-record-list";
import {useDispatch, useSelector} from "react-redux";
import {getListDSession} from "../../actions/dSession";
import DoctorDetail from "../doctor-detail";
import HealthRecordInfo from "../health-record-info";
import {useHistory} from "react-router-dom";

function MainHome() {
    const dispatch = useDispatch();
    const currentUser = useSelector(state => state.currentUser);
    const {dsessions} = useSelector(state => state.dSessions);

    useEffect(() => {
        if (currentUser.access_token) {
            dispatch(getListDSession(currentUser.access_token));
        }
    }, [currentUser]);

    function getCountDSessionAccept(){
        const count = dsessions && dsessions.accept ? dsessions.accept.length : 0;
        if (count > 0) {
            return <p>Bạn đang có {count} phiên khám đang diễn ra</p>
        } else {
            return <p>Bạn chưa có phiên khám nào</p>
        }
    }

    function getButtonEntryDSession(){
        const count = dsessions && dsessions.accept ? dsessions.accept.length : 0;
        if (count > 0) {
            return <Link to={"/dsession-list"} className={"btn bg-pink text-light text-uppercase "}>
                <FontAwesomeIcon icon={faSignInAlt} className={"mr-md-1"} /> Đi vào
            </Link>
        } else {
            return <Link to={"/doctor-list"}>
                <FontAwesomeIcon icon={faPlus} className={"mr-md-1"} /> Tạo phiên khám ngay
            </Link>
        }
    }

    return <div className="main-home">
        <Container>
            <Row>
                <Col md={6}>
                    <div className="card-home bg-white m-md-4 p-md-5 d-flex align-items-center justify-content-center">
                        <Row>
                            <Col md={3} className={"d-flex align-items-center text-primary"} style={{fontSize: "44px"}}>
                                <FontAwesomeIcon icon={faUserMd} />
                            </Col>
                            <Col md={9}>
                                <div className="dsession-count-current mb-md-2">
                                    <p className={"fs-18"}>
                                        {getCountDSessionAccept()}
                                    </p>
                                </div>
                                <div>
                                    {getButtonEntryDSession()}
                                </div>
                            </Col>
                        </Row>
                    </div>
                </Col>
                <Col md={6}>
                    <div className="card-home m-md-4 p-md-5 d-flex flex-column align-items-center justify-content-center" style={{background: "#f4ded5"}}>
                        <div className={"text-primary"} style={{fontSize: "40px"}}>
                            <FontAwesomeIcon icon={faCalendarAlt} />
                        </div>
                        <p className={"text-secondary"}>
                            Khám bệnh với PatientAccess
                        </p>
                        <Link to={"/doctor-list"} className={"btn bg-white text-violet"}>
                            <FontAwesomeIcon icon={faPlus} className={"mr-md-1"} /> Tạo phiên khám
                        </Link>
                    </div>
                </Col>
                <Col md={6}>
                    <div className="card-home m-md-4 p-md-5 d-flex flex-column align-items-center justify-content-center" style={{background: "#d0a3bf"}}>
                        <div className={"text-success"} style={{fontSize: "40px"}}>
                            <FontAwesomeIcon icon={faWallet} />
                        </div>
                        <p className={"text-white"}>
                            Khám bệnh thoải mái với <b>PatientAccess</b>
                        </p>
                        <a href={"/recharge"} className={"btn bg-white text-violet"}>
                            <FontAwesomeIcon icon={faCoins} className={"mr-md-1"} /> Nạp điểm
                        </a>
                    </div>
                </Col>
                <Col md={6}>
                    <div className="card-home m-md-4 p-md-5 d-flex flex-column align-items-center justify-content-center" style={{background: "#04535b"}}>
                        <div className={"text-white"} style={{fontSize: "40px"}}>
                            <FontAwesomeIcon icon={faBook} />
                        </div>
                        <p className={"text-secondary"}>
                            Sổ khám bệnh Online
                        </p>
                        <Link to={"/health-record"} className={"btn bg-white text-violet"}>
                            <FontAwesomeIcon icon={faEdit} className={"mr-md-1"} /> Tạo sổ khám
                        </Link>
                    </div>
                </Col>
            </Row>
        </Container>
    </div>
}
function Home() {
    return <div className="home">
        <Container fluid={true} className={"pl-md-4 pr-md-4"}>
            <Row>
                <Col md={2} >
                    <Menu/>
                </Col>
                <Col md={10} className={"bg-gray"} style={{borderRadius: "8px"}}>
                    <MainHome/>
                        {/*<Route path={"/doctor-list"} render={() => <DoctorList/>}/>*/}
                        {/*<Route path={"/dsession-list"} render={() => <DsessionList/>} />*/}
                        {/*/!*<Route path={"/dsession-chat/:id"} render={() => <DsessionDetail/>}/>*!/*/}
                        {/*<Route path={"/personal-information"} render={() => <PersonalInformation/>} />*/}
                        {/*<Route path={"/health-record"} render={() => <HealthRecordList />} />*/}
                        {/*<Route path={"/health-record-info/:id"} render={() => <HealthRecordInfo />} />*/}
                        {/*<Route path={"/doctor-detail/:id"} render={() => <DoctorDetail/>} />*/}
                </Col>
            </Row>
        </Container>
    </div>
}

export default Home;
