import React, {useEffect, useState} from "react";
import {Modal} from "react-bootstrap";
import dateformat from "dateformat";
import MedicineInputForm from "../medicine-input-form";
import {useDispatch, useSelector} from "react-redux";
import {postUpdateHealthRecord} from "../../actions/healthRecord";

const HealthRecordFormUpdate = ({record, close}) => {
    const [healthRecord, setHealthRecords] = useState({});
    const [examinationDate, setExaminationDate] = useState(dateformat(new Date(), "yyyy-mm-dd"));
    const [reExaminationDate, setReExaminationDate] = useState(dateformat(new Date(), "yyyy-mm-dd"));

    function handleClose(){
        close();
    }

    const dispatch = useDispatch();
    const currentUser = useSelector(state => state.currentUser);

    useEffect(() => {
        setHealthRecords(record);
        if(record.examination_date)
            setExaminationDate(dateformat(new Date(record.examination_date), "yyyy-mm-dd"));
        if(record.re_examination_date)
            setReExaminationDate(dateformat(new Date(record.re_examination_date), "yyyy-mm-dd"));
    }, [record]);

    function onChange(e){
        let {name, value} = e.target;
        setHealthRecords({...healthRecord, [name]: value})
    }

    function onChangeExaminationDate(e){
        setExaminationDate(e.target.value);
        if (e.target.value){
            setHealthRecords({...healthRecord,
                examination_date: dateformat(new Date(e.target.value), "yyyy-mm-dd")});
        }
    }

    function onChangeReExaminationDate(e){
        setReExaminationDate(e.target.value);
        if (e.target.value){
            setHealthRecords({...healthRecord,
                re_examination_date: dateformat(new Date(e.target.value), "yyyy-mm-dd")});
        }
    }

    function getMedicines() {
        if (healthRecord.medicine){
            let medicines = JSON.parse(healthRecord.medicine);

            return medicines.map((medicine, index) => {
                return <MedicineInputForm key={`medicine-${index}`}
                                          index={index}
                                          medicine={medicine}
                                          remove={(index) => removeMedicine(index)}
                                          setMedicine={(index, name, value) => setMedicine(index, name, value)}
                />
            });
        }
    }

    function removeMedicine(index) {
        let medicines = JSON.parse(healthRecord.medicine);
        medicines.splice(index, 1);
        setHealthRecords({...healthRecord, medicine: JSON.stringify(medicines)})
    }

    function setMedicine(index, name, value){
        let medicines = JSON.parse(healthRecord.medicine);
        medicines[index][name] = value;
        setHealthRecords({...healthRecord, medicine: JSON.stringify(medicines)})
    }

    function addMedicine(){
        let medicines = JSON.parse(healthRecord.medicine);
        medicines.push({unit: "Chai"});
        setHealthRecords({...healthRecord, medicine: JSON.stringify(medicines)})
    }

    function submit(e){
        e.preventDefault();
        dispatch(postUpdateHealthRecord(
            healthRecord,
            healthRecord.id,
            currentUser.access_token
        ));
        handleClose();
    }

    return <div className="health-record-form-create">
        <Modal show={Object.keys(record).length > 0}
               onHide={handleClose}
               dialogClassName={"custom-form-create"}
        >
            <Modal.Header closeButton>
                <Modal.Title>Update sổ khám bệnh</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <form onSubmit={submit}>
                    <div className="form-group">
                        <label>Tên bênh nhân</label>
                        <input className={"form-control"}
                               value={healthRecord["name"] ?? ""}
                               placeholder={"Nhập tên bênh nhân"}
                               onChange={onChange}
                               name={"name"}
                               required
                        />
                    </div>
                    <div className="form-group">
                        <label>Tuổi</label>
                        <input type={"number"}
                               min={0} max={200}
                               value={healthRecord.age ?? ''}
                               className={"form-control"}
                               placeholder={"Nhập tuổi bênh nhân"}
                               onChange={onChange}
                               name={"age"}
                               required
                        />
                    </div>
                    <div className="form-group">
                        <label className={"mr-md-2"}>Giới tính</label>
                        <input type={"radio"} value={"male"} name={"sex"} onChange={onChange} className={"mr-md-1"} checked={"male" === healthRecord.sex}/>
                        <label className={"mr-md-2"}>Nam</label>
                        <input type={"radio"} value={"female"} name={"sex"} onChange={onChange} className={"mr-md-1"} checked={"female" === healthRecord.sex}/>
                        <label>Nữ</label>
                    </div>
                    <div className="form-group">
                        <label>Nhập tên bệnh lý</label>
                        <input className={"form-control"}
                               placeholder={"Nhập tên bệnh lý"}
                               value={healthRecord.diagnose ?? ""}
                               onChange={onChange}
                               name={"diagnose"}
                               required
                        />
                    </div>
                    <div className="form-group">
                        <table className="table">
                            <thead>
                            <tr>
                                <th scope="col">Tên thuốc</th>
                                <th scope="col">Số lượng</th>
                                <th scope="col">Đơn vị tính</th>
                                <th scope="col">Cách dùng</th>
                                <th scope="col">Lưu ý đặc biệt</th>
                                <th scope="col" />
                            </tr>
                            </thead>
                            <tbody>
                            {getMedicines()}
                            </tbody>
                        </table>
                        <button className={"btn btn-primary"} type={"button"} onClick={addMedicine}>
                            Thêm thuốc
                        </button>
                    </div>
                    <div className="form-group">
                        <label>Lưu ý khác: </label>
                        <input type={"text"}
                               className={"form-control"}
                               value={healthRecord.note ?? ""}
                               onChange={onChange}
                               name={"note"}
                        />
                    </div>
                    <div className="form-group">
                        <label>Ngày khám bệnh</label>
                        <input type={"date"}
                               className={"form-control"}
                               value={examinationDate}
                               onChange={onChangeExaminationDate}
                               name={"examination_date"}
                               required
                        />
                    </div>
                    <div className="form-group">
                        <label>Ngày khám lại</label>
                        <input type={"date"}
                               className={"form-control"}
                               name={"re_examination_date"}
                               value={reExaminationDate}
                               onChange={onChangeReExaminationDate}
                               required
                        />
                    </div>
                    <div className="form-group text-right">
                        <button className={"btn btn-primary"}>Lưu phiếu khám</button>
                    </div>
                </form>
            </Modal.Body>
        </Modal>
    </div>
};

export default HealthRecordFormUpdate;
