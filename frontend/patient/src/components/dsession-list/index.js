import React, {useEffect, useState} from "react";
import DsessionFilter from "../dsession-filter";
import "./index.scss";
import {getListDSession} from "../../actions/dSession";
import DSessionItem from "../dsession-item";
import {useDispatch, useSelector} from "react-redux";
import {Col, Container, Row} from "react-bootstrap";
import Menu from "../menu";

function DsessionList() {

    const dispatch = useDispatch();
    const currentUser = useSelector(state => state.currentUser);
    const dSessions = useSelector(state => state.dSessions);

    const [dSessionType, setDSessionType] = useState(0);

    const dSessionTypeIndex = [
        {
            index: 1,
            tag: "waiting",
            title: "Phiên khám chờ"
        },
        {
            index: 2,
            tag: "accept",
            title: "Phiên đang khám"
        },
        {
            index: 3,
            tag: "complete",
            title: "Phiên khám hoàn thành"
        },
        {
            index: 4,
            tag: "cancel",
            title: "Phiên khám đã huỷ"
        }
    ];

    useEffect(() => {
        if (currentUser.access_token){
            dispatch(getListDSession(currentUser.access_token));
        }
    }, [currentUser]);


    function getDSessionSpecific(attr, title){
        let {dsessions} = dSessions;
        if (dsessions) {
            let dSessionArr = dsessions[attr];
            let dSessionElement = [];

            let count = 0;
            for (let dsession of dSessionArr) {
                count++;
                dSessionElement.push(<DSessionItem key={`dsession-${count}`} dsession={dsession}/>);
            }
            return  <div key={title} className="dsession-section pb-md-3 mt-md-4">
                <h4 className={"mb-md-3"}>{title}</h4>
                <div className="row">
                    {dSessionArr.length > 0 ? dSessionElement : <div className={"pl-md-3"}><i>Không tìm thấy phiên khám</i></div>}
                </div>
            </div>;
        }
    }

    return <div className="home">
            <Container fluid={true} className={"pl-md-4 pr-md-4"}>
                <Row>
                    <Col md={2} >
                        <Menu/>
                    </Col>
                    <Col md={10} className={"bg-gray"} style={{borderRadius: "8px"}}>
                        <div className="dsession-list p-md-3">
                            <DsessionFilter dsessionTypeIndex={dSessionTypeIndex}
                                            setDSessionType = {index => setDSessionType(index)}
                                            dSessionType = {dSessionType}
                            />
                            {dSessionTypeIndex.map((type, index) => {
                                return (dSessionType === 0 || dSessionType === type.index) ? getDSessionSpecific(type.tag, type.title) : ""
                            })}
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
}

export default DsessionList;
