import React from "react";
import "./index.scss";
import Countdown from "react-countdown";
import {useSelector} from "react-redux";
import DSessionStatus from "../../constants/dsession_status";

const countdownRenderer = ({ days, hours, minutes, seconds, completed }) => {
    if (completed) {
        // Render a completed state
        return "Phiên khám của bạn đã hoàn thành"
    } else {
        // Render a countdown
        let dayLabel = ''
        if (days > 0) {
            dayLabel = days + ' ngày + '
        }
        return (
            <span className='countdown'>({dayLabel}{hours}:{minutes}:{seconds})</span>
        )
    }
};

const ChatHeader = ({currentDSession}) => {

    const dSession = useSelector(state => state.currentDSession);

    function getCountDown(){
        let dateExpire = new Date(dSession.end_time * 1000);
        if (currentDSession.status === DSessionStatus.STATUS_ACCEPT ||
            currentDSession.status === DSessionStatus.STATUS_WAITING)return <Countdown renderer={countdownRenderer}
                          date={dateExpire}
                          onComplete={() => console.log("Complete")}
        />;
        else if (currentDSession.status === DSessionStatus.STATUS_DECLINE)
            return "Phiên khám của bạn đã bị huỷ";
        else if (currentDSession.status === DSessionStatus.STATUS_COMPLETE)
            return "Phiên khám của bạn đã hoàn thành";
    }

    return <div className="chat-header bg-violet m--15 pl-md-4 d-flex pt-md-3 pb-md-3 text-white">
        <div className="dsession-id mr-md-5 font-weight-bold">
            Mã phiên khám: {currentDSession.id}
        </div>
        <div className="count-down font-weight-bold">
            {getCountDown()}
        </div>
    </div>
}

export default ChatHeader;
