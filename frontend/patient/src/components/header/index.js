import React from "react";
import "./index.scss";
import Logo from "../../asset/images/logo.inline.svg";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faAngleDown, faArrowDown, faBell, faUserCircle} from "@fortawesome/free-solid-svg-icons";
import NotificationList from "../notification-list";
import UserMenu from "../user-menu";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import {callAPI} from "../../actions";
import notification from "../../reducers/notification";
import {setNotification} from "../../actions/notification";

class Header extends React.Component{
    constructor(props){
        super(props);
        this.state = {
          is_personal_info: false,
          is_notification: false,
          notifications: []
        };
    }

    toggle = (property) => {
        let state = this.state;
        if (!state[property]) {
            switch (property) {
                case "is_notification" :
                    state.is_personal_info = false;
                    break;
                case "is_personal_info":
                    state.is_notification = false;
                    break;
                default:
                    return;
            }
        }
        state[property] = !state[property];
        this.setState({
            ...state
        })
    };

    componentDidMount() {
        const {currentUser} = this.props;
        if (currentUser.access_token) {
            this.props.callAPI('patient/notifications', currentUser.access_token, data => {
                this.setState({
                    notifications: data.notifications
                })
                this.props.setNotification(data.notifications)
            }, error => {
                console.log("Error", error);
            })
        }
    }

    getDot(){
        let notificationFalseSeen = this.state.notifications.filter(item => item.is_seen === false);
        if (notificationFalseSeen.length > 0) {
            return <FontAwesomeIcon className={"fs-24 mr-md-2"} icon={faUserCircle} />;
        }
    };

    render() {
        let {is_personal_info} = this.state;
        let {is_notification} = this.state;
        let {currentUser} = this.props;

        let comp = <div className={"ml-auto"}>
            <Link to={"/login"} className={"text-violet"}>Đăng nhập/ Đăng xuất</Link>
        </div>;

        if (Object.keys(currentUser).length > 0) {
            comp = <div className={"ml-auto header-right d-flex"} >
                <div className="notification-own ml-auto position-relative mr-md-5"
                     onClick={() => this.toggle("is_notification")}>
                    <FontAwesomeIcon className={"fs-24 text-violet"} icon={faBell} />
                    <div className="notify-count bg-danger"/>
                </div>
                {is_notification ? <NotificationList notifications={this.props.notifications}/> : ""}
                <div className={"user text-violet d-flex align-items-between position-relative"}
                     onClick={() => this.toggle("is_personal_info")}>
                    {this.getDot()}
                    <span className={"font-weight-bold mr-md-2"}>{currentUser.name}</span>
                    <FontAwesomeIcon icon={faAngleDown} className={"mt-md-1"} />
                </div>
                {is_personal_info ? <UserMenu currentUser={currentUser}/> : ""}
            </div>;
        }

        return <div className="header d-flex position-relative align-items-center p-md-4">
            <div className="brand">
                <img src={Logo} alt={"Logo"} />
            </div>
            {comp}
        </div>
    }
}

const mapStateToProps = state => {
    return {
        currentUser: state.currentUser,
        notifications: state.notification
    }
};

const mapDispatchToProps = dispatch => {
    return {
        callAPI: (path, token , successFunc, errorFunc) => dispatch(callAPI(path, token, successFunc, errorFunc)),
        setNotification: (notification) => dispatch(setNotification(notification))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
