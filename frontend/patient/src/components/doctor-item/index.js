import React, {useEffect, useState} from "react";
import avatar from "../../asset/images/avatar.png";
import {Modal} from "react-bootstrap";
import {doctor} from "../../constants/doctor";
import {useDispatch, useSelector} from "react-redux";
import {getDSessionTypes} from "../../actions/dSessionType";
import {Link} from "react-router-dom";
import "./index.scss";


let Doctor = doctor;
function DoctorItem({is_left, doctor, showDSessionType, setDoctor}) {
    function getDegreeDoctor(id){
        let degree = Doctor.degree;
        for (let attr in degree) {
            if (degree[attr] == id) return attr;
        }
        return "Not found";
    }

    function onclick(){
        showDSessionType(doctor.degree);
        setDoctor(doctor);
    }

    function getMajors(){
        let {majors} = doctor;
        if (majors){
            majors = JSON.parse(majors);
            let str = majors.join(", ");
            if (str.length > 25){
                return str.substr(0, 25) + "...";
            }
            return str;
        }
        return <div/>;
    }

    return <div className={is_left ? "doctor-item p-md-4 bg-white mr-md-5" : "doctor-item p-md-4 bg-white ml-md-5"  }>
        <div className="doctor-item-top mb-md-2">
            <div className="row">
                <div className="col-md-3">
                    <img className={"avatar-doctor"}
                         src={doctor.User.avatar ?? avatar}
                         alt={"Avatar"}
                         style={{objectFit: "cover"}}
                    />
                </div>
                <div className="col-md-6 ml--15">
                    <p className="doctor-name fs-20">
                        <Link to={`/doctor-detail/${doctor.doctor_id}`}>
                            {getDegreeDoctor(doctor.degree)} {doctor.User.name}
                        </Link>
                    </p>
                    <div className="doctor-major mt--15">
                        {/*{doctor.majors.join(", ")}*/}
                        {getMajors()}
                    </div>
                </div>
                <div className="col-md-3">
                    <a onClick={onclick} className={"btn bg-pink text-light text-uppercase btn-medical-examination pt-md-3"}>Khám bệnh</a>
                </div>
            </div>
        </div>
        <div className="doctor-item-bottom">
            <div className="row">
                <div className="col-md-6 text-center" style={{borderRight: "1px solid #ccc"}}>
                    <span className={"fs-22 text-primary"}>
                        {doctor.experience_year} năm</span><br/>
                    <span>kinh nghiệm</span>
                </div>
                <div className="col-md-6 text-center">
                    <span className={"fs-22 text-danger"}>{Math.floor(doctor.average_rating / 5) * 100} %</span><br/>
                    <span>Hài lòng</span>
                </div>
            </div>
        </div>
    </div>
}

export default DoctorItem;
