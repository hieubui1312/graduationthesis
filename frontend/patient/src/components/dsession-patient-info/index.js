import React, {useEffect, useState} from "react";
import {Button, Modal} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {getInfoDSession, updateInfoDSession} from "../../actions/dSession";

const DSessionPatientInfo = () => {
    const [show, setShow] = useState(false);
    const [patientInfo, setPatientInfo] = useState({});
    const [patientInfoAfterUpdate, setPatientInfoAfterUpdate] = useState({});

    const dispatch = useDispatch();
    const currentUser = useSelector(state => state.currentUser);
    const currentDSession = useSelector(state => state.currentDSession);

    useEffect(() => {
        if (currentUser.access_token && currentDSession.id) {
            console.log("Da vao dddddddddddddddddddd");
            dispatch(getInfoDSession(currentDSession.id, currentUser.access_token, data => {
                setPatientInfo(data);
                setPatientInfoAfterUpdate(data);
            }))
        }
    }, [currentUser, currentDSession]);

    function handleClose() {
        setShow(false);
    }

    function submit(e){
        e.preventDefault();
        const body = {
            dsession_id: currentDSession.id,
            name: currentUser.name,
            sex: currentUser.sex,
            age: currentUser.age,
            address: currentUser.address,
            description: patientInfo.description,
            time_begin: patientInfo.time_begin
        };
        dispatch(updateInfoDSession(body, currentUser.access_token, data => {
            setPatientInfoAfterUpdate(patientInfo);
            setShow(false);
        }));
    }

    function onChange(e){
        let {name, value} = e.target;
        setPatientInfo({
           ...patientInfo,
           [name]: value
        });
    }

    return <div className="dsession-patient-info">
        <div className="form-group">
            <b>Triệu chứng: </b><span>{patientInfoAfterUpdate.description ?? "Chưa có"}</span>
        </div>
        <div className="form-group">
            <b>Thời gian bị: </b><span>{patientInfoAfterUpdate.time_begin ?? "Chưa có"}</span>
        </div>
        <button className={"btn btn-primary btn-sm"} onClick={() => setShow(true)}>
            Cập nhật
        </button>
        <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>
                    Cập nhật thông tin cá nhân
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <form onSubmit={submit}>
                    <div className="form-group">
                        <label>
                            Triệu chứng
                        </label>
                        <textarea className={"form-control"}
                                  placeholder={"Nhập triệu chứng của bạn"}
                                  required
                                  onChange={onChange}
                                  value={patientInfo.description}
                                  name={"description"}
                        />
                    </div>
                    <div className="form-group">
                        <label>Thời gian bắt đầu bị</label>
                        <textarea type={"text"} className={"form-control"}
                               placeholder={"Mô tả thời gian bạn bắt đầu bị"}
                               required
                               onChange={onChange}
                               value={patientInfo.time_begin}
                               name={"time_begin"}
                        />
                    </div>
                    <div className={"text-right"}>
                        <button type={"submit"} className={"btn btn-primary"}>
                            Cập nhật
                        </button>
                    </div>
                </form>
            </Modal.Body>
        </Modal>
    </div>
};

export default DSessionPatientInfo;
