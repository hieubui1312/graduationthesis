import React, {useEffect, useState} from "react";
import "./index.scss";
import avatar from "../../asset/images/avatar.jpg";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faArrowLeft, faPlus} from "@fortawesome/free-solid-svg-icons";
import {Link} from "react-router-dom";
import {useDispatch} from "react-redux";
import {getDetailDoctor} from "../../actions/doctor";
import {useLocation} from "react-router-dom";
import * as DoctorConstant from "../../constants/doctor";
import {Col, Container, Row} from "react-bootstrap";
import Menu from "../menu";
import HealthRecordFormCreate from "../health-record-form-create";
import HealthRecordFormUpdate from "../health-record-form-update";


const DoctorDetail = () => {
    const locationUrl = useLocation();
    const locationParse = locationUrl.pathname.split("/");
    let doctorID = locationParse[2];

    const [user, setUser] = useState({});
    const [doctor, setDoctor] = useState({});
    const [hospital, setHospital] = useState({});

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getDetailDoctor(doctorID, (data) => {
            const _user = data;
            const _doctor = data.Doctor;
            const _hospital = _doctor.Hospital;
            console.log("Doctor", data);
            setUser(_user);
            setDoctor(_doctor);
            setHospital(_hospital);
        }))
    }, []);

    function getDegreeText(degree){
        const degreeObj = DoctorConstant.doctor.degree;
        switch (degree) {
            case degreeObj.BS:
                return "Bác sĩ";
            case degreeObj.BS_CK1:
                return "Bác sĩ chuyên khoa 1";
            case degreeObj.BS_CK2:
                return "Bác sĩ chuyên khoa 2";
            case degreeObj.GS_TS_BS:
                return "Giáo sư, tiến sĩ, bác sĩ";
            case degreeObj.PGS_TS_BS:
                return "Phó giáo sư, tiến sĩ, bác sĩ";
            case degreeObj.ThS_BS:
                return "Thạc sĩ, bác sĩ";
            default:
                return "Bác sĩ";
        }
    }

    return <div className="home">
            <Container fluid={true} className={"pl-md-4 pr-md-4"}>
                <Row>
                    <Col md={2} >
                        <Menu/>
                    </Col>
                    <Col md={10} className={"bg-gray"} style={{borderRadius: "8px"}}>
                        <div className="doctor-detail p-4">
                            <div className="doctor-title text-center position-relative p-md-2">
                                <Link to={"/doctor-list"}>
                                    <FontAwesomeIcon icon={faArrowLeft} className={"position-absolute"} style={{left: "10px", top: "13px"}} />
                                </Link>
                                <b>Bác sĩ {user.name}</b>
                            </div>
                            <div className="row">
                                <div className="col-md-3">
                                    <div className={"text-center pt-4"}>
                                        <img className={"doctor-avatar"} src={user.avatar}/>
                                    </div>
                                    <div className="doctor-name text-center font-weight-bold mt-3">
                                        Bác sĩ {user.name}
                                    </div>
                                    <div className={"mt-3 "}>
                                        <ul>
                                            <li>
                                                {getDegreeText(parseInt(doctor.degree))}
                                            </li>
                                            <li>Gần {doctor.experience_year} năm kinh nghiệm</li>
                                            <li>{hospital.name}</li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="col-md-9">
                                    <div className="doctor-introduce-text mt-4">
                                        <i>{doctor.intro_text}</i>
                                    </div>
                                    <div className="doctor-section mt-3">
                                        <div className="doctor-section-title">
                                            KINH NGHIỆM
                                        </div>
                                        <div className="doctor-section-body" dangerouslySetInnerHTML={{__html: doctor.experience}} />
                                    </div>
                                    <div className="doctor-section mt-3">
                                        <div className="doctor-section-title">
                                            BẰNG CẤP
                                        </div>
                                        <div className="doctor-section-body" dangerouslySetInnerHTML={{__html: doctor.degree_text}} />
                                    </div>
                                    <div className="doctor-section mt-3">
                                        <div className="doctor-section-title">
                                            CHUYÊN NGÀNH
                                        </div>
                                        <div className="doctor-section-body" dangerouslySetInnerHTML={{__html: doctor.certificate}} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
};

export default DoctorDetail;
