import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faHome, faUserMd, faBook, faBookMedical} from "@fortawesome/free-solid-svg-icons";
import "./index.scss";

function Menu() {
    return <div className="menu pt-md-4">
        <div className="menu-item p-md-3">
            <div className="row fs-18">
                <div className="col-md-2">
                    <FontAwesomeIcon icon={faHome} />
                </div>
                <div className="col-md-10">
                    <a href="/" className={"text-violet"}>Home</a>
                </div>
            </div>
        </div>
        <div className="menu-item p-md-3">
            <div className="row fs-18">
                <div className="col-md-2">
                    <FontAwesomeIcon icon={faBookMedical} />
                </div>
                <div className="col-md-10">
                    <a href="/dsession-list" className={"text-violet"}>Phiên khám</a>
                </div>
            </div>
        </div>
        <div className="menu-item p-md-3">
            <div className="row fs-18">
                <div className="col-md-2 text-violet">
                    <FontAwesomeIcon icon={faUserMd} />
                </div>
                <div className="col-md-10 ">
                    <a href="/doctor-list" className={"text-violet"}>Khám bệnh</a>
                </div>
            </div>
        </div>
        <div className="menu-item p-md-3">
            <div className="row text-violet fs-18">
                <div className="col-md-2">
                    <FontAwesomeIcon icon={faBook} />
                </div>
                <div className="col-md-10">
                    <a href="/health-record" className={"text-violet"}>Số khám bệnh</a>
                </div>
            </div>
        </div>
    </div>
}

export default Menu;
