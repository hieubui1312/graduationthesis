import React, {useEffect, useState} from "react";
import "./index.scss";
import {useDispatch, useSelector} from "react-redux";
import {fetchHistoryTransaction} from "../../actions/historyTransaction";

function HistoryTransaction() {

    const dispatch = useDispatch();
    const currentUser = useSelector(state => state.currentUser);
    const [creditLogs, setCreditLogs] = useState([]);

    useEffect(() => {
        dispatch(fetchHistoryTransaction(currentUser.access_token, data => {
            console.log("Data credit", data.creditLogs);
            setCreditLogs(data.creditLogs);
        }))
    }, []);

    function getLogs(){
        return creditLogs.map((log, index) => {
            return <tr key={`log-${index}`}>
                <th scope="row">{index + 1}</th>
                <td>{log.created_at}</td>
                <td>{log.credit_to - log.credit_from}</td>
                <td>{log.credit_from}</td>
                <td>{log.credit_to}</td>
                <td>{log.reason}</td>
            </tr>
            }
        )
    }

    return <div className="history-transaction">
        <table className="table table-striped">
            <thead>
            <tr>
                <th scope="col">#ID</th>
                <th scope="col">Ngày</th>
                <th scope="col">Thay đổi</th>
                <th scope="col">Số dư trước</th>
                <th scope="col">Số dư sau</th>
                <th scope="col">Miêu tả</th>
            </tr>
            </thead>
            <tbody>
            {getLogs()}
            </tbody>
        </table>
    </div>
}

export default HistoryTransaction;
