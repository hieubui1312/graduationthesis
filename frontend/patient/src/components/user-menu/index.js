import React from "react";
import "./index.scss";
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import {logout} from "../../actions/auth";

class UserMenu extends React.Component{
    render() {
        return <div className="user-menu position-absolute bg-white ">
            <div className="user-menu-top p-md-4">
                <div className="row">
                    <div className="col-md-2">
                        <div className="avatar text-light text-uppercase bg-violet">
                            <b>HB</b>
                        </div>
                    </div>
                    <div className="col-md-10">
                        <div className="username text-violet font-weight-bold">{this.props.currentUser.name}</div>
                        <div className="user-link ">
                            <a href={"/personal-information/"} className={"text-pink "}>
                                My account
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div className="user-menu-bottom pt-md-3 pl-md-4 pb-md-3">
                <a onClick={() => this.props.logout()} style={{cursor: "pointer"}} className={"text-violet font-weight-bold"}>Đăng xuất</a>
            </div>
        </div>
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        logout: () => dispatch(logout())
    }
};

export default connect(null, mapDispatchToProps)(UserMenu);
