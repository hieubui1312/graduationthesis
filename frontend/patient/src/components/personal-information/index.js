import React, {useState} from "react";
import PersonalSelector from "../personal-selector";
import PatientInformation from "../patient-information";
import "./index.scss";
import HistoryTransaction from "../history-transaction";
import {BrowserRouter as Router, Switch, Route,} from "react-router-dom";
import {Col, Container, Row} from "react-bootstrap";
import Menu from "../menu";
import DsessionFilter from "../dsession-filter";

const selector = {
    patient_info: {
        name: "Thông tin tài khoản",
        index: 1
    },
    history_transaction: {
        name: "Lịch sử giao dịch",
        index: 2
    }
};

function PersonalInformation() {

    const [selected, setSelected] = useState(selector.patient_info.index);

    function generateComponent(index){
        switch (index) {
            case selector.patient_info.index:
                return <PatientInformation/> ;
            case selector.history_transaction.index:
                return <HistoryTransaction/>;
        }
    }

    return <div className="home">
            <Container fluid={true} className={"pl-md-4 pr-md-4"}>
                <Row>
                    <Col md={2} >
                        <Menu/>
                    </Col>
                    <Col md={10} className={"bg-gray"} style={{borderRadius: "8px"}}>
                        <div className="personal-information p-md-4">
                            <PersonalSelector
                                selected = {selected}
                                selector={selector}
                                select={(index) => setSelected(index)}
                            />
                            {generateComponent(selected)}
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
}

export default PersonalInformation;
