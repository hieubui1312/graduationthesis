import avatar from "../../asset/images/avatar.png";
import React from "react";
import {setCurrentDSession} from "../../actions/currentDSession";
import {useDispatch} from "react-redux";
import {Redirect, withRouter} from "react-router-dom";


function DSessionItem({dsession, history}) {

    function getTimeCreateDsession(){
        let date = new Date(dsession.created_at);
        return `${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}`;
    }

    function getLastMessage(){
        let {last_message} = dsession;
        if (last_message) {
            let words = last_message.split(" ");
            let str = "";
            for (let word of words) {
                if (str.length < 40) str+= `${word} `;
                else {
                    str.trim();
                    str += "...";
                    break;
                }
            }
            return str;
        }
        return "";
    }

    function onClick(){
        history.push(`/dsession-chat/${dsession.id}`);
    }

    const User = dsession.Doctor.User;

    return <div className="col-md-4 mb-md-2" onClick={onClick}>
        <div className="dsession-item bg-white p-md-3">
            <div className="row">
                <div className="col-md-3">
                    <img src={User.avatar ?? avatar}
                         className={"avatar-doctor"}
                         alt={"Avatar"}
                         style={{objectFit: "cover"}}
                    />
                </div>
                <div className="col-md-9">
                    <h5>Bs.{dsession.Doctor.User.name}</h5>
                    <p>{getLastMessage()}</p>
                    <small className={"text-danger mt--15 d-inline-block"}>
                        Mã phiên khám: {dsession.id} / Date: {getTimeCreateDsession()}
                    </small>
                </div>
            </div>
        </div>
    </div>
}

export default withRouter(DSessionItem);
