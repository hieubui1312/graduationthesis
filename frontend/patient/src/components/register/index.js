import React from "react";
import "./index.scss";
import {connect} from "react-redux";
import {Redirect} from "react-router-dom";
import {NotificationManager} from "react-notifications";
import {register} from "../../actions/auth";

class Register extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            body: {
                name: "",
                email: "",
                address: "",
                sex: "male",
                age: ""
            }
        };
    }

    onChange = (e) => {
        let {name, value} = e.target;
        let {body} = this.state;
        body[name] = value;
        this.setState({
            body
        })
    };

    onSubmit = (e) => {
        e.preventDefault();
        let {currentUser} = this.props;
        if (this.checkPropertyBodyEmpty()) {
            this.props.register(this.state.body, currentUser.access_token);
        }
    };

    checkPropertyBodyEmpty = () => {
        let {body} = this.state;
        for (let attr in body) {
            if (!body[attr]) {
                NotificationManager.error("Vui lòng điền đầy đủ các trường thông tin", "Lỗi", 5000);
                return false;
            }
        }
        return true;
    };

    render() {
        let {currentUser} = this.props;
        let {body} = this.state;
        let comp = <div className="register">
            <div className="register-body p-md-3">
                <div className="container-fluid pt-md-5 bg-gray">
                    <div className="register-form m-auto p-md-4">
                        <div className="title fs-18 text-center font-weight-bold text-violet mb-md-4">Tạo tài khoản Patient Access</div>
                        <form onSubmit={this.onSubmit}>
                            <div className="form-group">
                                <label className={"text-violet font-weight-bold fs-16"}>Họ tên</label>
                                <input required type={"text"} value={body.name} onChange={this.onChange} name={"name"} className={"form-control"} placeholder={"Nhập họ và tên"}/>
                            </div>
                            <div className="form-group">
                                <label className={"text-violet font-weight-bold fs-16"}>Email</label>
                                <input required type={"email"} value={body.email} onChange={this.onChange} name={"email"} className={"form-control"} placeholder={"Nhập email"}/>
                            </div>
                            <div className="form-group">
                                <label className={"text-violet font-weight-bold fs-16"}>Độ tuổi</label>
                                <input required type={"text"} value={body.age} onChange={this.onChange} name={"age"} className={"form-control"} placeholder={"Nhập tuổi"}/>
                            </div>
                            <div className="form-group">
                                <label className={"text-violet font-weight-bold fs-16 mr-md-3"}>Giới tính</label>
                                <input type={"radio"} name={"sex"} value={"male"} onChange={this.onChange} checked={body.sex === "male"} className={"mr-md-1"}/>
                                <label className={"mr-md-2"}>Nam</label>
                                <input className={"mr-md-1"} type={"radio"} name={"sex"} value={"female"} onChange={this.onChange} checked={body.sex === "female"}/>
                                <label>Nữ</label>
                            </div>
                            <div className="form-group">
                                <label className={"text-violet font-weight-bold fs-16"}>Địa chỉ</label>
                                <textarea required value={body.address} className={"form-control"} name={"address"} onChange={this.onChange} placeholder={"Nhập địa chỉ"} />
                            </div>
                            <div className="form-group">
                                <button type={"submit"} className={"btn w-100 bg-pink text-white text-uppercase font-weight-bold"}>Đăng kí</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>;

        if (Object.keys(currentUser).length > 0 && currentUser.name) {
            comp = <Redirect to={{pathname: "/home"}}/>
        }

        if (Object.keys(currentUser).length === 0) {
            comp = <Redirect to={{pathname: "/login"}}/>
        }

        return comp;
    }
}
const mapStateToProps = (state) => {
    return {
        currentUser: state.currentUser
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        register: (body, token) => dispatch(register(body, token))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Register);
