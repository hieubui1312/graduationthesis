import React from "react";
import {Modal, Button} from "react-bootstrap";

function PasswordChange({is_change_password, closeChangePassword}) {
    return <div className="password-change">
        <Modal show={is_change_password} onHide={closeChangePassword}>
            <Modal.Header closeButton>
                <Modal.Title>Thay đổi password</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <form>
                    <div className="form-group">
                        <label>Nhập mật khẩu cũ</label>
                        <input type={"password"} className={"form-control"} placeholder={"Nhập mật khẩu cũ"}/>
                    </div>
                    <div className="form-group">
                        <label>Nhập mật khẩu mới</label>
                        <input type={"password"} className={"form-control"} placeholder={"Nhập mật khẩu cũ"}/>
                    </div>
                    <div className="form-group">
                        <label>Nhập lại mật khẩu mới</label>
                        <input type={"password"} className={"form-control"} placeholder={"Nhập mật khẩu cũ"}/>
                    </div>
                </form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={closeChangePassword}>
                    Huỷ
                </Button>
                <Button variant="primary" onClick={closeChangePassword}>
                    Lưu thay đổi
                </Button>
            </Modal.Footer>
        </Modal>
    </div>
}

export default PasswordChange;
