import React, {useEffect, useState} from "react";
import {Modal, Button} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {NotificationManager} from "react-notifications";
import {postUpdateCurrentUser} from "../../actions/currentUser";
import "./index.scss";
import {SERVER_API} from "../../constants/server_base";

function UpdatePersonalInformation({show, handleClose}) {

    const currentUser = useSelector(state => state.currentUser);
    const dispatch = useDispatch();

    const [user, setUser] = useState(currentUser);
    const [body, setBody] = useState({});
    const [imgSrc, setImgSrc] = useState("");
    const [fileUpload, setFileUpload] = useState(null);

    useEffect(() => {
        setImgSrc(currentUser.avatar);
    }, [currentUser]);

    function onChange(e){
        let {name, value} = e.target;
        let newUser = {...user, [name] : value};
        setBody({...body, [name]: value});
        setUser(newUser);
    }

    async function onSubmit(e){
        e.preventDefault();
        if (Object.keys(body).length > 0) {
            dispatch(postUpdateCurrentUser(body, currentUser.access_token));
        }
        handleClose();
    }

    async function uploadFile(file){
        const formData = new FormData();
        formData.append("file", file);

        let result = await fetch(`${SERVER_API}/common/upload`, {
            method: "POST",
            body: formData
        });
        result = await result.json();
        setBody({...body, avatar: result.fileUrl});
    }

    async function onChangeFile(e){
        let {files} = e.target;
        let file = files[0],
            reader = new FileReader();
        reader.onloadend = () => {
            setImgSrc(reader.result);
        };
        reader.readAsDataURL(file);

        await uploadFile(file);
    }

    function getAvatarImage(){
        if (imgSrc) {
            return <img src={imgSrc} className={"avatar-image mb-2"} alt={"Avatar user"}/>
        } else return <i>Chưa có avatar</i>
    }

    return <div className="update-personal-information">
        <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>
                    Cập nhật thông tin cá nhân
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <form onSubmit={onSubmit}>
                    <div className="form-group">
                        <label>User name</label>
                        <input className={"form-control"}
                               name={"name"}
                               value={user.name}
                               placeholder={"Nhập user name của bạn"}
                               onChange={onChange}
                               required
                        />
                    </div>
                    <div className="form-group">
                        <label className={"mb-2"}>Avatar</label>
                            <div>
                                {getAvatarImage()}
                            </div>
                        <div>
                            <input type={"file"} onChange={onChangeFile} />
                        </div>
                    </div>
                    <div className="form-group">
                        <label>Email</label>
                        <input className={"form-control"}
                               name={"email"}
                               value={user.email}
                               placeholder={"Nhập email của bạn"}
                               onChange={onChange}
                               required
                        />
                    </div>
                    <div className="form-group">
                        <label>Địa chỉ</label>
                        <input className={"form-control"}
                               name={"address"}
                               value={user.address}
                               placeholder={"Nhập địa chỉ của bạn"}
                               onChange={onChange}
                               required
                        />
                    </div>
                </form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={handleClose}>
                    Huỷ
                </Button>
                <Button variant="primary" onClick={onSubmit}>
                    Lưu thay đổi
                </Button>
            </Modal.Footer>
        </Modal>
    </div>
}

export default UpdatePersonalInformation;
