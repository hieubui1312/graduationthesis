import React, {useEffect, useState} from "react";
import "./index.scss";
import {useDispatch, useSelector} from "react-redux";
import {callAPI} from "../../actions";

function NotificationList({notifications}) {
    const dispatch = useDispatch();
    const currentUser = useSelector(state => state.currentUser);

    // useEffect(() => {
    //     if (currentUser) {
    //         dispatch(callAPI('patient/notifications', currentUser.access_token, data => {
    //             console.log("Data notification", data);
    //             setNotifications(data.notifications);
    //         }, error => {
    //             console.log("Error", error);
    //         }))
    //     }
    // }, [currentUser]);

    function onClick(id){
        dispatch(callAPI(`patient/notification_seen/${id}`, currentUser.access_token, data => {
            console.log("Data", data);
        }, error => {
            console.log("Error", error);
        }))
    }

    function getNotifications(){
        return notifications.map((notification, key) => {
            const classNotify = notification.is_seen ? "notification-message mb-md-2 p-md-3" : "notification-message mb-md-2 p-md-3 is_seen";
           return <div className={classNotify} key={`notify-${key}`}>
               <div className="row">
                   <div className="col-md-1 pt-md-2">
                       <div className="dot bg-warning"/>
                   </div>
                   <div className="col-md-11 fs-14 ml--15">
                    <a href={notification.link} style={{textDecoration: "none"}} onClick={() => onClick(notification.id)}>
                        {notification.message}
                    </a>
                   </div>
               </div>
           </div>
        });
    }

    return <div className="notification-list position-absolute p-md-3">
        <div className="notification-head mb-md-3">
            <small className={"font-weight-bold text-violet"}>Thông báo gần đây</small>
        </div>
        {getNotifications()}
    </div>
}

export default NotificationList;
