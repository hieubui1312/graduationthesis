import React, {useEffect} from "react";
import ChatBox from "../chatbox";
import "./index.scss";
import DsessionInfo from "../dsession-info";
import {fetchDSession} from "../../actions/currentDSession";
import {useLocation} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import DSessionListChat from "../dsession-list-chat";

function DsessionDetail({match, location}) {
    const locationUrl = useLocation();
    const locationParse = locationUrl.pathname.split("/");
    let dsessionId = locationParse[2];

    const currentUser = useSelector(state => state.currentUser);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchDSession(dsessionId, currentUser.access_token));
    }, [locationUrl]);

    return <div className="dsession-detail">
        <div className="row">
            <div className="col-md-3">
                <DSessionListChat/>
            </div>
            <div className="col-md-6">
                <ChatBox/>
            </div>
            <div className="col-md-3 dsession-detail-info ml--15">
                <DsessionInfo />
            </div>
        </div>
    </div>
}

export default DsessionDetail;
