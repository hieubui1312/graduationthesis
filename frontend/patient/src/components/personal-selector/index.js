import React from "react";
import "./index.scss";

function PersonalSelector({selector, selected, select}) {

    function onClick(index){
        select(index);
    }

    function getSelector(){
        let selectorArr = [];
        let index = 0;
        for (let select in selector) {
            let classDiv = getClassItem(selector[select].index);
            let classLink = getClassLink(selector[select].index);

            selectorArr.push(<div className={classDiv} key={`selector-${index}`}>
                <a style={{cursor: "pointer"}} className={classLink}
                   onClick={() => onClick(selector[select].index)}>
                    {selector[select].name}
                </a>
            </div>);

            index++;
        }
        return selectorArr;
    }

    function getClassItem(index){
        let classDiv = "selector-item text-center p-md-1";
        if (selected === index) {
            classDiv += " bg-pink text-light"
        } else  {
            classDiv += " bg-white ";
        }
        return classDiv;
    }

    function getClassLink(index){
        let classDiv = "text-uppercase";
        if (selected === index) {
            classDiv += "text-white"
        } else  {
            classDiv += "text-violet ";
        }
        return classDiv;
    }

    return <div className="personal-selector d-flex align-items-center mb-md-4 justify-content-center">
        {getSelector()}
    </div>
}

export default PersonalSelector;
