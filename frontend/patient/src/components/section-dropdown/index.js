import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faAngleDown} from "@fortawesome/free-solid-svg-icons";
import "./index.scss";
import {useSelector} from "react-redux";
import * as MessageType from "../../constants/message_type";

function SectionDropdown({title, body}) {


    return <div className="section-dropdown m--15">
        <div className="section-head p-md-2 d-flex bg-secondary text-white justify-content-between text-uppercase align-items-center fs-18">
            <span>{title}</span>
            <FontAwesomeIcon icon={faAngleDown} />
        </div>
        <div className="section-body p-md-2 bg-white">
            {body}
        </div>
    </div>
}

export default SectionDropdown;
