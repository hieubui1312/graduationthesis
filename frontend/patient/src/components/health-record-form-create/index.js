import React, {useEffect, useState} from "react";
import {Modal, Button} from "react-bootstrap";
import "./index.scss";
import {useDispatch, useSelector} from "react-redux";
import MedicineInputForm from "../medicine-input-form";
import dateformat from "dateformat";
import {postAddHealthRecord} from "../../actions/healthRecord";
import {NotificationManager} from "react-notifications";

const HealthRecordFormCreate = ({toggle, close}) => {
    const [patientInfo, setPatientInfo] = useState({});
    const [medicines, setMedicines] = useState([]);
    const [noteOther, setNoteOther] = useState("");
    const [examinationDate, setExaminationDate] = useState(dateformat(new Date(), "yyyy-mm-dd"));
    const [reExaminationDate, setReExaminationDate] = useState(getReExaminationDateDefault());
    const [diagnose, setDiagnose] = useState("");

    const currentUser = useSelector(state => state.currentUser);

    const dispatch = useDispatch();

    useEffect(() => {
        setPatientInfo(currentUser);
    }, [currentUser]);

    function handleClose() {
        close();
    }

    function getReExaminationDateDefault(){
        let currentDate = new Date();
        currentDate.setDate(currentDate.getDate() + 3 * 30);
        return dateformat(currentDate, "yyyy-mm-dd");
    }

    function getMedicines(){
        return medicines.map((medicine, index) => {
            return <MedicineInputForm key={`medicine-${index}`} medicine={medicine}
                                      index={index}
                                      remove={(index) => removeMedicine(index)}
                                      setMedicine={(index, name, value) => setMedicine(index, name, value)}
            />
        })
    }

    function setMedicine(index, name, value){
        let newMedicine = [...medicines];
        newMedicine[index][name] = value;
        setMedicines(newMedicine);
    }

    function removeMedicine(index){
        let newMedicine = [...medicines];
        newMedicine.splice(index,1);
        setMedicines(newMedicine);
    }

    function addMedicine() {
        setMedicines([...medicines, {unit: "Chai"}])
    }

    function onChange(e) {
        let {name, value} = e.target;
        setPatientInfo({...patientInfo, [name]: value})
    }

    function submit(e) {
        e.preventDefault();
        if (medicines.length <= 0) {
            NotificationManager.error("Bạn cần điền đơn thuốc trước khi submit", "Thông báo", 5000);
            return false;
        }
        dispatch(postAddHealthRecord({
            user_id: parseInt(currentUser.id),
            diagnose,
            medicine: JSON.stringify(medicines),
            re_examination_date: dateformat(new Date(reExaminationDate), "yyyy-mm-dd hh:MM:ss"),
            sex: patientInfo.sex,
            examination_date: dateformat(new Date(examinationDate), "yyyy-mm-dd hh:MM:ss"),
            name: patientInfo.name,
            age: patientInfo.age,
            note: noteOther
        }, currentUser.access_token));
        handleClose();
    }

    return <div className="health-record-form-create">
        <Modal show={toggle}
               onHide={handleClose}
               dialogClassName={"custom-form-create"}
        >
            <Modal.Header closeButton>
                <Modal.Title>Tạo sổ khám bệnh</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <form onSubmit={submit}>
                    <div className="form-group">
                        <label>Tên bênh nhân</label>
                        <input className={"form-control"}
                               value={patientInfo.name ?? ""}
                               placeholder={"Nhập tên bênh nhân"}
                               onChange={onChange}
                               name={"name"}
                               required
                        />
                    </div>
                    <div className="form-group">
                        <label>Tuổi</label>
                        <input type={"number"}
                               min={0} max={200}
                               value={patientInfo.age ?? ''}
                               className={"form-control"}
                               placeholder={"Nhập tuổi bênh nhân"}
                               onChange={onChange}
                               name={"age"}
                               required
                        />
                    </div>
                    <div className="form-group">
                        <label className={"mr-md-2"}>Giới tính</label>
                        <input type={"radio"} value={"male"} name={"sex"} onChange={onChange} className={"mr-md-1"} checked={"male" === patientInfo.sex}/>
                        <label className={"mr-md-2"}>Nam</label>
                        <input type={"radio"} value={"female"} name={"sex"} onChange={onChange} className={"mr-md-1"} checked={"female" === patientInfo.sex}/>
                        <label>Nữ</label>
                    </div>
                    <div className="form-group">
                        <label>Nhập tên bệnh lý</label>
                        <input className={"form-control"}
                               placeholder={"Nhập tên bệnh lý"}
                               value={diagnose}
                               onChange={e => setDiagnose(e.target.value)}
                               required
                        />
                    </div>
                    <div className="form-group">
                        <table className="table">
                            <thead>
                            <tr>
                                <th scope="col">Tên thuốc</th>
                                <th scope="col">Số lượng</th>
                                <th scope="col">Đơn vị tính</th>
                                <th scope="col">Cách dùng</th>
                                <th scope="col">Lưu ý đặc biệt</th>
                                <th scope="col" />
                            </tr>
                            </thead>
                            <tbody>
                                {getMedicines()}
                            </tbody>
                        </table>
                        <button className={"btn btn-primary"} type={"button"} onClick={addMedicine}>
                            Thêm thuốc
                        </button>
                    </div>
                    <div className="form-group">
                        <label>Lưu ý khác: </label>
                        <input type={"text"}
                               className={"form-control"}
                               value={noteOther}
                               onChange={e => setNoteOther(e.target.value)}
                        />
                    </div>
                    <div className="form-group">
                        <label>Ngày khám bệnh</label>
                        <input type={"date"}
                               className={"form-control"}
                               value={examinationDate}
                               onChange={(e) => setExaminationDate(e.target.value)}
                               required
                        />
                    </div>
                    <div className="form-group">
                        <label>Ngày khám lại</label>
                        <input type={"date"}
                               className={"form-control"}
                               value={reExaminationDate}
                               onChange={e => setReExaminationDate(e.target.value)}
                               required
                        />
                    </div>
                    <div className="form-group text-right">
                        <button className={"btn btn-primary"}>Lưu phiếu khám</button>
                    </div>
                </form>
            </Modal.Body>
        </Modal>
    </div>
};

export default HealthRecordFormCreate;
