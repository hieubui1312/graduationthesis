import React, {useEffect, useState} from "react";
import {Col, Container, Modal, Row} from "react-bootstrap";
import "./index.scss";
import DoctorItem from "../doctor-item";
import {getDoctors} from "../../actions/doctor";
import {getMajor} from "../../actions/major";
import {useDispatch, useSelector} from "react-redux";
import {doctor} from "../../constants/doctor";
import {getDSessionTypes} from "../../actions/dSessionType";
import {createDSession} from "../../actions/dSession";
import {NotificationManager} from "react-notifications";
import {Link} from "react-router-dom";
import {useHistory} from "react-router-dom";
import Menu from "../menu";


function DsessionType({show, handleClose, doctor}) {
    const dispatch = useDispatch();

    const dSessionTypes = useSelector(state => state.dSessionType);
    const currentUser = useSelector(state => state.currentUser);

    const history = useHistory();

    function onClick(type_id, fee){
        let {doctor_id} = doctor;
        if (fee > currentUser.credit) {
            NotificationManager.info("Số tiền trong tài khoản của bạn không đủ để tạo phiên khám. Vui lòng nạp tiền", "Thông báo!");
        } else {
            dispatch(createDSession({doctor_id, type_id}, currentUser.access_token , (data) => {
                history.push(`/dsession-chat/${data.data.id}`);
                NotificationManager.success("Bạn đã tạo phiên khám thành công", "Thông báo");
            }, (error) => {
                NotificationManager.error("Có lỗi xảy ra. Báo với bộ phận kĩ thuật để được hỗ trợ", "Lỗi !");
            }))
        }
    }

    function getDSessionType(){
        let types = dSessionTypes;
        let typeArr = [];
        if (types) {
            types.forEach((type, index) => {
                typeArr.push(<div key={`dSession-Type-${index}`} className={"d-flex p-md-2 justify-content-between align-items-center"}>
                    <a style={{cursor: "pointer"}} onClick={() => onClick(type.dsession_type, type.fee)}  className={"text-decoration-none"}>
                        <span>{type.DsessionType.name}</span>
                    </a>
                    <small className={"text-danger"}>{type.fee} VNĐ</small>
                </div>)
            });
        }
        return typeArr;
    }

    let modalCreateDSession =  <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
            <Modal.Title>Chọn gói khám </Modal.Title>
        </Modal.Header>
        <Modal.Body>
            {getDSessionType()}
        </Modal.Body>
    </Modal>;

    let modalLogin = <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
            <Modal.Title>Bạn cần đăng nhập để tạo phiên khám với bác sĩ</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <a href={"/login"} className={"btn btn-primary"}>
                Đăng nhập
            </a>
        </Modal.Body>
    </Modal>;

    return Object.keys(currentUser).length > 0 ? modalCreateDSession : modalLogin;
}

function DoctorList() {

    const dispatch = useDispatch();
    const accessToken = useSelector(state => state.currentUser.access_token);
    const doctors = useSelector(state => state.doctors);
    const majors = useSelector(state => state.majors.majors);

    const [degreeDoctor, setDegreeDoctor] = useState("");
    const [major, setMajor] = useState("");
    const [dSessionTypeShow, setDSessionTypeShow] = useState(false);
    const [doctorSelect, setDoctorSelect] = useState({});

    useEffect(() => {
        dispatch(getDoctors(accessToken));
        dispatch(getMajor());
    }, []);

    function showDSessionType(doctorDegree){
        dispatch(getDSessionTypes(doctorDegree));
        setDSessionTypeShow(true);
    }

    function showDoctors(){
        let doctorElements = [];
        if (doctors.data) {
            console.log("Doctor list", doctors);
            (doctors.data).forEach((doctor, index) => {
                doctorElements.push(<Col md={6} key={`doctor-${index}`} className={"mb-md-3"}>
                    <DoctorItem is_left={true}
                                doctor={doctor}
                                setDoctor={(doctor) => setDoctorSelect(doctor)}
                                showDSessionType = {(degree) => showDSessionType(degree)}
                    />
                </Col>);
            });
        }
        return doctorElements
    }

    function selectMajors(){
        let majorElements = [];
        if (majors) {
            for (let major of majors) {
                majorElements.push(<option key={`major-${major.id}`} value={major.name}>
                    {major.name}
                </option>)
            }
        }
        return <select className="form-control mr-md-2" value={major}
                       onChange={(e) => setMajor(e.target.value)}>
            <option value={""}>Chọn chuyên ngành</option>
            {majorElements}
        </select>
    }


    function selectDegreeDoctors() {
        let degree = doctor.degree;
        let degreeOption = [];
        for (let attr in degree) {
            degreeOption.push(<option value={degree[attr]} key={attr}>
                {attr}
            </option>);
        }
        return <select className="form-control mr-md-2" value={degreeDoctor} onChange={e =>
            setDegreeDoctor(e.target.value)}>
            <option value={""}>Chọn trình độ</option>
            {degreeOption}
        </select>
    }

    function onSubmit(e){
        e.preventDefault();
        let query = {};
        if (degreeDoctor) query.degree = degreeDoctor;
        if (major) query.major = major;
        dispatch(getDoctors("null", query))
    }

    function close() {
        setDSessionTypeShow(false);
    }

    return <div className="home">
            <Container fluid={true} className={"pl-md-4 pr-md-4"}>
                <Row>
                    <Col md={2} >
                        <Menu/>
                    </Col>
                    <Col md={10} className={"bg-gray"} style={{borderRadius: "8px"}}>
                        <div className="doctor-list p-md-5">
                            <Container>
                                <div className="doctor-filter bg-white mb-md-2 p-md-4">
                                    <h3 className={"text-center"}>Danh sách bác sĩ</h3>
                                    <form onSubmit={onSubmit} className={"d-flex align-items-center justify-content-center"}>
                                        <label className={"mr-md-2"}>
                                            <b>Lọc bác sĩ:</b>
                                        </label>
                                        {selectMajors()}
                                        {selectDegreeDoctors()}
                                        <button className={"btn btn-primary"}>Tìm kiếm</button>
                                    </form>
                                </div>
                            </Container>
                            <Container fluid={true}>
                                <Row>
                                    {showDoctors()}
                                </Row>
                            </Container>
                            {dSessionTypeShow ? <DsessionType show={dSessionTypeShow}
                                                              handleClose={close}
                                                              doctor={doctorSelect}
                            /> : ""}
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
}

export default DoctorList;
