import React, {useEffect, useRef, useState} from "react";
import MessageItem from "../message-item";
import "./index.scss";
import ChatForm from "../chat-form";
import ChatHeader from "../chat-header";
import {useDispatch, useSelector} from "react-redux";
import currentDSession from "../../reducers/currentDsession";
import {fetchMessage} from "../../actions/messageDSession";
import * as SocketEvent from "../../constants/socket-event";
import {addMessage} from "../../actions/messageDSession";
import {NotificationManager} from "react-notifications";
import VideoCallModal from "../videocall-modal";
import firebasePatient from "../../util/firebase";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faClock} from "@fortawesome/free-solid-svg-icons";
import {
    MESSAGE_APPOINTMENT_SCHEDULE,
    MESSAGE_DISEASE_TEST,
    MESSAGE_IMAGE,
    MESSAGE_PRESCRIPTION_SENT
} from "../../constants/message_type";
import dateFormat from "dateformat";
import DSessionStatus from "../../constants/dsession_status";
import {setDSessionSimple} from "../../actions/dsessionSimple";
import {setNotification} from "../../actions/notification";
import {setCurrentDSession} from "../../actions/currentDSession";

function ChatBox() {

    const currentDSession = useSelector(state => state.currentDSession);
    const currentUser = useSelector(state => state.currentUser);
    const messageDSessions = useSelector(state => state.messageDSessions);
    const dsessions = useSelector(state => state.dsessionSimple);
    const notification = useSelector(state => state.notification);

    const [isTyping, setIsTyping] = useState(false);

    const dispatch = useDispatch();

    const conversationRef = React.createRef();

    useEffect(() => {
        scrollToBottom();
    }, [messageDSessions]);


    useEffect(() => {
        console.log("Dsession", dsessions);
    }, [dsessions]);

    useEffect(() => {
        let {access_token} = currentUser;
        let dsession_id = currentDSession.id;
        if (dsession_id) {
            dispatch(fetchMessage(access_token, dsession_id));
            scrollToBottom();

            if (window.socket) {

                window.socket.on(SocketEvent.CHAT_MESSAGE,  (message) => {
                    if (message.dsession_id === dsession_id)
                        dispatch(addMessage(message));

                    let key = -1;
                    let _dsessions = dsessions.map((dsession, index) => {
                        if (dsession.id === message.dsession_id) {
                            if (message.type === MESSAGE_IMAGE) dsession.last_message = "Gửi hình ảnh";
                            else if (message.type === MESSAGE_APPOINTMENT_SCHEDULE) dsession.last_message = "Hẹn lịch khám";
                            else if (message.type === MESSAGE_DISEASE_TEST) dsession.last_message = "Gửi phiếu xét nghiệm";
                            else if (message.type === MESSAGE_PRESCRIPTION_SENT) dsession.last_message = "Gửi đơn thuốc";
                            else dsession.last_message = message.message;
                            dsession.last_message_user = message.user_id;
                            key = index;
                        }
                        return dsession;
                    });
                    _dsessions.sort(function(x,y){
                        return x.id === message.dsession_id ? -1 : y.id === message.dsession_id ? 1 : 0; });
                    dispatch(setDSessionSimple(_dsessions))
                });

                window.socket.on(SocketEvent.START_TYPING, (data) => {
                    if (data.id !== currentUser.id) {
                        setIsTyping(true);
                    }
                });

                window.socket.on(SocketEvent.STOP_TYPING, data => {
                    if (data.id !== currentUser.id) {
                        setIsTyping(false);
                    }
                });

                window.socket.on(SocketEvent.DSESSION_ACTION, message => {
                    console.log("Da co dsession");
                });

                window.socket.on(SocketEvent.DSESSION_ACTION, message => {
                    let _dsession = dsessions.map(item => {
                        if (item.id === message.dsession_id) {
                            if (message.status === "accept"){
                                item.status = 2;
                                NotificationManager.success("Bác sĩ đã chấp nhận phiên khám của bạn", "Thông báo");
                            }
                            if(message.status === "decline"){
                                item.status = 5;
                                NotificationManager.info("Bác sĩ đã từ chối phiên khám của bạn", "Thông báo");
                            }
                        }
                        return item;
                    });
                    dispatch(setDSessionSimple(_dsession));

                    if (message.dsession_id === currentDSession.id){
                        const _currentDSession = {...currentDSession, status: message.status === "decline" ? 5 : 2};
                        dispatch(setCurrentDSession(_currentDSession));
                    }
                });
                //
                window.socket.on("notify", (message) => {
                    let _notification = [message, ...notification];
                    dispatch(setNotification(_notification));
                })

            } else {
                NotificationManager.error("Lỗi kết nối socket", "Lỗi", 5000);
            }

            return () => {
                window.socket.off();
            }
        }
    }, [currentDSession.id, dsessions]);

    function scrollToBottom() {
        let conversation = conversationRef.current;
        conversation.scrollTop = conversation.scrollHeight - conversation.clientHeight;
    }

    function getAppointment() {
        let appointments = messageDSessions.filter(message => {
            return message.type === MESSAGE_APPOINTMENT_SCHEDULE && Date.now() < new Date(message.message).getTime()
        });
        if (appointments.length > 0) {
            return <div>
                <div className={"d-inline-block wish"} style={{position: "fixed", top: "140px", left: "378px"}}>
                    <div><FontAwesomeIcon icon={faClock}/> Hẹn khám</div>
                </div>
                <div style={{position: "fixed", top: "172px", left: "385px"}}>
                    {
                        appointments.map(item => <p>
                            {
                                getTime(item.message)
                            }
                        </p>)
                    }
                </div>
            </div>
        }
    }

    function getTime(date) {
        date = new Date(date);
        return `${date.getHours()}:${date.getMinutes()} ngày ${date.getDate()}/${date.getMonth() + 1}`
    }

    function getMessages(){
        let messageArr = [];
        let count = 0;
        let key;
        for (let message of messageDSessions) {
            key = `message-${count}`;
            if (message.user_id === -1)
                messageArr.push(<div key={key} className={"text-center text-danger"}>{message.message}</div>);
            else {
                let isOwner = currentUser.id === message.user_id;
                messageArr.push(<MessageItem key={key} isMessageOwner={isOwner} message={message}/>)
            }
            count++;
        }
        return messageArr;
    }

    getMessages();

    return <div className="chatbox">
        <VideoCallModal/>
        <ChatHeader currentDSession = {currentDSession}/>
        <div className="message-list" ref={conversationRef} >
            {getAppointment()}
            {getMessages()}
            {isTyping ? <div className="typing">
                Đang soạn tin...
            </div> : ""}
        </div>
        <ChatForm/>
    </div>
}

export default ChatBox;
