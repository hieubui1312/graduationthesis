import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {setNotification} from "../../actions/notification";

const Socket = () => {
    const currentUser = useSelector(state => state.currentUser);
    const dsessions = useSelector(state => state.dsessionSimple);
    const currentDSession = useSelector(state => state.currentDSession);
    const dSessions = useSelector(state => state.dSessions);
    const notification = useSelector(state => state.notification);
    const dispatch = useDispatch();

    useEffect(() => {
        setTimeout(() => {
            if (window.socket){
                window.socket.on("notify", (message) => {
                    dispatch(setNotification([message, ...notification]))
                })
            }
        }, 1000);
    }, [dSessions, dsessions, currentDSession]);
    return <div className="socket">

    </div>
};

export default Socket;
