import React from "react";
import "./index.scss";

function DsessionFilter({dsessionTypeIndex, setDSessionType, dSessionType}) {

    function getTypes(){
        return dsessionTypeIndex.map(type => {
            return <div key={`type-${type.index}`} className={getClassNameItem(type.index)} onClick={() => setDSessionType(type.index)}>
                {type.title}
            </div>
        })
    }

    function getClassNameItem(index){
        return dSessionType === index ? "filter-item bg-pink " : "filter-item bg-white text-violet"
    }

    return <div className="dsession-filter mb-md-5 d-flex align-items-center justify-content-center">
        <div className={getClassNameItem(0)}
             onClick={() => setDSessionType(0)}>
            Tất cả phiên khám
        </div>
        {
            getTypes()
        }
        {/*<div className="filter-item bg-white text-violet" onClick={() => setDSessionType(2)}>*/}
        {/*    Phiên khám chờ*/}
        {/*</div>*/}
        {/*<div className="filter-item bg-white text-violet">*/}
        {/*    Phiên đang khám*/}
        {/*</div>*/}
        {/*<div className="filter-item bg-white text-violet">*/}
        {/*    Phiên hoàn thành*/}
        {/*</div>*/}
        {/*<div className="filter-item bg-white text-violet">*/}
        {/*    Phiên đã huỷ*/}
        {/*</div>*/}
    </div>
}

export default DsessionFilter;
