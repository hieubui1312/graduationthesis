import React, {useState} from "react";
import DoctorInfo from "../doctor-info";
import SectionDropdown from "../section-dropdown";
import {useDispatch, useSelector} from "react-redux";
import * as MessageType from "../../constants/message_type";
import "./index.scss";
import ImageModal from "../image-modal";
import {setCurrentMessageImage} from "../../actions/currentMessageImage";
import DSessionPatientInfo from "../dsession-patient-info";

function DsessionInfo() {
    const messageDSessions = useSelector(state => state.messageDSessions);
    const dispatch = useDispatch();
    const currentMessageImage = useSelector(state => state.currentMessageImage);

    function getImages(){
        let messages = messageDSessions.filter(message => {
            return message.type === MessageType.MESSAGE_IMAGE
        });

        return messages.map((message, index) => {
            return <img className={"message-image m-1"}
                        src={message.message}
                        alt={"Message image"}
                        key={`image-${index}`}
                        onClick={() => chooseMessage(message)}
            />
        });
    }

    function getPrescriptionSent(){
        return messageDSessions.filter(message => {
            return message.type === MessageType.MESSAGE_PRESCRIPTION_SENT
        }).map((message, index) => <span className={"mr-2"} key={`prescription-${index}`}>
            <a href={message.obj_url}>Phiếu khám bệnh {message.message}</a>
        </span>);
    }

    function getDiseaseTest() {
        return messageDSessions.filter(message => {
            return message.type === MessageType.MESSAGE_DISEASE_TEST
        }).map((message, index) => <span className={"mr-2"} key={`disease-test-${index}`}>
            <a href={message.obj_url}>Phiếu xét ngiệm {message.message}</a>
        </span>);
    }

    function chooseMessage(message){
        dispatch(setCurrentMessageImage(message));
    }


    return <div className="dsession-info">
        <div className="dsession-info-header ml--15 mr--15 text-center">
            Thông tin phiên khám
        </div>
        <div className={"dsession-info-body"}>
            <DoctorInfo/>
            <SectionDropdown title={"Cập nhập triệu chứng"}
                             body={<DSessionPatientInfo/>}
            />
            <SectionDropdown title={"Đơn thuốc"}
                             body={getPrescriptionSent()}
            />
            <SectionDropdown title={"Xét nghiệm"}
                             body={getDiseaseTest()}
            />
            <SectionDropdown title={"Hình ảnh chia sẻ"}
                             body={getImages()}
            />
            <ImageModal />
        </div>
    </div>
}
export default DsessionInfo;
