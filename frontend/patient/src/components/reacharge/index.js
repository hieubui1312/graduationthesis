import React, {useEffect, useState} from "react";
import "./index.scss";
import {useDispatch, useSelector} from "react-redux";
import {payment} from "../../actions/payment";
import {useHistory} from "react-router-dom";
import {NotificationManager} from "react-notifications";

const Recharge = () => {
    const [recharge, setRecharge] = useState({});
    const [showFormFillInfo, setShowFormFillInfo] = useState(false);

    const dispatch = useDispatch();
    const currentUser = useSelector(state => state.currentUser);
    const history = useHistory();

    function onChange(e) {
        let {value, name} = e.target;
        setRecharge({
            ...recharge,
            [name]: value
        })
    }

    useEffect(() => {
        if (recharge.bank && recharge.bank.length > 0) {
            setShowFormFillInfo(true);
        } else {
            setShowFormFillInfo(false);
        }
    }, [recharge.bank]);

    function submit(e) {
        e.preventDefault();
        if (parseInt(recharge.amount) % 10000){
            NotificationManager.error("Số tiền bạn rút cần phải là bội số của 10000", "Lỗi", 5000);
            return false;
        }
        dispatch(payment({amount: parseInt(recharge.amount)},
            currentUser.access_token, () => {
                history.push("/");
            }
        ));
    }

    return <div className={"recharge p-5 d-flex justify-content-center"}>
        <div className={"p-3"}>
            <h4>Nạp điểm vào tài khoản Patient Access</h4>
            <div className={"form-group"}>
                <label>Nhập số điểm cần nạp</label>
                <input type={"number"}
                       className={"form-control"}
                       name={"amount"}
                       value={recharge.amount ?? ""}
                       onChange={onChange}
                       required
                />
            </div>
            <form onSubmit={submit}>
                <div className="form-group mb-2">
                    <label>Chọn phương thức thanh toán</label>
                    <div>
                        <input type={"radio"}
                               required
                               className={"mr-2"}
                               name={"method-recharge"}
                               onChange={onChange}
                        />
                        <label>Chuyển khoản </label>
                        <small>
                            (Bạn chuyển khoản trực tiếp đến: Ngân hàng Vietcombank - Chi nhánh Hà Nội
                            Tên người nhận: Công ty cố phần dịch vụ Y tế Gia Hân - Số tài khoản: 0021000444428)
                        </small>
                    </div>
                    <div >
                        <input type={"radio"}
                               required
                               className={"mr-2"}
                               name={"method-recharge"}
                               onChange={onChange}
                        />
                        <label>Tài khoản Internet Banking</label>
                    </div>
                    <div >
                        <input type={"radio"}
                               required
                               className={"mr-2"}
                               name={"method-recharge"}
                               onChange={onChange}
                        />
                        <label>Thẻ Visa/Master</label>
                    </div>
                    <div className="form-group">
                        <label>Chọn ngân hàng</label>
                        <select className={"form-control"}
                                name={"bank"}
                                value={recharge.bank}
                                onChange={onChange}
                                required
                        >
                            <option value={""}>Chọn ngân hàng</option>
                            <option value={"vietcombank"}>Ngân hàng VietcomBank</option>
                            <option value={"viettinbank"}>Ngân hàng ViettinBank</option>
                            <option value={"achau"}>Ngân hàng Á châu</option>
                        </select>
                    </div>
                    {
                        recharge.bank ? <div>
                            <div className="form-group">
                                <label>Nhập sổ thẻ</label>
                                <input type={"text"}
                                       className={"form-control"}
                                       name={"card-number"}
                                       value={recharge["card-number"]}
                                       required
                                />
                            </div>
                            <div className="form-group">
                                <label>Tên in trên thẻ</label>
                                <input type={"text"}
                                       className={"form-control"}
                                       name={"name-in-card"}
                                       value={recharge["name-in-card"]}
                                       required
                                />
                            </div>
                        </div> : ""
                    }
                </div>
                <div className={"text-right"}>
                    <button type={"submit"} className={"btn btn-primary"}>
                        Nạp tiền
                    </button>
                </div>
            </form>
        </div>
    </div>
};

export default Recharge;
