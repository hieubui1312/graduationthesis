import React, {useEffect, useState} from "react";
import {Modal, Button, Container, Row, Col} from "react-bootstrap";
import "./index.scss"
import dateformat from "dateformat";
import {useLocation} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {callAPI} from "../../actions";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faArrowLeft, faPlus} from "@fortawesome/free-solid-svg-icons";
import Menu from "../menu";
import HealthRecordFormCreate from "../health-record-form-create";
import HealthRecordFormUpdate from "../health-record-form-update";

const HealthRecordInfo = () => {
    const locationUrl = useLocation();
    const locationParse = locationUrl.pathname.split("/");
    let id = locationParse[2];

    const dispatch = useDispatch();
    const currentUser = useSelector(state => state.currentUser);
    const [data, setData] = useState({});

    useEffect(() => {
        dispatch(callAPI(`patient/health-info/${id}`, currentUser.access_token, data => {
            console.log("Data health info", data);
            setData(data[0]);
        }, error => {
            console.log("Error", error);
        }))
    }, [id]);
    const [medicines, setMedicines] = useState([]);

    useEffect(() => {
        if (data.medicine){
            setMedicines(JSON.parse(data.medicine));
        }
    }, [data]);

    function getMedicineContent(){
        return medicines.map((medicine, index) => {
           return <tr key={`medicine-${index}`}>
               <th scope="row">{medicine["name-med"] ?? ""}</th>
               <td>{`${medicine.count} ${medicine.unit}`}</td>
               <td>{medicine.usage}</td>
               <td>{medicine.note}</td>
           </tr>
        });
    }

    return <div className="home">
            <Container fluid={true} className={"pl-md-4 pr-md-4"}>
                <Row>
                    <Col md={2} >
                        <Menu/>
                    </Col>
                    <Col md={10} className={"bg-gray"} style={{borderRadius: "8px"}}>
                        <div className={"p-md-4"}>
                            <div className={"text-center mb-4 position-relative"} style={{fontSize: "20px"}}>
                                <a href="/health-record" className={"position-absolute"}
                                   style={{top: "0", left: "0"}}
                                >
                                    <FontAwesomeIcon icon={faArrowLeft} />
                                </a>
                                <b>Thông tin sổ khám bệnh</b>
                            </div>
                            <div className="form">
                                <div className="form-group">
                                    <div className="row">
                                        <div className="col-md-2">
                                            <b>Tên bệnh nhân:</b>
                                        </div>
                                        <div className="col-md-10">
                                            {data.name ?? ""}
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="row">
                                        <div className="col-md-2">
                                            <b>Tuổi</b>
                                        </div>
                                        <div className="col-md-10">
                                            {data.age ?? ""}
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="row">
                                        <div className="col-md-2">
                                            <b>Giới tính</b>
                                        </div>
                                        <div className="col-md-10">
                                            {data.sex === "male" ? "Nam" : "Nữ"}
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="row">
                                        <div className="col-md-2">
                                            <b>Chuẩn đoán</b>
                                        </div>
                                        <div className="col-md-10">
                                            {data.diagnose ?? ""}
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <table className="table">
                                        <thead>
                                        <tr>
                                            <th scope="col">Tên</th>
                                            <th scope="col">Số lượng</th>
                                            <th scope="col">Cách dùng</th>
                                            <th scope="col">Lưu ý</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {getMedicineContent()}
                                        </tbody>
                                    </table>
                                </div>
                                <div className="form-group">
                                    <div className="row">
                                        <div className="col-md-2">
                                            <b>Lưu ý</b>
                                        </div>
                                        <div className="col-md-10">
                                            {data.note ?? "Không có"}
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="row">
                                        <div className="col-md-2">
                                            <b>Ngày khám </b>
                                        </div>
                                        <div className="col-md-10">
                                            {data.examination_date ? dateformat(data.examination_date, "dd-mm-yyyy") : ""}
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="row">
                                        <div className="col-md-2">
                                            <b>Ngày khám lại</b>
                                        </div>
                                        <div className="col-md-10">
                                            {data.re_examination_date ? dateformat(data.re_examination_date, "dd-mm-yyyy") : ""}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
};

export default HealthRecordInfo;
