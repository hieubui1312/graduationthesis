import React from "react";
import avatar from "../../asset/images/avatar.png";
import {connect, useSelector} from "react-redux";

function DoctorInfo() {
    const currentDSession = useSelector(state => state.currentDSession);
    const avatarDoctor = currentDSession.Doctor ? currentDSession.Doctor.User.avatar : "";

    function getDoctorName(){
        if (currentDSession ) {
            let doctor = currentDSession.Doctor;
            if (doctor){
                let user = doctor.User;
                return user.name;
            }
        }
        return  "";
    }

    function getMajor(){
        const doctor = currentDSession.Doctor;
        if (!doctor) return ;
        let majors = doctor.majors;
        if (majors) {
            majors = JSON.parse(majors);
            majors = majors.join(", ");
            return <p>Chuyên ngành: {majors}</p>
        }
    }

    return <div className="doctor-info mb-md-4 pt-md-3">
        <div className="row">
            <div className="col-md-3">
                <img src={ avatarDoctor ?? avatar}
                     style={{objectFit: "cover"}}
                     alt={"avatar doctor"}
                     className={"avatar-doctor"}/>
            </div>
            <div className="col-md-9">
                <div className="doctor-name fs-18">
                    <b>Bs.</b> {getDoctorName()}
                </div>
                <div className="doctor-major">
                    {getMajor()}
                </div>
            </div>
        </div>
    </div>
}


export default DoctorInfo;
