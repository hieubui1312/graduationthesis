import React, {useEffect, useState} from "react";
import DSessionChatItem from "../dsession-chat-item";
import "./index.scss";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faArrowLeft} from "@fortawesome/free-solid-svg-icons";
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {getListDSession} from "../../actions/dSession";
import {callAPI} from "../../actions";
import * as SocketEvent from "../../constants/socket-event";
import {addMessage} from "../../actions/messageDSession";
import DSessionStatus from "../../constants/dsession_status";
import {
    MESSAGE_APPOINTMENT_SCHEDULE,
    MESSAGE_DISEASE_TEST,
    MESSAGE_IMAGE,
    MESSAGE_PRESCRIPTION_SENT
} from "../../constants/message_type";
import {NotificationManager} from "react-notifications";
import {setDSessionSimple} from "../../actions/dsessionSimple";

const DSessionListChat = () => {
    const dispatch = useDispatch();
    const currentUser = useSelector(state => state.currentUser);
    const currentDSession = useSelector(state => state.currentDSession);
    // const [dsessions, setDsessions] = useState([]);
    const dsessions= useSelector(state => state.dsessionSimple);
    const dSessions = useSelector(state => state.dSessions);

    useEffect(() => {
        if (currentUser.access_token){
            dispatch(getListDSession(currentUser.access_token));
        }
    }, [currentUser]);

    useEffect(() => {
        if (currentUser.access_token) {
            dispatch(callAPI('patient/dsessions/simple', currentUser.access_token, data => {
                dispatch(setDSessionSimple(data.dsessions));
            }, error => {
                console.log("Error", error);
            }));
        }
    }, []);

    useEffect(() => {
        // setTimeout(() => {
            let dsessionFilter = dsessions.filter(item => {
                return item.status === DSessionStatus.STATUS_WAITING || item.status === DSessionStatus.STATUS_ACCEPT
            }).map(item => {
                return item.id
            });
            if (window.socket) {
                window.socket.emit(SocketEvent.JOIN_ROOM,
                    {rooms: dsessionFilter,
                        token: currentUser.access_token
                    });
            }
    }, [dsessions]);

    function getListData(){
        return dsessions.map((dsession, index) => {
            return <DSessionChatItem
                key={`dsession-${index}`}
                dsession={dsession}
            />
        });
    }

    // useEffect(() => {
    //     setTimeout(() => {
    //         if (window.socket && dsessions.length > 0) {
    //             console.log("Vo socket lan 2 lucky");
    //             window.socket.on(SocketEvent.CHAT_MESSAGE,  (message) => {
    //                 console.log("Hello Worlddddddddddddddddd");
    //                 let key = -1;
    //                 let _dsessions = dsessions.map((dsession, index) => {
    //                     if (dsession.id === message.dsession_id) {
    //                         if (message.type === MESSAGE_IMAGE) dsession.last_message = "Gửi hình ảnh";
    //                         else if (message.type === MESSAGE_APPOINTMENT_SCHEDULE) dsession.last_message = "Hẹn lịch khám";
    //                         else if (message.type === MESSAGE_DISEASE_TEST) dsession.last_message = "Gửi phiếu xét nghiệm";
    //                         else if (message.type === MESSAGE_PRESCRIPTION_SENT) dsession.last_message = "Gửi đơn thuốc";
    //                         else dsession.last_message = message.message;
    //                         dsession.last_message_user = message.user_id;
    //                         key = index;
    //                     }
    //                     return dsession;
    //                 });
    //                 _dsessions.sort(function(x,y){
    //                     return x.id === message.dsession_id ? -1 : y.id === message.dsession_id ? 1 : 0; });
    //                 setDsessions(_dsessions);
    //             });
    //
    //             window.socket.on(SocketEvent.DSESSION_ACTION, message => {
    //                 let _dsession = dsessions.map(item => {
    //                     if (item.id === message.dsession_id) {
    //                         if (message.status === "accept"){
    //                             item.status = 2;
    //                             NotificationManager.success("Bác sĩ đã chấp nhận phiên khám của bạn", "Thông báo");
    //                         }
    //                         if(message.status === "decline"){
    //                             item.status = 5;
    //                             NotificationManager.info("Bác sĩ đã từ chối phiên khám của bạn", "Thông báo");
    //                         }
    //                     }
    //                     return item;
    //                 });
    //                 setDsessions(_dsession);
    //             });
    //         }
    //     }, 1000)
    // }, [currentDSession.id]);

    return <div className="dsession-list-chat mr--15">
        <div className="section-head position-relative">
            <Link to={"/dsession-list"} className={"position-absolute"} style={{left: "15px", fontSize: "20px", color: "white"}}>
                <FontAwesomeIcon icon={faArrowLeft}  />
            </Link>
            Danh sách phiên khám
        </div>
        <div className={"section-list"}>
            {getListData()}
        </div>
    </div>
};

export default DSessionListChat;
