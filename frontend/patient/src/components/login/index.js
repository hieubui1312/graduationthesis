import React, {useEffect, useState} from "react";
import Header from "../header";
import "./index.scss";
import firebaseAuth from "../../util/firebase";
import * as firebase from "firebase";
import {NotificationContainer, NotificationManager} from "react-notifications";
import {connect} from "react-redux";
import {login} from "../../actions/auth";
import {Redirect} from "react-router-dom";


function OtpForm({login}){
    const [otp, setOtp] = useState("");

    function onChange(e){
        let otp = e.target.value;
        if (otp.length <= 6) {
            setOtp(otp);
        }
    }

    async function onSubmit(e) {
        e.preventDefault();
        try{
            await window.confirmationResult.confirm(otp);
            let token = await firebase.auth().currentUser.getIdToken(true);
            login(token);
        } catch (e) {
            console.log("error", e);
            NotificationManager.error("OTP của bạn không chính xác. Vui lòng nhập lại !", "Thông báo");
        }
    }


    return <form onSubmit={onSubmit}>
        <div className="form-group">
            <label className={"text-violet font-weight-bold fs-16"}>Xác thực OTP</label>
            <input type={"text"} className={"form-control"} onChange={onChange} value={otp} placeholder={"Nhập mã OTP"}/>
        </div>
        <div className="form-group">
            <button type={"submit"} className={"btn w-100 bg-pink text-white text-uppercase font-weight-bold"}>Đăng nhập</button>
        </div>
        {/*<div className="form-group">*/}
        {/*    <button type={"submit"} className={"btn w-100 bg-pink text-white text-uppercase font-weight-bold"}>Gửi lại OTP</button>*/}
        {/*</div>*/}
    </form>
}

class PhoneForm extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            phone: ""
        };
    }

    onChange = (e) =>{
        let phone = e.target.value;
        phone = phone.replace(/^0/, '+84');
        this.setState({phone});
    };

    onSubmit = (e) => {
        e.preventDefault();
    };

    componentDidMount() {
        firebaseAuth.auth().languageCode = "en";
        window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier("sign-in-button", {
            'size': 'invisible',
            'callback': async  (response) => {
                await this.signIn();
            }
        });
        window.recaptchaVerifier.render();
    }


    signIn = async () => {
        try{
            let appVerifier = window.recaptchaVerifier;
            window.confirmationResult = await firebase.auth().signInWithPhoneNumber(this.state.phone, appVerifier);
            this.props.openOtpForm();
        } catch (e) {
            this.setState({
                phone: ""
            });
            NotificationManager.error('Error message', e.message);
        }
    };

    render() {
         return <form onSubmit={this.onSubmit}>
            <div className="form-group">
                <label className={"text-violet font-weight-bold fs-16"}>Số điện thoại</label>
                <input type={"text"} className={"form-control"} required value={this.state.phone} onChange={this.onChange} placeholder={"Nhập số điện thoại"}/>
            </div>
            <div className="form-group">
                <input type={"submit"} id={"sign-in-button"} className={"btn w-100 bg-pink text-white text-uppercase font-weight-bold"} value={"Đăng nhập"} />
            </div>
        </form>
    }
}

class Login extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            isPhoneNumber: true
        };
    }

    openOtpForm = () => {
        this.setState({
            isPhoneNumber: false
        })
    };

    render() {
        let currentUser = this.props.currentUser;
        let comp =   <div className="login">
            <div className="login-body p-md-3">
                <div className="container-fluid pt-md-5 bg-gray">
                    <div className="login-form ml-auto mr-auto p-md-4">
                        <div className="title fs-18 font-weight-bold text-violet mb-md-4">Welcome to Patient Access</div>
                        {
                            this.state.isPhoneNumber ? <PhoneForm openOtpForm={this.openOtpForm} /> :
                                <OtpForm login = {token => this.props.login(token)}/>
                        }
                    </div>
                </div>
            </div>
        </div>;
        if (Object.keys(currentUser).length > 0) {
            if (currentUser.name) {
                comp = <Redirect to={{pathname: "/"}} />;
            } else {
                comp = <Redirect to={{pathname: "/register"}} />;
            }
        }
        return comp
    }
}

const mapStateToProps = (state) => {
    return {
        currentUser: state.currentUser
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        login: token => dispatch(login(token))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
