import {callAPI} from "./index";
import * as Action from "../constants/action";

export const getMajor = () => {
    return callAPI("patient/majors", "null", (data, dispatch) => {
        console.log("Data", data);
        dispatch(setMajor(data));
    }, (error) => {

    });
};

export const setMajor = (majors) => {
    return {
        type: Action.SET_MAJORS,
        majors
    }
};
