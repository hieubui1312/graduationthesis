import {callAPI} from "./index";
import * as Action from "../constants/action";
import {body} from "express-validator";

export const fetchMessage = (access_token, dsession_id) => {
    return callAPI(`patient/dsession/${dsession_id}`, access_token, (data, dispatch) => {
        console.log("Message", data);
        dispatch(initialMessage(data.messages));
    }, error => {

    })
};

export const sendMessage = (access_token, body) => {
    return callAPI('patient/dsession/chat', access_token, (data, dispatch) => {
       console.log("Response send message", data);
    }, error => {
        console.log("Error send message", error);
    }, body);
};

export const sendVideoCallLog = (access_token, body) => {
    console.log(" Data vap send video call");
    return callAPI("patient/dsession/video-call-log", access_token, (data, dispatch) => {
        console.log("Data", data);
    }, error => {
        console.log("Error", error);
    }, body)
};

export const initialMessage = (messages) => {
    return {
        type: Action.INITIAL_MESSAGE,
        messages
    }
};

export const addMessage = (message) => {
    return {
        type: Action.ADD_MESSAGE,
        message
    }
};
