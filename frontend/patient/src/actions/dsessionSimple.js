import * as Action from "../constants/action";
export const setDSessionSimple = (dsessions) => {
    return {
        type: Action.SET_DSESSIONS_SIMPLE,
        dsessions
    }
};
