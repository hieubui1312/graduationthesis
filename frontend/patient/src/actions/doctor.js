import {callAPI} from "./index";
import * as Action from "../constants/action";

export const getDoctors = (accessToken, query) => {
    let url = 'patient/doctors';
    if (query) {
        let extraParam = [];
        for (let attr of Object.keys(query)) {
            extraParam.push(`${attr}=${query[attr]}`);
        }
        url += `?${extraParam.join("&")}`
    }

    return callAPI(url, accessToken, (data, dispatch) => {
        dispatch(setDoctor(data));
    }, error => {
    })
};

export const getDetailDoctor = (id, successFunc) => {
    return callAPI(`patient/doctor-detail/${id}`, "null", data => {
        console.log("Data detail doctor", data);
        successFunc(data);
    }, error => {
        console.log("Error", error);
    })
};

export const setDoctor = (doctors) => {
    return {
        type: Action.SET_DOCTORS,
        doctors
    }
};
