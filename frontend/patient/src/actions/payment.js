import {callAPI} from "./index";
import {NotificationManager} from "react-notifications";

export const payment = (data, accessToken, successFunc) => {
  return callAPI("patient/payment", accessToken, data => {
      NotificationManager.success("Nạp tiền thành công", "Thông báo");
      successFunc();
  }, error => {
      NotificationManager.error("Nạp tiền thất bại", "Thông báo");
  }, data);
};
