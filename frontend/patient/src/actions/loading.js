import * as Action from "../constants/action";

export function loadingOn() {
    return {
        type: Action.LOADING_ON
    }
}

export function loadingOff() {
    return {
        type: Action.LOADING_OFF
    }
}
