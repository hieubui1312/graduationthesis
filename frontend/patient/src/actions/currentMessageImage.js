import * as Action from "../constants/action";

export const setCurrentMessageImage = (message) => ({
    type: Action.SET_CURRENT_MESSAGE_IMAGE,
    message
});

export const removeMessageImage = () => ({
    type: Action.REMOVE_CURRENT_MESSAGE_IMAGE
});
