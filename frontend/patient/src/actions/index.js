import {API_SERVER} from "../constants/relativeApiUrl";
import {removeCurrentUser, updateAccessToken} from "./currentUser";
import {loadingOn, loadingOff} from "./loading";
import {NotificationManager} from "react-notifications";

export function callAPI(path, accessToken, successFunc, errorFunc, postBody, method) {
    return dispatch => {
        // dispatch(loadingOn());
        if (!method) {
            method = 'GET';
            if (postBody)
                method = 'POST'
        }

        let fetchOptions = {
            headers: {
                'Content-Type': 'application/json',
                'Access-Token': accessToken
            },
            method
        };

        if (postBody) {
            fetchOptions.body = JSON.stringify(postBody)
        }

        fetch(API_SERVER + path, fetchOptions).then(res => {
            console.log("Res", res.headers);
            if (res.status === 401) {
                console.log("::::::::");
                console.log("TOken", accessToken);
                // localStorage.removeItem("currentUser");
                dispatch(removeCurrentUser());
                throw Error('Có lỗi xác thực')
            }
            return res.json()
        }).then(res => {
            if (res.errorCode) {
                let err = new Error(res.error ? res.error : 'unknown error')
                err.customCode = res.errorCode;
                throw err
            }
            // dispatch(loadingOff());

            if (res.access_token) {
                dispatch(updateAccessToken(res.access_token));
            }
            if (successFunc)
                successFunc(res, dispatch)
        }).catch(err => {
            dispatch(loadingOff());
            if (errorFunc){
                errorFunc(err);
            }
        })
    }
}
