import {callAPI} from "./index";
import {setCurrentUser, removeCurrentUser} from "./currentUser";
import {NotificationManager} from "react-notifications";

export function login(token) {
    return callAPI('patient/login', "null", (data, dispatch) => {
        let currentUser = data.user;
        localStorage.setItem("currentUser", JSON.stringify(currentUser));
        dispatch(setCurrentUser(currentUser));
    }, (error) => {
        NotificationManager.error("Có lỗi xả ra. Bạn không thể đăng nhập với số điện thoại này", "Thông báo");
        console.log("Error", error);
    }, {
        token
    })
}

export function register(body, access_token) {
    return callAPI('patient/register', access_token, (data, dispatch) => {
        let currentUser = data.user;
        localStorage.setItem("currentUser", JSON.stringify(currentUser));
        dispatch(setCurrentUser(data.user))
    }, error => {
        NotificationManager.error("Email của bạn đã được sử dụng. Vui lòng tạo tài khoản email mới", "Thông báo", 5000);
    }, body);
}

export function logout() {
    localStorage.removeItem("currentUser");
    return dispatch => {
        dispatch(removeCurrentUser());
    }
}
