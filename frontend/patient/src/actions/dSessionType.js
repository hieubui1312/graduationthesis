import {callAPI} from "./index";
import * as Action from "../constants/action";

export const getDSessionTypes = (doctorDegree) => {
    return callAPI(`patient/dsession/type?degree=${doctorDegree}`, "null", (data, dispatch) => {
        console.log("Dsession type", data);
        dispatch(setDSessionTypes(data.data));
    }, (error) => {

    });
};

export const setDSessionTypes = (dSessionTypes) => {
    return {
        type: Action.SET_DSESSION_TYPES,
        dSessionTypes
    }
}
