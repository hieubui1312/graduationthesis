import {callAPI} from "./index";

export const fetchData = (id, successFunc) => {
    return callAPI(`patient/prescription-sent/${id}`, "null", data => {
        console.log("Data prescription sent", data);
        successFunc(data);
    }, error => {
        console.log("Error", error);
    })
};
