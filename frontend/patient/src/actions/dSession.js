import {callAPI} from "./index";
import {NotificationManager} from "react-notifications";
import * as Action from "../constants/action";

export const createDSession = ({doctor_id, type_id}, accessToken, successFunc, errorFunc) => {
    return callAPI('patient/dsession/create', accessToken, successFunc, errorFunc, {
        doctor_id, type_id
    })
};

export const getInfoDSession = (dsession_id, accessToken, successFunc) => {
    return callAPI(`patient/dsession/patient-info/${dsession_id}`, accessToken, (data) => {
        successFunc(data);
    }, error => {
        console.log("Error", error);
    })
};

export const updateInfoDSession = (body, access_token, successFunc) => {
    return callAPI(`patient/dsession/update/patient-info/`, access_token, data => {
        NotificationManager.success("Cập nhật thông tin thành công", "Thông báo", 5000);
        successFunc(data);
    }, error => {
        console.log("Error", error);
        NotificationManager.error("Update thông tin thất bại", "Thông báo", 5000);
    }, body)
};


export const getListDSession = (accessToken) => {
    return callAPI('patient/dsessions', accessToken, (data, dispatch) => {
        dispatch(setListDSession(data));
    }, error => {
        console.log("Error", error);
    })
};

export const setListDSession = (dSessions) => {
    return {
        type: Action.SET_DSESSIONS,
        dsessions: dSessions
    }
};

