import * as Action from "../constants/action";
import {callAPI} from "./index";
import {NotificationManager} from "react-notifications";


export function setCurrentUser(user) {
    return {
        type: Action.SET_CURRENT_USER,
        currentUser: user
    }
}

export function updateAccessToken(accessToken) {
    return {
        type: Action.UPDATE_TOKEN_CURRENT_USER,
        accessToken
    }
}

export function removeCurrentUser() {
    return {
        type: Action.REMOVE_CURRENT_USER
    }
}

export function postUpdateCurrentUser(body, accessToken) {
    return callAPI("patient/update-info", accessToken, (data, dispatch) => {
        dispatch(updateCurrentUser(body));
        NotificationManager.success("Update thông tin thành công", "Thông báo");
    }, error => {
        NotificationManager.error("Update thông tin không thành công", "Thông báo");
    }, body)
}

export function updateCurrentUser(body) {
    return {
        type: Action.UPDATE_CURRENT_USER,
        body
    }
}
