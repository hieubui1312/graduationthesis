import {callAPI} from "./index";

export const fetchHistoryTransaction = (access_token, successFunc) => {
    return callAPI("patient/credit-logs", access_token, data => {
        successFunc(data);
    }, error => {
    })
};
