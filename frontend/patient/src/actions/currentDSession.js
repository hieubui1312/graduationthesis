import * as Action from "../constants/action";
import {callAPI} from "./index";

export const fetchDSession = (dsession_id, access_token) => {
  return callAPI(`patient/dsession/specific/${dsession_id}`, access_token, (data, dispatch) => {
      dispatch(setCurrentDSession(data.dSession));
  }, error => {
      console.log("Error", error);
  })
};

export const setCurrentDSession = (dsession) => {
    return {
        type: Action.SET_CURRENT_DSESSION,
        dsession
    }
};

export const seenLastMessage = (dsession_id, access_token) => {
    return callAPI(`patient/dsession/${dsession_id}`, access_token, (data) => {
        console.log("Da seen", data);
    }, error => {
        console.log("Error", error);
    })
};
