import {callAPI} from "./index";
import {NotificationManager} from "react-notifications";
import {ADD_HEALTH_RECORD,
    INITIAL_HEALTH_RECORD,
    DELETE_HEALTH_RECORD,
    UPDATE_HEALTH_RECORD} from "../constants/action";

export const postAddHealthRecord = (body, access_token) => {
  return callAPI('patient/health-record/create', access_token, (data, dispatch) => {
      NotificationManager.success("Tạo sổ khám bệnh thành công", "Thông báo", 5000);
      dispatch(addHealthRecord(data));
  }, error => {
      console.log("Error", error);
  }, body)
};

export const fetchHealthRecord = (access_token) => {
    return callAPI('patient/health-record', access_token, (data, dispatch) => {
        dispatch(initialHealthRecord(data));
    }, error => {
        console.log("Error list record", error);
    })
};

export const postDeleteHealthRecord = (id, access_token) => {
    return callAPI(`patient/health-record/delete/${id}`, access_token, (data, dispatch) => {
        dispatch(deleteHealthRecord(id));
    }, error => {
        NotificationManager.error("Lỗi xoá sổ khám bệnh", "Thông báo", 5000);
    })
};

export const postUpdateHealthRecord = (body, id, access_token) => {
    return callAPI(`patient/health-record/update/${id}`, access_token, (data, dispatch) => {
        console.log("Data", data);
        dispatch(updateHealthRecord(id, body));
        NotificationManager.success("Update sổ khám bệnh thành công", "Thông báo");
    }, error => {
        NotificationManager.error("Lỗi update sổ khám bệnh", "Thông báo", 5000);
    }, body)
};

export const addHealthRecord = (record) => {
    return {
        type: ADD_HEALTH_RECORD,
        health_record: record
    }
};

export const initialHealthRecord = (record) => {
    return {
        type: INITIAL_HEALTH_RECORD,
        health_records: record
    }
};

export const deleteHealthRecord = (id) => {
    return {
        type: DELETE_HEALTH_RECORD,
        id
    }
};

export const updateHealthRecord = (id, data) => {
    return {
        type: UPDATE_HEALTH_RECORD,
        id,
        data
    }
};
