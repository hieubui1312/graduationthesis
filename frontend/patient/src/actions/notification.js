import * as Action from "../constants/action";

export const setNotification = (notifications) => {
    return {
        type: Action.SET_NOTIFICATIONS,
        notifications
    }
};
