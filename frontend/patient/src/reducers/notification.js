import * as Action from "../constants/action";

export default function (state = [], action) {
    switch (action.type) {
        case Action.SET_NOTIFICATIONS:
            return action.notifications;
        default:
            return state;
    }
}
