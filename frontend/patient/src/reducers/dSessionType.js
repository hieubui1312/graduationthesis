import * as Action from "../constants/action";

export default function dSessionTypeReducer(state = [], action) {
    switch (action.type) {
        case Action.SET_DSESSION_TYPES:
            return action.dSessionTypes;
        default:
            return state;
    }
}
