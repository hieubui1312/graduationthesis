import * as Action from "../constants/action";

export default function dSessionsReducer(state = {}, action) {
    switch (action.type) {
        case Action.SET_DSESSIONS:
            return action.dsessions;
        default:
            return state;
    }
}
