import * as Action from "../constants/action";

export default function currentMessageImage(state = {}, action) {
    switch (action.type) {
        case Action.SET_CURRENT_MESSAGE_IMAGE:
            console.log("Da vao set current message image");
            console.log("Message image", action.message);
            return action.message;
        case Action.REMOVE_CURRENT_MESSAGE_IMAGE:
            return {};
        default:
            return state;
    }
};
