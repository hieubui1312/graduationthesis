import * as Action from "../constants/action";

export default function (state = [], action) {
    switch (action.type) {
        case Action.SET_DSESSIONS_SIMPLE:
            return action.dsessions;
        default:
            return state;
    }
}
