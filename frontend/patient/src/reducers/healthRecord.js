import * as Action from "../constants/action";

const updateHealthRecord = (state, item, id) => {
    console.log("New state", state)
    console.log("Item", item);
    for(let i = 0; i < state.length; i++) {
        if (state[i].id === item.id) {
            state[i] = item;
        }
    }
    console.log("State", state);
    return state;
};

export default function healthRecordReducer(state = [], action) {
    let {id, data} = action;
    let newState = [...state];

    switch (action.type) {
        case Action.INITIAL_HEALTH_RECORD:
            return action.health_records;
        case Action.ADD_HEALTH_RECORD:
            return [action.health_record, ...state];
        case Action.DELETE_HEALTH_RECORD:
            newState = newState.filter(item => item.id !== id);
            return newState;
        case Action.UPDATE_HEALTH_RECORD:
            newState = updateHealthRecord(newState, data, id);
            return newState;
        default:
            return state;
    }
}
