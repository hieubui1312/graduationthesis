import {combineReducers} from "redux";
import currentUser from "./currenUser";
import loading from "./loading";
import doctors from "./doctor";
import majors from "./major";
import dSessionTypeReducer from "./dSessionType";
import dSessionsReducer from "./dSession";
import currentDSession from "./currentDsession";
import messageDSessionReducer from "./messageDSession";
import healthRecordReducer from "./healthRecord";
import currentMessageImage from "./currentMessageImage";
import dsessionSimple from "./dsessionSimple";
import notification from "./notification";

export default combineReducers({
   currentUser,
   loading,
   doctors,
   majors,
   dSessionType: dSessionTypeReducer,
   dSessions: dSessionsReducer,
   currentDSession,
   messageDSessions: messageDSessionReducer,
   healthRecords: healthRecordReducer,
   currentMessageImage,
   dsessionSimple,
   notification
});
