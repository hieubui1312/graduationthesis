import * as Action from "../constants/action";

export default function messageDSessionReducer(state = [], action) {
    switch (action.type) {
        case Action.INITIAL_MESSAGE:
            return action.messages;
        case Action.ADD_MESSAGE:
            return [...state, action.message];
        default:
            return  state
    }
}
