import * as Action from  "../constants/action";

export default function currentUser(state = {}, action) {
    switch (action.type) {
        case Action.SET_CURRENT_USER:
            state = {...action.currentUser};
            return state;
        case Action.UPDATE_CURRENT_USER:
            return {...state, ...action.body};
        case Action.UPDATE_TOKEN_CURRENT_USER:
            state = {...state, access_token: action.accessToken};
            return state;
        case Action.REMOVE_CURRENT_USER:
            return {};
        default:
            return state;
    }
}
