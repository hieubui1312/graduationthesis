import * as Action from "../constants/action";

export default function doctorReducer(state = [], action) {
    switch (action.type) {
        case Action.SET_DOCTORS:
            return action.doctors;
        default:
            return state
    }
}
