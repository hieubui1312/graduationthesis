import * as Action from "../constants/action";

export default function loading(state = false, action) {
    switch (action.type) {
        case Action.LOADING_ON:
            return true;
        case Action.LOADING_OFF:
            return false;
        default:
            return state;
    }
}
