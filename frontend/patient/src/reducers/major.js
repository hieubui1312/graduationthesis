import * as Action from "../constants/action";

export default function majorReducer(state = [], action){
    switch (action.type) {
        case Action.SET_MAJORS:
            return action.majors;
        default:
            return state;
    }
}
