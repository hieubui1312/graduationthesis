import * as firebase from "firebase/app";
import "firebase/messaging";
import firebaseApp from "../src/util/firebase";

const messaging = firebaseApp.messaging();
messaging.usePublicVapidKey("BJKPe5WnY0PAwIvR2LQG4a-xYjBIuL-hm7hbXe-41GR-PNTNZF7R5BWh6bF1nQwmjUOZnfg6ITEMSgTLsBHuFIw");

export { messaging };
