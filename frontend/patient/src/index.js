import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';
import "./style/common.scss";
import 'react-notifications/lib/notifications.css';
import {createStore, applyMiddleware} from "redux";
import reducer from "./reducers/index";
import {Provider} from "react-redux";
import thunk from "redux-thunk";
import logger from 'redux-logger'
import {SERVER_API} from "./constants/server_base";

if ("serviceWorker" in navigator) {
    navigator.serviceWorker
        .register("./firebase-messaging-sw.js")
        .then(function(registration) {
            console.log("Registration successful, scope is:", registration.scope);
        })
        .catch(function(err) {
            console.log("Service worker registration failed, error:", err);
        });
}

var store;


let currentUser = localStorage.getItem("currentUser");

if (currentUser) {
    currentUser = JSON.parse(currentUser);
    fetch(SERVER_API + "/patient/me/info", {
        headers: {
            'Content-Type': 'application/json',
            'Access-Token': currentUser.access_token
        }
    }).then(
        res => res.json()
    ).then(res => {
        console.log("Res currentUserv da vao", res);
        if (res.errorCode) {
            throw new Error(res.error);
        } else {
            store = createStore(reducer, {currentUser: res.user}, applyMiddleware(thunk, logger));
            initialApp(store);
        }
    }).catch(err => {
        store = createStore(reducer,{}, applyMiddleware(thunk, logger));
        initialApp(store);
    });
} else {
    store = createStore(reducer,{}, applyMiddleware(thunk, logger));
    initialApp(store);
}

function initialApp(store){
    ReactDOM.render(<Provider store={store}>
        <App />
    </Provider>, document.getElementById('root'));
}

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
