import {SERVER_API} from "../config/server";
import {removeAccessToken, setAccessToken} from "./access_token";
import {error} from "winston";

export const callApi = (path, access_token, successFunc, errorFunc, body, method = "GET") => {
    return async (dispatch) => {
        let fetchOptions = {
            headers: {
                'Content-Type': 'application/json',
                'Access-Token': access_token
            },
            method
        };
        if (body) {
            fetchOptions.body = JSON.stringify(body);
            fetchOptions.method = "POST";
        }

        try{
            let result = await fetch(`${SERVER_API}/${path}`, fetchOptions);
            if (result.status === 401) {
                dispatch(removeAccessToken());
            }
            result = await result.json();
            if (result.errorCode) {
                throw new Error(result.error ?? "Unknown error");
            }
            if (result.access_token) {
                dispatch(setAccessToken(result.access_token))
            }
            successFunc(result, dispatch);
        } catch (e) {
            errorFunc(e);
        }
    }
};
