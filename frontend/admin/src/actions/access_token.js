import * as ActionType from "../config/action";

export const setAccessToken = (access_token) => {
    localStorage.setItem("access_token", access_token);
    return {
        type: ActionType.SET_ACCESS_TOKEN,
        token: access_token
    }
};

export const removeAccessToken = () => {
    return {
        type: ActionType.REMOVE_ACCESS_TOKEN
    }
};

