import {Degree} from "../config/doctor";
import {Form} from "react-bootstrap";
import React from "react";

export const getDegree = (degree) => {
    switch (degree) {
        case Degree.BS:
            return "Bác sĩ";
        case Degree.BS_CK1:
            return "Bác sĩ chuyên khoa 1";
        case Degree.BS_CK2:
            return "Bác sĩ chuyên khoa 2";
        case Degree.ThS_BS:
            return "Thạc sĩ bác sĩ";
        case Degree.PGS_TS_BS:
            return "Phó giáo sư tiến sĩ bác sĩ";
        case Degree.TS_BS:
            return "Tiến sĩ bác sĩ";
        case Degree.GS_TS_BS:
            return "Giáo sư tiến sĩ bác sĩ";
        default:
            return "Bác sĩ";
    }
};

export const selectDegree = (name, value, onChange) => {
    return <Form.Control as={"select"} name={name} value={value} onChange={onChange} >
        <option value={Degree.BS}>
            Bác sĩ
        </option>
        <option value={Degree.ThS_BS}>
            Thạc sĩ, bác sĩ
        </option>
        <option value={Degree.BS_CK1}>
            Bác sĩ chuyên khoa 1
        </option>
        <option value={Degree.BS_CK2}>
            Bác sĩ chuyên khoa 2
        </option>
        <option value={Degree.GS_TS_BS}>
            Giáo sư, tiến sĩ, bác sĩ
        </option>
        <option value={Degree.PGS_TS_BS}>
            Phó giáo sư, tiến sĩ, bác sĩ
        </option>
        <option value={Degree.TS_BS}>
            Tiến sĩ, bác sĩ
        </option>
    </Form.Control>
};
