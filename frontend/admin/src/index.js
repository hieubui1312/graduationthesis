import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-notifications/lib/notifications.css';
import {applyMiddleware, createStore} from "redux";
import reducer from "./reducers";
import thunk from "redux-thunk";
import {Provider} from "react-redux";

var store;
let accessToken = localStorage.getItem("access_token");
if (accessToken) {
    store = createStore(reducer, {accessToken: accessToken}, applyMiddleware(thunk));
} else {
    store = createStore(reducer, {accessToken}, applyMiddleware(thunk));
}
console.log("State", store.getState());

ReactDOM.render(
  <Provider store={store}>
      <React.StrictMode>
          <App />
      </React.StrictMode>
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
