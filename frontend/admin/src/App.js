import React from 'react';
import logo from './logo.svg';
import './App.css';
import Header from "./components/header";
import {useSelector} from "react-redux";
import Login from "./containers/login";
import {NotificationContainer} from "react-notifications";
import Home from "./components/home";

function App() {
    const accessToken = useSelector(state => state.accessToken);
    return (
        <div>
            {
                accessToken ? <Home/>:<Login />
            }
            <NotificationContainer />
        </div>
    );
}


export default App;
