import React from "react";
import {Navbar, Nav, NavDropdown} from "react-bootstrap";
import "./index.scss";
import {useDispatch} from "react-redux";
import {removeAccessToken} from "../../actions/access_token";

function Header(){
    const dispatch = useDispatch();

    function logout(){
        dispatch(removeAccessToken())
    }

    return <Navbar bg="primary" variant={"dark"} className={"mb-5"} expand="lg">
        <Navbar.Brand href="#home">
            PatientAccess
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
                <Nav.Link href="#home">Home</Nav.Link>
                <NavDropdown title="Quản lí bệnh nhân" id="basic-nav-dropdown">
                    <NavDropdown.Item href="/patient-list/1">Danh sách bệnh nhân</NavDropdown.Item>
                    <NavDropdown.Item href="/dsession-list/1">Danh sách phiên khám</NavDropdown.Item>
                </NavDropdown>
                <NavDropdown title="Quản lí bác sĩ" id="basic-nav-dropdown">
                    <NavDropdown.Item href="/doctor-list/1">Danh sách bác sĩ</NavDropdown.Item>
                    <NavDropdown.Item href="/doctor-create">Thêm bác sĩ</NavDropdown.Item>
                    <NavDropdown.Item href={"/doctor-analytic"}>Thống kê hiệu quả bác sĩ</NavDropdown.Item>
                </NavDropdown>
                <NavDropdown title="Quản lí giao dịch" id="basic-nav-dropdown">
                    <NavDropdown.Item href="/credit-change-log/1">Danh sách giao dịch</NavDropdown.Item>
                    <NavDropdown.Item href="/payment/1">Quản lí nạp tiền</NavDropdown.Item>
                    <NavDropdown.Item href="/withdraw/1">Yêu cầu rút tiền</NavDropdown.Item>
                </NavDropdown>
                <NavDropdown title="Quản lí gói khám" id="basic-nav-dropdown">
                    <NavDropdown.Item href="/dsession-type/list">Danh sách gói khám</NavDropdown.Item>
                    <NavDropdown.Item href="/dsession-type/create">Thêm gói khám</NavDropdown.Item>
                    <NavDropdown.Item href="/dsession-fee">Định gía gói khám</NavDropdown.Item>
                </NavDropdown>
            </Nav>
        </Navbar.Collapse>
        <div>
            <a href="#" onClick={logout} className={"logout"}>Đăng xuất</a>
        </div>
    </Navbar>
}

export default Header;
