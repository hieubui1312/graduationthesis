import React from "react";
import {Modal, Button} from "react-bootstrap";

const WithDrawAction = ({handleClose, id, decline, accept}) => {
    return <div className="withdraw-action">
        <Modal show={id.length > 0} onHide={handleClose}>
            <Modal.Body>
                Xử lý yêu cầu rút tiền
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={() => decline(id)}>
                    Từ chối
                </Button>
                <Button variant="primary" onClick={() => accept(id)}>
                    Chấp nhận
                </Button>
            </Modal.Footer>
        </Modal>
    </div>
};

export default WithDrawAction;
