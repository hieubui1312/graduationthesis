import React from "react";
import {Container, Row, Table} from "react-bootstrap";

const DoctorAnalytic = ({doctors}) => {

    function getData(){
        return doctors.map((doctor, index) => {
          return <tr key={`doctor-${index}`}>
              <td>{index + 1}</td>
              <td>{doctor.name}</td>
              <td>{doctor.all}</td>
              <td>{doctor.accept}</td>
              <td>{doctor.complete}</td>
              <td>{doctor.decline}</td>
          </tr>
        })
    }

    return <div className="doctor-analytic">
        <Container fluid>
            <Row>
                <h3>Thống kê hiệu quả bác sĩ</h3>
                <Table striped bordered hover>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Bác sĩ</th>
                        <th>Tất cả phiên khám</th>
                        <th>Phiên đang khám</th>
                        <th>Phiên khám hoàn thành</th>
                        <th>Phiên khám từ chối</th>
                    </tr>
                    </thead>
                    <tbody>
                        {getData()}
                    </tbody>
                </Table>
            </Row>
        </Container>
    </div>
};

export default DoctorAnalytic;
