import React, {useState} from "react";
import {Button, Container, Form} from "react-bootstrap";
import "./index.scss";

const Login = ({submit}) => {
    const [info, setInfo] = useState({});

    function onChange(e) {
        let {value, name} = e.target;
        setInfo({
            ...info,
            [name]: value
        })
    }

    function onSubmit(e){
        e.preventDefault();
        submit(info);
    }

    return <div className="login">
        <Container m={"5"}>
            <Form className={"p-3"} onSubmit={onSubmit}>
                <Form.Group className={"text-center"}>
                    <Form.Label >
                        <b>Đăng nhập</b>
                    </Form.Label>
                </Form.Group>
                <Form.Group>
                    <Form.Label>
                        Email
                    </Form.Label>
                    <Form.Control type={"email"}
                                  placeholder={"Nhập email của bạn"}
                                  name={"email"}
                                  value={info.email ?? ""}
                                  onChange={onChange}
                                  required
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>
                        Password
                    </Form.Label>
                    <Form.Control type={"password"}
                                  placeholder={"Nhập password của bạn"}
                                  value={info.password ?? ""}
                                  name={"password"}
                                  onChange={onChange}
                                  required
                    />
                </Form.Group>
                <Form.Group className={"text-right"}>
                    <Button variant={"primary"} type={"submit"}>
                        Submit
                    </Button>
                </Form.Group>
            </Form>
        </Container>
    </div>
};

export default Login;
