import React from "react";
import {Container, Row} from "react-bootstrap";
import {Table} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEdit} from "@fortawesome/free-solid-svg-icons";
import InputUpdate from "../input-update";
import dateformat from "dateformat";
import {getDegree} from "../../util/function";
import {Link} from "react-router-dom";

const RowInfo = ({title, value = ""}) => {
    return <tr>
        <td>
            <b>{title}</b>
        </td>
        <td>{value}</td>
    </tr>
};
const DoctorDetail = ({user, doctor}) => {

    function formatCertDate(certDate){
        if (certDate)
        return dateformat(new Date(certDate), "dd/mm/yyyy");
    }

    return <Container fluid>
        <Row>
            <h3 className={"mb-3"}>Thông tin bác sĩ</h3>
            <Table striped bordered hover size="sm">
                <tbody>
                    <RowInfo title={"Họ tên"}
                             value={user.name}/>
                    <RowInfo title={"Tuổi"}
                             value={user.phone}/>
                    <RowInfo title={"Giới tính"}
                             value={user.sex ? "Nam" : "Nữ"}/>
                    <RowInfo title={"Email"}
                             value={user.email}/>
                    <RowInfo title={"Địa chỉ"}
                             value={user.address}/>
                    <RowInfo title={"Avatar"}
                             value={user.avatar ? <img style={{objectFit: "cover", width: "200px"}} src={user.avatar} alt={"Avatar doctor"}/> : "Trống"}
                    />
                    <RowInfo title={"Trạng thái"}
                             value={user.activated ?
                                 <span className={"badge badge-success"}>Actice</span> :
                                 <span className={"badge badge-warning"}>Non-Actice</span>
                             }
                    />
                    <RowInfo title={"Nơi làm việc"}
                             value={doctor.Hospital ? doctor.Hospital.name : "Không có"}
                    />
                    <RowInfo title={"Bằng cấp"}
                             value={doctor.degree ? getDegree(parseInt(doctor.degree)) : ""}
                    />
                    <RowInfo title={"Ngày cấp giấy chứng chỉ hành nghề"}
                             value={formatCertDate(doctor.cert_date)}
                    />
                    <RowInfo title={"ATM number"}
                             value={doctor.atm_number}
                    />
                    <RowInfo title={"ATM info"}
                             value={doctor.atm_info}
                    />
                    <RowInfo title={"Kinh nghiệm"}
                             value={doctor.experience}
                    />
                    <RowInfo title={"Mô tả bằng cấp"}
                             value={doctor.degree_text}
                    />
                    <RowInfo title={"Chứng chỉ"}
                             value={doctor.certificate}
                    />
                    <RowInfo title={"Text giới thiệu"}
                             value={doctor.intro_text}
                    />
                    <RowInfo title={"Chuyên ngành"}
                             value={doctor.majors}
                    />
                </tbody>
            </Table>
            <div className={"text-right"}>
                <Link to={`/doctor-update/${user.id}`} className={"btn btn-primary"}>
                    Update
                </Link>
            </div>
        </Row>
    </Container>
};

export default DoctorDetail;
