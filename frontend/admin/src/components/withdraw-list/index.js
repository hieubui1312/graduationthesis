import React from "react";
import {Container, Row, Table} from "react-bootstrap";
import dateFormat from "dateformat";
import PaginationElement from "../pagination";

const WithDrawList = ({withdraws, pages, active, choose}) => {

    function getData(){
        return withdraws.map((item, index) => {
            return <tr key={`withdraw-${index}`}>
                <td>{index + 1}</td>
                <td>{item.User.name}</td>
                <td>{item.amount}</td>
                <td>{getStatus(item)}</td>
                <td>{getCreatedAt(item.created_at)}</td>
            </tr>
        })
    }

    function getStatus(item) {
        switch (item.status) {
            case 1:
                return <button className={"btn btn-primary"} onClick={() => choose(item.id)} >
                    Đang chờ
                </button>;
            case 2:
                return <button className={"btn btn-success"}>
                    Đã xử lí
                </button>;
            case 3:
                return <button className={"btn btn-warning"}>
                    Đã từ chối
                </button>;
        }
    }

    function getCreatedAt(created_at){
        if (created_at)return dateFormat(new Date(created_at), "yyyy:mm:dd hh:MM:ss")
    }

    return <div className="withdraw-list">
        <Container fluid>
            <Row>
                <h3 className={"mb-3"}>Danh sách yêu cầu rút tiền</h3>
                <Table striped bordered hover>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Bác sĩ</th>
                        <th>Số tiền</th>
                        <th>Trạng thái</th>
                        <th>Thời gian</th>
                    </tr>
                    </thead>
                    <tbody>
                    {getData()}
                    </tbody>
                </Table>
            </Row>
            <div className={"d-flex align-item-center justify-content-center"}>
                <PaginationElement
                    pages={pages}
                    active={active}
                />
            </div>
        </Container>
    </div>
};

export default WithDrawList;
