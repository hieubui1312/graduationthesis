import React from "react";
import {Container, Row, Table} from "react-bootstrap";
import {Link} from "react-router-dom";

const DSessionTypeList = ({types, selectDelete}) => {
    function getData(){
        return types.map((type, index) => {
            return <tr key={`type-${index}`}>
                <td>{index + 1}</td>
                <td>{type.name}</td>
                <td>{type.time / 86400} ngày</td>
                <td>{type.description}</td>
                <td>{getAction(type)}</td>
            </tr>
        })
    }

    function getAction(type){
        return <div>
            <Link to={`/dsession-type/update/${type.id}`} className={"btn btn-primary mr-md-2"}>Update</Link>
            <button className={"btn btn-primary"} onClick={() => selectDelete(type.id)}>Delete</button>
        </div>
    }

    return <div className="dsession-type-list">
        <Container fluid>
            <Row>
                <h3>Danh sách gói khám</h3>
                <Table striped bordered hover>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Tên gói khám</th>
                        <th>Thời gian</th>
                        <th>Miêu tả</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    {getData()}
                    </tbody>
                </Table>
            </Row>
        </Container>
    </div>
};

export default DSessionTypeList;
