import React from "react";
import {Container, Row} from "react-bootstrap";
import {Table} from "react-bootstrap";
import dateFormat from "dateformat";
import PaginationElement from "../pagination";

const Payment = ({payments, pages, active, phone}) => {
    function getPayment() {
        return payments.map((item, index) => {
            return <tr key={`payment-${index}`}>
                <td>{index + 1}</td>
                <td>{item.User.name}</td>
                <td>{item.User.phone}</td>
                <td>{item.amount}</td>
                <td>{getCreatedAt(item.created_at)}</td>
            </tr>
        });
    }

    function getCreatedAt(date){
        if (date){
            return dateFormat(new Date(date), "yyyy:mm:dd hh:MM:ss");
        }
    }

    return <div className={"payment"}>
        <Container fluid>
                <h3>Danh sách nạp tiền</h3>
                <form method={"GET"} action={`/payment/1`}>
                    <div className="form-group">
                        <label>Số điện thoại</label>
                        <input className={"form-control"} name={"phone"} type={"text"} defaultValue={phone}/>
                    </div>
                    <div className="form-group">
                        <button className={"btn btn-primary"}>Tìm kiếm</button>
                    </div>
                </form>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Tên User</th>
                            <th>Số điện thoại</th>
                            <th>Số tiền</th>
                            <th>Thời gian</th>
                        </tr>
                    </thead>
                    <tbody>
                    {getPayment()}
                    </tbody>
                </Table>
            <div className={"d-flex align-item-center justify-content-center"}>
                <PaginationElement
                    pages={pages}
                    active={active}
                />
            </div>
        </Container>
    </div>
};

export default Payment;
