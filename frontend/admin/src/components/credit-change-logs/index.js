import React from "react";
import {Container, Row, Table} from "react-bootstrap";
import dateformat from "dateformat";
import PaginationElement from "../pagination";

const CreditChangeLogs = ({logs, pages, active, phone}) => {
    function getData(){
        return logs.map((item, index) => {
            return <tr key={`credit-${index}`}>
                <td>{index + 1}</td>
                <td>{item.User.name}</td>
                <td>{item.User.phone}</td>
                <td>{item.credit_from}</td>
                <td>{item.credit_to}</td>
                <td>{getCreditType(item.type)}</td>
                <td>{item.reason}</td>
                <td>{getDateCreate(item.created_at)}</td>
            </tr>
        });
    }

    function getDateCreate(date) {
        if (date)
            return dateformat(new Date(date), "yyyy/mm/dd hh:MM:ss");
    }

    function getCreditType(type){
        switch (type) {
            case 1:
                return "Tạo phiên khám";
            case 2:
                return "Khám bệnh";
            case 3:
                return "Nạp điểm";
            case 4:
                return "Rút tiền";
        }
    }

    return <div className="transaction-list">
      <Container fluid>
          <h3 className={"mb-2"}>Biến động số dư</h3>
          <form method={"GET"} action={"/credit-change-log/1"}>
              <div className="form-group">
                  <label>Số điện thoại</label>
                  <input className={"form-control"} name={"phone"} type={"text"} defaultValue={phone}/>
              </div>
              <div className="form-group">
                  <button className={"btn btn-primary"}>
                      Tìm kiếm
                  </button>
              </div>
          </form>
          <Row>
              <Table striped bordered hover>
                  <thead>
                  <tr>
                      <th>#</th>
                      <th>User</th>
                      <th>Số điện thoại</th>
                      <th>Số dư ban đầu</th>
                      <th>Số dư sau thay đổi</th>
                      <th>Loại giao dịch</th>
                      <th>Mô tả</th>
                      <th>Thời gian</th>
                  </tr>
                  </thead>
                  <tbody>
                  {getData()}
                  </tbody>
              </Table>
          </Row>
          <div className={"d-flex justify-content-center align-item-center"}>
              <PaginationElement
                  pages={pages}
                  active={active}
              />
          </div>
      </Container>
  </div>
};

export default CreditChangeLogs;
