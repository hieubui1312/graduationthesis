import React from "react";
import {Container, Row, Table} from "react-bootstrap";
import PaginationElement from "../pagination";

const PatientList = ({patients, pages, active, toggleActive, phone}) => {
    function getList(){
        return patients.map((patient, index) => {
            return <tr key={`patient-${index}`}>
                <td>{index + 1}</td>
                <td>{patient.name}</td>
                <td>{patient.phone}</td>
                <td>{patient.email}</td>
                <td>{patient.address}</td>
                <td>{patient.credit}</td>
                <td>{getButtonActive(patient)}</td>
            </tr>
        });
    }

    function getButtonActive(patient) {
        if (patient.activated) {
            return <button className={"btn btn-primary"}
                           onClick={() => toggleActive(patient)}
            >
                Active
            </button>
        } else {
            return <button className={"btn btn-warning"}
                           onClick={() => toggleActive(patient)}
            >
                Non-Active
            </button>
        }
    }

    return <div className="patient-list">
        <Container fluid>
            <h3 className={"mb-3"}>Danh sách bệnh nhân</h3>
            <form action={'/patient-list/1'} method={"GET"}>
                <div className="form-group">
                    <label>Số điện thoại</label>
                    <input className={"form-control"} defaultValue={phone} name={"phone"} type={"text"}/>
                </div>
                <div className="form-group">
                    <button className={"btn btn-primary"}>Tìm kiếm</button>
                </div>
            </form>
            <Row>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Tên đăng nhập</th>
                            <th>Số điện thoại</th>
                            <th>Email</th>
                            <th>Địa chỉ</th>
                            <th>Tài khoản</th>
                            <th>Trạng thái</th>
                        </tr>
                    </thead>
                    <tbody>
                        {getList()}
                    </tbody>
                </Table>
            </Row>
            <div className={"d-flex align-item-center justify-content-center"}>
                <PaginationElement
                    pages={pages}
                    active={active}
                />
            </div>
        </Container>
    </div>
};

export default PatientList;
