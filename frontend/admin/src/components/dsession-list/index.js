import React from "react";
import {Container, Row, Table} from "react-bootstrap";
import dateFormat from "dateformat";
import PaginationElement from "../pagination";
import {STATUS} from "../../config/dsession";

const DSessionList = ({dsessions, pages, active}) => {
    function getDSessionStatus(status){
        switch (status) {
            case STATUS.STATUS_ACCEPT:
                return <button className={"btn btn-primary"}>
                    Đang khám
                </button>;
            case STATUS.STATUS_CANCEL:
                return <button className={"btn btn-danger"}>
                    Đã huỷ
                </button>
            case STATUS.STATUS_COMPLETE:
                return <button className={"btn btn-success"}>
                    Hoàn thành
                </button>
            case STATUS.STATUS_DECLINE:
                return <button className={"btn btn-danger"}>
                    Từ chối
                </button>
            case STATUS.STATUS_WAITING:
                return <button className={"btn btn-primary"}>
                    Đang chờ
                </button>
        }
    }

    function getData() {
        return dsessions.map((dsession, index) => {
            return <tr key={`dsession-${index}`}>
                <td>{index + 1}</td>
                <td>{dsession.status ? getDSessionStatus(dsession.status) : ""}</td>
                <td>{getTimeCreated(dsession.created_at)}</td>
                <td>{dsession.User ? dsession.User.name : ""}</td>
                <td>{dsession.Doctor ? dsession.Doctor.name : ""}</td>
                <td>{dsession.id}</td>
            </tr>
        })
    }

    function getTimeCreated(created_at){
        if (created_at) {
            return dateFormat(new Date(created_at), "yyyy:mm:dd hh:MM:ss")
        }
    }

    return <div className="dsession-list">
        <Container fluid>
            <Row>
                <h3 className={"mb-3"}>Danh sách phiên khám</h3>
                <Table striped bordered hover>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Trạng thái</th>
                        <th>Thời gian tạo phiên</th>
                        <th>Bệnh nhân</th>
                        <th>Bác sĩ</th>
                        <th>Mã phiên khám</th>
                    </tr>
                    </thead>
                    <tbody>
                    {getData()}
                    </tbody>
                </Table>
            </Row>
            <div className={"d-flex justify-content-center align-items-center"}>
                <PaginationElement
                    pages={pages}
                    active={active}
                />
            </div>
        </Container>
    </div>
};

export default DSessionList;
