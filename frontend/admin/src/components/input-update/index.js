import React, {useState} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEdit} from "@fortawesome/free-solid-svg-icons";

const InputUpdate = ({title, value, onChange, input_type = "text", options = []}) => {
    const [isEdit, setIsEdit] = useState(false);

    function getBtnAction() {
        if (isEdit) return <div>
            <button className={"btn btn-success btn-sm mr-2"}>
                Sửa
            </button>
            <button className={"btn btn-danger btn-sm"} onClick={() => setIsEdit(false)}>
                Hoàn tác
            </button>
        </div>;
        else return <FontAwesomeIcon icon={faEdit}
                                     onClick={() => setIsEdit(true)}
        />;
    }

    function getValue(){
        if (isEdit) return getInput();
        else return <div>{value}</div>
    }

    function getInput() {
        switch (input_type) {
            case "textarea":
                return <textarea className={"form-control"}
                                 value={value}
                                 onChange={onChange}
                />;
            case "select":
                return <select className={"form-control"} onChange={onChange}>
                    {getOptions()}
                </select>;
            case "input_number":
                return <input type={"number"} className={"form-control"}
                              value={value}
                              onChange={onChange}
                />;
            default:
                return <input type={"text"} className={"form-control"}
                              value={value}
                              onChange={onChange}
                />
        }
    }

    function getOptions(){
        return options.map((option, key) => {
            return <option value={option.value}
                           selected={option.value === value}
                           key={`option-${key}`} >
                {option.label}
            </option>
        })
    }

    return <tr>
        <td>
            <b>{title}</b>
        </td>
        <td>{getValue()}</td>
        <td>
            {getBtnAction()}
        </td>
    </tr>
};

export default InputUpdate;
