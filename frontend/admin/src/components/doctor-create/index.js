import React from "react";
import {Button, Container, Form, Row} from "react-bootstrap";
import {selectDegree} from "../../util/function";


const DoctorCreate = ({user, doctor, onChangeUser, onChangeDoctor, title, majors, checked, uploadImage, submit, hospitals}) => {
    function getMajorCheckBox(){
        return majors.map((major, key) => {
            return <div className={"col-3"} key={`major-${key}`}>
                <input type={"checkbox"} value={major.name} checked={checkMajor(major.name)} onChange={checked} />
                <label className={"ml-md-2"}>{major.name}</label>
            </div>
        })
    }

    function checkMajor(major) {
        if (!doctor.majors) return false;
        let _major = JSON.parse(doctor.majors);
        return _major.indexOf(major) !== -1;
    }

    function getHospital(){
        return <Form.Control as={"select"} value={doctor.hospital_id ?? ""} name={"hospital_id"} onChange={onChangeDoctor} >
            {hospitals.map((item, index) => {
                return <option value={item.id} key={`hospital-${index}`}>
                    {item.name}
                </option>;
            })}
        </Form.Control>;
    }

    return <div className="doctor-update">
        <Container fluid>
            <Row>
                <h3>{title}</h3>
            </Row>
            <Form onSubmit={submit}>
                <Form.Group>
                    <Form.Label>
                        Họ tên
                    </Form.Label>
                    <Form.Control type={"text"}
                                  value={user.name ?? ""}
                                  name={"name"}
                                  onChange={onChangeUser}
                                  required
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>
                        Số điện thoại
                    </Form.Label>
                    <Form.Control type={"text"}
                                  value={user.phone ?? ""}
                                  name={"phone"}
                                  onChange={onChangeUser}
                                  required
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>
                        Tuổi
                    </Form.Label>
                    <Form.Control type={"number"}
                                  value={user.age ?? ""}
                                  name={"age"}
                                  onChange={onChangeUser}
                                  required
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>
                        Giới tính
                    </Form.Label>
                    <Form.Control as={"select"} value={user.sex ?? "male"} name={"sex"} onChange={onChangeUser} >
                        <option value={"male"}>Nam</option>
                        <option value={"female"}>Nữ</option>
                    </Form.Control>
                </Form.Group>
                <Form.Group>
                    <Form.Label>
                        Email
                    </Form.Label>
                    <Form.Control type={"email"}
                                  value={user.email ?? ""}
                                  name={"email"}
                                  onChange={onChangeUser}
                                  required
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>
                        Địa chỉ
                    </Form.Label>
                    <Form.Control type={"text"}
                                  value={user.address ?? ""}
                                  name={"address"}
                                  onChange={onChangeUser}
                                  required
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>
                        Avatar
                    </Form.Label>
                    <div>
                        {user.avatar ? <img style={{width: "150px", height: "150px", objectFit: "cover"}}
                                            src={user.avatar ?? ""}
                                            alt={"Doctor avatar"}
                        /> : ""}
                    </div>
                    <Form.Control type={"file"} onChange={uploadImage} />
                </Form.Group>
                <Form.Group>
                    <Form.Label>
                        Bằng cấp
                    </Form.Label>
                    {selectDegree("degree", parseInt(doctor.degree), onChangeDoctor)}
                </Form.Group>
                <Form.Group>
                    <Form.Label>
                        Nơi làm việc
                    </Form.Label>
                    {getHospital()}
                </Form.Group>
                <Form.Group>
                    <Form.Label>
                        Số năm kinh nghiệm
                    </Form.Label>
                    <Form.Control type={"text"}
                                  value={doctor.experience_year ?? ""}
                                  name={"experience_year"}
                                  onChange={onChangeDoctor}
                                  required
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>
                        ATM number
                    </Form.Label>
                    <Form.Control type={"text"}
                                  value={doctor.atm_number ?? ""}
                                  name={"atm_number"}
                                  onChange={onChangeDoctor}
                                  required
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>
                        ATM info
                    </Form.Label>
                    <Form.Control type={"text"}
                                  value={doctor.atm_info ?? ""}
                                  name={"atm_info"}
                                  onChange={onChangeDoctor}
                                  required
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>
                        Kinh nghiệm
                    </Form.Label>
                    <textarea className={"form-control"}
                              value={doctor.experience ?? ""}
                              name={"experience"}
                              onChange={onChangeDoctor}
                              required
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>
                        Bằng cấp
                    </Form.Label>
                    <textarea className={"form-control"}
                              value={doctor.degree_text ?? ""}
                              name={"degree_text"}
                              onChange={onChangeDoctor}
                              required
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>
                        Chứng chỉ
                    </Form.Label>
                    <textarea className={"form-control"}
                              value={doctor.certificate ?? ""}
                              name={"certificate"}
                              onChange={onChangeDoctor}
                              required
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>
                        Text giới thiệu
                    </Form.Label>
                    <textarea className={"form-control"}
                              value={doctor.intro_text ?? ""}
                              name={"intro_text"}
                              onChange={onChangeDoctor}
                              required
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>
                        Chuyên ngành
                    </Form.Label>
                    <div className={"row"}>
                        {getMajorCheckBox()}
                    </div>
                </Form.Group>
                <Button type={"submit"}>
                    Update
                </Button>
            </Form>
        </Container>
    </div>
};

export default DoctorCreate;
