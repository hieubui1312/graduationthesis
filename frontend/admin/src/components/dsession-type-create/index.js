import React from "react";
import {Container} from "react-bootstrap";

const DSessionTypeCreate = ({body, submit, onChange}) => {
    return <div className="dsession-type-create">
        <Container>
            <h3>Tạo gói khám</h3>
            <form onSubmit={submit}>
                <div className="form-group">
                    <label>Tên gói khám</label>
                    <input type={"text"}
                           value={body.name ?? ""}
                           className={"form-control"}
                           name={"name"}
                           onChange={onChange}
                           required
                    />
                </div>
                <div className="form-group">
                    <label>Thời gian</label>
                    <input type={"number"}
                           value={body.time ? body.time / 86400 : ""}
                           className={"form-control"}
                           name={"time"}
                           onChange={onChange}
                           required
                    />
                </div>
                <div className="form-group">
                    <label>Mô tả</label>
                    <input type={"text"}
                           className={"form-control"}
                           name={"description"}
                           onChange={onChange}
                           required
                           value={body.description ?? ""}
                    />
                </div>
                <button type={"submit"}>
                    Tạo gói khám
                </button>
            </form>
        </Container>
    </div>
};

export default DSessionTypeCreate;
