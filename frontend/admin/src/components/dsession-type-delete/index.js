import React from "react";
import {Modal, Button} from "react-bootstrap";

const DSessionTypeDelete = ({handleClose, id, deleteType}) => {
    return <div className="dsession-type-delete">
        <Modal show={id.length > 0} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>Bạn có chắc chắc muốn xoá gói khám</Modal.Title>
            </Modal.Header>
            <Modal.Footer>
                <Button variant="secondary" onClick={handleClose}>
                    Huỷ
                </Button>
                <Button variant="primary" onClick={() => deleteType(id)}>
                    Xoá
                </Button>
            </Modal.Footer>
        </Modal>
    </div>
};

export default DSessionTypeDelete;
