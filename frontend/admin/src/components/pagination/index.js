import React from "react";
import {Pagination} from "react-bootstrap";
import {Link} from "react-router-dom";
import "./index.scss";
import {useLocation} from "react-router-dom";

const PaginationElement = ({pages, active}) => {
    const location = useLocation();
    console.log("Location", location);
    const search = location.search;
    function getItem(){
        let items = [];
        for (let i = 1; i <= pages; i++) {
            items.push(<Pagination.Item key={`pagination-${i}`}
                                        active={i == active}
                                        href={JSON.stringify(i) + search}
            >{i}
            </Pagination.Item>)
        }
        return items;
    }
    const firstIndex = parseInt(active) - 1;
    const lastIndex = parseInt(active) + 1;

    return <Pagination>
        <Pagination.First href={JSON.stringify(firstIndex)}
                          disabled={parseInt(active) === 1}
        />
        {getItem()}
        <Pagination.Last
            href={JSON.stringify(lastIndex)}
                         disabled={parseInt(active) === pages}
        />
    </Pagination>
};

export default PaginationElement;
