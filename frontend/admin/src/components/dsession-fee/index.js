import React, {useState} from "react";
import {Container, Row} from "react-bootstrap";
import {Table} from "react-bootstrap";
import {useSelector} from "react-redux";

const DSessionFee = ({data, change}) => {
    const [fee, setFee] = useState(0);

    function getLevelDoctor(i){
        switch (i) {
            case 1:
                return "Bác sĩ, bác sĩ chuyên khoa 1, thạc sĩ";
            case 2:
                return "Bác sĩ chuyên khoa 2, tiến sĩ";
            case 3:
                return "Phó giáo sư, tiến sĩ";
        }
    }

    function getRow(fees, type){
        let result = [];
        for (let i = 1; i <= 3; i++) {
            result.push(<tr key={`index-${i}`}>
                <td>{getLevelDoctor(i)}</td>
                <td>
                    <input type={"text"} className={"form-control"} onChange={e => setFee(e.target.value)} required defaultValue={getFee(fees, i)}/>
                </td>
                <td>
                    <button className={"btn btn-primary"} onClick={() => onClick(type.id, i, fee)}>
                        Thay đổi
                    </button>
                </td>
            </tr>);
        }
        return result;
    }

    function onClick(dsession_type, doctor_level, fee){
        change(dsession_type, doctor_level, fee);
    }

    function getFee(fees, index){
        let fee = fees.find(item => item.doctor_level === index);
        if (fee) {
            return fee.fee;
        }
        return 0;
    }

    function getData(){
        return data.map((item, index) => {
            const {type, fees} = item;
            return <Table striped bordered hover key={`fee-${index}`}>
                <thead>
                    <tr>
                        <th colSpan={3} className={"text-center"}>{type.name}</th>
                    </tr>
                    <tr>
                        <th>Cấp bậc bác sĩ</th>
                        <th>Phí khám</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                {getRow(fees, type)}
                </tbody>
            </Table>
        });
    }

    return <div className="dsession-fee">
        <Container fluid>
            <Row>
                <h3 className={"mb-3"}>Định giá phiên khám</h3>
                {getData()}
            </Row>
        </Container>
    </div>
};

export default DSessionFee;
