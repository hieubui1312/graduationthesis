import React from "react";
import {Container, Row} from "react-bootstrap";
import {Table} from "react-bootstrap";
import PaginationElement from "../pagination";
import {Link} from "react-router-dom";

const DoctorList = ({doctors, toggleActive, pages, active}) => {
    function getData() {
        return doctors.map((doctor, index) => {
            return <tr key={`doctor-${index}`}>
                <td>{index + 1}</td>
                <td>{doctor.name}</td>
                <td>{doctor.phone}</td>
                <td>{doctor.email}</td>
                <td>{doctor.address}</td>
                <td>{getActivate(doctor)}</td>
                <td>
                    <Link to={`/doctor-detail/${doctor.id}`} className={"btn btn-primary"}>
                        Chi tiết
                    </Link>
                </td>
            </tr>
        });
    }

    function getActivate(doctor){
        if (doctor.activated) {
            return <button className={"btn btn-primary"}
                           onClick={() => onClick(doctor)}>
                Activate
            </button>
        } else return <button className={"btn btn-warning"}
                              onClick={() => onClick(doctor)}>
            Non-Active
        </button>
    }

    function onClick(doctor) {
        toggleActive(doctor)
    }

    return <div className="doctor-list">
        <Container fluid>
            <h3 className={"mb-3"}>Danh sách bác sĩ</h3>
            <Row>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Họ tên</th>
                            <th>Số điện thoại</th>
                            <th>Email</th>
                            <th>Địa chỉ</th>
                            <th>Trạng thái</th>
                            <th>Chi tiết</th>
                        </tr>
                    </thead>
                    <tbody>
                        {getData()}
                    </tbody>
                </Table>
            </Row>
            <div className={"d-flex align-item-center justify-content-center"}>
                <PaginationElement
                    pages={pages}
                    active={active}
                />
            </div>
        </Container>
    </div>
};

export default DoctorList;
