import * as ActionType from "../config/action";

export default function accessToken(state = null, action){
    switch (action.type) {
        case ActionType.SET_ACCESS_TOKEN:
            return action.token;
        case ActionType.REMOVE_ACCESS_TOKEN:
            return null;
        default:
            return state;
    }
}
