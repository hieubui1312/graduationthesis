import accessToken from "./access_token";
import {combineReducers} from "redux";

const reducer = combineReducers({
   accessToken
});

export default reducer;
