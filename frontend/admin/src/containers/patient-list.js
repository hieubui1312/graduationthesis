import React, {useEffect, useState} from "react";
import PatientList from "../components/patient-list";
import {useDispatch, useSelector} from "react-redux";
import {callApi} from "../actions";

const PatientListContainer = ({match, location}) => {
    const [patients, setPatients] = useState([]);
    const [pages, setPages] = useState(1);

    const dispatch = useDispatch();
    const accessToken = useSelector(state => state.accessToken);
    const {id} = match.params;
    const phoneNumber = new URLSearchParams(location.search).get("phone");
    console.log("Location", new URLSearchParams(location.search).get("phone"));
    console.log("Match", match);

    useEffect(() => {
        let url = `admin/patient/${id}`;
        if (phoneNumber) {
            url += `?phone=${phoneNumber}`;
        }
        dispatch(callApi(url, accessToken, result => {
            console.log("Data patient list", result);
            const {data, pages} = result;
            setPatients(data);
            setPages(pages);
        }, error => {
            console.log("Error patient list", error);
        }));
    }, []);

    function toggleActive(patient){
        dispatch(callApi(`admin/patient/toggle-active/${patient.id}`, accessToken, result => {
            updatePatientActive(patient);
        }, error => {
            console.log("Error patient toggle", error);
        }))
    }

    function updatePatientActive(patient){
        let newPatients = [...patients];
        newPatients.forEach((item) => {
            if (item.id === patient.id) item.activated = !item.activated;
        });
        setPatients(newPatients);
    }

    return <PatientList
            phone={phoneNumber}
            patients={patients}
            pages={pages}
            active={id}
            toggleActive={(patient) => toggleActive(patient)}
        />
};

export default PatientListContainer;
