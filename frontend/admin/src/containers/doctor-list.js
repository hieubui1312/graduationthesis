import React, {useEffect, useState} from "react";
import DoctorList from "../components/doctor-list";
import {useDispatch, useSelector} from "react-redux";
import {callApi} from "../actions";

const DoctorListContainer = ({match}) => {
    const dispatch = useDispatch();
    const accessToken = useSelector(state => state.accessToken);

    const [doctors, setDoctors] = useState([]);
    const [pages, setPages] = useState(1);
    const {id} = match.params;

    useEffect(() => {
        dispatch(callApi(`admin/doctors/${id}`, accessToken, data => {
            setDoctors(data.doctors);
            setPages(data.pages);
        }, error => {
            console.log("Error fetch doctor", error);
        }))
    }, []);

    function toggleActive(doctor) {
        dispatch(callApi(`admin/doctor/toggle-active/${doctor.id}`, accessToken, data => {
            changeIsActive(doctor);
        }, error => {
            console.log("Error toggle doctor", error);
        }))
    }

    function changeIsActive(doctor){
        let _doctors = doctors.map(item => {
            if (item.id === doctor.id) {
                item.activated = !item.activated;
            }
            return item;
        });
        setDoctors(_doctors);
    }

    return <DoctorList doctors={doctors}
                       toggleActive={(doctor) => toggleActive(doctor)}
                       pages={pages}
                       active={id}
    />
};

export default DoctorListContainer;
