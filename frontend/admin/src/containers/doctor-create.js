import React, {useEffect, useState} from "react";
import DoctorUpdate from "../components/doctor-update";
import {callApi} from "../actions";
import {useDispatch, useSelector} from "react-redux";
import DoctorCreate from "../components/doctor-create";
import {NotificationManager} from "react-notifications";
import {useHistory} from "react-router-dom";
import {SERVER_API} from "../config/server";

const DoctorCreateContainer = () => {
    const [user, setUser] = useState({sex:"male"});
    const [doctor, setDoctor] = useState({degree: "1", hospital_id: 1});
    const [majors, setMajors] = useState([]);
    const [hospitals, setHospitals] = useState([]);

    const accessToken = useSelector(state => state.accessToken);
    const dispatch = useDispatch();
    const history = useHistory();

    useEffect(() => {
        dispatch(callApi("admin/majors", accessToken, data => {
            setMajors(data);
        }, error => {
            console.log("Error fetch major", error);
        }));

        dispatch(callApi("admin/hospitals", accessToken, data => {
            setHospitals(data);
        }, error => {
            console.log("Error hospital", error);
        }));
    }, []);

    function onChangeUser(e){
        let {name, value} = e.target;
        setUser({
            ...user, [name]: value
        })
    }

    function onChangeDoctor(e){
        let {name, value} = e.target;
        setDoctor({
            ...doctor, [name]: value
        })
    }

    function checked(e){
        let _majors = doctor.majors;
        if (!_majors) _majors = [];
        else _majors = JSON.parse(_majors);
        let {checked, value} = e.target;
        if (checked) {
            _majors.push(value);
        } else {
            let index = _majors.indexOf(value);
            _majors.splice(index, 1);
        }
        setDoctor({...doctor, majors: JSON.stringify(_majors)});
    }

    function submit(e){
        e.preventDefault();
        let _user = {...user, age: parseInt(user.age)};
        dispatch(callApi('admin/doctor/create', accessToken, data => {
            NotificationManager.success("Tạo bác sĩ thành công", "Thông báo");
            history.push("/doctor-list/1");
        }, error => {
            NotificationManager.error("Email/SĐT đã được sử dụng", "Thông báo");
        }, {
            user: _user,
            doctor
        }))
    }

    function uploadImage(e){
        let file = e.target.files[0];
        const data = new FormData();
        data.append("file", file);
        fetch(`${SERVER_API}/common/upload`, {
            method: "POST",
            body: data
        }).then(data => {
            return data.json();
        }).then(data => {
            let url = data.fileUrl;
            console.log("Url", url);
            setUser({...user, avatar: url})
        })
    }

    return <DoctorCreate user={user}
                         title={"Tạo tài khoản bác sĩ"}
                         doctor={doctor}
                         majors={majors}
                         onChangeUser={onChangeUser}
                         onChangeDoctor={onChangeDoctor}
                         checked={checked}
                         submit={submit}
                         hospitals={hospitals}
                         uploadImage={uploadImage}
    />
};

export default DoctorCreateContainer;
