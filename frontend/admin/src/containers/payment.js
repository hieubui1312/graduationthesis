import React, {useEffect, useState} from "react";
import Payment from "../components/payment";
import {useDispatch, useSelector} from "react-redux";
import {callApi} from "../actions";

const PaymentContainer = ({match, location}) => {
    const dispatch = useDispatch();
    const accessToken = useSelector(state => state.accessToken);
    const {id} = match.params;
    const phoneNumber = new URLSearchParams(location.search).get("phone");
    const [payments, setPayments] = useState([]);
    const [pages, setPages] = useState(1);

    useEffect(() => {
        let url = `admin/payment/${id}`;
        if (phoneNumber) url += `?phone=${phoneNumber}`;
        dispatch(callApi(url, accessToken, result => {
            console.log("Data payment", result);
            setPayments(result.data);
            setPages(result.pages);
        }, error => {
            console.log("Error", error);
        }))
    }, []);

    return <Payment payments={payments}
                    pages={pages}
                    active={id}
                    phone={phoneNumber}
    />
};

export default PaymentContainer;
