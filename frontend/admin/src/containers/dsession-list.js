import React, {useEffect, useState} from "react";
import DSessionList from "../components/dsession-list";
import {useDispatch, useSelector} from "react-redux";
import {callApi} from "../actions";
import {STATUS} from "../config/dsession";

const DSessionListContainer = ({match}) => {
    const dispatch = useDispatch();
    const accessToken = useSelector(state => state.accessToken);
    const {id} = match.params;

    const [dsessions, setDsessions] = useState([]);
    const [pages, setPages] = useState(1);


    useEffect(() => {
        dispatch(callApi(`admin/dsession/${id}`, accessToken, data => {
            console.log("Data dsession", data.dsessions);
            setDsessions(data.dsessions);
            setPages(data.pages);
        }, error => {
            console.log("Error ", error);
        }))
    }, []);
    return <DSessionList dsessions={dsessions}
                         pages={pages}
                         active={id}
    />
};

export default DSessionListContainer;
