import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {callApi} from "../actions";
import WithDrawList from "../components/withdraw-list";
import WithDrawAction from "../components/withdraw-action";
import {NotificationManager} from "react-notifications";

const WithDrawContainer = ({match}) => {
    const dispatch = useDispatch();
    const {id} = match.params;
    const accessToken = useSelector(state => state.accessToken);

    const [withdraws, setWithdraws] = useState([]);
    const [pages, setPages] = useState(1);
    const [withdrawID, setWithdrawID] = useState("");

    useEffect(() => {
        dispatch(callApi(`admin/withdraw/${id}`, accessToken, data => {
            console.log("Data", data);
            setWithdraws(data.data);
            setPages(data.pages);
        }, error => {
            console.log("Error", error);
        }))
    }, []);

    function updateStatus(id, status) {
        let _with = withdraws.map((item, index) => {
            if (item.id == id)
                item.status = status;
            return item;
        });
        setWithdraws(_with);
    }

    function decline(id) {
        dispatch(callApi(`admin/withdraw/decline/${id}`, accessToken, data => {
            updateStatus(id, 3);
            NotificationManager.success("Đã xử lý", "Thông báo");
            setWithdrawID("");
        }, error => {
            console.log("Error", error);
        }));
    }

    function accept(id) {
        dispatch(callApi(`admin/withdraw/accept/${id}`, accessToken, data => {
            updateStatus(id, 2);
            NotificationManager.success("Đã xử lý", "Thông báo");
            setWithdrawID("");
        }, error => {

        }));
    }

    return <div>
        <WithDrawList
            withdraws = {withdraws}
            pages={pages}
            active={id}
            choose={(id) => setWithdrawID(JSON.stringify(id))}
        />
        <WithDrawAction id={withdrawID}
            handleClose={() => setWithdrawID("")}
            decline={(id) => decline(id)}
            accept={(id) => accept(id)}
        />
    </div>
};

export default WithDrawContainer;
