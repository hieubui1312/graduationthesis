import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {callApi} from "../actions";
import DSessionFee from "../components/dsession-fee";
import {error} from "winston";
import {NotificationManager} from "react-notifications";

const DSessionFeeContainer = () => {
    const dispatch = useDispatch();
    const accessToken = useSelector(state => state.accessToken);
    const [data, setData] = useState([]);

    useEffect(() => {
        dispatch(callApi("admin/dsession-fee", accessToken, data => {
            console.log("Data dsession fee", data);
            setData(data);
        }, error => {
            console.log()
        }))
    }, []);

    function submit(dsession_type, doctor_level, fee) {
        dispatch(callApi("admin/dsession-fee/create", accessToken, data => {
            NotificationManager.success("Cập nhật thành công", "Thông báo");
        }, error => {
            NotificationManager.error("Lỗi", "Thông báo");
        }, {
            dsession_type,
            doctor_level,
            fee
        }))
    }

    return <div>
        <DSessionFee data={data}
                     change={(dsession_type, doctor_level, fee) => submit(dsession_type, doctor_level, fee)}
        />
    </div>
};

export default DSessionFeeContainer;
