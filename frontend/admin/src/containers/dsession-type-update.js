import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import DSessionTypeCreate from "../components/dsession-type-create";
import {callApi} from "../actions";
import {useHistory} from "react-router-dom";

const DSessionTypeUpdateContainer = ({match}) => {
    const dispatch = useDispatch();
    const accessToken = useSelector(state => state.accessToken);
    const [body, setBody] = useState({});
    const history = useHistory();
    const {id} = match.params;
    useEffect(() => {
        dispatch(callApi(`admin/dsession-type/${id}`, accessToken, data => {
            console.log("Data", data);
            setBody(data);
        }, error => {
            console.log("Error", error);
        }))
    }, []);

    function onChange(e){
        let {name, value} = e.target;
        let _body;
        if (name === "time") {
            _body = {
                ...body,
                time: value * 86400
            }
        } else {
            _body = {
                ...body,
                [name]: value
            };
        }
        setBody(_body);
    }

    function submit(e){
        e.preventDefault();
        dispatch(callApi(`admin/dsession-type/update/${id}`, accessToken, data => {
            console.log("Data update", data);
            history.push("/dsession-type/list");
        }, error => {
            console.log("Error", error);
        }, body));
    }

    return <DSessionTypeCreate
            body={body}
            onChange={onChange}
            submit={submit}
    />
};

export default DSessionTypeUpdateContainer;
