import React, {useEffect, useState} from "react";
import DoctorAnalytic from "../components/doctor-analytic";
import {useDispatch, useSelector} from "react-redux";
import {callApi} from "../actions";

const DoctorAnalyticContainer = () => {
    const dispatch = useDispatch();
    const accessToken = useSelector(state => state.accessToken);
    const [doctors, setDoctors] = useState([]);

    useEffect(() => {
        dispatch(callApi("admin/doctor-analytic", accessToken, data => {
            console.log("Data", data);
            setDoctors(data);
        }, error => {
            console.log("Error", error);
        }));
    }, []);

    return <DoctorAnalytic doctors={doctors}/>
};

export default DoctorAnalyticContainer;
