import React, {useEffect, useState} from "react";
import DSessionTypeList from "../components/dsession-type-list";
import {useDispatch, useSelector} from "react-redux";
import {callApi} from "../actions";
import DSessionTypeDelete from "../components/dsession-type-delete";

const DSessionTypeListContainer = () => {
    const dispatch = useDispatch();
    const accessToken = useSelector(state => state.accessToken);
    const [types, setTypes] = useState([]);
    const [id, setId] = useState("");
    useEffect(() => {
        dispatch(callApi("admin/dsession-type/list", accessToken, data => {
            setTypes(data);
        }, error => {
            console.log("Error ", error);
        }));
    }, []);

    function deleteType(id){
        dispatch(callApi(`admin/dsession-type/delete/${id}`, accessToken, data => {
            console.log("Data", data);
            let _types = types.filter(item => {
                return item.id != id;
            });
            setTypes(_types);
            setId("");
        }, error => {
            console.log("Error", error);
        }))
    }

    function selectDelete(id) {
        console.log("Da cho", id);
        setId(JSON.stringify(id));
    }

    return <div>
        <DSessionTypeList types={types}
                          selectDelete={(id) => selectDelete(id)}
        />
        <DSessionTypeDelete id={id}
                            deleteType={(id) => deleteType(id)}
                            handleClose={() => setId("")}
        />
    </div>
};

export default DSessionTypeListContainer;
