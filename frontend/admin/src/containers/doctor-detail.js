import DoctorDetail from "../components/doctor-detail";
import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {callApi} from "../actions";

const DoctorDetailContainer = ({match}) => {
    const dispatch = useDispatch();
    const accessToken = useSelector(state => state.accessToken);
    const [doctor, setDoctor] = useState({});
    const [user, setUser] = useState({});
    const {id} = match.params;

    useEffect(() => {
        dispatch(callApi(`admin/doctor/${id}`, accessToken, data => {
            console.log("Data doctor", data.Doctor);
            setDoctor(data.Doctor);
            delete data.Doctor;
            console.log("Data user", data);
            setUser(data);
        }, error => {
            console.log(("Error doctor", error));
        }));
    }, []);
    return <DoctorDetail
        user={user}
        doctor={doctor}
    />
};

export default DoctorDetailContainer;
