import React, {useEffect, useState,} from "react";
import DSessionTypeCreate from "../components/dsession-type-create";
import {useDispatch, useSelector} from "react-redux";
import {callApi} from "../actions";
import {NotificationManager} from "react-notifications";
import {useHistory} from "react-router-dom";

const DSessionTypeCreateContainer = () => {
    const dispatch = useDispatch();
    const accessToken = useSelector(state => state.accessToken);
    const [body, setBody] = useState({});
    const history = useHistory();

    function submit(e){
        e.preventDefault();
        dispatch(callApi("admin/dsession-type/create", accessToken, data => {
            NotificationManager.success("Thêm gói khám thành công", "Thông báo");
            history.push("/dsession-type/list");
        }, error => {

        }, body))
    }

    function onChange(e){
        let {name, value} = e.target;
        let _body;
        if (name === "time") {
            _body = {
                ...body,
                time: value * 86400
            }
        } else {
            _body = {
                ...body,
                [name]: value
            };
        }
        setBody(_body);
    }

    return <DSessionTypeCreate body={body}
                               submit={submit}
                               onChange={onChange}
    />;
};

export default DSessionTypeCreateContainer;
