import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {callApi} from "../actions";
import DoctorUpdate from "../components/doctor-update";
import {SERVER_API} from "../config/server";
import {NotificationManager} from "react-notifications";

const DoctorUpdateContainer = ({match}) => {
    const dispatch = useDispatch();
    const accessToken = useSelector(state => state.accessToken);
    const [doctor, setDoctor] = useState({});
    const [user, setUser] = useState({});
    const [majors, setMajors] = useState([]);
    const {id} = match.params;

    useEffect(() => {
        dispatch(callApi(`admin/doctor/${id}`, accessToken, data => {
            console.log("Data doctor", data.Doctor);
            setDoctor(data.Doctor);
            delete data.Doctor;
            console.log("Data user", data);
            setUser(data);
        }, error => {
            console.log(("Error doctor", error));
        }));
        dispatch(callApi("admin/majors", accessToken, data => {
            console.log("Major", data);
            setMajors(data);
        }, error => {
            console.log("Error fetch major", error);
        }));

    }, []);

    function onChangeUser(e){
        let {name, value} = e.target;
        let _user = {
            ...user, [name]: value
        };
        setUser(_user);
    }

    function onChangeDoctor(e){
        let {name, value} = e.target;
        let _doctor = {
            ...doctor, [name]: value
        };
        setDoctor(_doctor);
    }

    function checked(e){
        let _majors = doctor.majors;
        if (!_majors) _majors = [];
        else _majors = JSON.parse(_majors);
        let {checked, value} = e.target;
        if (checked) {
            _majors.push(value);
        } else {
            let index = _majors.indexOf(value);
            _majors.splice(index, 1);
        }
        setDoctor({...doctor, majors: JSON.stringify(_majors)});
    }

    function uploadImage(e){
        let file = e.target.files[0];
        const data = new FormData();
        data.append("file", file);
        fetch(`${SERVER_API}/common/upload`, {
            method: "POST",
            body: data
        }).then(data => {
            return data.json();
        }).then(data => {
            let url = data.fileUrl;
            console.log("Url", url);
            setUser({...user, avatar: url})
        })
    }

    function submit(e) {
        e.preventDefault();
        dispatch(callApi(`admin/doctor/update/${id}`, accessToken, data => {
            NotificationManager.success("Update thành công", "Thông báo");
        }, error => {
            NotificationManager.error("Update thất bại", "Thông báo");
            console.log("Error", error);
        }, {
            user,
            doctor
        }))
    }

    return <DoctorUpdate
        title={"Update thông tin bác sĩ"}
        user={user}
        doctor={doctor}
        onChangeUser={onChangeUser}
        onChangeDoctor={onChangeDoctor}
        majors={majors}
        checked={checked}
        uploadImage={uploadImage}
        submit={submit}
    />
};

export default DoctorUpdateContainer;
