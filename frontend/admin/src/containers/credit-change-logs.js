import CreditChangeLogs from "../components/credit-change-logs";
import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {callApi} from "../actions";

const CreditChangeLogContainer = ({match, location}) => {
    const dispatch = useDispatch();
    const accessToken = useSelector(state => state.accessToken);
    const {id} = match.params;

    const [logs, setLogs] = useState([]);
    const [pages, setPages] = useState(1);
    const phoneNumber = new URLSearchParams(location.search).get("phone");

    useEffect(() => {
        let url = `admin/credit-log/${id}`;
        if (phoneNumber) url += `?phone=${phoneNumber}`;
        dispatch(callApi(url, accessToken, data => {
            console.log("Data", data);
            setLogs(data.credit_logs);
            setPages(data.pages);
        }, error => {
            console.log("Error fetch credit logs", error);
        }))
    }, []);

    return <CreditChangeLogs
            logs={logs}
            pages={pages}
            active={id}
            phone={phoneNumber}
    />
};

export default CreditChangeLogContainer;
