import Login from "../components/login";
import React from "react";
import {useDispatch} from "react-redux";
import {login} from "../actions/auth";
import {callApi} from "../actions";
import {NotificationManager} from "react-notifications";
import {setAccessToken} from "../actions/access_token";

const LoginContainer = () => {
    const dispatch = useDispatch();

    function submit(body) {
        dispatch(callApi("admin/login", "null", (data, dispatch) => {
            const {access_token} = data.user;
            dispatch(setAccessToken(access_token));
        }, error => {
            console.log("Error", error);
            NotificationManager.error("Tài khoản hoặc mật khẩu không đúng", "Thông báo!");
        }, body));
    }
    return <Login
        submit={info => submit(info)}
    />
};

export default LoginContainer;
