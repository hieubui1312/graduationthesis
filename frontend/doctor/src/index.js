import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';
import './style/base.scss';
import * as serviceWorker from './serviceWorker';
import 'react-notifications/lib/notifications.css';
import reducer from "./redux/reducers/index";
import {createStore} from "redux";
import {applyMiddleware} from "redux";
import thunk from "redux-thunk";
import {Provider} from "react-redux";
import * as Server from "./config/server_path";
import { createLogger } from 'redux-logger'

var store;
var logger = createLogger();

function initialApp(store) {
    ReactDOM.render(<Provider store={store}>
        <App />
    </Provider>, document.getElementById('root'));
}

let currentUser = localStorage.getItem("currentUser");
let passCodeFlag = sessionStorage.getItem("passCodeFlag");

console.log("Session storage ", passCodeFlag);

if (passCodeFlag) {
    passCodeFlag = !!passCodeFlag;
}

if (currentUser) {
    currentUser = JSON.parse(currentUser);
    fetch(Server.API_SERVER + "doctor/me", {
        headers: {
            'Content-Type': 'application/json',
            'Access-Token': currentUser.access_token
        }
    }).then(
        res => res.json()
    ).then(res => {
        if (res.errorCode) {
            throw new Error(res.error);
        } else {
            localStorage.setItem("currentUser", JSON.stringify(res.user));
            if (passCodeFlag) {
                store = createStore(reducer, {currentUser: res.user, passCodeFlag}, applyMiddleware(thunk, logger));
            } else {
                store = createStore(reducer, {currentUser: res.user}, applyMiddleware(thunk, logger));
            }
            initialApp(store);
        }
    }).catch(err => {
        // localStorage.removeItem("currentUser");
        store = createStore(reducer,{}, applyMiddleware(thunk, logger));
        initialApp(store);
    });

    // if (currentUser.name) {
    //     store = createStore(reducer, {currentUser: currentUser}, applyMiddleware(thunk))
    // } else {
    //     store = createStore(reducer,{}, applyMiddleware(thunk));
    // }
} else {
    store = createStore(reducer,{}, applyMiddleware(thunk, logger));
    initialApp(store);
}



// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
