import React, {useEffect, useState} from "react";
import PatientItem from "../patient-item";
import "./index.scss";
import {useDispatch, useSelector} from "react-redux";
import {callApi} from "../../redux/actions";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faArrowCircleDown} from "@fortawesome/free-solid-svg-icons";
import {ListGroup} from "react-bootstrap";
import {addNewMessageChange, initialDSession} from "../../redux/actions/dsessions";
import {addMessage, fetchMessages} from "../../redux/actions/messageDSession";
import {setCurrentDSession} from "../../redux/actions/currentDSession";
import SocketEvent from "../../config/socket_event";

function PatientList() {

    const dispatch = useDispatch();
    const currentUser = useSelector(state => state.currentUser);
    const dSessions= useSelector(state => state.dsessions);

    const [toggleFilter, setToggleFilter] = useState(false);
    const [status, setStatus] = useState("accept");

    const dSessionStatus = [
        {
            attr: "complete",
            des: "Phiên khám hoàn thành"
        },
        {
            attr: "waiting",
            des: "Phiên khám chờ"
        },
        {
            attr: "accept",
            des: "Phiên đang khám"
        },
        {
            attr: "decline",
            des: "Phiên khám đã huỷ"
        }
    ];

    useEffect(() => {
        dispatch(callApi("doctor/dsessions", currentUser.access_token, (data, dispatch) => {
            dispatch(initialDSession(data.dSession ?? []));
            if (data.dSession[status].length > 0) {
                dispatch(setCurrentDSession(data.dSession[status][0]));
            }
        }, error => {
        }))
    }, []);

    function getTitle(){
        let att = dSessionStatus.find(item => item.attr === status);
        return att.des;
    }

    useEffect(() => {
        if (window.socket && dSessions.accept) {
            let rooms = dSessions.accept.map(dsession => dsession.id);
            window.socket.emit(SocketEvent.JOIN_ROOM, {rooms, token: currentUser.access_token})
        }
    }, [dSessions.accept]);

    useEffect(() => {
        setTimeout(() => {
            if (window.socket) {
                window.socket.on("chat-message", function (data) {
                    console.log("Prescrip sent message", data);
                    dispatch(addNewMessageChange(data));
                })
            }
        }, []);
    }, []);

    function chooseDSessionStatus(attr){
        setStatus(attr);
        setToggleFilter(false);
    }

    useEffect(() => {
        console.log("Status dsession", status);
        if (Object.keys(dSessions).length > 0) {
            console.log("Status dsession", status);
            let dSessionsArr = dSessions[status];
            if (dSessionsArr) {
                let dSessionBegin = dSessionsArr[0];

                if (dSessionBegin) {
                    dispatch(setCurrentDSession(dSessionBegin));
                }
            }
        }
    }, [status]);

    function getDSessions(){
        let dSessionsArr = dSessions[status];
        if (dSessionsArr) {
            // let dSessionBegin = dSessionsArr[0];

            // if (dSessionBegin) {
            //     dispatch(setCurrentDSession(dSessionBegin));
            // }

            return dSessionsArr.map((dsession, index) => {
                return <PatientItem key={`dsession-${index}`} dSession={dsession}/>
            })
        }
    }

    return <div className="patient-list p-md-2">
        <div className="patient-info mt-md-2">
            <div className={"d-flex justify-content-between align-items-center p-md-3 bg-primary text-light"}
                 onClick={() => setToggleFilter(!toggleFilter)}
                 style={{cursor: "pointer"}}>
                <b>{getTitle()}</b>
                <FontAwesomeIcon icon={faArrowCircleDown} />
            </div>
            {toggleFilter ? <ListGroup>
                {
                    dSessionStatus.map((dSession, index) => <ListGroup.Item style={{cursor: "pointer"}} key={`status-${index}`}
                        onClick={() => chooseDSessionStatus(dSession.attr)}>
                        {dSession.des}
                    </ListGroup.Item>)
                }
            </ListGroup> : ""}
        </div>
        {getDSessions()}
    </div>
}

export default PatientList;
