import {Component} from "react";
import {Col, Container, Row} from "react-bootstrap";
import Header from "../header";
import React from "react";
import PatientList from "../patient-list";
import ChatBox from "../chat-box";
import "./index.scss";
import InfoArea from "../info-area";

function Home() {
    return <>
        <Header/>
        <div className="content">
            <Container fluid={true}>
                <Row>
                    <Col md={3}>
                        <PatientList/>
                    </Col>
                    <Col md={6}>
                        <ChatBox/>
                    </Col>
                    <Col md={3}>
                        <InfoArea/>
                    </Col>
                </Row>
            </Container>
        </div>
    </>;
}

export default Home;
