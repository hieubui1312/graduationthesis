import React, {useEffect, useState} from "react";
import {Col, Container, Row} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCapsules, faImage, faVials} from "@fortawesome/free-solid-svg-icons";
import "./index.scss";
import {useDispatch, useSelector} from "react-redux";
import {postAddImage, postAddMessage} from "../../redux/actions/messageDSession";
import {Modal, Button} from "react-bootstrap";
import {callApi} from "../../redux/actions";
import DiseaseTest from "../disease-test";
import PrescriptionSent from "../prescription-sent";
import {API_SERVER} from "../../config/server_path";
import {NotificationManager} from "react-notifications";
import * as DSessionStatus from "../../config/dSession";

function ChatForm() {
    const [message, setMessage] = useState("");
    const [file, setFile] = useState("");
    const [toggleDiseaseTest, setToggleDiseaseTest] = useState(false);
    const [togglePrescriptionSent, setTogglePrescriptionSent] = useState(false);

    const currentUser = useSelector(state => state.currentUser);
    const currentDSession = useSelector(state => state.currentDSession);

    const dispatch = useDispatch();

    function onChange(e){
        let {value} = e.target;
        setMessage(value);
    }

    function submit(e){
        e.preventDefault();
        if (message.length > 0) {
            dispatch(postAddMessage(currentDSession.id,
                message,
                currentUser.access_token));
            setMessage("");
        }
    }

    function uploadFile(e){
        let {files} = e.target;
        const formData = new FormData();
        formData.append("file", files[0]);

        fetch(`${API_SERVER}common/upload`, {
            method: "POST",
            body: formData
        }).then(response =>
            response.json()
        ).then(data => {
            dispatch(postAddImage({
                id: currentDSession.id,
                url: data.fileUrl
            }, currentUser.access_token));
        }).catch(e => {
            NotificationManager.error("Có lỗi xảy ra", "Lỗi", 5000);
        })
    }

    function getDSessionStatusText(){
        switch (currentDSession.status) {
            case DSessionStatus.STATUS_WAITING:
                return "Phiên khám chờ";
            case DSessionStatus.STATUS_COMPLETE:
                return "Phiên khám hoàn thành";
            case DSessionStatus.STATUS_DECLINE:
                return "Phiên khám đã huỷ";
            default:
                return ""
        }
    }

    return <div className="chat-form ml--15">
        {
            currentDSession.status === DSessionStatus.STATUS_ACCEPT ? <div>
                <Container>
                    <Row className={"p-md-2"}>
                        <Col md={9} className={"mr--15"}>
                            <form action="" onSubmit={submit}>
                                <input name={"message"}
                                       value={message}
                                       onChange={onChange}
                                       placeholder={"Nhấn shift + enter để xuống dòng"}
                                       type={"text"}
                                />
                            </form>
                        </Col>
                        <Col md={3} className={"d-flex align-items-center position-relative"}>
                            <FontAwesomeIcon icon={faCapsules}
                                             className={"mr-md-2 text-danger"}
                                             onClick={() => setTogglePrescriptionSent(true)}
                            />
                            <FontAwesomeIcon icon={faImage} className={"mr-md-2 text-primary"} />
                            <FontAwesomeIcon icon={faVials}
                                             className={"text-warning"}
                                             onClick={() => setToggleDiseaseTest(true)}
                                             style={{cursor: "pointer"}}
                            />
                            <input type={"file"} name={"file"} accept="image/*"  className={"position-absolute"} onChange={uploadFile}/>
                        </Col>
                    </Row>
                </Container>
                <DiseaseTest toggle={toggleDiseaseTest}
                             close={() => setToggleDiseaseTest(false)}
                />
                <PrescriptionSent toggle={togglePrescriptionSent}
                                  close = {() => setTogglePrescriptionSent(false)}
                />
            </div> : <div className={"text-center text-danger"}>
                {getDSessionStatusText()}
            </div>
        }
    </div>
}

export default ChatForm;
