import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {getInfoSpecial} from "../../redux/actions/currentUser";
import {callApi} from "../../redux/actions";
import dateformat from "dateformat";
import {updateInfoSpecial} from "../../redux/actions/currentUser";

const DoctorDegrees = [
    {
        title: "Bác sĩ",
        id: 1
    }, {
        title: "Bác sĩ chuyên khoa 1",
        id: 2
    }, {
        title: "Thạc sĩ, bác sĩ",
        id: 3
    }, {
        title: "Bác sĩ chuyên khoa 2",
        id: 4
    }, {
        title: "Tiến sĩ bác sĩ",
        id: 5
    }, {
        title: "Phó giáo sư, tiến sĩ",
        id: 6
    }, {
        title: "Giáo sư, tiến sĩ",
        id: 7
    }
];

function UpdateSpecializeForm() {
    const [info, setInfo] = useState({});
    const [hospitals, setHospitals] = useState([]);
    const [majors, setMajors] = useState([]);

    const currentUser = useSelector(state => state.currentUser);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getInfoSpecial(currentUser, (data) => {
            setInfo(data);
        }));
        dispatch(callApi("doctor/hospital", currentUser.access_token, data => {
            setHospitals(data);
        }, error => {
            console.log("Error", error);
        }));
        dispatch(callApi("doctor/major", currentUser.access_token, data => {
            setMajors(data);
        }, error => {
            console.log("Error", error);
        }))
    }, []);

    function getHospitalOption(){
        return <select className={"form-control"}
                       value={getValueHospitalDefault()}
                       onChange={selectHospital}
        >
            {hospitals.map(hospital =>
                <option key={`hospital-${hospital.id}`} value={hospital.id}>
                    {hospital.name}
                </option>
            )}
        </select>
    }

    function getValueHospitalDefault(){
        let hospitalId = info.hospital_id;
        hospitalId = parseInt(hospitalId);
        if (isNaN(hospitalId)) {
            return info.hospital_id;
        } else return hospitalId;
    }

    function selectHospital(e){
        let infoNew = {...info, hospital_id: e.target.value};
        setInfo(infoNew);
    }

    function getCheckboxMajor(){
        return majors.map((major, index) => {
            let checked = getCheckedMajor(major);

            return <div key={`major-${index}`} className="col-md-4 mb-md-2">
                <input className="form-check-input"
                       type="checkbox"
                       checked={checked}
                       onChange={e => checkedMajor(e, major)}
                />
                <label className="form-check-label" >
                    {major.name}
                </label>
            </div>
        })
    }

     function checkedMajor(e, major){
        let {checked} = e.target;
        let {majors} = info;

        if (!majors) majors = [];
        else majors = JSON.parse(majors);

        let index = majors.indexOf(major.name);
        if (checked) {
            majors.push(major.name);
        } else {
            majors.splice(index, 1);
        }

        setInfo({...info, majors: JSON.stringify(majors)})
     }

    function getCheckedMajor(major){
        let {majors} = info;
        if (!majors) return false;

        majors = JSON.parse(majors);
        if (majors.indexOf(major.name) > -1) return true;
        else return false;
    }

    function getDoctorDegreeOption(){
        return <select className="form-control"
                       value={info.degree ? parseInt(info.degree) : ""}
                       onChange={selectDoctor}
        >
            {DoctorDegrees.map((degree, index) => <option key={`degree-${index}`} value={degree.id}>
                {degree.title}
            </option>)}
        </select>;
    }

    function selectDoctor(e){
        let {value} = e.target;
        let infoNew = {...info, degree: value};
        setInfo(infoNew);
    }

    function getCertDate(){
        if (info.cert_date) return dateformat(new Date(info.cert_date), "yyyy-mm-dd");
        return true;
    }

    function changeCertDate(e){
        if (e.target.value.length > 0) {
            setInfo({
                ...info, cert_date: e.target.value
            })
        }
    }

    function submit(e){
        e.preventDefault();
        let body = {...info};
        delete body.Hospital;
        dispatch(updateInfoSpecial(body, currentUser.access_token));
    }

    return <div className="update-specialize-form p-md-5">
        <form onSubmit={submit}>
            <div className="form-group">
                <div className="row">
                    <div className="col-md-3 d-flex align-items-center text-secondary">
                        <b>Nơi công tác</b>
                    </div>
                    <div className="col-md-9">
                        {getHospitalOption()}
                    </div>
                </div>
            </div>
            <div className="form-group">
                <div className="row">
                    <div className="col-md-3 d-flex align-items-center text-secondary">
                        <b>Ngày cấp chứng chỉ hành nghê</b>
                    </div>
                    <div className="col-md-9">
                        <input type="date" className={"form-control"}
                               value={getCertDate()}
                               placeholder={"Nhập ngày cấp chứng chỉ hành nghề"}
                               onChange={changeCertDate}
                        />
                    </div>
                </div>
            </div>
            <div className="form-group">
                <div className="row">
                    <div className="col-md-3 d-flex align-items-center text-secondary">
                        <b>Bằng cấp</b>
                    </div>
                    <div className="col-md-9">
                        {getDoctorDegreeOption()}
                    </div>
                </div>
            </div>
            <div className="form-group">
                <div className="row">
                    <div className="col-md-3 d-flex align-items-center text-secondary">
                        <b>Chuyên ngành</b>
                    </div>
                    <div className="col-md-9">
                        <div className="row pl-4">
                            {getCheckboxMajor()}
                        </div>
                    </div>
                </div>
            </div>
            <div className="form-group text-center">
                <button className={"btn btn-primary"}>Cập nhật</button>
            </div>
        </form>
    </div>
}

export default UpdateSpecializeForm;
