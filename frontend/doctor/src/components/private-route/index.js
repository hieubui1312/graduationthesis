import React from "react";
import {Route, Redirect} from "react-router-dom";
import {connect} from "react-redux";

class PrivateRoute extends React.Component{
    constructor(props){
        super(props);
    }

    generateComp = () => {
        let {currentUser, passCodeFlag} = this.props;
        let MyComp = this.props.comp;
        if (Object.keys(currentUser).length > 0 && passCodeFlag) {
            return <MyComp />;
        } else return <Redirect to={"/login"}/>;
    };

    render() {
        let {path} = this.props;
        return <Route exact path={path} render = {() => this.generateComp()
        }/>
    }
}

const mapStateToProps = (state) => {
    return {
        currentUser: state.currentUser,
        passCodeFlag: state.passCodeFlag
    }
};

export default connect(mapStateToProps)(PrivateRoute)
