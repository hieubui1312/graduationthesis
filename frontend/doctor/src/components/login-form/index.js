import React, {useEffect, useState} from "react";
import "./index.scss";
import background from "../../assets/images/bg.jpg";
import firebaseAuth from "../../util/firebase-client";
import * as firebase from "firebase";
import {NotificationManager} from "react-notifications";
import LoginLevelTwo from "../../containers/loginLevelTwoContainer";
import passCodeFlag from "../../redux/reducers/passcodeFlag";
import {Redirect} from "react-router-dom";
import currentUser from "../../redux/reducers/currentUser";

function OtpForm({login}){

    const [otp, setOtp] = useState("");

    function onChange(e){
        let otp = e.target.value;
        if (otp.length <=6){
            setOtp(otp);
        }
    }

    async function submit(e){
        e.preventDefault();
        if (otp.length <= 0) {
            alert("Điền OTP trước khi submit");
        } else {
            try{
                await window.confirmationResult.confirm(otp);
                let token = await firebase.auth().currentUser.getIdToken(true);
                login(token);
            } catch (e) {
                NotificationManager.error("OTP không chính xác. Vui lòng nhập lại!", "Lỗi");
            }
        }
    }

    return <div className="otp-form p-md-3">
        <form onSubmit={submit}>
            <div className="form-group">
                <input value={otp}
                       onChange={onChange}
                       type={"text"}
                       className={"form-control bg-white"}
                       placeholder={"Nhập mã otp"}
                />
            </div>
            <div className="form-group">
                <button className={"btn btn-primary w-100 mb-2"}>
                    <b>Xác thực</b>
                </button>
                {/*<button className={"btn btn-primary w-100"}>*/}
                {/*    <b>Gửi lại OTP</b>*/}
                {/*</button>*/}
            </div>
        </form>
    </div>
}

class CreatePassCodeForm extends React.Component{

    constructor(props){
        super(props);
        this.state = {
          password: "",
          rePassword: ""
        };
    }

    onSubmit = (e) => {
        let {currentUser} = this.props;
        e.preventDefault();
        let {password, rePassword} = this.state;
        if (password.length < 6) {
            NotificationManager.error("Password của bạn phải chứa 6 kí tự", "Thông báo", 5000);
        } else if (password !== rePassword) {
            NotificationManager.error("Nhập lại password của bạn không đúng", "Thông báo", 5000);
        } else {
            this.props.createPassword(currentUser.access_token, password);
        }
    };

    onChange = (e) => {
        let {name, value} = e.target;
        if (value.length <= 6) {
            this.setState({
                [name]: value
            })
        }
    };

    render() {
        let {password, rePassword} = this.state;
        return <div className="create-pass-code p-md-3">
            <form onSubmit={this.onSubmit}>
                <div className="form-group">
                    <input name={"password"}
                           type={"password"}
                           className={"form-control"}
                           placeholder={"Nhập mật khẩu của bạn"}
                           onChange={this.onChange}
                           value={password}
                    />
                </div>
                <div className="form-group">
                    <input name={"rePassword"}
                           type={"password"}
                           className={"form-control"}
                           placeholder={"Nhập lại mật khẩu của bạn"}
                           onChange={this.onChange}
                           value={rePassword}
                    />
                </div>
                <div className="form-group text-center">
                    <button type={"submit"} className={"btn btn-primary w-100 mb-2"}>Submit</button>
                </div>
            </form>
        </div>
    }
}



class PhoneForm extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            phoneNumber: ""
        }
    }

    onChange = (e) => {
        let phoneNumber = e.target.value;
        phoneNumber = phoneNumber.replace(/^0/, "+84");
        this.setState({
            phoneNumber
        })
    };

    componentDidMount() {
        firebaseAuth.auth().languageCode = "en";
        window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('sign-in-button', {
            "size": 'invisible',
            "callback": async (response) => {
                console.log("Response login", response);
                await this.signIn();
            }
        });
        window.recaptchaVerifier.render();
    }

    signIn = async () => {
        try{
            let {phoneNumber} = this.state;

            if (phoneNumber.length <= 0) return ;

            let appVerifier = window.recaptchaVerifier;
            window.confirmationResult = await firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier);

            this.props.openOtpForm();
        } catch (e) {

        }
    };

    render() {
        let {phoneNumber} = this.state;

        return <div className="phone-form p-md-3">
            <form onSubmit={e => e.preventDefault()}>
                <div className="form-group">
                    <input value={phoneNumber} onChange={this.onChange} type={"text"}
                           className={"form-control bg-white"} placeholder={"Nhập số điện thoại"}/>
                </div>
                <div className="form-group">
                    <button type={"submit"} className={"btn btn-primary w-100"} id={"sign-in-button"}>
                        <b>Xác thực</b>
                    </button>
                </div>
            </form>
        </div>
    }
}


const FORM_PHONE_NUMBER = 1;
const FORM_OTP = 2;
const FORM_PASSCODE = 3;

class Login extends React.Component{
    constructor(props){
        super(props);
        this.state = {
          formSpecific: 1
        };
    }

    componentDidMount() {
        // this.props.histor
    }

    generateForm = () => {
      let {currentUser} = this.props;

      if (Object.keys(currentUser).length  > 0) {
          if (currentUser.password) {
              return <LoginLevelTwo />;
          } else {
              return <CreatePassCodeForm
                  currentUser = {currentUser}
                  createPassword = {(accessToken, password) => this.props.createPassword(accessToken, password)}
              />;
          }
      } else return this.generateFormLevelOne();
    };

    generateFormLevelOne = () => {

        let {formSpecific} = this.state;

        switch (formSpecific) {
            case FORM_PHONE_NUMBER:
                return <PhoneForm
                    openOtpForm={() => this.setState({formSpecific: FORM_OTP})}
                />;
            case FORM_OTP:
                return <OtpForm login={token => this.login(token)}/>;
            default:
                break;
        }
    };

    login = (token) => {
        this.props.login(token, (data) => {
            console.log("Data login", data);
        }, error => {
            console.log("Error login", error);
        });
    };


    render() {
        let {currentUser, passCodeFlag} = this.props;

        let comp = <div className={"login-area"}>
            <img src={background} alt={"Background"} />
            <div className="login">
                <div className="title text-center text-uppercase text-light bg-secondary mb-md-1 p-md-2">
                    <b>Đăng nhập</b>
                </div>
                {this.generateForm()}
            </div>
        </div>;

        if (Object.keys(currentUser).length > 0 && passCodeFlag) {
            return <Redirect to = "/"/>;
        } else return comp;
    }
}

export default Login;
