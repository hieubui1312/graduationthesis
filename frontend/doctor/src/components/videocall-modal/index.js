/* global JitsiMeetExternalAPI */
import React, {useEffect} from "react";
import {Modal, Button} from "react-bootstrap";
import "./index.scss"
import {useDispatch, useSelector} from "react-redux";
import {logVideoCall} from "../../redux/actions/messageDSession";

const VideoCallModal = ({status, handleClose, patientName, room, out}) => {
    const dispatch = useDispatch();
    const currentUser = useSelector(state => state.currentUser);
    const currentDSession = useSelector(state => state.currentDSession);

    useEffect(() => {
        if (status === 1 && room){
            const domain = 'jitsi.vietskin.vn';
            const options = {
                roomName: room,
                width: 700,
                height: 700,
                parentNode: document.querySelector('#meet')
            };
            const api = new JitsiMeetExternalAPI(domain, options);
            api.addEventListener("videoConferenceLeft", () => {
                dispatch(logVideoCall(currentUser.access_token, {
                    dsession_id: currentDSession.id,
                    message: "Cuộc gọi thành công"
                }));
                out();
            })
        }
    }, [status]);

    function cancel(){
        dispatch(logVideoCall(currentUser.access_token, {
            dsession_id: currentDSession.id,
            message: "Cuộc gọi nhỡ"
        }));
        handleClose();
    }

    function getModal(){
        switch (status) {
            case 0:
                return <div>
                    <div>Đang gọi điện cho bênh nhân <b>{patientName}</b>...</div>
                    <Button variant="danger" onClick={cancel}>
                        Huỷ cuộc gọi
                    </Button>
                </div>;
            case 1:
                return <div id="meet" />;
            default:
                return <div />
        }
    }

    return <Modal show={status === 0 || status === 1} className={"custom-video-modal"}>
        <Modal.Body>
            {getModal()}
        </Modal.Body>
    </Modal>
};

export default VideoCallModal;
