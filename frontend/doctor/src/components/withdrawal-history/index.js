import React, {useEffect, useState} from "react";
import {Table} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {callApi} from "../../redux/actions";
import dateformat from "dateformat";
import WithdrawStatus from "../../config/withdraw_status";

function WithdrawalHistory() {
    const [withdraws, setWithdraws] = useState([]);

    const dispatch = useDispatch();
    const currentUser = useSelector(state => state.currentUser);

    useEffect(() => {
        fetchData();
    }, []);

    function fetchData(){
        dispatch(callApi("doctor/withdraw", currentUser.access_token, data => {
            console.log("Data withdraw", data);
            setWithdraws(data);
        }, error => {
            console.log("Error", error);
        }))
    }

    function getContent(){
        return withdraws.map((withdraw, index) => {
            return <tr key={index}>
                <td>{index + 1}</td>
                <td>{withdraw.amount}</td>
                <td>{getStatus(withdraw.status)}</td>
                <td>{formatTime(withdraw.created_at)}</td>
            </tr>
        })
    }

    function getStatus(status){
        switch (status) {
            case WithdrawStatus.STATUS_WAITING:
                return <span className={"badge badge-warning"}>Đang chờ xử lí</span>;
            case WithdrawStatus.STATUS_ACCEPT:
                return <span className={"badge badge-success"}>Đã xử lí</span>;
            case WithdrawStatus.STATUS_DECLINE:
                return <span className={"badge badge-danger"}>Đã từ chối</span>;
        }
    }

    function formatTime(time){
        return dateformat(new Date(time), "yyyy-mm-dd hh:MM:ss")
    }

    return <div className="withdrawal-history p-md-5">
        <Table striped bordered hover>
            <thead>
            <tr>
                <th>#</th>
                <th>Amount</th>
                <th>Trạng thái</th>
                <th>Thời gian</th>
            </tr>
            </thead>
            <tbody>
            {getContent()}
            </tbody>
        </Table>
    </div>
}

export default WithdrawalHistory;
