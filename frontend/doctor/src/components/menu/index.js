import React from "react";
import {Col, Row} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import "./index.scss";
import {useDispatch} from "react-redux";
import {logout} from "../../redux/actions/currentUser";


function Menu({title, menus, switchTab, tabActive}) {
    const dispatch = useDispatch();
    function clickLogout(){
        dispatch(logout());
    }

    function getMenu(){
      return menus.map((menu, index) => {
          let classActive = tabActive === menu.id ? "menu-item-active" : "";

         return <Row className={`p-md-3 menu-item ${classActive}`} key={index}>
             <Col md={3}>
                 <div className={"menu-icon text-center "}><FontAwesomeIcon icon={menu.icon} /></div>
             </Col>
             <Col md={9} className={"d-flex align-items-center"}>
                 <div className="menu-content" >
                     <a style={{cursor: "pointer"}} onClick={() => switchTab(menu.id)}>
                         {menu.name}
                     </a>
                 </div>
             </Col>
         </Row>
      });
    }

    return <div className={"menu"}>
        <div className="menu-title text-center p-md-3 text-light bg-primary text-uppercase">
            <b>{title}</b>
        </div>
        <div className="menu-list">
            {getMenu()}
            <div className={"text-center pt-3"}>
                <a onClick={clickLogout}
                   style={{cursor: "pointer"}}
                   className={"text-primary"}
                >
                    <b>Đăng xuất</b>
                </a>
            </div>
        </div>
    </div>
}

export default Menu;
