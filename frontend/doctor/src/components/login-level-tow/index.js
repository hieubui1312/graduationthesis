import React from "react";
import {NotificationManager} from "react-notifications";

class LoginLevelTwo extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            password: ""
        };
    }

    onChange = (e) => {
        let {name, value} = e.target;
        if (value.length <= 6) {
            this.setState({
                [name]: value
            })
        }
    };

    onSubmit = (e) => {
        e.preventDefault();
        let {currentUser} = this.props;
        let {password} = this.state;
        if (password.length < 6) {
            NotificationManager.error("Password của bạn cần có 6 ký tự. Vui lòng nhập lại!", "Thông báo!");
            return false;
        }
        this.props.loginLevelTwo(password, currentUser.access_token);
    };

    render() {
        let {currentUser} = this.props;
        return <div className={"p-md-3"}>
            <p>Xin chào bác sĩ {currentUser.name}</p>
            <p>{currentUser.phone}</p>
            <form onSubmit={this.onSubmit} className={"mb-md-2"}>
                <div className="form-group">
                    <input onChange={this.onChange}
                           value={this.state.password}
                           type={"text"}
                           className={"form-control"}
                           placeholder={"Nhập mật khẩu"}
                           name={"password"}
                    />
                </div>
                <button className={"btn btn-primary w-100"}>
                    <b>Xác thực</b>
                </button>
            </form>
            <div className={"d-flex justify-content-between"}>
                <a href="#"><small>Thoát tài khoản</small></a>
                <a href="#"><small>Quên mật khẩu</small></a>
            </div>
        </div>
    }
}

export default LoginLevelTwo
