import {Col, Container, Row} from "react-bootstrap";
import React, {useEffect} from "react";
import Notification from "../notification";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faUserMd, faWallet} from "@fortawesome/free-solid-svg-icons";
import "./index.scss";
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {addDSessionWaiting} from "../../redux/actions/dsessions";

function Header() {
    const currentUser= useSelector(state => state.currentUser);
    const dispatch = useDispatch();

    useEffect(() => {
        setTimeout(() => {
            if (window.socket) {
                window.socket.on("dsession_create", data => {
                    data = JSON.parse(data);
                    if (data.doctor_id === currentUser.id){
                        dispatch(addDSessionWaiting(data))
                    }
                })
            }
        }, 1000);
    }, [currentUser]);

    return <div className="p-3 header">
        <Container fluid={true}>
            <Row>
                <Col md={3}>
                    <Link to="/home" className={"brand"}>
                        <span>PatientAccess</span><span>-Doctor</span>
                    </Link>
                </Col>
                <Col md={5}>
                </Col>
                <Col md={4} className={"d-flex justify-content-end"}>
                    <Notification/>
                    <Link to="/withdraw" className={"mr-md-5 text-dark"}>
                        <FontAwesomeIcon icon={faWallet} style={{fontSize: "28px"}}/>
                    </Link>
                    <Link to="/personal-information" className={"mr-md-5 text-dark"}>
                        <FontAwesomeIcon icon={faUserMd} style={{fontSize: "28px"}} />
                    </Link>
                </Col>
            </Row>
        </Container>
    </div>
}

export default Header
