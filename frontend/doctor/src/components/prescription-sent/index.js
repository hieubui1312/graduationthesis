import React, {useEffect, useState} from "react";
import {Modal, Button} from "react-bootstrap";
import "./index.scss";
import {useDispatch, useSelector} from "react-redux";
import MedicineItem from "../medicine-item";
import {NotificationManager} from "react-notifications";
import dateformat from "dateformat";
import {sendPrescriptionSet} from "../../redux/actions/messageDSession";

function PrescriptionSent({toggle, close}) {
    const [diagnose, setDiagnose] = useState("");
    const [diet, setDiet] = useState("");
    const [body_clean, setBodyClean] = useState("");
    const [other_note, setOtherNode] = useState("");
    const [date_test_again, setDateTestAgain] = useState(dateTestAgainDefault());

    const [medicineItems, setMedicineItems] = useState([]);
    const [medicineArr, setMedicineArr] = useState([]);

    const currentDSession = useSelector(state => state.currentDSession);
    const currentUser = useSelector(state => state.currentUser);
    const dispatch = useDispatch();

    function dateTestAgainDefault (){
        let date = new Date(Date.now() + 30 * 24 * 60 * 60 * 1000);
        return dateformat(date, "yyyy-mm-dd");
    }

    function addMedicine(){
        setMedicineItems([...medicineItems, <MedicineItem key={medicineItems.length}
                                                          index={medicineItems.length}
                                                          onChangeMedicine={(med, index) => setMedicine(med, index)}
                                                          remove={(index) => removeMedicine(index)}
        />]);
        setMedicineArr([...medicineArr, {unit: "Hộp"}]);
    }

    function setMedicine(med, index){
        let medArr = medicineArr;

        medArr[index] = med;
        setMedicineArr(medArr);
    }

    function removeMedicine(order){
        setMedicineItems(medicineItems.filter((item, index) => index !== order));
        setMedicineArr(medicineArr.filter((item, index) => index !== order));
    }

    function submit(){
        let body = {
            diagnose,
            id: currentDSession.id,
            medicines: medicineArr,
            diet,
            body_clean,
            other_note,
            date_test_again: dateformat(new Date(date_test_again), "yyyy-mm-dd hh:mm:ss")
        };

        if (validate()) {
            dispatch(sendPrescriptionSet(body, currentUser.access_token));
            close();
        } else {
            NotificationManager.error("Vui lòng điền đầy đủ thông tin trước khi submit", "Thông báo", 5000);
        }
    }

    function validate(){
        if(diagnose === "" || diet === "" ||body_clean === "" ) return false;
        return true;
    }

    return <div className="prescription-sent">
        <Modal show={toggle} dialogClassName = "custom-prescription" onHide={close} >
            <Modal.Header closeButton>
                <Modal.Title>Kê đơn thuốc</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div className={"form-group"}>
                    <div><b>Chuẩn đoán bệnh</b></div>
                    <input className={"form-control"}
                           type={"text"}
                           placeholder={"Nhập tên bệnh"}
                           value={diagnose}
                           onChange={(e) => setDiagnose(e.target.value)}
                    />
                </div>
                <div className={"form-group"}>
                    <div><b>Thuốc</b></div>
                    <table className="table">
                        <thead>
                        <tr>
                            <th scope="col" style={{width: "250px"}}>Tên thuốc</th>
                            <th scope="col">Số lượng</th>
                            <th scope="col">Đơn vị</th>
                            <th scope="col">Cách dùng</th>
                            <th scope="col">Lưu ý đặc biệt</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            medicineItems
                        }
                        </tbody>
                    </table>
                    <button className={"btn btn-primary btn-sm"} onClick={addMedicine}>Thêm thuốc</button>
                </div>
                <div className={"form-group"}>
                    <div><b>Chế độ ăn uống</b></div>
                    <input className={"form-control"}
                           type={"text"}
                           value={diet}
                           onChange={(e) => setDiet(e.target.value)}
                    />
                </div>
                <div className={"form-group"}>
                    <div><b>Vệ sinh thân thể</b></div>
                    <input className={"form-control"}
                           type={"text"}
                           value={body_clean}
                           onChange={(e) => setBodyClean(e.target.value)}
                    />
                </div>
                <div className={"form-group"}>
                    <div><b>Lưu ý khác</b></div>
                    <input className={"form-control"}
                           type={"text"}
                           value={other_note}
                           onChange={(e) => setOtherNode(e.target.value)}
                    />
                </div>
                <div className={"form-group"}>
                    <div><b>Ngày tư vấn lại</b></div>
                    <input className={"form-control"}
                           type={"date"}
                           value={date_test_again}
                           onChange={e => setDateTestAgain(e.target.value)}
                    />
                </div>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="primary" onClick={submit}>
                    Kê đơn thuốc
                </Button>
            </Modal.Footer>
        </Modal>
    </div>
}

export default PrescriptionSent;
