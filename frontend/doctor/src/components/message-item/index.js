import React from "react";
import {Col, Container, Row} from "react-bootstrap";
import avatar from "../../assets/images/avatar.png";
import "./index.scss";
import * as MessageType from "../../config/message_type";
import dateformat from "dateformat";
import {useSelector} from "react-redux";

function MessageItem({isMessageOwner = false, message, onClick}) {
    const currentDSession = useSelector(state => state.currentDSession);

    function getContent(){
        switch (message.type) {
            case MessageType.MESSAGE_TEXT:
                return message.message;
            case MessageType.MESSAGE_IMAGE:
                return <img onClick={() => onClick(message.message)} src={message.message} alt={"Image"} style={{cursor: "pointer"}} />;
            case MessageType.MESSAGE_DISEASE_TEST:
                return <a href={message.obj_url} >Phiếu xét nghiệm </a>;
            case MessageType.MESSAGE_PRESCRIPTION_SENT:
                return <a href={message.obj_url} >Đơn thuốc</a>;
            case MessageType.MESSAGE_APPOINTMENT_SCHEDULE:
                return getTimeAppointment(message);
            default:
                break;
        }
    }

    function getTimeAppointment(message){
        let time = message.message;
        time = new Date(time);
        return <span>Bác sĩ đã đăt lịch hẹn cho bạn vào lúc <span className="badge badge-primary">
                    {time.getHours()}h:{time.getMinutes()}&nbsp;
                    ngày {dateformat(time, "dd:mm:yyyy")}
        </span></span>;
    }

    return <div className="message-item mb-md-3">
        <Container>
            <Row className={isMessageOwner ? "d-flex flex-row-reverse" : ""}>
                {isMessageOwner ? "" : <Col md={1} className={isMessageOwner ? "position-relative" : "ml--15 position-relative"}>
                    <img className={"avatar-friend"}
                         src={currentDSession.User.avatar ?? avatar}
                         alt={"Avatar"}
                         style={{objectFit: "cover"}}
                    />
                </Col>}
                <Col md={8} className={isMessageOwner ? "text-right " : "ml--15"}>
                    <div className="message-content p-md-3 mb-md-1">
                        {getContent()}
                    </div>
                </Col>
            </Row>
        </Container>
    </div>
}

export default MessageItem;
