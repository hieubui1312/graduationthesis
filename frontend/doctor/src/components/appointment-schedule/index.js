import React, {useState} from "react";
import {Modal, Button} from "react-bootstrap";
import dateformat from "dateformat";
import {NotificationManager} from "react-notifications";
import {callApi} from "../../redux/actions";
import {useDispatch, useSelector} from "react-redux";
import {orderAppointment} from "../../redux/actions/messageDSession";


function AppointmentSchedule({toggle, close}) {
    const [date, setDate] = useState(getCurrentDate());
    const [hour, setHour] = useState("00");
    const [minute, setMinute] = useState("00");

    const dispatch = useDispatch();
    const currentUser = useSelector(state => state.currentUser);
    const currentDSession = useSelector(state => state.currentDSession);

    function getCurrentDate(){
        return dateformat(new Date(), "yyyy-mm-dd");
    }

    function getHour(){
        let hourOptions = [], hour;
        for(let i = 0; i < 24; i++) {
            if (i < 10) hour = `0${i}`;
            else hour = i;

            hourOptions.push(<option key={`option-${i}`} value={hour}>
                {hour}
            </option>)
        }
        return hourOptions;
    }

    function submit(e){
        e.preventDefault();

        let dateStr = `${date} ${hour}:${minute}:00`;
        let dateObj = new Date(dateStr);

        if (dateObj.getTime() < Date.now()) {
            NotificationManager.error("Bạn không thể đặt lịch trong quá khứ", "Thông báo", 5000);
        }

        dispatch(orderAppointment({id: currentDSession.id, time: dateformat(dateObj, "yyyy-mm-dd HH:MM:ss")},
            currentUser.access_token));
        close();
    }


    return <Modal show={toggle} onHide={close}>
        <Modal.Header closeButton>
            <Modal.Title>Đặt lịch hẹn với bênh nhân</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <div>
                <form onSubmit={submit}>
                    <div className="form-group">
                        <label>Chọn ngày</label>
                        <input type={"date"} className={"form-control"}
                               value={date}
                               onChange={e => setDate(e.target.value)}
                        />
                    </div>
                    <div className="form-group">
                        <label className={"mr-md-2"}>Chọn giờ</label>
                        <select className={"mr-md-2"} value={hour} onChange={e => setHour(e.target.value)}>
                            {getHour()}
                        </select>
                        <select value={minute} onChange={e => setMinute(e.target.value)}>
                            <option value={"00"}>00</option>
                            <option value={"15"}>15</option>
                            <option  value={"30"}>30</option>
                            <option value={"45"}>45</option>
                        </select>
                    </div>
                    <div className="form-group text-right">
                        <button className={"btn btn-primary"}>
                            Đặt lịch khám
                        </button>
                    </div>
                </form>
            </div>
        </Modal.Body>
    </Modal>
}

export default AppointmentSchedule;
