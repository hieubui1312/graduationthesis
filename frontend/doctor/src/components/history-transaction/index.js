import React, {useEffect, useState} from "react";
import {Table} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {callApi} from "../../redux/actions";
import dateformat from "dateformat";
import CreditLog from "../../config/credit_log_file";

function HistoryTransaction() {
    const [logs, setLogs] = useState([]);

    const dispatch = useDispatch();
    const currentUser = useSelector(state => state.currentUser);

    useEffect(() => {
        fetchCreditLogs();
    }, []);

    function fetchCreditLogs(){
        dispatch(callApi('doctor/credit-change-log', currentUser.access_token, data => {
            setLogs(data.logs);
        }, error => {
            console.log("Error", error);
        }));
    }

    function getRowLogs(){
        return logs.map((item, index) => {
            return <tr key={`log-${index}`}>
                <td>{index}</td>
                <td>{getTimeCreated(item.created_at)}</td>
                <td>{item.credit_from}</td>
                <td>{item.credit_to}</td>
                <td>{item.reason}</td>
                <td>{getTypeTransaction(item.type)}</td>
            </tr>
        });
    }

    function getTimeCreated(time){
        return dateformat(new Date(time), "yyyy-mm-dd hh:mm:ss");
    }

    function getTypeTransaction(type){
        switch (type) {
            case CreditLog.ACCEPT_DSESSION:
                return "Khám bệnh";
            case CreditLog.CREATE_DSESSION:
                return "Tạo phiên";
        }
    }

    return <div className="history-transaction p-md-5">
        <Table striped bordered hover>
            <thead>
            <tr>
                <th>#</th>
                <th>Ngày giao dịch</th>
                <th>Số dư ban đầu</th>
                <th>Số dư sau </th>
                <th>Mô tả</th>
                <th>Loại giao dịch</th>
            </tr>
            </thead>
            <tbody>
                {getRowLogs()}
            </tbody>
        </Table>
    </div>
}

export default HistoryTransaction;
