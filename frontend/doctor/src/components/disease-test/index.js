import React, {useEffect, useState} from "react";
import {Button, Modal} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {callApi} from "../../redux/actions";
import "./index.scss";
import {NotificationManager} from "react-notifications";

function DiseaseTest({toggle, close}) {
    const dispatch = useDispatch();

    const currentUser = useSelector(state => state.currentUser);
    const currentDSession = useSelector(state => state.currentDSession);

    const [testGroup, setTestGroup] = useState([]);
    const [testSub, setTestSub] = useState([]);
    const [testParent, setTestParent] = useState();

    const [testArr, setTestArr] = useState([]);

    //Fetch nhom cac xet nghiem
    useEffect(() => {
        dispatch(callApi('doctor/disease-test/group', currentUser.access_token, (data) => {
            console.log("Data disease test", data);
            data = data.filter(item => {
               return item.parent_id == null;
            });
            setTestGroup(data);
            if (data.length > 0) {
                setTestParent(data[0].id)
            }
        }, error => {
            console.log("Error fetch disease test group");
        }))
    }, []);

    // Fetch xet nghiem cua 1 nhom
    useEffect(() => {
        if (!testParent) return;
        dispatch(callApi(`doctor/disease-test/${testParent}`, currentUser.access_token, (data) => {
            setTestSub(data);
        }, error => {
            console.log("Error fetch test sub", error);
        }))
    }, [testParent]);

    function addTestArr(test){
        if (testArr.length > 0) {
            let tests = testArr.map(t => t.id);
            let index = tests.indexOf(test.id);
            if (index === -1) {
                setTestArr([...testArr, test]);
            } else {
                setTestArr(testArr.splice(index, 1));
            }
        } else {
            setTestArr([...testArr, test]);
        }
    }

    function getTestGroup(){
        return <ul className="list-group">
            {
                testGroup.map((test, index) => {
                    let classTestGroup = test.id === testParent ? "list-group-item active" : "list-group-item";
                    return <li key={`test-group-${index}`}
                               onClick={() => setTestParent(test.id)}
                               className={classTestGroup}>
                        {test.name}
                    </li>
                })
            }
        </ul>
    }

    function checkTestExist(test){
        if (testArr.length === 0) return false;

        let tests = testArr.map(t => t.id);
        let index = tests.indexOf(test.id);

        return index !== -1;
    }

    function getTestSub(){
        return <ul className="list-group">
            {
                testSub.map((test, index) =>{
                    let classItemSub = checkTestExist(test) ? "list-group-item active" : "list-group-item";
                    return <li key={`test-sub-${index}`}
                               onClick={() => addTestArr(test)}
                               className={classItemSub}>{test.name}</li>;
                })
            }
        </ul>
    }

    function getTestSelected(){
        let testNames = testArr.map(test => test.name);
        return "Các thí nghiệm: " + testNames.join(", ");
    }

    function sendDiseaseTest(){
        if (testArr.length <=0) {
            alert("Chọn thí nghiệm trước khi submit");
            return false;
        }

        let disease_test = testArr.map(test => test.id);

        dispatch(callApi('doctor/dsession/disease-test', currentUser.access_token, data => {
            console.log("Data create disease test", data);
            close();
        }, error => {
            console.log("Error", error);
            NotificationManager.error("Có lỗi xảy ra", "Thông báo!");
        }, {
            dsession_id: currentDSession.id,
            disease_test: JSON.stringify(disease_test)
        }))
    }

    return <Modal show={toggle} dialogClassName="modal-90w" onHide={close}>
        <Modal.Header closeButton>
            <Modal.Title>Phiếu xét nghiệm</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <div className="row disease-test">
                <div className="col-md-3">
                    <h5>
                        Nhóm xét nghiệm
                    </h5>
                    {
                        getTestGroup()
                    }
                </div>
                <div className="col-md-9">
                    <h5>
                        Xét nghiệm
                    </h5>
                    {
                        getTestSub()
                    }
                </div>
            </div>
            <div className={"mt-md-3"}>
                {testArr.length > 0 ? getTestSelected() : ""}
            </div>
        </Modal.Body>
        <Modal.Footer>
            <Button variant="secondary" onClick = {close}>
                Close
            </Button>
            <Button variant="primary" onClick={sendDiseaseTest}>
                Gửi phiếu xét nghiệm
            </Button>
        </Modal.Footer>
    </Modal>
}

export default DiseaseTest;
