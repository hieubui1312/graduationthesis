import React, {useState} from "react";
import {NotificationManager} from "react-notifications";
import {useDispatch, useSelector} from "react-redux";
import {changePassword} from "../../redux/actions/currentUser";

function UpdatePasswordForm() {
    const [password, setPassword] = useState("");
    const [newPassword, setNewPassword] = useState("");
    const [rePassword, setRePassword] = useState("");

    const dispatch = useDispatch();
    const currentUser = useSelector(state => state.currentUser);

    function setPasswordInput(e){
        if (e.target.value.length <= 6)
        setPassword(e.target.value);
    }

    function setNewPasswordInput(e){
        if (e.target.value.length <= 6)
        setNewPassword(e.target.value);
    }

    function setRePasswordInput(e){
        if (e.target.value.length <= 6)
        setRePassword(e.target.value);
    }


    function submit(e){
        e.preventDefault();
        if (newPassword !== rePassword) {
            NotificationManager.error("Re-password của bạn không đúng chính xác vui lòng nhập lại", "Thông báo", 5000);
            return false;
        } else {
            dispatch(changePassword({oldPassword: password, newPassword}, currentUser.access_token));
        }
    }

    return <div className="update-password-form p-md-5">
        <form action="" onSubmit={submit}>
            <div className="form-group">
                <div className="row">
                    <div className="col-md-3 d-flex align-items-center">
                        <b>Mật khẩu cũ</b>
                    </div>
                    <div className="col-md-9">
                        <input type={"password"}
                               className={"form-control"}
                               placeholder={"Nhập mật khẩu cũ của bạn"}
                               name={"Password"}
                               onChange={setPasswordInput}
                               value={password}
                               required
                        />
                    </div>
                </div>
            </div>
            <div className="form-group">
                <div className="row">
                    <div className="col-md-3 d-flex align-items-center">
                        <b>Mật khẩu mới</b>
                    </div>
                    <div className="col-md-9">
                        <input type="password"
                               className={"form-control"}
                               placeholder={"Nhập mật khẩu mới"}
                               name={"NewPassword"}
                               onChange={setNewPasswordInput}
                               value={newPassword}
                               required
                        />
                    </div>
                </div>
            </div>
            <div className="form-group">
                <div className="row">
                    <div className="col-md-3 d-flex align-items-center">
                        <b>Gõ lại mật khẩu mới</b>
                    </div>
                    <div className="col-md-9">
                        <input type="password"
                               className={"form-control"}
                               placeholder={"Nhập lại mật khẩu mới"}
                               name={"RePassword"}
                               onChange={setRePasswordInput}
                               value={rePassword}
                               required
                        />
                    </div>
                </div>
            </div>
            <div className="form-group text-center">
                <button className={"btn btn-primary"}>Cập nhật</button>
            </div>
        </form>
    </div>
}

export default UpdatePasswordForm;
