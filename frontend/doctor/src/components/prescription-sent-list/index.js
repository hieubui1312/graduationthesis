import React, {useState} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faArrowCircleDown} from "@fortawesome/free-solid-svg-icons";
import {ListGroup} from "react-bootstrap";
import {useSelector} from "react-redux";
import * as MessageType from "../../config/message_type";


function PrescriptionSentList() {
    const [showDetail, setShowDetail] = useState(false);

    let messageDSession = useSelector(state => state.messageDSession);

    function getPrescriptionSent(){
        let prescriptions = messageDSession.filter(message => message.type === MessageType.MESSAGE_PRESCRIPTION_SENT);
        let presElement = [];
        prescriptions.forEach(pres => {
            presElement.push(<li>
                <a href={pres.obj_url}>
                    Phiếu khám bệnh {pres.message}
                </a>
            </li>);
        });
        return presElement;
    }

    return <div className="image-dsession mt-md-2">
        <div className={"d-flex justify-content-between align-items-center p-md-3 bg-primary text-light"}
             onClick={() => setShowDetail(!showDetail)} style={{cursor: "pointer"}}>
            <b>ĐƠN THUỐC</b>
            <FontAwesomeIcon icon={faArrowCircleDown} />
        </div>
        {
            showDetail ? <ListGroup>
                <ListGroup.Item>
                    {getPrescriptionSent()}
                </ListGroup.Item>
            </ListGroup> : ""
        }
    </div>
}

export default PrescriptionSentList;
