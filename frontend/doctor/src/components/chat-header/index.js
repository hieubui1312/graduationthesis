import React, {useEffect, useState} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCalendarAlt, faVideo} from "@fortawesome/free-solid-svg-icons";
import "./index.scss";
import AppointmentSchedule from "../appointment-schedule";
import VideoCallModal from "../videocall-modal";
import {useSelector} from "react-redux";
import firebaseAdmin from "../../util/firebase-client";
import * as VideoCallStatus from "../../config/video-call";
import Countdown from "react-countdown";


const countdownRenderer = ({ days, hours, minutes, seconds, completed }) => {
    if (completed) {
        // Render a completed state
        return "Phiên khám của bạn đã hoàn thành"
    } else {
        // Render a countdown
        let dayLabel = ''
        if (days > 0) {
            dayLabel = days + ' ngày + '
        }
        return (
            <span className='countdown'>({dayLabel}{hours}:{minutes}:{seconds})</span>
        )
    }
};

function ChatHeader() {
    const [toggleSchedule, setToggleSchedule] = useState(false);
    const currentDSession = useSelector(state => state.currentDSession);

    const [patientCalled, setPatientCalled] = useState(0);
    const currentUser = useSelector(state => state.currentUser);
    const database = firebaseAdmin.database();
    const [status, setStatus] = useState(-1);
    const [patient, setPatient] = useState(null);
    const [room, setRoom] = useState(null);
    const [startCall, setStartCall] = useState(false);
    const [endTime, setEndTime] = useState(1000);

    function onClickVideo(){
        if (currentDSession) {
            setPatientCalled(currentDSession.patient_id);
            let ref = database.ref(`patient-access-${currentDSession.patient_id}`);
            ref.once('value').then( snapshot => {
                let value = snapshot.val();
                if (value && value.status === VideoCallStatus.VIDEO_CALL_FIREBASE_ACCEPT) {
                    alert("Bệnh nhân đang có cuộc gọi. Vui lòng gọi lại sau");
                } else {
                    ref.set({
                        status: VideoCallStatus.VIDEO_CALL_FIREBASE_INVITE,
                        sender: "doctor",
                        fromUserName: currentUser.name,
                        room: `patient4455646464646-${currentDSession.patient_id}`
                    });
                    setStartCall(true);
                }
            });
        }
    }

    useEffect(() => {
        setEndTime(currentDSession.end_time);
    }, [currentDSession]);

    function handleClose(){
        setPatientCalled(0);
        database.ref(`patient-access-${currentDSession.patient_id}`).set({
            status: VideoCallStatus.VIDEO_CALL_FIREBASE_CANCEL,
            sender: "doctor",
            room: `patient4455646464646-${currentDSession.patient_id}`
        });
        setTimeout(() => {
            database.ref(`patient-access-${currentDSession.patient_id}`).remove();
        }, 1000);
    }

    useEffect(() => {
        if(currentDSession.patient_id && startCall){
            let data = database.ref(`patient-access-${currentDSession.patient_id}`);
            data.on("value", snapshot => {
                let value = snapshot.val();
                if (!value) return;
                setStatus(value.status);
                setPatient(value.fromUserName);
                setRoom(value.room);
                if (value.status === VideoCallStatus.VIDEO_CALL_FIREBASE_COMPLETE && value.sender === "patient"){
                    alert("Bệnh nhân đã kết thúc cuộc gọi");
                }
                // switch (value.status) {
                //     case 2:
                //         setPatientCalled(0);
                //         alert("Bệnh nhân đã từ chối cuộc gọi của bạn");
                //         break;
                //     default:
                //         return 0;
                // }
            });
        }
    }, [currentDSession, startCall]);

    useEffect(() => {
        if (!startCall){
            database.ref(`patient-access-${currentDSession.patient_id}`).off();
        }
    }, [startCall]);

    useEffect(() => {
        switch (status) {
            case 2:
                alert("Bệnh nhân đã từ chối cuộc gọi của bạn!");
                setStartCall(false);
                break;
            default:
                return;
        }
    }, [status]);

    function out(){
        database.ref(`patient-access-${currentDSession.patient_id}`).set({
            status: VideoCallStatus.VIDEO_CALL_FIREBASE_COMPLETE,
            sender: "doctor",
            fromUserName: currentUser.name,
            room: `patient4455646464646-${currentDSession.patient_id}`
        });
        setStartCall(false);
        setTimeout(() => {
            database.ref(`patient-access-${currentDSession.patient_id}`).remove();
        }, 1000);
    }

    function getCountDown(){
        let dateExpire = new Date(endTime * 1000);
        return <Countdown renderer={countdownRenderer}
                          date={dateExpire}
                          onComplete={() => console.log("Complete")}
        />
    }

    return <div className="chat-header p-md-3 d-flex justify-content-between align-items-center ml--15">
        <div className="time">
            <small><b>Thời gian: </b>
                {getCountDown()}
            </small>
        </div>
        <div className="video-call">
            <FontAwesomeIcon icon={faVideo}
                             onClick={onClickVideo}
            />
        </div>
        <div className="calendar-video"
             onClick={() => setToggleSchedule(true)}
             style={{cursor: "pointer"}}
        >
            <b>Đặt lịch hẹn: </b>
            <FontAwesomeIcon icon={faCalendarAlt} />
        </div>
        <AppointmentSchedule toggle={toggleSchedule}
                             close = {() => setToggleSchedule(false)}
        />
        <VideoCallModal status={status}
                        handleClose={handleClose}
                        patientName={currentDSession.User ? currentDSession.User.name : ""}
                        room={room}
                        out={out}
        />
    </div>
}

export default ChatHeader;
