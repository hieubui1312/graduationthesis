import {ListGroup} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faWallet} from "@fortawesome/free-solid-svg-icons";
import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {callApi} from "../../redux/actions";
import DSessionType from "../../config/dSession";


function Wallet() {
    const dispatch = useDispatch();
    const currentUser = useSelector(state => state.currentUser);
    const [dsessionAnalytic, setDSessionAnalytic] = useState([]);

    useEffect(() => {
        dispatch(callApi("doctor/account-info", currentUser.access_token, data => {
            console.log("Data", data);
            setDSessionAnalytic(data);
        }, error => {
            console.log("Error", error);
        }))
    }, []);

    function getTotalDSession(){
        if (dsessionAnalytic.length > 0) {
            return dsessionAnalytic.reduce((total, currentValue) => {
                return total + parseInt(currentValue.count);
            }, 0)
        }
        return  0;
    }

    function getDsessionCount(dsessionType){
        let dsession = dsessionAnalytic.filter(item => item.status == dsessionType);
        if (dsession.length > 0) return dsession[0].count;
        return 0;
    }

    return <div className="wallet-info p-md-5">
        <ListGroup>
            <ListGroup.Item variant="primary" className={"p-md-4 d-flex justify-content-between"}>
                <b>
                    <FontAwesomeIcon icon={faWallet} className={"mr-md-1"}/>
                    <span>Số dư tài khoản:</span>
                </b>
                <b>
                    {currentUser.credit}(VNĐ)
                </b>
            </ListGroup.Item>
            <ListGroup.Item variant="secondary" className={"d-flex justify-content-between"}>
                <b>Số tiền khoanh giữ</b>
                <b>{currentUser.credit_hold} (VNĐ)</b>
            </ListGroup.Item>
            <ListGroup.Item variant="secondary" className={"d-flex justify-content-between"}>
                <b>Tổng số phiên khám</b>
                <b>{getTotalDSession()}</b>
            </ListGroup.Item>
            <ListGroup.Item variant="secondary" className={"d-flex justify-content-between"}>
                <span>Phiên đang khám</span>
                <span>{getDsessionCount(DSessionType.STATUS_ACCEPT)}</span>
            </ListGroup.Item>
            <ListGroup.Item variant="secondary" className={"d-flex justify-content-between"}>
                <span>Phiên khám chờ</span>
                <span>{getDsessionCount(DSessionType.STATUS_WAITING)}</span>
            </ListGroup.Item>
            <ListGroup.Item variant="secondary" className={"d-flex justify-content-between"}>
                <span>Phiên hoành thành</span>
                <span>{getDsessionCount(DSessionType.STATUS_COMPLETE)}</span>
            </ListGroup.Item>
            <ListGroup.Item variant="secondary" className={"d-flex justify-content-between"}>
                <span>Phiên khám đã huỷ</span>
                <span>{getDsessionCount(DSessionType.STATUS_CANCEL)}</span>
            </ListGroup.Item>
            <ListGroup.Item variant="secondary" className={"d-flex justify-content-between"}>
                <span>Phiên khám từ chối</span>
                <span>{getDsessionCount(DSessionType.STATUS_DECLINE)}</span>
            </ListGroup.Item>
        </ListGroup>
    </div>
}

export default Wallet;
