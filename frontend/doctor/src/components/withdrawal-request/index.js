import React, {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {NotificationManager} from "react-notifications";
import {callApi} from "../../redux/actions";
import md5 from "md5";

function WithdrawalRequest() {
    const [amount, setAmount] = useState("");
    const [password, setPassword] = useState("");

    const currentUser = useSelector(state => state.currentUser);
    const dispatch = useDispatch();

    function onChange(e){
        setAmount(e.target.value)
    }

    function changePasswordInput(e){
        let {value} = e.target;
        if (value.length <= 6) {
            setPassword(value);
        }
    }

    function submit(e){
        e.preventDefault();
        if (parseInt(amount) > currentUser.credit){
            NotificationManager.error("Bạn không thể nào rút số tiền lớn hơn số tiền trong tài khoảng", "Thông báo", 5000);
            return false;
        }

        dispatch(callApi("doctor/withdraw-request", currentUser.access_token, data => {
            NotificationManager.success("Yêu cầu rút tiền của bạn đã được gửi", "Thông báo", 5000);
            setAmount("");
            setPassword("");
        }, error => {
            NotificationManager.error(error.message, "Lỗi");
        }, {
            amount
        }))
    }

    return <div className="withdrawal-request p-md-5">
        <div className="account-current bg-primary mb-md-4 p-md-3 text-light">
            <b>Số dư tài khoản: </b>
            <b>{currentUser.credit} (VNĐ)</b>
        </div>
        <form onSubmit={submit}>
            <div className="form-group">
                <div className="row">
                    <div className="col-md-3 d-flex align-items-center text-secondary">
                        <b>Số tiền cần rút:</b>
                    </div>
                    <div className="col-md-9">
                        <input type={"number"}
                               className={"form-control"}
                               placeholder={"Nhập số tiền bạn cần rút"}
                               value={amount}
                               onChange={onChange}
                        />
                    </div>
                </div>
            </div>
            <div className="form-group text-center">
                <button className={"btn btn-primary"}>Gửi yêu cầu</button>
            </div>
        </form>
    </div>
}

export default WithdrawalRequest;
