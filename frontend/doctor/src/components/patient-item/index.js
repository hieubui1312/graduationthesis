import React from "react";
import avatar from "../../assets/images/avatar.png";
import "./index.scss";
import {useDispatch, useSelector} from "react-redux";
import {setCurrentDSession} from "../../redux/actions/currentDSession";

function PatientItem({dSession}) {

    const currentDSession = useSelector(state => state.currentDSession);
    const currentUser = useSelector(state => state.currentUser);
    const dispatch = useDispatch();

    const classWrap = currentDSession.id === dSession.id ? "patient-item mb-md-2 patient-select" : "patient-item mb-md-2";

    function onClick(){
        dispatch(setCurrentDSession(dSession))
    }

    function formatLastMessage(message){
        if (message && message.length > 40){
            return message.substr(0, 40) + "...";
        }
        return message;
    }

    function getUserMessage(){
        if (currentUser.id !== dSession.last_message_user){
            return "Bn:"
        } else return "Bạn:"
    }

    return <div className={classWrap} onClick={onClick}>
        <div className="row p-md-2">
            <div className="col-md-3">
                <img className={"avatar-patient"} src={dSession.User.avatar ?? avatar} alt={"Avatar patient"}/>
            </div>
            <div className="col-md-8 mlr--15">
                <div className="patient-info">
                    <span className={"name-patient"}>
                        <b>{dSession.User.name}</b>
                    </span>
                    -
                    <span className={"code-patient"}>
                        Mã phiên: {dSession.id}
                    </span>
                </div>
                <div className="message-last">
                    <p>
                        <small>
                            {getUserMessage()} {formatLastMessage(dSession.last_message)}
                        </small>
                    </p>
                </div>
            </div>
        </div>
    </div>
}

export default PatientItem;
