import React from "react";
import "./index.scss";
import PatientInfo from "../patient-info";
import ImageDSessions from "../image-dsession";
import PrescriptionSentList from "../prescription-sent-list";
import DiseaseTestList from "../disease-test-list";

function InfoArea() {
    return <div className="info-area pr-2 ml--15">
        <div className="title">
            <PatientInfo/>
            <ImageDSessions/>
            <PrescriptionSentList/>
            <DiseaseTestList/>
        </div>
    </div>
}

export default InfoArea
