import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {callApi} from "../../redux/actions";
import Select from "react-select";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTimes} from "@fortawesome/free-solid-svg-icons";

export default function MedicineItem({index, onChangeMedicine, remove}) {
    // const [medicine, setMedicine] = useState("");
    // const [unit, setUnit] = useState("");
    // const [count, setCount] = useState(0);
    // const [usage, setUsage] = useState("");
    // const [note, setNote] = useState("");
    const [options, setOptions] = useState([]);

    const [medicine, setMedicine] = useState({});

    const currentUser = useSelector(state => state.currentUser);

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(callApi('doctor/medicine', currentUser.access_token, data => {
            data = data.map((item) => {
                return {value: item.name, label: item.name}
            });

            setOptions(data);
        }, error => {
            console.log("Error fetch medicine", error.message);
        }))
    }, []);

    function onChange(e){
        let {name, value} = e.target;
        setMedicine({
            ...medicine, [name]: value
        })
    }

    useEffect(() => {
        onChangeMedicine(medicine, index);
    }, [medicine]);

    function setTypeMedicine(e){
        setMedicine({
            ...medicine, medicine_id: e.value
        })
    }

    return <tr>
        <td>
            <Select options={options} onChange={(e) => setTypeMedicine(e)}/>
        </td>
        <td>
            <input type={"number"}
                   className={"form-control"}
                   name={"count"}
                   onChange={onChange}
            />
        </td>
        <td>
            <select className={"form-control"} name={"unit"} onChange={onChange}>
                <option value={"Hộp"}>Hộp</option>
                <option value={"Chai"}>Chai</option>
                <option value={"Viên"}>Viên</option>
                <option value={"Vỉ"}>Vỉ</option>
            </select>
        </td>
        <td>
            <input type={"text"}
                   className={"form-control"}
                   name={"usage"}
                   onChange={onChange}
            />
        </td>
        <td>
            <input type={"text"}
                   className={"form-control"}
                   name={"note"}
                   onChange={onChange}
            />
        </td>
        <td>
            <FontAwesomeIcon icon={faTimes}
                             style={{cursor: "pointer"}}
                             onClick={() => remove(index)}
            />
        </td>
    </tr>
}
