import React, {useEffect, useState} from "react";
import {Col, Container, Row} from "react-bootstrap";
import Menu from "../menu";
import {faUserMd} from "@fortawesome/free-solid-svg-icons";
import Header from "../header";
import {ListGroup} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faWallet} from "@fortawesome/free-solid-svg-icons";
import HistoryTransaction from "../history-transaction";
import WithdrawalRequest from "../withdrawal-request";
import WithdrawalHistory from "../withdrawal-history";
import Wallet from "../wallet";
import {useDispatch, useSelector} from "react-redux";
import {callApi} from "../../redux/actions";

let menus = [
    {icon: faUserMd, name: "Thông tin ví", id: 1},
    {icon: faUserMd, name: "Lịch sử giao dịch", id: 2},
    {icon: faUserMd, name: "Lịch sử rút tiền", id: 3},
    {icon: faUserMd, name: "Rút tiền", id: 4}
];

function WalletInfo() {
    let [tabActive, setTabActive] = useState(1);


    function getContentForm(){
        switch (tabActive) {
            case 1:
                return <Wallet/>;
            case 2:
                return <HistoryTransaction/>;
            case 3:
                return <WithdrawalHistory/>;
            case 4:
                return <WithdrawalRequest/>;
            default:
                return "";
        }
    }

    return <>
        <Header/>
        <Container fluid={true}>
            <Row>
                <Col md={3}>
                    <Menu
                        title={"Thông tin ví VietSkin"}
                        menus={menus}
                        tabActive={tabActive}
                        switchTab={(id) => {
                            console.log("id", id);
                            setTabActive(id)
                        }}
                    />
                </Col>
                <Col md={9}>
                    {getContentForm()}
                </Col>
            </Row>
        </Container>
        </>
}

export default WalletInfo;
