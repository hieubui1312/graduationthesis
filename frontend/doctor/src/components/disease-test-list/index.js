import React, {useEffect, useState} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faArrowCircleDown} from "@fortawesome/free-solid-svg-icons";
import {ListGroup} from "react-bootstrap";
import {useSelector} from "react-redux";
import * as MessageType from "../../config/message_type";

function DiseaseTestList() {
    const [showDetail, setShowDetail] = useState(false);

    const messageDSession = useSelector(state => state.messageDSession);

    function getDiseaseTest() {
        let diseaseTests = messageDSession.filter(message => message.type === MessageType.MESSAGE_DISEASE_TEST);
        let elements = [];
        diseaseTests.forEach((diseaseTest) => {
            elements.push(<li key={`disease-${diseaseTest.message}`}>
                <a href={diseaseTest.obj_url}>Phiếu xét nghiệm {diseaseTest.message}</a>
            </li>)
        });
        return elements;
    }

    return <div className="disease-test mt-md-2">
        <div className={"d-flex justify-content-between align-items-center p-md-3 bg-primary text-light"}
             onClick={() => setShowDetail(!showDetail)} style={{cursor: "pointer"}}>
            <b>PHIẾU XÉT NGHIỆM</b>
            <FontAwesomeIcon icon={faArrowCircleDown} />
        </div>
        {
            showDetail ? <ListGroup>
                <ListGroup.Item>
                    {getDiseaseTest()}
                </ListGroup.Item>
            </ListGroup> : ""
        }
    </div>
}

export default DiseaseTestList;
