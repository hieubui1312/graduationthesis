import React, {useEffect, useRef, useState} from "react";
import MessageItem from "../message-item";
import "./index.scss";
import ChatForm from "../chat-form";
import ChatHeader from "../chat-header";
import {useDispatch, useSelector} from "react-redux";
import {addMessage, fetchMessages, initialMessage} from "../../redux/actions/messageDSession";
import firebaseAdmin from "../../util/firebase-client";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faClock} from "@fortawesome/free-solid-svg-icons";
import * as MessageType from "../../config/message_type";
import dateFormat from "dateformat";
import ImageModal from "../image-modal";

function ChatBox() {
    const messagesRef = useRef();

    const messages = useSelector(state => state.messageDSession);
    const currentUser = useSelector(state => state.currentUser);
    const currentDSession = useSelector(state => state.currentDSession);
    const dispatch = useDispatch();

    const [image, setImage] = useState("");
    const images = messages.filter(message => {
       return message.type === MessageType.MESSAGE_IMAGE;
    });

    useEffect(() => {

    }, []);

    useEffect(() => {
        if (currentDSession.id) {
            dispatch(fetchMessages(currentDSession.id, currentUser.access_token));
        }
    }, [currentDSession.id]);

    useEffect(() => {
       setTimeout(() => {
           if (window.socket) {
               window.socket.on("chat-message", function (data) {
                   console.log("Message add", data);
                   dispatch(addMessage(data));
               })
           }
       }, 10);
    }, [currentUser]);

    useEffect(() => {
        scrollToBottom();
    }, []);

    useEffect(() => {
        scrollToBottom();
    }, [messages]);

    function getMessages(){
        return messages.map((message, index) => {
            if (message.user_id !== -1) {
                let isMessageOwner = message.user_id === currentUser.id;
                return <MessageItem message={message}
                                    key={`message-${index}`}
                                    isMessageOwner={isMessageOwner}
                                    onClick={(image) => setImage(image)}
                />
            } else return <div key={`message-${index}`}
                               className={"text-center text-danger"}
            >
                {message.message}
            </div>
        })
    }

    function scrollToBottom(){
        if (messagesRef.current) {
            setTimeout(() => {
                messagesRef.current.scrollTop = messagesRef.current.scrollHeight;
            }, 150);
        }
    }

    function getAppointment() {
        return messages.filter(item => {
           return item.type === MessageType.MESSAGE_APPOINTMENT_SCHEDULE && new Date(item.message).getTime() > Date.now();
        });
    }
    const appointments = getAppointment();


    function formatDate(date){
        if (date) {
            return dateFormat(new Date(date), "hh:MM dd:mm")
        }
    }

    function prev(){
        let index = images.findIndex(item => {
            return item.message === image;
        });
        if (index === 0) {
            setImage(images[images.length - 1].message);
        } else {
            setImage(images[index - 1].message);
        }
    }

    function next(){
        let index = images.findIndex(item => {
            return item.message === image;
        });
        if (index === images.length - 1){
            setImage(images[0].message);
        } else {
            setImage(images[index + 1].message);
        }
    }

    return <div className="chat-box">
        {
            [1, 2].indexOf(currentDSession.status) !== -1 ? <ChatHeader/> : ""
        }
        <div className="message-list" ref={messagesRef}>
            {
                appointments.length > 0 ? <div style={{position: "fixed", top: "150px", left: "370px", zIndex: 10}}>
                    <div className={"position-relative"}>
                        <div className="appointment">
                            <FontAwesomeIcon icon={faClock} /> Hẹn khám
                        </div>
                        <div className={"position-absolute"} >
                            {
                                appointments.map((item, index) => {
                                    return <div key={`dateformat-${index}`}>{formatDate(item.message)}</div>
                                })
                            }
                        </div>
                    </div>
                </div> : ""
            }
            {getMessages()}
        </div>
        <ChatForm/>
        <ImageModal image={image}
                    onHide={() => setImage("")}
                    prev={prev}
                    next={next}
        />
    </div>
}

export default ChatBox;
