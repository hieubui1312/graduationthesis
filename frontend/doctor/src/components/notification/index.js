import ReactCSSTransitionGroup from "react-addons-css-transition-group";
import React, {useState} from "react";
import {ListGroup} from "react-bootstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faBell} from "@fortawesome/free-solid-svg-icons";
import "./index.scss";
import {useDispatch, useSelector} from "react-redux";
import dsessions from "../../redux/reducers/dsessions";
import {Modal, Button } from "react-bootstrap";
import {postAcceptDSession, postDeclineDSession} from "../../redux/actions/dsessions";

function IconNotification({onClick, count, toggleDot}) {

    function getCountNotification(){
        if (count > 0) return <div className="counter-notify">{count}</div>;
    }

    return <div style={{fontSize: "20px", position: "relative"}}>
        <FontAwesomeIcon icon={faBell} size={"lg"} onClick={onClick}/>
        {toggleDot ? <div className="dot-notify" /> : ""}
        {getCountNotification()}
    </div>
}
function Notification() {
    let [toggleNotification, setToggleNotification] = useState(false);

    const dSessions = useSelector(state => state.dsessions);
    const dSessionWaiting = dSessions.waiting ?? [];
    const currentUser = useSelector(state => state.currentUser);
    const dispatch = useDispatch();

    const [show, setShow] = useState(false);
    const [dSession, setDSession] = useState({});

    function showNotifications(toggle) {
        if (toggle) return <ListGroup as="ul" >
            {getNotifications()}
        </ListGroup>;
    }

    function selectDSession(dsession){
        setDSession(dsession);
        setShow(true);
        setToggleNotification(false);
    }

    function getNotifications(){
        if (dSessionWaiting) {
            return <ListGroup as={"ul"}>
                {dSessionWaiting.map((dsession, index) =>
                    <ListGroup.Item
                        onClick={() => selectDSession(dsession)}
                        key={`dsession-waiting-${index}`}
                        style={{cursor: "pointer"}} as="li">
                        {`Phiên khám ${dsession.id} - Bn: ${dsession.User.name}`}
                    </ListGroup.Item>
                )}
            </ListGroup>
        }
        return "";
    }

    function getToggleDot(){
        if (dSessionWaiting && dSessionWaiting.length > 0){
            return true;
        }
        return false;
    }


    function declineDSession(){
        dispatch(postDeclineDSession(dSession, currentUser.access_token));
        setShow(false);
    }

    function acceptDSession(){
        dispatch(postAcceptDSession(dSession, currentUser.access_token));
        setShow(false);
    }

    return <div className="notification-header mr-md-5" style={{position: "relative"}}>
        <IconNotification
            onClick={() => setToggleNotification(!toggleNotification)}
            toggleDot={getToggleDot()}
        />
        <div className={"notification-content bg-white"} >
            {showNotifications(toggleNotification)}
        </div>
        <Modal show={show} onHide={() => setShow(false)}>
            <Modal.Header closeButton>
                <Modal.Title>Yêu cầu khám bệnh</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div>
                    <b>Mã phiên khám: </b>{dSession.id}
                </div>
                <div>
                    <b>Bệnh nhân: </b>{dSession.User ? dSession.User.name : ""}
                </div>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={declineDSession}>
                    Từ chối
                </Button>
                <Button variant="primary" onClick={acceptDSession}>
                    Chấp nhận
                </Button>
            </Modal.Footer>
        </Modal>
    </div>
}

export default Notification
