import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import "./index.scss";
import * as Server from "../../config/server_path";
import {updateInfoUser} from "../../redux/actions/currentUser";

function UpdateInfoForm() {
    let [userInfo, setUserInfo] = useState({name: "", phone: "", sex: ""});
    let [imageSrc, setImageSrc] = useState({});
    let [file, setFile] = useState(null);

    const dispatch = useDispatch();
    const currentUser = useSelector(state => state.currentUser);

    useEffect(() => {
        setUserInfo(currentUser);
        setImageSrc(currentUser.avatar);
    }, [currentUser]);

    function onChange(e){
        let {name, value} = e.target;
        let userInfoNew = {
          ...userInfo, [name]: value
        };
        setUserInfo(userInfoNew);
    }

    function getAvatar(){
        if (imageSrc) return <img className={"avatar"} src={imageSrc}/>;
        return "";
    }

    function selectAvatar(e){
        let {files} = e.target;
        let file = files[0];
        setFile(file);

        let reader = new FileReader();
        reader.onloadend = () => {
            let base64Image = reader.result;
            setImageSrc(base64Image);
        };
        reader.readAsDataURL(file);
    }

    async function update(e){
        e.preventDefault();
        if (file) {
            let url = await uploadAvatar(file);
            userInfo = {
                ...userInfo, avatar: url
            }
        }
        dispatch(updateInfoUser(userInfo, currentUser.access_token));
    }

    async function uploadAvatar(file){
        const formData = new FormData();
        formData.append("file", file);

        let result = await fetch(`${Server.API_SERVER}common/upload`, {
            method: "POST",
            body: formData
        });
        result = await result.json();

        return  result.fileUrl;
    }

    return <div className="update-info-form p-md-5">
        <form onSubmit={update}>
            <div className="form-group">
                <div className="row">
                    <div className="col-md-3 text-secondary ">
                        <b>Trạng thái</b>
                    </div>
                    <div className="col-md-9">
                        <span className={"badge badge-success"}>Đã xác thực</span>
                    </div>
                </div>
            </div>
            <div className="form-group">
                <div className="row">
                    <div className="col-md-3 text-secondary">
                        <b>Ảnh đại diện</b>
                    </div>
                    <div className="col-md-9">
                        {getAvatar()}
                        <input type={"file"}
                               className={"ml-md-2"}
                               name={"avatar"}
                               accept="image/*"
                               onChange={selectAvatar}
                        />
                    </div>
                </div>
            </div>
            <div className="form-group">
                <div className="row">
                    <div className="col-md-3 text-secondary d-flex align-items-center">
                        <b>Họ tên</b>
                    </div>
                    <div className="col-md-9">
                        <input className={"form-control"}
                               type={"text"}
                               placeholder={"Nhập họ tên của bạn"}
                               value={userInfo.name}
                               name={"name"}
                               onChange={onChange}
                        />
                    </div>
                </div>
            </div>
            <div className="form-group">
                <div className="row">
                    <div className="col-md-3 text-secondary d-flex align-items-center">
                        <b>Giới tính</b>
                    </div>
                    <div className="col-md-9">
                        <div className="form-check form-check-inline">
                            <input className="form-check-input"
                                   type="radio"
                                   value={"male"}
                                   name={"sex"}
                                   checked={userInfo.sex === "male"}
                                   onChange={onChange}
                            />
                                   <label className="form-check-label">Nam</label>
                        </div>
                        <div className="form-check form-check-inline">
                            <input className="form-check-input"
                                   type="radio"
                                   value={"female"}
                                   name={"sex"}
                                   checked={userInfo.sex === "female"}
                                   onChange={onChange}
                            />
                                <label className="form-check-label" htmlFor="inlineRadio2">Nữ</label>
                        </div>
                    </div>
                </div>
            </div>
            <div className="form-group">
                <div className="row">
                    <div className="col-md-3 text-secondary d-flex align-items-center">
                        <b>Số điện thoại</b>
                    </div>
                    <div className="col-md-9">
                        <input className={"form-control"}
                               type={"text"}
                               name={"phone"}
                               placeholder={"Nhập số điện thoại của bạn"}
                               value={userInfo.phone}
                               onChange={onChange}
                        />
                    </div>
                </div>
            </div>
            <div className="form-group text-center">
                <button type={"submit"} className={"btn btn-primary"}>
                    Cập nhật
                </button>
            </div>
        </form>
    </div>
}

export default UpdateInfoForm;
