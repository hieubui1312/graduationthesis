import React, {useEffect, useState} from "react";
import {ListGroup} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faArrowCircleDown} from "@fortawesome/free-solid-svg-icons";
import {useDispatch, useSelector} from "react-redux";
import {getPatientInfo} from "../../redux/actions/dsessions";

function PatientInfo() {
    let [showDetail, setShowDetail] = useState(true);
    let [patientInfo, setPatientInfo] = useState({});

    const currentDSession = useSelector(state => state.currentDSession);
    const currentUser = useSelector(state => state.currentUser);
    const dispatch = useDispatch();

    useEffect(() => {
        if (currentUser.access_token && currentDSession.id) {
            dispatch(getPatientInfo(currentDSession.id, currentUser.access_token, data => {
                if (data.patient){
                    setPatientInfo(data.patient);
                }
            }, error => {
                console.log("error", error);
            }));
        }
    }, [currentDSession]);

    return <div className="patient-info mt-md-2">
        <div className={"d-flex justify-content-between align-items-center p-md-3 bg-primary text-light"}
             onClick={() => setShowDetail(!showDetail)} style={{cursor: "pointer"}}>
            <b>THÔNG TIN BỆNH NHÂN</b>
            <FontAwesomeIcon icon={faArrowCircleDown} />
        </div>
        {
            showDetail && currentDSession.User ? <ListGroup>
                <ListGroup.Item>Họ tên: {patientInfo.name ?? currentDSession.User.name}</ListGroup.Item>
                <ListGroup.Item>Tuổi: {patientInfo.age ?? currentDSession.User.age}</ListGroup.Item>
                <ListGroup.Item>Giới tính: {(patientInfo.sex ?? currentDSession.User.sex) === "male" ? "Nam" : "Nữ"}</ListGroup.Item>
                <ListGroup.Item>Triệu chứng: {patientInfo.description ?? "Chưa có"}</ListGroup.Item>
                <ListGroup.Item>Thời gian bắt đầu: {patientInfo.time_begin ?? "Chưa có"}</ListGroup.Item>
            </ListGroup> : ""
        }
    </div>
}
export default PatientInfo;
