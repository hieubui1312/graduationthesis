import React, {useState} from "react";
import {Col, Container, Row} from "react-bootstrap";
import Menu from "../menu";
import Header from "../header";
import {faUserMd} from "@fortawesome/free-solid-svg-icons";
import UpdateInfoForm from "../update-info-form";
import UpdateSpecializeForm from "../update-specialize-form";
import UpdateAtmForm from "../update-atm-form";
import UpdatePasswordForm from "../update-password-form";
import {useDispatch, useSelector} from "react-redux";

const TAB_ID = {
    PERSONAL_INFORMATION : 1,
    SPECIALIZE: 2,
    ATM_FORM: 3,
    PASSWORD_CHANGE: 4
};

const menus = [
    {icon: faUserMd, name: "Thông tin cá nhân", id: 1},
    {icon: faUserMd, name: "Chuyên môn", id: 2},
    {icon: faUserMd, name: "Tài khoản ngân hàng", id: 3},
    // {icon: faUserMd, name: "Đổi mật khẩu", id: 4}
];
function PersonalInformation() {
    let [tabID, setTabID] = useState(TAB_ID.PERSONAL_INFORMATION);

    const dispatch = useDispatch();
    const currentUser = useSelector(state => state.currentUser);

    function getContentForm(){
        switch (tabID) {
            case TAB_ID.PERSONAL_INFORMATION:
                return <UpdateInfoForm/>;
            case TAB_ID.SPECIALIZE:
                return <UpdateSpecializeForm/>;
            case TAB_ID.ATM_FORM:
                return <UpdateAtmForm/>;
            case TAB_ID.PASSWORD_CHANGE:
                return <UpdatePasswordForm/>;
        }
    }

    return <>
        <Header/>
        <div className="content">
            <Container fluid={true}>
                <Row>
                    <Col md={3}>
                        <Menu
                            tabActive={tabID}
                            title={"Thông tin cá nhân"}
                            menus={menus}
                            switchTab={(id) => setTabID(id)}
                        />
                    </Col>
                    <Col md={9}>
                        {getContentForm()}
                    </Col>
                </Row>
            </Container>
        </div>
        </>
}

export default PersonalInformation;
