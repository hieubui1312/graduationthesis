import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {getInfoSpecial, updateInfoSpecial} from "../../redux/actions/currentUser";

function UpdateAtmForm() {
    const [info, setInfo] = useState({});

    const dispatch = useDispatch();
    const currentUser = useSelector(state => state.currentUser);

    useEffect(() => {
        dispatch(getInfoSpecial(currentUser, (data) => {
            setInfo(data);
        }));
    }, []);

    function onChange(e){
        let {value, name} = e.target;
        let infoNew = {...info, [name]: value};
        setInfo(infoNew);
    }

    function submit(e){
        e.preventDefault();
        let body = {...info};
        delete body.Hospital;
        dispatch(updateInfoSpecial(body, currentUser.access_token));
    }

    return <div className="update-atm-form p-md-5">
        <form onSubmit={submit}>
            <div className="form-group">
                <div className="row">
                    <div className="col-md-3 text-secondary d-flex align-items-center">
                        <b>Tên ngân hàng</b>
                    </div>
                    <div className="col-md-9">
                        <input type={"text"}
                               className={"form-control"}
                               placeholder={"Nhập tên ngân hàng"}
                               value={info.atm_info ?? ""}
                               name={"atm_info"}
                               onChange={onChange}
                        />
                    </div>
                </div>
            </div>
            <div className="form-group">
                <div className="row">
                    <div className="col-md-3 text-secondary d-flex align-items-center">
                        <b>Số tài khoản</b>
                    </div>
                    <div className="col-md-9">
                        <input type={"text"} className={"form-control"}
                               placeholder={"Nhập số tài khoản của bạn"}
                               value={info.atm_number ?? ""}
                               name={"atm_number"}
                               onChange={onChange}
                        />
                    </div>
                </div>
            </div>
            <div className="form-group">
                <div className="row">
                    <div className="col-md-3 text-secondary d-flex align-items-center">
                        <b>Tên chủ tài khoản</b>
                    </div>
                    <div className="col-md-9">
                        <input type={"text"} className={"form-control"}
                               placeholder={"Nhập tên chủ tài khoản ngân hàng"}
                               value={info.atm_own_name ?? ""}
                               name={"atm_own_name"}
                               onChange={onChange}
                        />
                    </div>
                </div>
            </div>
            <div className="form-group text-center">
                <button className={"btn btn-primary"}>Cập nhật</button>
            </div>
        </form>
    </div>
}

export default UpdateAtmForm;
