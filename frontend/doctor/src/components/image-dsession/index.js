import React, {useEffect, useState} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faArrowCircleDown} from "@fortawesome/free-solid-svg-icons";
import {ListGroup} from "react-bootstrap";
import {useSelector} from "react-redux";
import * as MessageType from "../../config/message_type";
import "./index.scss";
import {Modal} from "react-bootstrap";
import ImageModal from "../image-modal";
import {addDSessionWaiting} from "../../redux/actions/dsessions";

function ImageDSessions() {
    let [showDetail, setShowDetail] = useState(false);
    let [images, setImages] = useState([]);
    let [image, setImage] = useState("");

    let messageDSessions = useSelector(state => state.messageDSession);

    useEffect(() => {
        let imagesFilter = messageDSessions.filter(image => image.type === MessageType.MESSAGE_IMAGE);
        setImages(imagesFilter);
    }, [messageDSessions]);

    function getImages(){
        let imagesElement = [];
        images.forEach((image, index) => {
            imagesElement.push(<img alt={"image"}
                                    className={"image-item"}
                                    src={image.obj_url}
                                    onClick={() => setImage(image.obj_url)}
                                    key={`image-${index}`} />)
        });
        return imagesElement;
    }

    function prev() {
        const index = images.findIndex(item => item.obj_url === image);
        if (index === 0) {
            setImage(images[images.length - 1].obj_url)
        } else {
            setImage(images[index - 1].obj_url)
        }
    }

    function next(){
        const index = images.findIndex(item => item.obj_url === image);
        if (index === (images.length - 1)) {
            setImage(images[0].obj_url)
        } else {
            setImage(images[index + 1].obj_url)
        }
    }

    return <div className="image-dsession mt-md-2">
        <div className={"d-flex justify-content-between align-items-center p-md-3 bg-primary text-light"}
             onClick={() => setShowDetail(!showDetail)} style={{cursor: "pointer"}}>
            <b>HÌNH ẢNH CHIA SẺ</b>
            <FontAwesomeIcon icon={faArrowCircleDown} />
        </div>
        {
            showDetail ? <ListGroup>
                <ListGroup.Item>
                    {getImages()}
                </ListGroup.Item>
            </ListGroup> : ""
        }
        <ImageModal image={image}
                    onHide={() => setImage("")}
                    prev={prev}
                    next={next}
        />
    </div>
}

export default ImageDSessions;
