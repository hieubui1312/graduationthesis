import React from "react";
import {Modal} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faChevronLeft, faChevronRight} from "@fortawesome/free-solid-svg-icons";

const ImageModal = ({image, onHide, prev, next}) => {
    return <div className="image-modal">
        <Modal show={image !== ""} onHide={onHide}>
            <Modal.Body>
                <div className={"text-center d-flex align-items-center justify-content-center"}>
                    <FontAwesomeIcon
                        icon={faChevronLeft}
                        style={{fontSize: "20px", cursor: "pointer"}}
                        onClick={prev}
                    />
                    <img className={"image-selected"} alt={"image"} src={image}/>
                    <FontAwesomeIcon
                        icon={faChevronRight}
                        style={{fontSize: "20px", cursor: "pointer"}}
                        onClick={next}
                    />
                </div>
            </Modal.Body>
        </Modal>
    </div>
};

export default ImageModal;
