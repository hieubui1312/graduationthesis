import * as firebase from "firebase";

var firebaseConfig = {
    apiKey: "AIzaSyC8pn-AqLaE2TJQYd1o335qgU7-Bj-a3zs",
    authDomain: "patientaccess-bfeab.firebaseapp.com",
    databaseURL: "https://patientaccess-bfeab.firebaseio.com",
    projectId: "patientaccess-bfeab",
    storageBucket: "patientaccess-bfeab.appspot.com",
    messagingSenderId: "369644303597",
    appId: "1:369644303597:web:1c73736f47d5ee048e48e4",
    measurementId: "G-X1NKQYLKSC"
};

var app = firebase.initializeApp(firebaseConfig);

export default app;

