export const VIDEO_CALL_FIREBASE_INVITE = 0;
export const VIDEO_CALL_FIREBASE_ACCEPT = 1;
export const VIDEO_CALL_FIREBASE_DECLINE = 2;
export const VIDEO_CALL_FIREBASE_CANCEL = 3;
export const VIDEO_CALL_FIREBASE_COMPLETE = 4;
