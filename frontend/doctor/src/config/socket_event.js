module.exports = {
    LOGIN: "login",
    LOGIN_SUCCESS: "login_success",
    LOGIN_FAIL: "login_fail",

    JOIN_ROOM: "join_room",

    CHAT: "chat-message",

    INTERNAL_ERROR: "internal_error",
    AUTH_FAIL: "auth_fail"
};
