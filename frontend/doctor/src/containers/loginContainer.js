import Login from "../components/login-form";
import {callApi} from "../redux/actions";
import {connect} from "react-redux";
import {setCurrentUser} from "../redux/actions/currentUser";
import passCodeFlag from "../redux/reducers/passcodeFlag";
import {withRouter} from "react-router-dom";
import {NotificationManager} from "react-notifications";

const mapStateToProps = (state) => {
    return {
        currentUser: state.currentUser,
        passCodeFlag: state.passCodeFlag
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        login: (token) => dispatch(callApi("doctor/login", "null", (data, dispatch) => {
                dispatch(setCurrentUser(data.user));
            }, error => {
                console.log("Error login", error);
                NotificationManager.error("Login thất bại", "Lỗi", 5000);
            }, {token})
        ),
        createPassword : (accessToken, password) => dispatch(callApi("doctor/create-password", accessToken, (data, dispatch ) => {
            console.log("Data create password", data);
        }, (error) =>{
            console.log("Error create password", error)
        }, {
            password
        }))
    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Login));
