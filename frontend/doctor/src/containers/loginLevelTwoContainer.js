import LoginLevelTwo from "../components/login-level-tow";
import {connect} from "react-redux";
import {callApi} from "../redux/actions";
import {NotificationManager} from "react-notifications";
import {setCurrentUser} from "../redux/actions/currentUser";
import {turnOnPassCode} from "../redux/actions/passCode";

const mapStateToProps = (state) => {
    return {
        currentUser: state.currentUser
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        loginLevelTwo: (password, accessToken) => dispatch(callApi("doctor/check-password", accessToken, (data, dispatch ) => {
            dispatch(turnOnPassCode());
        }, error => {
            NotificationManager.error("Password của bạn không đúng. Vui lòng nhập lại!", "Thông báo", 5000);
        }, {
            password
        }))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginLevelTwo)


