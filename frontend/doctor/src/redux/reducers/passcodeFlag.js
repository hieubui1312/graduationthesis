import * as Action from "../constants/action";

export default function passCodeFlagReducer(state = true, action) {
    switch (action.type) {
        case Action.TURN_ON_PASS_CODE_FLAG:
            return true;
        default:
            return state
    }
}
