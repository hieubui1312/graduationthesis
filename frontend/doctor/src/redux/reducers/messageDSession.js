import * as Action from "../constants/action";


export default function MessageDSessionReducer(state = [], action) {
    switch (action.type) {
        case Action.INITIAL_MESSAGES_DSESSION:
            return action.messages;
        case Action.ADD_MESSAGE:
            return [ ...state, action.message];
        case Action.ADD_MESSAGES:
            return [...action.messages, ...state];
        default:
            return state;
    }
}
