import * as Action from "../constants/action";

export default function currentUserReducer(state = {}, action) {
    switch (action.type) {
        case Action.SET_CURRENT_USER:
            return action.currentUser;
        case Action.UPDATE_ACCESS_TOKEN:
            return {...state, access_token: action.access_token};
        case Action.REMOVER_CURRENT_USER:
            return {};
        default:
            return state;
    }
}
