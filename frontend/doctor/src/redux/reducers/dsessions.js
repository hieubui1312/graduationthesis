import * as Action from "../constants/action";
import {MESSAGE_DISEASE_TEST, MESSAGE_IMAGE, MESSAGE_PRESCRIPTION_SENT} from "../../config/message_type";

export default function dSessionsReducer(state = {}, action) {
    switch (action.type) {
        case Action.INITIAL_DSESSIONS:
            return action.dsession;
        case Action.DECLINE_DSESSION:
            let newState = {...state};
            let dsession = action.dsession;

            newState.decline = [...newState.decline, dsession];

            let waiting = newState.waiting;
            waiting = waiting.filter(d => d.id !== dsession.id);
            newState.waiting = waiting;

            return newState;
        case Action.ACCEPT_DSESSION:
            let stateNew = {...state};
            let dsession1 = action.dsession;

            stateNew.accept = [dsession1, ...stateNew.accept];

            let dsessionWaiting = stateNew.waiting;
            dsessionWaiting = dsessionWaiting.filter(d => d.id !== dsession1.id);
            stateNew.waiting = dsessionWaiting;
            return stateNew;
            return ;
        case Action.ADD_DSESSION_WAITING:
            let stateNew1 = Object.assign({}, state);
            stateNew1.waiting = [action.dsession, ...stateNew1.waiting];
            return stateNew1;
        case Action.CHANGE_NEW_MESSAGE:
            let message = action.message;
            let stateNew2 = Object.assign({}, state);
            let index = -1;
            stateNew2.accept = stateNew2.accept.map((item, key) => {
               if (item.id === message.dsession_id) {
                   if (message.type === MESSAGE_IMAGE)
                       item.last_message = "Gửi hình ảnh";
                   else if(message.type === MESSAGE_PRESCRIPTION_SENT){
                       item.last_message = "Gửi đơn thuốc"
                   } else if (message.type === MESSAGE_DISEASE_TEST){
                       item.last_message = "Gửi phiếu xét nghiệm"
                   } else item.last_message = message.message;
                   item.last_message_user = message.user_id;
                   index = key;
               }
               return item;
            });
            stateNew2.accept.sort((x, y) => {
                return x.id === message.dsession_id ? -1 : y.id === message.dsession_id ? 1 : 0
            });
            console.log("State new step two", stateNew2);
            return stateNew2;
        default:
            return state;
    }
}
