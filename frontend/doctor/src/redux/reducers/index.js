import {combineReducers} from "redux";
import currentUser from "./currentUser";
import passCodeFlag from "./passcodeFlag";
import dsessions from "./dsessions";
import messageDSession from "./messageDSession";
import currentDSession from "./currentDSession";

export default combineReducers({
    currentUser,
    passCodeFlag,
    dsessions,
    messageDSession,
    currentDSession
})
