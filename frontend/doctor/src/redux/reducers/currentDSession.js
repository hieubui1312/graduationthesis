import * as Action from "../constants/action";

export default function currentDSession(state = {}, action) {
    switch (action.type) {
        case Action.SET_CURRENT_DSESSION:
            console.log("DSession current", state);
            return action.dsession;
        default:
            return state;
    }
}
