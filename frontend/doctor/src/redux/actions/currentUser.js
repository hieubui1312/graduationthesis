import * as Action from "../constants/action";
import {callApi} from "./index";
import {NotificationManager} from "react-notifications";

export const setCurrentUser = (currentUser) => {
    localStorage.setItem("currentUser", JSON.stringify(currentUser));
    return {
        type: Action.SET_CURRENT_USER,
        currentUser
    }
};

export const getInfoSpecial = (currentUser, success) => {
    return callApi("doctor/specialize", currentUser.access_token, (data, dispatch) => {
        console.log("Data", data);
        success(data);
    }, error => {
        console.log("Error", error);
    });
};

export const updateInfoSpecial = (body, access_token) => {
    return callApi("doctor/update-info-special", access_token, (data, dispatch) => {
        NotificationManager.success("Update thông tin thành công");
        console.log("Data update info", data);
    }, error => {
        NotificationManager.error("Không thể update thông tin", "Lỗi", 5000);
        console.log("Error", error);
    }, body)
};

export const changePassword= (body, access_token) => {
    return callApi("doctor/change-password", access_token, (data, dispatch) => {
        NotificationManager.success("Thay đổi password thành công", "Thông báo", 5000);
    }, error => {
        console.log("error message", error);
        NotificationManager.error(error.message, "Lỗi", 5000);
    }, body);
};

export const updateInfoUser = (body, access_token) => {
    return callApi("doctor/update-info", access_token, (data, dispatch) => {
        let currentUser = localStorage.getItem("currentUser");
        currentUser = JSON.parse(currentUser);
        currentUser = Object.assign(currentUser, body);

        dispatch(setCurrentUser(currentUser));

        localStorage.setItem("currentUser", JSON.stringify(currentUser));

        NotificationManager.success("Update thông tin thành công", "Thông báo", 5000);
    }, error => {
        NotificationManager.error("Update thông tin bị lỗi", "Thông báo", 5000);
    }, body)
};

export const updateCurrentAccessToken = (accessToken) => {
    let currentUser = localStorage.getItem("currentUser");
    currentUser = JSON.parse(currentUser);
    currentUser.access_token = accessToken;
    localStorage.setItem("currentUser", JSON.stringify(currentUser));

    return {
        type: Action.UPDATE_ACCESS_TOKEN,
        access_token: accessToken
    }
};

export const logout = () => {
    localStorage.removeItem("currentUser");
    return {
        type: Action.REMOVER_CURRENT_USER
    }
};
