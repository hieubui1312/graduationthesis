import * as Action from "../constants/action";

export const setCurrentDSession = (dsession) => {
    return {
        type: Action.SET_CURRENT_DSESSION,
        dsession
    }
};
