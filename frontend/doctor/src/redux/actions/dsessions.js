import * as Action from "../constants/action";
import {callApi} from "./index";
import DSessionStatus from "../../config/dSession";
import {NotificationManager} from "react-notifications";

export const postAcceptDSession = (dSession, accessToken) => {

    return callApi('doctor/dsession/accept', accessToken, (data, dispatch) => {
        dSession.status = DSessionStatus.STATUS_ACCEPT;

        dispatch(acceptDSession(dSession));

        NotificationManager.success("Chấp nhận phiên khám thành công", "Thông báo");
    }, (error) => {
        console.log("Error accept dsession", error);
    }, {id: dSession.id})
};

export const addDSessionWaiting = (dsession) => {
    return {
        type: Action.ADD_DSESSION_WAITING,
        dsession
    }
};

export const addNewMessageChange = (message) => {
    return {
        type: Action.CHANGE_NEW_MESSAGE,
        message
    }
}

export const postDeclineDSession = (dSession, accessToken) => {

    return callApi('doctor/dsession/decline', accessToken, (data, dispatch) => {
        dSession.status = DSessionStatus.STATUS_DECLINE;

        dispatch(declineDSession(dSession));

        NotificationManager.success("Từ chối phiên khám thành công", "Thông báo");
    }, (error) => {
        console.log("Error decline dsession", error);
    }, {id: dSession.id})
};

export const getPatientInfo = (dSessionId, accessToken, success, error) => {
    return callApi(`doctor/dsession/patient-info/${dSessionId}`, accessToken, success, error);
};

export const initialDSession = (dsession) => {
    return {
        type: Action.INITIAL_DSESSIONS,
        dsession
    }
};

export const declineDSession = (dsession) => {
    return {
        type: Action.DECLINE_DSESSION,
        dsession
    }
};

export const acceptDSession = (dsession) => {
    return {
        type: Action.ACCEPT_DSESSION,
        dsession
    }
};
