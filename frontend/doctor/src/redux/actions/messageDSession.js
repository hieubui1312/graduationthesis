import * as Action from "../constants/action";
import {callApi} from "./index";
import {NotificationManager} from "react-notifications";
import {error} from "winston";

export const fetchMessages = (dsessionID, accessToken) => {
    return callApi(`doctor/dsession/message/${dsessionID}`, accessToken, (data, dispatch) => {
        dispatch(initialMessage(data.messages));
    }, error => {
       console.log("Error fetch data", error);
    });
};

export const postAddMessage = (dsession_id, message , access_token) => {
    return callApi(`doctor/dsession/chat`, access_token, (data, dispatch) => {
        console.log("Message add status", data);
    }, error => {
        NotificationManager.error("Gửi message không thành công", "Thông báo!");
    }, {
        dsession_id, message
    })
};

export const logVideoCall = (access_token, body) => {
    return callApi('doctor/dsession/video-call-log', access_token, data => {

    }, error => {

    }, body);
};

export const postAddImage = (body, access_token) => {
    return callApi('doctor/dsession/image', access_token, (data, dispatch) => {
        console.log("Message add image", data);
    }, error => {
        NotificationManager.error("Gửi ảnh không thành công", "Thông báo!");
    }, body);
};

export const orderAppointment = (body, access_token) => {
    return callApi("doctor/dsession/appointment-schedule", access_token, (data) => {
    } , error => {
        NotificationManager.error("Có lỗi xảy ra", "Thông báo !");
    }, body)
};


export const sendPrescriptionSet = (body, access_token) => {
    return callApi("doctor/dsession/prescription", access_token, (data, dispatch) => {
        console.log("Data prescription sent", data);
    }, error => {
        NotificationManager.error(error.message, "Lỗi", 5000);
    }, {...body})
};

export const initialMessage = (messages) => {
    return {
        type: Action.INITIAL_MESSAGES_DSESSION,
        messages
    }
};

export const addMessage = (message) => {
    return {
        type: Action.ADD_MESSAGE,
        message
    }
};
