import {NotificationManager} from "react-notifications";
import * as Config from "../../config/server_path";
import {updateCurrentAccessToken, logout} from "./currentUser";

export function callApi(apiPath, accessToken, successFnc, errorFnc, postBody) {
    return dispatch => {
        let method = 'GET';
        if (postBody)
            method = 'POST';

        let fetchOptions = {
            headers: {
                'Content-Type': 'application/json',
                'Access-Token': accessToken
            },
            method
        };

        if (postBody) {
            fetchOptions.body = JSON.stringify(postBody)
        }

        fetch(
            Config.API_SERVER + apiPath,
            fetchOptions
        )
            .then(res => {
                if (res.status == 401) {
                    console.log('dispatching logout');
                    dispatch(logout());
                    let e = new Error('Có lỗi xác thực');
                    e.statusCode = 401;
                    throw e
                }
                return res.json()
            })
            .then(res => {
                if (res.errorCode) {
                    let e = new Error(res.message);
                    e.statusCode = res.errorCode;
                    throw e
                }

                // support for sliding sessions
                if (res.access_token) {
                    dispatch(updateCurrentAccessToken(res.access_token))
                }

                if (successFnc)
                    successFnc(res, dispatch)
            })
            .catch(err => {
                if (err.statusCode !== 401) {
                    if (errorFnc)
                        errorFnc(err);
                    else {
                        NotificationManager.error(err.message, 'Có lỗi xảy ra', 5000);
                        console.error(err)
                    }
                }
            })
    }
}

