import * as Action from "../constants/action";

export const turnOnPassCode = () => {
    sessionStorage.setItem("passCodeFlag", "true");
    return {
        type: Action.TURN_ON_PASS_CODE_FLAG
    }
};
