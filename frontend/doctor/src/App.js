import React, {useEffect} from 'react';
import './App.css';
import Home from "./components/home";
import PersonalInformation from "./components/personal-information";
import WalletInfo from "./components/wallet-info";
import Login from "./containers/loginContainer";
import {NotificationContainer} from "react-notifications";
import {BrowserRouter as Router, Switch, withRouter, Route, Link, Redirect} from "react-router-dom";
import PrivateRoute from "./components/private-route";
import * as Server from "./config/server_path";
import SocketEvent from "./config/socket_event";
import {useSelector} from "react-redux";
import openSocket from "socket.io-client";


function App() {

    const currentUser = useSelector(state => state.currentUser);

    useEffect(() => {
        if (!window.socket && currentUser.access_token) {
            console.log("Da vao socket", currentUser.access_token);
            window.socket = openSocket(Server.SOCKET_SERVER);

            window.socket.emit(SocketEvent.LOGIN, currentUser.access_token);

            window.socket.on(SocketEvent.LOGIN_SUCCESS, data => {
                console.log("Data login success", data);
            });
            window.socket.on(SocketEvent.LOGIN_FAIL, data => {
                console.log("Data login fail", data);
            });

            window.socket.on(SocketEvent.INTERNAL_ERROR, data => {
               console.log("Error socket", data);
            })

            // window.socket.on("dsession_create", data => {
            //     console.log("Co bn tao phien kham");
            //     console.log("Data", data);
            // })
        }
    }, [currentUser]);

  return (
    <div>
        <Router>
            <Switch>
                <Route exact path="/" render={() => {return <Redirect to={"/home"}/>}} />
                <Route path={"/login"} render={() => <Login />} />
                <PrivateRoute comp={Home} path={"/home"} />
                <PrivateRoute comp={PersonalInformation} path={"/personal-information"} />
                <PrivateRoute comp={WalletInfo} path={"/withdraw"} />
            </Switch>
        </Router>
      <NotificationContainer/>
    </div>
  );
}

export default App;
