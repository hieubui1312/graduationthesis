const ErrorCode = require("../app_modules/error_code");

class NotFoundError extends Error{
    constructor(message){
        super(message);
        this.name = 'Not found';
        this.errorCode = ErrorCode.NOT_FOUND.code;
        this.httpError = 404;
    }
}


module.exports = NotFoundError;
