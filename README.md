# Ứng dụng khám bệnh trực tuyến

## Cài đặt server API 
* Điều kiện trước
    * Đã cài đặt hệ quản trị cơ sở dữ liệu postgresql
    * Cài đặt username, password, database trong file config/config.
* Cài đặt thư viện 
> npm install
* Run migration generate bảng:
> npx sequelize-cli db:migrate
* Run seeder:
> npx sequelize-cli db:seed:all
* Chạy server
> npm run start

## Cài đặt server fontend patient
* Điều kiện trước
    * Setting đường dẫn server api trong file constants/server_base.js
* Chạy trên môi trường development
> npm run start
* Chạy trên môi trường production
> npm run build
> node index-patient.js

## Cài đặt server frontent doctor
* Điều kiện trước
    * Setting đường dẫn server api trong file config/server_path.js
* Chạy trên môi trường development
> npm run start
* Chạy trên môi trường production
> npm run build
> node index-doctor.js

## Cài đặt server frontend admin
* Điều kiện trước
    * Setting đường dẫn server api trong file config/server.js
* Chạy trên môi trường development
> npm run start
* Chạy trên môi trường production
> npm run build
> node index-admin.js

## Cài đặt server socket
* Chạy server
> node index-socket.js
