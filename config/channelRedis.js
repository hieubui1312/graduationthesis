module.exports = {
  CHAT_CHANNEL: "chat_channel",
  SEEN_CHANNEL : "seen_channel",
  NOTIFY_CHANNEL : "notify_channel",
  DSESSION_ACTION : "dsession_action",
  CREATE_DSESSION: "create_dsession"
};
