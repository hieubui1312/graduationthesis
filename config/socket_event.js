module.exports = {
    LOGIN: "login",
    LOGIN_SUCCESS: "login_success",
    LOGIN_FAIL: "login_fail",

    JOIN_ROOM: "join_room",

    INTERNAL_ERROR: "internal_error",
    AUTH_FAIL: "auth_fail",

    START_TYPING: "start_typing",
    STOP_TYPING: "stop_typing",

    DSESSION_COMPLETE: "dsession_complete",

    RE_EXAMINATION: "re_examination"
};
