// module.export const SERVER_API = "http://localhost:3000";
// export const SERVER_PATIENT = "http://localhost:3000";
// export const SERVER_DOCTOR = "http://localhost:3000";
module.exports = {
    // SERVER_API: "http://localhost:8000",
    // SERVER_PATIENT: "http://localhost:3000",
    // SERVER_DOCTOR: "http://localhost:3000"
    SERVER_API: "https://api.devnode.online",
    SERVER_PATIENT: "https://patient.devnode.online",
    SERVER_DOCTOR: "https://doctor.devnode.online"
};
