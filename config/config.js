
module.exports = {
    development: {
        username: 'hieusmacpro',
        password: '',
        database: "do_an_tn",
        host: '127.0.0.1',
        dialect: 'postgres',
        timezone: '+07:00',
        dialectOptions: {
            useUTC: false
        }
    },
    test: {
        username: 'database_test',
        password: null,
        database: 'database_test',
        host: '127.0.0.1',
        dialect: 'mysql'
    },
    production: {
        username: process.env.DB_USERNAME,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME,
        host: process.env.DB_HOSTNAME,
        dialect: 'mysql',
    }
};
