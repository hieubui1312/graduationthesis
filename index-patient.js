const express = require('express');
const path = require('path');
const app = express();
app.use(express.static(path.join(__dirname, 'react-build/patient')));
app.get('*', function(req, res) {
    res.sendFile(path.join(__dirname, 'react-build/patient', 'index.html'));
});
app.listen(9004);
