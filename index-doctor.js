const express = require('express');
const path = require('path');
const app = express();
console.log("Dir name", path.join(__dirname, 'react-build/doctor'));
app.use(express.static(path.join(__dirname, 'react-build/doctor')));
app.get('*', function(req, res) {
    res.sendFile(path.join(__dirname, 'react-build/doctor', "index.html"));
});
app.listen(9001);
